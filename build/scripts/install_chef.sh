set -e

wget https://packages.chef.io/files/stable/chef/14.7.17/ubuntu/18.04/chef_14.7.17-1_amd64.deb
dpkg -i chef_14.7.17-1_amd64.deb
rm chef_14.7.17-1_amd64.deb
