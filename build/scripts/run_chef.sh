set -e

cd $(dirname $0)/../../chef

if [ "$1" ]; then
    chef-client --local-mode -r "recipe[basics],recipe[python3],recipe[ssl_certs],recipe[nginx],recipe[postgres],recipe[nodejs],recipe[foods_config],recipe[docker]" -j $1
elif [ "$ENVIRONMENT" == "production" ]; then
    chef-client --local-mode -r "recipe[basics],recipe[python3],recipe[ssl_certs],recipe[nginx],recipe[postgres],recipe[nodejs],recipe[foods_config],recipe[docker]" -j "production_attributes.json"
else
    chef-client --local-mode -r "recipe[basics],recipe[python3],recipe[ssl_certs],recipe[nginx],recipe[postgres],recipe[nodejs],recipe[foods_config],recipe[docker],recipe[terraform]" -j "vagrant_attributes.json"
fi
