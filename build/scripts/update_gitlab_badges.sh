#!/bin/bash

# Build status: 74242
# pytest coverage: 74504
# jest coverage: 74515

set -e

gitlab_access_token=$1
if [[ -z $gitlab_access_token ]]; then
	gitlab_access_token=$(cat ~/gitlab_access_token)
fi
if [[ -z $gitlab_access_token ]]; then
	>&2 echo "No access token!"
	exit 1
fi

gitlab_badges_url="https://gitlab.com/api/v4/projects/15622340/badges"


percent_covered=$(jq .totals.percent_covered food/coverage.json | xargs printf "%.1f")

if [[ percent_covered > 95 ]]; then
	color=brightgreen
elif [[ percent_covered > 90 ]]; then
	color=yellow
else
	color=red
fi

image_url="https://img.shields.io/badge/pytest%20coverage-$percent_covered%2525-$color"

curl --request PUT -H "PRIVATE-TOKEN: $gitlab_access_token" --data "link_url=https://playfulpachyderm.com&image_url=$image_url" $gitlab_badges_url/74504



# Total statements covered from "All files"
percent_covered=$(python -c 'import bs4
with open("react/coverage/lcov-report/index.html") as file:
	print(float(bs4.BeautifulSoup(file).select(".pad1")[0].select(".strong")[0].text[:4]))')

if [[ percent_covered > 95 ]]; then
	color=brightgreen
elif [[ percent_covered > 90 ]]; then
	color=yellow
else
	color=red
fi

image_url="https://img.shields.io/badge/jest%20coverage-$percent_covered%2525-$color"

curl --request PUT -H "PRIVATE-TOKEN: $gitlab_access_token" --data "link_url=https://playfulpachyderm.com&image_url=$image_url" $gitlab_badges_url/74515
