#!/usr/bin/env sh

DOCKER_DIR=$(readlink -f $0 | xargs dirname | xargs dirname)/docker

echo $DOCKER_DIR

find $DOCKER_DIR/*/build.sh | bash
