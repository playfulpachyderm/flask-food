import React from "react";
import PropTypes from "prop-types";
import { createUseStyles } from "react-jss";

import ArrowButton from "components/generic/ArrowButton";

const useStyles = createUseStyles({
  h1: {
    "display": "flex",
    "flex-direction": "row",
    "justify-content": "center",
  },
  titleInput: {
    "&::placeholder": {
      color: "#8bf",
    },
  },
});


/**
 * Display and edit the name of a recipe in a FoodTable
 */
export default function FoodTableHeader({ name, on_title_change, nav }) {
  const classes = useStyles();

  return (
    <h1 className={classes.h1}>
      <ArrowButton href={nav.prev} reversed={true} />
      <input
        className={classes.titleInput}
        value={name}
        onChange={on_title_change}
        placeholder="Enter title"
      />
      <ArrowButton href={nav.next} />
    </h1>
  );
}
FoodTableHeader.propTypes = {
  name: PropTypes.string.isRequired,
  on_title_change: PropTypes.func.isRequired,
  nav: PropTypes.shape({
    next: PropTypes.string,
    prev: PropTypes.string,
  }).isRequired,
};
