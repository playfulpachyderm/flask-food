import PropTypes from "prop-types";

/**
 * Format a canonical string for an ingredient's amount
 *
 * `Float#toPrecision` rounds it to N significant figures (regardless of
 * decimal).
 * `Number(x)` drops trailing zeroes after the decimal.
 */
export function render_amount(ingredient) {
  const { units, quantity, base_food } = ingredient;

  if (units === 1)
    return `${ Number((quantity * (base_food.mass || 100)).toPrecision(4)) }g`;
  else if (units === 0)
    return Number(quantity.toPrecision(4)).toString();

  return "";
}


/**
 * Prop types for an ingredient object.
 *
 * Values of units:
 *    1: grams
 *    0: count
 *    -1: no quantity (single item)
 */
export const ingredient_proptypes = PropTypes.shape({
  units: PropTypes.number.isRequired,
  base_food: PropTypes.object.isRequired,
  quantity: PropTypes.number.isRequired,
  inputRef: PropTypes.shape({ current: PropTypes.object }),
});
