import React from "react";
import { createUseStyles } from "react-jss";
import PropTypes from "prop-types";

import { ingredient_proptypes } from "../utils";

import PantryRow from "./PantryRow";

const useStyles = createUseStyles({
  container: {
    margin: "6% 3% 6% 0",
    width: "18em",
  },
  header: {
    fontFamily: "Arvo",
    fontSize: "18px",
    textAlign: "center",
    padding: "0.6em",
  },
  contents: {
    padding: "8%",
    minHeight: "10em",
    width: "100%",

    border: "1px solid blue",
    borderRadius: "1em",
    boxSizing: "border-box",
  },
});

/**
 * Sidebar showing pantry contents
 */
export default function PantrySidebar({ ingredients, on_click }) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.header}>Pantry</div>
      <div className={classes.contents}>
        { ingredients.map(
          v => <PantryRow ingredient={v} key={v.base_food.id} on_click={on_click} />
        ) }
      </div>
    </div>
  );
}
PantrySidebar.propTypes = {
  ingredients: PropTypes.arrayOf(ingredient_proptypes).isRequired,
  on_click: PropTypes.func.isRequired,
};
