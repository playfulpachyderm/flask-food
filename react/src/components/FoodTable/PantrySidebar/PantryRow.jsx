import React from "react";
import { createUseStyles } from "react-jss";
import PropTypes from "prop-types";

import { ingredient_proptypes, render_amount } from "../utils";

const plus_button_color = "#2d2";
const useStyles = createUseStyles({
  pantry_row: {
    fontFamily: "Raleway",
    fontSize: "15px",
    marginBottom: "0.5em",
  },
  plus_button: {
    display: "inline-block",
    paddingBottom: "1.2em",
    width: "1.2em",
    height: "0",

    borderRadius: "50%",
    border: `1px solid ${plus_button_color}`,

    textAlign: "center",
    color: plus_button_color,
    marginRight: "0.5em",
  },
  link: {
    color: "inherit",
  },
});

/**
 * A row in the pantry bar
 */
export default function PantryRow({ ingredient, on_click }) {
  const classes = useStyles();

  const label = [render_amount(ingredient), ingredient.base_food.name].filter(x => x).join(" ");

  return (
    <div className={classes.pantry_row}>
      <span onClick={() => on_click(ingredient)} className={classes.plus_button}>+</span>
      <span>
        <a className={classes.link} href={`/recipe/${ingredient.base_food.id}`}>{label}</a>
      </span>
    </div>
  );
}
PantryRow.propTypes = {
  ingredient: ingredient_proptypes,
  on_click: PropTypes.func.isRequired,
};
