import React from "react";
import PropTypes from "prop-types";

import { render_amount, ingredient_proptypes } from "./utils";

import FakeForm from "components/FakeForm";

import FoodTableHeader from "./FoodTableHeader";
import PantrySidebar from "./PantrySidebar";
import { Ingredient, NewIngredient } from "./Ingredient";
import PercentageCell from "./PercentageCell";


/**
 * Validate that an entered amount matches an acceptable format
 */
function is_amount_text_valid(value) {
  const regex = /^-?\d*\.?\d*g?$/;
  return regex.test(value) || value.length === 0;
}

/**
 * Parse an entered ingredient amount
 */
export function parse_amount(amount, base_mass) {
  base_mass = base_mass || 100;

  const ret = {};
  if (amount.length === 0) {
    // Handle empty string
    ret.units = -1;
    ret.quantity = 1;
    return ret;
  }

  if (amount[amount.length - 1] === "g") {
    ret.units = 1;
    ret.quantity = parseFloat(amount.substring(0, amount.length - 1)) / base_mass;
  } else {
    ret.units = 0;
    ret.quantity = parseFloat(amount);
  }
  return ret;
}

const additive_columns = [
  "cals",
  "carbs",
  "protein",
  "fat",
  "sugar",
  "alcohol",
  "water",
  "potassium",
  "sodium",
  "calcium",
  "magnesium",
  "phosphorus",
  "iron",
  "zinc",
  "mass",
  "price",
];


/* eslint-disable react/no-unused-prop-types */

/**
 * Display a recipe.
 */
export default class FoodTable extends React.Component {
  /**
   * Props set the initial state, which can change as the user edits the table.
   */
  constructor(props) {
    super(props);
    this.state = JSON.parse(JSON.stringify(props));
    // {...props};

    for (let i = 0; i < this.state.ingredients.length; i++) {
      this.state.ingredients[i].inputRef = React.createRef();
      this.state.ingredients[i].amount_string = render_amount(this.state.ingredients[i]);
    }

    if (!this.state.ingredients.length) {
      this.state.ingredients.push({
        base_food: Object(),
        units: -1,
        quantity: 1,
        temporary: true,
        amount_string: "",
        inputRef: React.createRef(),
      });
    }
    this.state.changes = {};
    this.state.pantry = [];
    this.state.available_ingredients = [];  // Needs a temp value

    this.on_amount_change = this.on_amount_change.bind(this);
    this.on_ingredient_delete = this.on_ingredient_delete.bind(this);
    this.move_ingredient = this.move_ingredient.bind(this);
    this.new_row = this.new_row.bind(this);
    this.on_soft_select = this.on_soft_select.bind(this);
    this.recalc_total = this.recalc_total.bind(this);
    this.harden_new_ingredient = this.harden_new_ingredient.bind(this);
  }

  /**
   * Load available ingredients and pantry
   */
  componentDidMount() {
    fetch("/recipe/get_available_ingredients")
      .then(resp => resp.json())
      .then(data => this.setState({available_ingredients: data}) )
      // eslint-disable-next-line no-console
      .catch(err => console.log(err));

    fetch("/pantry/as_json")
      .then(resp => resp.json())
      .then(data => {
        this.setState({pantry: data});
      });
  }

  /**
   * Callback function to update the total when an amount is changed
   */
  on_amount_change(new_amount, row_num) {
    if (!is_amount_text_valid(new_amount))
      return;

    const {state} = this;
    const ingredient = state.ingredients[row_num];

    const { quantity, units } = parse_amount(new_amount, ingredient.base_food.mass);

    ingredient.quantity = quantity;
    ingredient.units = units;
    ingredient.amount_string = new_amount;

    if (ingredient.base_food.id && !ingredient.termporary) {
      if (typeof state.changes[ingredient.base_food.id] === "undefined")
        state.changes[ingredient.base_food.id] = {};

      state.changes[ingredient.base_food.id].quantity = quantity;
      state.changes[ingredient.base_food.id].units = units;
    }
    this.setState(state);
    this.recalc_total();
  }

  /**
   * Callback function to delete an ingredient from the recipe
   */
  on_ingredient_delete(row_num) {
    const {state} = this;
    // Remove the ingredient and return it
    const ingredient = state.ingredients.splice(row_num, 1)[0];

    if (this.props.ingredients.some(i => i.base_food.id === ingredient.base_food.id)) {
      // Don't update `changes` if the deleted ingredient wasn't in the original recipe
      state.changes[ingredient.base_food.id] = {delete: true};

      // Update `changes` (reorder): shift subsequent ingredients up
      for (let i = row_num; i < state.ingredients.length; i++) {
        const {id} = state.ingredients[i].base_food;
        if (typeof state.changes[id] === "undefined")
          state.changes[id] = {};
        state.changes[id].reorder = i;
      }
    }

    if (!state.ingredients.length) {
      state.ingredients.push({
        base_food: Object(),
        units: -1,
        quantity: 1,
        temporary: true,
        amount_string: "",
        inputRef: React.createRef(),
      });
    }

    this.setState(state);
    this.recalc_total();
  }

  /**
   * Callback function to re-order ingredients in the recipe
   */
  move_ingredient(row_num, arrowKey) {
    const {state} = this;
    const {ingredients} = state;

    const new_row_num = row_num + (arrowKey === "ArrowUp" ? -1 : 1);
    if (new_row_num < 0 || new_row_num >= ingredients.length)
      return;

    const ingredient_moved = ingredients.splice(row_num, 1)[0];
    ingredients.splice(new_row_num, 0, ingredient_moved);

    if (![ingredient_moved.base_food.id, ingredients[row_num].base_food.id].includes(undefined)) {
      // No changes should be made unless both ingredients are real
      for (const id of [ingredient_moved.base_food.id, ingredients[row_num].base_food.id])
        if (typeof state.changes[id] === "undefined")
          state.changes[id] = {};

      state.changes[ingredient_moved.base_food.id].reorder = new_row_num;
      state.changes[ingredients[row_num].base_food.id].reorder = row_num;
    }

    state.ingredients = ingredients;
    this.setState(state);

    const to_focus = this.state.ingredients[row_num].inputRef.current;
    to_focus.focus();
  }

  /**
   * Callback function to add a new row
   */
  new_row(after_index) {
    const {state} = this;
    state.ingredients.splice(after_index + 1, 0, {
      base_food: Object(),
      units: -1,
      quantity: 1,
      temporary: true,
      amount_string: "",
      inputRef: React.createRef(),
    });
    this.setState(state);
  }

  /**
   * Returns `true` if recipe already has an ingredient with this base food in it, false otherwise
   */
  check_for_existing_ingredient(base_food) {
    return this.state.ingredients
      .map(i => i.base_food.id)
      .includes(base_food.id);
  }

  /**
   * Callback function to change the name in a new ingredient
   */
  on_soft_select(row_num, base_food) {
    if (this.check_for_existing_ingredient(base_food))
      return;

    const {state} = this;
    state.ingredients[row_num].base_food = base_food;
    this.setState(state);
    this.recalc_total();
  }

  /**
   * Callback function to harden a new ingredient into the recipe
   */
  harden_new_ingredient(row_num, base_food) {
    const {state} = this;
    const new_ingredient = state.ingredients[row_num];
    new_ingredient.base_food = base_food;  // Possibly unnecessary, but better safe than sorry

    state.ingredients[row_num].temporary = false;

    state.changes[new_ingredient.base_food.id] = {
      created: true,
      quantity: new_ingredient.quantity,
      units: new_ingredient.units,
      reorder: row_num,
    };

    // Update `changes` (reorder): shift subsequent ingredients down
    for (let i = row_num + 1; i < state.ingredients.length; i++) {
      const {id} = state.ingredients[i].base_food;
      if (typeof state.changes[id] === "undefined")
        state.changes[id] = {};
      state.changes[id].reorder = i;
    }

    this.setState(state);
  }

  /**
   * Recalculate the total based on current ingredients
   */
  recalc_total() {
    const total = {...this.state.total};
    for (const col of additive_columns) {
      total[col] = 0;
      for (const ingredient of this.state.ingredients) {
        if (ingredient.base_food.id)
          total[col] += ingredient.base_food[col] * ingredient.quantity;
      }
    }
    this.setState(prevState => ( {...prevState, total} ));
  }

  /**
   * Render
   */
  render() {
    // Create the base food for the Total row
    const total = { ...this.state.total, name: "Total" };

    const save_button_enabled = (
      // Enable the button if there are changes, or if it's an iteration page
      Object.keys(this.state.changes).length > 0
      || typeof this.state.change_title !== "undefined"
      || window.location.pathname.includes("iterate")
    );

    const fake_form_data = {ingredient_changes: this.state.changes};
    if (typeof this.state.change_title !== "undefined")
      fake_form_data.change_title = this.state.change_title;

    // Using arrow function because it automatically binds "this"
    const on_title_change = e => {
      const {state} = this;
      state.total.name = e.target.value;
      state.change_title = e.target.value;
      this.setState(state);
    };

    const on_pantry_item_clicked = ingredient => {
      const {state} = this;
      const new_ingredient = {
        base_food: ingredient.base_food,
        units: ingredient.units,
        quantity: 1,
        temporary: false,
        inputRef: React.createRef(),
      };
      new_ingredient.amount_string = render_amount(new_ingredient);
      state.ingredients.push(new_ingredient);

      state.changes[new_ingredient.base_food.id] = {
        created: true,
        quantity: new_ingredient.quantity,
        units: new_ingredient.units,
      };
      this.setState(state);
      this.recalc_total();
    };

    return (
      <>
        <FoodTableHeader
          name={this.state.total.name}
          on_title_change={on_title_change}
          nav={this.props.nav}
        />
        <div className="pantry-foodtable-flex-container" style={{display: "flex"}}>
          <PantrySidebar ingredients={this.state.pantry} on_click={on_pantry_item_clicked} />
          <table>
            <thead>
              <tr>
                <th />
                <th>Amount</th>
                <th className="name-column">Name</th>
                <th>Cals</th>
                <th>Carbs</th>
                <th>Protein</th>
                <th>Fat</th>
                <th>Sugar</th>
                <th>Price</th>
                <th>K</th>
                <th>Na</th>
                <th>Ca</th>
                <th>Mg</th>
                <th>P</th>
                <th>Fe</th>
                <th>Zn</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.ingredients.map((v, i) => (
                  v.temporary ?
                    <NewIngredient
                      ingredient={v}
                      key={i}
                      row_num={i}
                      on_amount_change={this.on_amount_change}
                      on_delete={this.on_ingredient_delete}
                      move_ingredient={this.move_ingredient}
                      on_soft_select={this.on_soft_select}
                      on_harden={this.harden_new_ingredient}
                      base_foods={this.state.available_ingredients}
                    /> :
                    <Ingredient
                      ingredient={v}
                      key={i}
                      row_num={i}
                      on_amount_change={this.on_amount_change}
                      on_ingredient_delete={this.on_ingredient_delete}
                      move_ingredient={this.move_ingredient}
                      on_new_row={this.new_row}
                      has_dollar_sign={i === 0}
                    />
                ))
              }

              <Ingredient
                ingredient={ {units: -1, base_food: total, quantity: 1} }
                key={this.state.ingredients.length}
                row_num={0}
                has_link={false}
                classes="total-row"
                has_dollar_sign={true}
              />
              <tr className="percentages-row">
                <td />
                <td>
                  {
                    this.state.total.id &&
                      <a href={`${window.location.pathname}/iterate`}>Iterate</a>
                  }
                </td>
                <td>
                  {
                    !window.location.pathname.includes("splat") &&
                      <FakeForm
                        target={window.location.pathname}
                        data={fake_form_data}
                        is_enabled={save_button_enabled}
                      />
                  }
                </td>
                <td>
                  {
                    this.props.total.id &&
                      <a href={`${window.location.pathname}/splat`}>Splat</a>
                  }
                </td>
                <PercentageCell amount={total.carbs * 4 / total.cals} />
                <PercentageCell amount={total.protein * 4 / total.cals} />
                <PercentageCell amount={total.fat * 9 / total.cals} />
                <PercentageCell amount={total.sugar / total.carbs} />
                <td />
                <PercentageCell amount={total.potassium * 1000 / 3500} />
                <PercentageCell amount={total.sodium * 1000 / 3000} />
                <PercentageCell amount={total.calcium * 1000 / 1500} />
                <PercentageCell amount={total.magnesium * 1000 / 420} />
                <PercentageCell amount={total.phosphorus * 1000 / 1250} />
                <PercentageCell amount={total.iron * 1000 / 8} />
                <PercentageCell amount={total.zinc * 1000 / 12} />
              </tr>
            </tbody>
          </table>
        </div>
      </>
    );
  }
}
FoodTable.propTypes = {
  ingredients: PropTypes.arrayOf(ingredient_proptypes).isRequired,
  total: PropTypes.object.isRequired,
  nav: PropTypes.shape({
    next: PropTypes.string,
    prev: PropTypes.string,
  }),
};
FoodTable.defaultProps = {
  nav: {
    next: null,
    prev: null,
  },
};
