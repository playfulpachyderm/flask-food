import React from "react";
import PropTypes from "prop-types";


/**
 * Turn a decimal number into a formatted string percentage
 */
export default function PercentageCell(props) {
  const percent_amount = (props.amount * 100).toFixed(0);
  return <td>{`(${percent_amount}%)`}</td>;
}

PercentageCell.propTypes = {
  amount: PropTypes.number.isRequired,
};
