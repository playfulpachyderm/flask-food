import React from "react";
import PropTypes from "prop-types";


/**
 * Format the price
 */
export default function PriceCell(props) {
  const has_data = typeof props.price !== "undefined" && !isNaN(props.price);
  return (
    <td className="price-cell">
      {has_data ? `${props.has_dollar_sign ? "$" : ""}${props.price.toFixed(2)}` : "--"}
    </td>
  );
}
PriceCell.propTypes = {
  price: PropTypes.number,
  has_dollar_sign: PropTypes.bool.isRequired,
};
