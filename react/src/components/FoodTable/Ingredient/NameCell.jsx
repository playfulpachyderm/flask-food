import React from "react";
import PropTypes from "prop-types";


/**
 * Format the name into a link, if appropriate
 */
export default function NameCell(props) {
  let contents = props.base_food.name;
  if (props.has_link) {
    const base_path = props.base_food.fundamental_level === 0 ? "food" : "recipe";
    contents = (
      <a href={`/${base_path}/${props.base_food.id}`}>
        {contents}
      </a>
    );
  }
  return (
    <td className="name-cell">
      {contents}
    </td>
  );
}
NameCell.propTypes = {
  has_link: PropTypes.bool,
  base_food: PropTypes.object.isRequired,
};
NameCell.defaultProps = {
  has_link: true,
};
