import React, { useCallback } from "react";
import PropTypes from "prop-types";

import { ingredient_proptypes } from "../utils";

import DeleteCell from "./DeleteCell";
import AmountCell from "./AmountCell";
import PriceCell from "./PriceCell";

import ComboBox from "components/generic/ComboBox";


/**
 * Round a field to the nearest whole number, or stub it as "--" if it doesn't exist
 */
function round_property(property, quantity, digits = 0) {
  if (typeof property === "undefined")
    return "--";
  if (digits !== 0)
    return (property * quantity).toFixed(digits);
  return Math.round(property * quantity, digits);
}


/**
 * A new (i.e., added) ingredient row in a FoodTable
 */
export default function NewIngredient({ ingredient, row_num, ...props }) {
  const { base_food, quantity } = ingredient;

  const key_down_callback = useCallback(e => {
    if (e.shiftKey && e.ctrlKey && ["ArrowUp", "ArrowDown"].includes(e.key)) {
      props.move_ingredient(row_num, e.key);
    }
  }, [props.move_ingredient, row_num]);

  return (
    <tr>
      <DeleteCell has_button={true} callback={() => props.on_delete(row_num)} />
      <AmountCell
        ingredient={ingredient}
        change_callback={ e => props.on_amount_change(e.target.value, row_num) }
        key_down_callback={key_down_callback}
      />
      <td className="name-cell">
        <ComboBox
          key_for={i => i.id}
          options={props.base_foods}
          label_for={i => i.name}
          render={i => (
            <span>
              {i.name}
              <span>{`(${i.id})`}</span>
            </span>
          )}
          on_soft_select={ item => props.on_soft_select(row_num, item) }
          on_harden={ item => props.on_harden(row_num, item) }
        />
      </td>

      <td>{round_property(base_food.cals, quantity)}</td>
      <td>{round_property(base_food.carbs, quantity)}</td>
      <td>{round_property(base_food.protein, quantity)}</td>
      <td>{round_property(base_food.fat, quantity)}</td>
      <td>{round_property(base_food.sugar, quantity)}</td>

      <PriceCell
        price={base_food.price * quantity / 100}
        has_dollar_sign={false}
      />
      <td className="mineral-column">{round_property(base_food.potassium, quantity * 1000)}</td>
      <td className="mineral-column">{round_property(base_food.sodium, quantity * 1000)}</td>
      <td className="mineral-column">{round_property(base_food.calcium, quantity * 1000)}</td>
      <td className="mineral-column">{round_property(base_food.magnesium, quantity * 1000)}</td>
      <td className="mineral-column">{round_property(base_food.phosphorus, quantity * 1000)}</td>
      <td className="mineral-column">{round_property(base_food.iron, quantity * 1000, 1)}</td>
      <td className="mineral-column">{round_property(base_food.zinc, quantity * 1000, 1)}</td>
    </tr>
  );
}
NewIngredient.propTypes = {
  ingredient: ingredient_proptypes,
  base_foods: PropTypes.array.isRequired,
  on_soft_select: PropTypes.func.isRequired,
  on_harden: PropTypes.func.isRequired,
  row_num: PropTypes.number.isRequired,
  on_amount_change: PropTypes.func.isRequired,
  on_delete: PropTypes.func,
  move_ingredient: PropTypes.func,
};
