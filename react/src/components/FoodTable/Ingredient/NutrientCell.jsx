import React from "react";
import PropTypes from "prop-types";

/**
 * A cell containing a nutrient amount
 */
export default function NutrientCell(props) {
  return <td>{Math.round(props.amount)}</td>;
}
NutrientCell.propTypes = {
  amount: PropTypes.number.isRequired,
};
