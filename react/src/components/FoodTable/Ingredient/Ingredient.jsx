import React, { useCallback } from "react";
import { createUseStyles } from "react-jss";
import PropTypes from "prop-types";

import { ingredient_proptypes } from "../utils";

import NameCell from "./NameCell";
import DeleteCell from "./DeleteCell";
import AmountCell from "./AmountCell";
import PriceCell from "./PriceCell";


const useStyles = createUseStyles({
  mineral: {
    // borderLeft: "1px solid #eee",
    fontSize: "0.9em",
  },
});

/**
 * An ingredient row in a FoodTable
 */
export default function Ingredient({ ingredient, row_num, has_link, ...props }) {
  const { base_food, quantity } = ingredient;
  const styles = useStyles();

  const check_ctrl_shift_arrow = useCallback(e => {
    if (e.shiftKey && e.ctrlKey && ["ArrowUp", "ArrowDown"].includes(e.key)) {
      props.move_ingredient(row_num, e.key);
    } else if (e.key === "Enter") {
      props.on_new_row(row_num);
    }
  }, [props.move_ingredient, props.on_new_row, row_num]);

  return (
    <tr className={props.classes}>
      <DeleteCell has_button={has_link} callback={() => props.on_ingredient_delete(row_num)} />
      <AmountCell
        ingredient={ingredient}
        change_callback={ e => props.on_amount_change(e.target.value, row_num) }
        key_down_callback={check_ctrl_shift_arrow}
      />
      <NameCell base_food={base_food} has_link={has_link} />

      <td>{Math.round(base_food.cals * quantity)}</td>
      <td>{Math.round(base_food.carbs * quantity)}</td>
      <td>{Math.round(base_food.protein * quantity)}</td>
      <td>{Math.round(base_food.fat * quantity)}</td>
      <td>{Math.round(base_food.sugar * quantity)}</td>

      <PriceCell
        price={base_food.price * quantity / 100}
        has_dollar_sign={props.has_dollar_sign}
      />
      <td className={styles.mineral}>{Math.round(base_food.potassium * quantity * 1000)}</td>
      <td className={styles.mineral}>{Math.round(base_food.sodium * quantity * 1000)}</td>
      <td className={styles.mineral}>{Math.round(base_food.calcium * quantity * 1000)}</td>
      <td className={styles.mineral}>{Math.round(base_food.magnesium * quantity * 1000)}</td>
      <td className={styles.mineral}>{Math.round(base_food.phosphorus * quantity * 1000)}</td>
      <td className={styles.mineral}>
        {base_food.iron ? (base_food.iron * quantity * 1000).toFixed(1) : "0"}
      </td>
      <td className={styles.mineral}>
        {base_food.zinc ? (base_food.zinc * quantity * 1000).toFixed(1) : "0"}
      </td>
    </tr>
  );
}

Ingredient.propTypes = {
  ingredient: ingredient_proptypes,
  has_link: PropTypes.bool,
  has_dollar_sign: PropTypes.bool,
  classes: PropTypes.string,
  row_num: PropTypes.number.isRequired,
  on_amount_change: PropTypes.func,
  on_ingredient_delete: PropTypes.func,
  move_ingredient: PropTypes.func,
  on_new_row: PropTypes.func,
};

Ingredient.defaultProps = {
  has_link: true,
  has_dollar_sign: false,
  classes: "",
};
