import React from "react";
import PropTypes from "prop-types";

import { ingredient_proptypes } from "../utils";

/**
 * Format an amount cell
 */
export default function AmountCell({ ingredient, ...props }) {
  return (
    <td className="amount-cell">
      <input
        name="amount"
        value={ingredient.amount_string}
        ref={ingredient.inputRef}
        onChange={props.change_callback}
        onKeyDown={props.key_down_callback}
      />
    </td>
  );
}
AmountCell.propTypes = {
  ingredient: ingredient_proptypes,
  change_callback: PropTypes.func,
  key_down_callback: PropTypes.func,
};
