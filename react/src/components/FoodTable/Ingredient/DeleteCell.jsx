import React from "react";
import PropTypes from "prop-types";


/**
 * Table cell containing the red X button to delete an ingredient
 */
export default function DeleteCell(props) {
  let contents = null;
  if (props.has_button) {
    contents = (
      <a className="delete-button" onClick={props.callback}>
        <span className="alignment-helper" />
        <img src="/img/x-icon.png" />
      </a>
    );
  }
  return (
    <td className="delete-cell">
      {contents}
    </td>
  );
}

DeleteCell.propTypes = {
  has_button: PropTypes.bool.isRequired,
  callback: PropTypes.func,
};

// DeleteCell.defaultProps = {
//   has_button: true,
// };
