import React from "react";
import PropTypes from "prop-types";
import { createUseStyles } from "react-jss";


const useStyles = createUseStyles({
  a: {
    display: "inherit",
  },
  arrow: {
    "display": "inline-block",
    "height": "1.6em",
    "z-index": -1,
  },
  reversed: {
    transform: "scaleX(-1)",
  },
  grayed_out: {
    filter: "grayscale(100%)",
  },
});


/**
 * Create an arrow button to go next / prev.  Grays out if no link is provided
 */
export default function ArrowButton({ href, reversed }) {
  const classes = useStyles();
  const arrow_img = "/img/arrow-next.png";

  const arrow_classes = [classes.arrow];
  if (!href)
    arrow_classes.push(classes.grayed_out);
  if (reversed)
    arrow_classes.push(classes.reversed);

  const img = <img className={arrow_classes.join(" ")} src={arrow_img} />;

  if (href)
    return <a className={classes.a} href={href}>{img}</a>;
  return img;
}
ArrowButton.propTypes = {
  href: PropTypes.string,
  reversed: PropTypes.bool,
};
ArrowButton.defaultProps = {
  href: null,
  reversed: false,
};
