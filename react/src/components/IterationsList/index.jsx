import React from "react";
import PropTypes from "prop-types";
import { createUseStyles } from "react-jss";

import Iteration from "./Iteration";

const useStyles = createUseStyles({
  dotted_table: {
    "& th": {
      borderBottom: "1px solid black",
    },
    "& th:nth-child(n+2), td:nth-child(n+2)": {
      borderLeft: "1px dashed gray",
    },
  },
  rotated_header: {
    "height": "8em",
    "maxWidth": "6em",
    "& div": {
      transform: "rotate(-60deg)",
    },
  },
});

/**
 * Render a list of iterations of a recipe.
 */
export default function IterationsList({ recipe_list, highlight }) {
  const styles = useStyles();

  const base_foods = [];
  for (const r of recipe_list) {
    for (const i of r.ingredients) {
      if (!base_foods.some(x => x.id === i.base_food.id))
        base_foods.push(i.base_food);
    }
  }

  return (
    <>
      <h1>{`${recipe_list[0].self.name}: Iterations`}</h1>
      <table className={styles.dotted_table}>
        <thead>
          <tr>
            <th />
            { base_foods.map(base_food => (
              <th className={styles.rotated_header} key={base_food.id}>
                <div>{base_food.name}</div>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          { recipe_list.map(r => (
            <Iteration
              key={r.self.id}
              recipe={r}
              base_foods={base_foods}
              is_highlighted={r.self.id === highlight}
            />
          )) }
        </tbody>
      </table>
    </>
  );
}
IterationsList.propTypes = {
  recipe_list: PropTypes.array.isRequired,
  highlight: PropTypes.number.isRequired,
};
