import React from "react";
import PropTypes from "prop-types";
import { createUseStyles } from "react-jss";

import { render_amount } from "components/FoodTable/utils";

const useStyles = createUseStyles({
  highlighted: {
    border: "2px solid #d99",
    backgroundColor: "#fdd",
  },
});

/**
 * A row in an IterationList, representing one iteration of that recipe
 */
export default function Iteration({ recipe, base_foods, is_highlighted}) {
  const styles = useStyles();

  const values = base_foods.map(
    base_food => recipe.ingredients.find(i => i.base_food.id === base_food.id)
  );

  return (
    <tr className={is_highlighted ? styles.highlighted : ""}>
      <td>
        <a href={`/recipe/${recipe.self.id}`}>{recipe.self.name}</a>
      </td>

      {
        values.map((v, i) => (
          v ? <td key={`id-${v.id}`}>{render_amount(v) || 1}</td>
            : <td key={i} />
        ))
      }
    </tr>
  );
}
Iteration.propTypes = {
  recipe: PropTypes.object.isRequired,
  base_foods: PropTypes.array.isRequired,
  is_highlighted: PropTypes.bool,
};
Iteration.defaultProps = {
  is_highlighted: false,
};
