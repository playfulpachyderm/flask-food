/**
 * Implement a fake form.
 * See for more info:
 * https://www.thesitewizard.com/archive/textsubmit.shtml
 */

import React from "react";
import PropTypes from "prop-types";

/**
 * Create a form that can be submitted to use the browser's natural workflow
 * via javascript.
 */
export default function FakeForm(props) {
  return (
    <form name="submit" action={props.target} method={props.method}>
      <input type="hidden" name="data" value={JSON.stringify(props.data)} />
      <input type="submit" value="Save" disabled={!props.is_enabled} />
    </form>
  );
}

FakeForm.propTypes = {
  target: PropTypes.string.isRequired,
  method: PropTypes.string,
  data: PropTypes.object.isRequired,
  is_enabled: PropTypes.bool,
};

FakeForm.defaultProps = {
  method: "POST",
  is_enabled: true,
};
