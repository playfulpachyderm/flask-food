import React from "react";
import ReactDOM from "react-dom";

import IterationsList from "components/IterationsList";


if (typeof(data) !== "undefined") {
  ReactDOM.render(
    // eslint-disable-next-line no-undef
    <IterationsList recipe_list={data} highlight={highlight} />,
    document.getElementById("app")
  );
}
