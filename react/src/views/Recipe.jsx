import React from "react";
import ReactDOM from "react-dom";

import FoodTable from "components/FoodTable";

if (typeof(data) !== "undefined") {
  ReactDOM.render(
    // eslint-disable-next-line no-undef
    <FoodTable ingredients={data.ingredients} total={data.self} nav={data.nav} />,
    document.getElementById("app")
  );
}
