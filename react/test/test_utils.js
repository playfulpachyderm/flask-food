/**
 * Some helpful testing functions to easily test input elements on the fake
 * DOM.
 */

const data = [
  {id: 1, name: 'parmigiano', cals: 393.0, carbs: 0.0, protein: 33.0, fat: 30.0, sugar: 0.0, alcohol: 0.0, water: 0.0, potassium: 0.0, sodium: 0.0, mass: 100.0, density: -1.0, price: 399.0, cook_ratio: null, fundamental_level: 0, date: null},
  {id: 68, name: 'chicken breast', cals: 165.0, carbs: 0.0, protein: 31.0, fat: 4.0, sugar: 0.0, alcohol: 0.0, water: 0.0, potassium: 0.259, sodium: 0.0, mass: 100.0, density: -1.0, price: 220.0, cook_ratio: 0.72, fundamental_level: 0, date: null},
  {id: 35, name: "onion", cals: 88, carbs: 20, protein: 2, fat: 0, sugar: 9, alcohol: 0, water: 0, potassium: 0.312, sodium: 0.008, mass: 220, density: -1, price: 17, cook_ratio: 0.62, fundamental_level: 0, date: null},
];


/**
 * Find an `input` tag on the simulated page
 */
function get_input_by_name(dom_root, name) {
  return dom_root.find(x => x.type === "input" && x.props.name === name);
}

/**
 * Set the text on an input element
 */
function set_text(element, text) {
  element.props.onChange({target: { value: text }});
}

/**
 * Run an `expect` assertion on the text of an input elemtn
 */
function expect_text(element, text) {
  expect(element.props.value).toBe(text);
}

/**
 * Assert equivalence between objects
 */
function expect_json_equivalent(o1, o2) {
  expect(JSON.stringify(o1)).toEqual(JSON.stringify(o2));
}

/**
 * Mock function that pretends to change the focus when a row is deleted.
 * Since the dom is never actually rendered, the real focusing function will
 * break.
 *
 * See more: https://github.com/facebook/react/issues/7740
 */
function mock_func(element) {
  if (element.type === "input") {
    return { focus: () => {} };
  }
  return null;
}


export { get_input_by_name, set_text, expect_text, expect_json_equivalent, data, mock_func };
