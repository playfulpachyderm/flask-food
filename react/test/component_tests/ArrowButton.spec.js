/**
 * Tests of the ArrowButton component.
 */

import React from "react";
import { create } from "react-test-renderer";

import ArrowButton from "components/generic/ArrowButton";


describe("ArrowButton component", () => {
  const href = "http://asdf";
  const component = create(<ArrowButton href={href} />);
  const dom_root = component.root;

  test("Renders properly", () => {
    const a = dom_root.findByType("a");
    const img = a.findByType("img");
    expect(a.props.href).toBe(href);
    expect(img.props.className).toBe("arrow");
  });
});

describe("Reversed (left-pointing) ArrowButton component", () => {
  const href = "http://asdf";
  const component = create(<ArrowButton href={href} reversed={true} />);
  const dom_root = component.root;

  test("Arrow is reversed", () => {
    const a = dom_root.findByType("a");
    const img = a.findByType("img");
    expect(a.props.href).toBe(href);
    expect(img.props.className).toBe("arrow reversed");
  });
});

describe("Grayed-out ArrowButton component", () => {
  const href = null;
  const component = create(<ArrowButton href={href} />);
  const dom_root = component.root;

  test("No link", () => {
    expect(dom_root.findAllByType("a")).toHaveLength(0);
  });

  test("Arrow is grayed out", () => {
    const img = dom_root.findByType("img");
    expect(img.props.className).toBe("arrow grayed_out");
  });
});
