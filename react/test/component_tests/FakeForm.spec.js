/**
 * Tests of the FakeForm component.
 */

import React from "react";
import { create } from "react-test-renderer";

import FakeForm from "components/FakeForm";
import { get_input_by_name, expect_text } from "test/test_utils";

describe("FakeForm component", () => {
  const data = {a: 1, b: 2};
  const component = create(<FakeForm target="blah1" method="Blah2" data={data} />);
  const dom_root = component.root;
  const form = dom_root.findByType("form");

  test("Form is proper", () => {
    expect(form.props.action).toBe("blah1");
    expect(form.props.method).toBe("Blah2");
  });
  test("Form data is correct", () => {
    expect_text(get_input_by_name(dom_root, "data"), JSON.stringify(data));
  });
});
