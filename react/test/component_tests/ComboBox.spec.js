/**
 * Tests of the ComboBox component.
 */

import React from "react";
import { create, act } from "react-test-renderer";

import ComboBox from "components/generic/ComboBox";
import { set_text, expect_text, expect_json_equivalent } from "test/test_utils";

const options = [
  {name: "One"},
  {name: "Two"},
  {name: "Three"},
];

/***************************** Helper functions *****************************/

/**
 * Assert a ComboBox list item is selected or not
 */
function expect_selected(li, truth = true) {
  const exp = expect(li.props.className.toLowerCase());
  (truth ? exp : exp.not).toMatch("selected");
}

/**
 * Assert selection for a group of ComboBox list items
 */
function expect_only_selected(li_list, index) {
  for (let i = 0; i < li_list.length; ++i)
    expect_selected(li_list[i], i === index);
}

/**
 * Assert the dropdown is hidden or not
 */
function expect_hidden(ul, truth = true) {
  const exp = expect(ul.props.className.toLowerCase());
  (truth ? exp : exp.not).toMatch("hidden");
}

/***************************** Tests begin here *****************************/

describe("ComboBox component with default settings", () => {
  let hardened = null;
  let softened = null;
  const component = create(
    <ComboBox options={options} on_harden={i => {hardened = i;}} on_soft_select={i => {softened = i;}} />
  );
  const dom_root = component.root;
  const input = dom_root.findByType("input");

  test("Creates an empty input", () => {
    expect(input.props.value).toBe("");
  });

  test("Creates list items", () => {
    const li = dom_root.findAllByType("li");
    expect(li).toHaveLength(options.length);
    for (let i = 0; i < options.length; ++i) {
      expect(li[i].children).toHaveLength(1);
      expect(li[i].children[0]).toBe(options[i].name);
      // None of them should be selected
      expect_selected(li[i], false);
    }
  });

  test("Arrow keys should move the selection", () => {
    const li = dom_root.findAllByType("li");
    expect(li).toHaveLength(3);

    for (const l of li)
      expect_selected(l, false);

    for (const i of [0, 1, 2, 0, 1, 2, 0]) {
      act(() => input.props.onKeyDown({key: "ArrowDown"}) );
      expect_only_selected(li, i);
      expect_json_equivalent(softened, options[i]);
    }

    for (const i of [2, 1, 0, 2]) {
      act(() => input.props.onKeyDown({key: "ArrowUp"}) );
      expect_only_selected(li, i);
      expect_json_equivalent(softened, options[i]);
    }
  });

  test("List items get filtered by entered text", () => {
    act(() => set_text(input, "t") );
    let li = dom_root.findAllByType("li");
    expect(li).toHaveLength(2);
    expect(li[0].children[0]).toBe("Two");
    expect(li[1].children[0]).toBe("Three");

    // Should select the first item
    expect_only_selected(li, 0);
    expect_json_equivalent(softened, options[1]);

    act(() => set_text(input, "tw") );
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);
    expect(li[0].children[0]).toBe("Two");
    expect_selected(li[0]);
    expect_json_equivalent(softened, options[1]);

    // Changing the text should update the soft-selected value
    act(() => set_text(input, "thr") );
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);
    expect(li[0].children[0]).toBe("Three");
    expect_selected(li[0]);
    expect_json_equivalent(softened, options[2]);
  });

  test("Hardens a selection", () => {
    act(() => input.props.onKeyDown({key: "Enter"}) );

    // should have triggered the callback
    expect_json_equivalent(hardened, options[2]);
    expect_text(input, "Three");
  });
});


describe("Dropdown list appears and disppears at the correct times", () => {
  const component = create(<ComboBox options={options} on_harden={() => {}} />);
  const dom_root = component.root;
  const input = dom_root.findByType("input");
  const dropdown = dom_root.findByType("ul");

  test("Dropdown should be hidden", () => {
    expect_hidden(dropdown);
  });

  test("Dropdown should appear when input is focused", () => {
    act(() => input.props.onFocus() );
    expect_hidden(dropdown, false);
  });

  test("Dropdown should disappear on <Escape> press, and appear on arrow key press", () => {
    expect_hidden(dropdown, false);
    act(() => input.props.onKeyDown({key: "Escape"}) );
    expect_hidden(dropdown, true);

    act(() => input.props.onKeyDown({key: "ArrowUp"}) );
    expect_hidden(dropdown, false);

    act(() => input.props.onKeyDown({key: "Escape"}) );
    expect_hidden(dropdown, true);

    act(() => input.props.onKeyDown({key: "ArrowDown"}) );
    expect_hidden(dropdown, false);
  });

  test("Dropdown should disappear when input is blurred", () => {
    act(() => input.props.onBlur() );
    expect_hidden(dropdown, true);
  });

  test("Dropdown should appear when typing", () => {
    act(() => input.props.onChange({target: {value: "t"}}) );
    expect_hidden(dropdown, false);
  });
});


describe("ComboBox with label function", () => {
  let hardened = null;
  const component = create(
    <ComboBox options={options} on_harden={i => {hardened = i;}} label_for={i => (i.name + i.name.length)} />
  );
  const dom_root = component.root;
  const input = dom_root.findByType("input");

  test("List items have the correct text", () => {
    const li = dom_root.findAllByType("li");
    expect(li).toHaveLength(3);
    expect(li[0].children[0]).toBe("One3");
    expect(li[1].children[0]).toBe("Two3");
    expect(li[2].children[0]).toBe("Three5");
  });

  test("Can search by the rendered text", () => {
    act(() => set_text(input, "5"));
    let li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);
    expect(li[0].children[0]).toBe("Three5");

    act(() => set_text(input, "one"));
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);
    expect(li[0].children[0]).toBe("One3");

    act(() => set_text(input, "one3"));
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);
    expect(li[0].children[0]).toBe("One3");
  });

  test("Hardening an item uses the label according to the label function", () => {
    act(() => input.props.onKeyDown({key: "Enter"}) );

    // The actual value returned to `on_harden` should be the ORIGINAL value!
    expect_json_equivalent(hardened, options[0]);
    // Only the LABEL should be different
    expect_text(input, "One3");
  });
});


describe("ComboBox that renders different text", () => {
  const component = create(
    <ComboBox options={options} on_harden={() => {}} render={i => (i.name + i.name.length)} />
  );
  const dom_root = component.root;
  const input = dom_root.findByType("input");

  test("List items have the correct text", () => {
    const li = dom_root.findAllByType("li");
    expect(li).toHaveLength(3);
    expect(li[0].children[0]).toBe("One3");
    expect(li[1].children[0]).toBe("Two3");
    expect(li[2].children[0]).toBe("Three5");
  });

  test("Cannot search by the rendered text", () => {
    act(() => set_text(input, "3"));
    let li = dom_root.findAllByType("li");
    expect(li).toHaveLength(0);

    act(() => set_text(input, "one"));
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(1);

    act(() => set_text(input, "one3"));
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(0);
  });
});

describe("ComboBox that renders JSX", () => {
  // eslint-disable-next-line func-style
  const render = i => (
    <span>
      {i.name}
      <span>{i.name.length}</span>
    </span>
  );
  const component = create(<ComboBox options={options} on_harden={() => {}} render={render} />);
  const dom_root = component.root;
  const input = dom_root.findByType("input");

  test("List items render as JSX", () => {
    const li = dom_root.findAllByType("li");
    expect(li).toHaveLength(3);
    expect(li[0].children[0].type).toBe("span");
    expect(li[0].children[0].children).toHaveLength(2);
    expect(li[0].children[0].children[0]).toBe("One");
    expect(li[0].children[0].children[1].type).toBe("span");
    expect(li[0].children[0].children[1].children).toHaveLength(1);
    expect(li[0].children[0].children[1].children[0]).toBe("3");
  });

  test("Search still only uses the label", () => {
    act(() => set_text(input, "3"));
    let li = dom_root.findAllByType("li");
    expect(li).toHaveLength(0);

    act(() => set_text(input, "t"));
    li = dom_root.findAllByType("li");
    expect(li).toHaveLength(2);
    expect(li[0].children[0].children[0]).toBe("Two");
    expect(li[0].children[0].children[1].type).toBe("span");
    expect(li[0].children[0].children[1].children).toHaveLength(1);
    expect(li[0].children[0].children[1].children[0]).toBe("3");
  });

  test("Hardening an item uses the label function", () => {
    act(() => input.props.onKeyDown({key: "Enter"}) );
    expect_text(input, "Two");
  });
});


describe("ComboBox with many items, so display needs to be sliced (truncated)", () => {
  let softened = null;
  const overflow_options = [
    {name: "One"},
    {name: "Two"},
    {name: "Three"},
    {name: "Four"},
    {name: "Five"},
    {name: "Six"},
    {name: "Seven"},
    {name: "Eight"},
  ];
  const max_displayed_options = 3;
  const component = create(
    <ComboBox
      options={overflow_options}
      max_displayed_options={max_displayed_options}
      on_soft_select={i => {softened = i;}}
      on_harden={() => {}}
    />
  );
  const dom_root = component.root;
  const input = dom_root.findByType("input");
  act(() => set_text(input, "e") );
  const li = dom_root.findAllByType("li");

  test("Should truncate list correctly", () => {
    expect(li).toHaveLength(3);
    expect(li[0].children[0]).toBe("One");
    expect(li[1].children[0]).toBe("Three");
    expect(li[2].children[0]).toBe("Five");
    expect_only_selected(li, 0);
  });

  test("Arrow keys should scroll and update soft-selection properly", () => {
    for (const i of [1, 2, 0, 1, 2, 0]) {
      act(() => input.props.onKeyDown({key: "ArrowDown"}) );
      expect_only_selected(li, i);
      expect(softened.name).toBe(["One", "Three", "Five"][i]);
    }
  });
});
