/**
 * Tests of the Iteration component.
 */

import React from "react";
import { create } from "react-test-renderer";

import { data } from "test/test_utils";
import Iteration from "components/IterationsList/Iteration";


const recipe = {
  self: {
    name: "The Recipe",
    id: 12345,
  },
  ingredients: [
    {base_food: data[0], quantity: 0.2, units: 1, id: 1},
    {base_food: data[1], quantity: 1, units: -1, id: 2},
  ],
};

describe("Iteration row renders", () => {
  const component = create(<Iteration recipe={recipe} base_foods={data} />);
  const dom_root = component.root;

  test("Should render", () => {
    const TDs = dom_root.findAllByType("td");
    expect(TDs).toHaveLength(data.length + 1);

    expect(TDs[0].children[0].type).toBe("a");
    expect(TDs[0].children[0].props.href).toBe("/recipe/12345");
    expect(TDs[0].children[0].children[0]).toBe("The Recipe");

    expect(TDs[1].children[0]).toBe("20g");
    expect(TDs[2].children[0]).toBe("1");
    expect(TDs[3].children).toHaveLength(0);
  });

  test("Should not be highlighted", () => {
    const TR = dom_root.findByType("tr");
    expect(TR.props.className).toBe("");
  });
});

describe("Highlighted iteration row", () => {
  const component = create(<Iteration recipe={recipe} base_foods={data} is_highlighted={true} />);
  const dom_root = component.root;

  test("<tr> element should have `highlighted` CSS class", () => {
    const TR = dom_root.findByType("tr");
    expect(TR.props.className).toBe("highlighted");
  });
});
