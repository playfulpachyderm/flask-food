/**
 * Tests of the IterationsList component.
 */

import React from "react";
import { create } from "react-test-renderer";

import { data, expect_json_equivalent } from "test/test_utils";
import IterationsList from "components/IterationsList";
import Iteration from "components/IterationsList/Iteration";


const recipes = [
  {
    self: {
      name: "The Recipe 1",
      id: 12345,
    },
    ingredients: [
      {base_food: data[0], quantity: 0.2, units: 1, id: 1},
      {base_food: data[1], quantity: 1, units: -1, id: 2},
    ],
  },
  {
    self: {
      name: "The Recipe 2",
      id: 12346,
    },
    ingredients: [
      {base_food: data[1], quantity: 1, units: 0, id: 3},
      {base_food: data[2], quantity: 10, units: 0, id: 4},
    ],
  },
];

describe("IterationsList component", () => {
  const component = create(<IterationsList recipe_list={recipes} highlight={12345} />);
  const dom_root = component.root;

  test("Should render", () => {
    expect(dom_root.findByType("h1").children[0]).toContain("The Recipe");

    const THs = dom_root.findAllByType("th");
    expect(THs).toHaveLength(4);

    expect(THs[0].children).toHaveLength(0);
    expect(THs[1].children[0].children[0]).toBe("parmigiano");
    expect(THs[2].children[0].children[0]).toBe("chicken breast");
    expect(THs[3].children[0].children[0]).toBe("onion");

    const tbody = dom_root.findByType("tbody");
    const iters = tbody.findAllByType(Iteration);
    expect(iters).toHaveLength(2);
    expect(iters[0].props.recipe.self.id).toBe(12345);
    expect(iters[0].props.is_highlighted).toBe(true);
    expect_json_equivalent(iters[0].props.base_foods, data);

    expect(iters[1].props.recipe.self.id).toBe(12346);
    expect(iters[1].props.is_highlighted).toBe(false);
  });
});
