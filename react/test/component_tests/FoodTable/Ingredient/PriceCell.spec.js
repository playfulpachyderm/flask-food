/**
 * Tests of the PriceCell component.
 */

import React from "react";
import { create } from "react-test-renderer";

import PriceCell from "components/FoodTable/Ingredient/PriceCell";


describe("PriceCell component", () => {
  test("Renders with data", () => {
    const test_cases = [
      {number: 1 / 3, dollar_sign: true, expected: "$0.33"},
      {number: 0, dollar_sign: false, expected: "0.00"},
    ];
    for (const {number, dollar_sign, expected} of test_cases) {
      const component = create(<PriceCell price={number} has_dollar_sign={dollar_sign} />);
      const dom_root = component.root;
      const td = dom_root.findByType("td");
      expect(td).not.toBeNull();
      expect(td.children).toHaveLength(1);
      const contents = td.children[0];
      expect(contents).toBe(expected);
    }
  });

  test("Renders '--' without data", () => {
    for (const junk of [NaN, undefined]) {
      const component = create(<PriceCell price={junk} has_dollar_sign={true} />);
      const dom_root = component.root;
      const td = dom_root.findByType("td");
      expect(td).not.toBeNull();
      expect(td.children).toHaveLength(1);
      const contents = td.children[0];
      expect(contents).toBe("--");
    }
  });
});
