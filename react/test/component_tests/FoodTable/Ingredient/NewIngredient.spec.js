/**
 * Tests of the NewIngredient component.
 */

import React from "react";
import { create, act } from "react-test-renderer";

import { NewIngredient } from "components/FoodTable/Ingredient";
import AmountCell from "components/FoodTable/Ingredient/AmountCell";
import { data, set_text, expect_text } from "test/test_utils";

/**
 * A function that does nothing
 */
function dummy_func() {}

describe("Empty NewIngredient component", () => {
  const ingredient = {
    base_food: Object(),
    units: -1,
    quantity: 1,
    amount_string: "",
    inputRef: {current: null},
  };
  const component = create(
    <NewIngredient
      ingredient={ingredient}
      base_foods={data}
      has_dollar_sign={false}
      row_num={0}
      on_amount_change={dummy_func}
      on_delete={dummy_func}
      move_ingredient={dummy_func}
      on_soft_select={dummy_func}
      on_harden={dummy_func}
    />
  );
  const dom_root = component.root;
  const row = dom_root.findByType("tr");
  const amount_cell = row.find(x => x.props.className === "amount-cell");
  const amount_input = amount_cell.findByType("input");
  const name_cell = row.find(x => x.props.className === "name-cell");
  const name_input = name_cell.findByType("input");
  const sodium_cell = row.findAllByType("td")[10];

  test("Renders with no data", () => {
    expect(row).not.toBeNull();

    expect(amount_cell).not.toBeNull();
    expect(amount_input).not.toBeNull();
    expect_text(amount_input, "");

    expect(name_cell).not.toBeNull();
    expect_text(name_input, "");

    expect(sodium_cell).not.toBeNull();
    expect(sodium_cell.children[0]).toBe("--");
  });

  test("Can type in the input box", () => {
    act(() => set_text(name_input, "a"));
    expect_text(name_input, "a");
  });
});


describe("NewIngredient component with a base food", () => {
  const ingredient = {
    base_food: data[0],
    units: 0,
    quantity: 19,
    amount_string: "19",
    inputRef: {current: null},
  };
  const component = create(
    <NewIngredient
      ingredient={ingredient}
      base_foods={data}
      has_dollar_sign={false}
      row_num={0}
      on_amount_change={dummy_func}
      on_delete={dummy_func}
      move_ingredient={dummy_func}
      on_soft_select={dummy_func}
      on_harden={dummy_func}
    />
  );
  const dom_root = component.root;
  const amount_input = dom_root.findByType(AmountCell).findByType("input");
  const protein_cell = dom_root.findAllByType("td")[5];
  const price_cell = dom_root.find(x => x.props.className === "price-cell");

  test("Should fill out nutrient cells", () => {
    expect_text(amount_input, "19");
    expect(protein_cell.children[0]).toBe("627");
    expect(price_cell.children[0]).toBe("75.81");
  });
});
