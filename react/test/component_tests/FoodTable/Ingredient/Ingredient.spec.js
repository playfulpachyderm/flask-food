/**
 * Tests of the Ingredient component.
 */

import React from "react";
import { create } from "react-test-renderer";

import { Ingredient } from "components/FoodTable/Ingredient";
import { data } from "test/test_utils";

describe("Ingredient component", () => {
  const base_food = data[2];  // onion
  const ingredient = {
    base_food,
    units: 1,
    quantity: 2.45909090909091,
    amount_string: "541g",
    inputRef: {current: null},
  };
  const component = create(<Ingredient ingredient={ingredient} row_num={0} />);
  const dom_root = component.root;
  const TDs = dom_root.findAllByType("td");

  test("Ingredient component displays amount", () => {
    expect(TDs[1].children).toHaveLength(1);  // amount
    expect(TDs[1].children[0].type).toBe("input");
    expect(TDs[1].children[0].props.value).toBe("541g");
  });


  test("Ingredient component displays name w/ link", () => {
    expect(TDs[2].children).toHaveLength(1);  // name
    expect(TDs[2].children[0].type).toBe("a");
    expect(TDs[2].children[0].props.href).toBe(`/food/${base_food.id}`);
    expect(TDs[2].children[0].children).toHaveLength(1);
    expect(TDs[2].children[0].children[0]).toBe("onion");
  });

  test("Ingredient macros display properly", () => {
    expect(TDs[3].children).toHaveLength(1);  // cals
    expect(TDs[3].children[0]).toBe("216");
    expect(TDs[4].children).toHaveLength(1);  // carbs
    expect(TDs[4].children[0]).toBe("49");
    expect(TDs[5].children).toHaveLength(1);  // protein
    expect(TDs[5].children[0]).toBe("5");
    expect(TDs[6].children).toHaveLength(1);  // fat
    expect(TDs[6].children[0]).toBe("0");
    expect(TDs[7].children).toHaveLength(1);  // sugar
    expect(TDs[7].children[0]).toBe("22");
    expect(TDs[8].children).toHaveLength(1);  // price
    expect(TDs[8].children[0]).toBe("0.42");
    expect(TDs[9].children).toHaveLength(1);  // potassium
    expect(TDs[9].children[0]).toBe("767");
    expect(TDs[10].children).toHaveLength(1);  // sodium
    expect(TDs[10].children[0]).toBe("20");
  });

  test("Ingredient price displays properly", () => {
    expect(TDs[8].children).toHaveLength(1);
    expect(TDs[8].children[0]).toBe("0.42");
  });
});

describe("Ingredient with no units", () => {
  const base_food = data[2];  // onion
  const ingredient = {
    base_food,
    units: -1,
    quantity: 1,
    amount_string: "",
    inputRef: {current: null},
  };
  const component = create(<Ingredient ingredient={ingredient} row_num={0} />);
  const dom_root = component.root;
  const TDs = dom_root.findAllByType("td");

  test("No units are displayed", () => {
    expect(TDs[1].children).toHaveLength(1);  // amount
    expect(TDs[1].children[0].props.value).toBe("");
  });
});

describe("Ingredient with other parameters", () => {
  const base_food = data[2];  // onion
  const ingredient = {
    base_food,
    units: 0,
    quantity: 1 / 3,
    amount_string: "0.3333",
    inputRef: {current: null},
  };
  const component = create(<Ingredient ingredient={ingredient} has_link={false} has_dollar_sign={true} row_num={0} />);
  const dom_root = component.root;
  const TDs = dom_root.findAllByType("td");

  test("Units display correctly for fractional amount", () => {
    expect(TDs[1].children).toHaveLength(1);  // amount
    expect(TDs[1].children[0].props.value).toBe("0.3333");
  });

  test("The `has_link` prop works", () => {
    expect(TDs[2].children).toHaveLength(1);  // name
    expect(TDs[2].children[0]).toBe("onion");
  });

  test("The `has_dollar_sign` prop works", () => {
    expect(TDs[8].children).toHaveLength(1);
    expect(TDs[8].children[0]).toBe("$0.06");
  });
});
