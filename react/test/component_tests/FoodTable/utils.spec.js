import { parse_amount } from "components/FoodTable/FoodTable";


describe("Parse amounts", () => {
  test("Parse amounts with just numbers", () => {
    expect(parse_amount("123", 100)).toStrictEqual({units: 0, quantity: 123});
    expect(parse_amount("1.23", 100)).toStrictEqual({units: 0, quantity: 1.23});
  });

  test("Parse amounts in grams", () => {
    expect(parse_amount("123g", 100)).toStrictEqual({units: 1, quantity: 1.23});
    expect(parse_amount("10.2g", 100)).toStrictEqual({units: 1, quantity: 0.102});
  });

  test("Parse empty string", () => {
    expect(parse_amount("", 100)).toStrictEqual({units: -1, quantity: 1});
  });
});
