/**
 * Tests of the FoodTable component.
 */

jest.mock("react-jss");

import React from "react";
import { create, act } from "react-test-renderer";
import fetchMock from "fetch-mock";

import FoodTable from "components/FoodTable";
import FoodTableHeader from "components/FoodTable/FoodTableHeader";
import { Ingredient, NewIngredient } from "components/FoodTable/Ingredient";
import NameCell from "components/FoodTable/Ingredient/NameCell";
import AmountCell from "components/FoodTable/Ingredient/AmountCell";
import PantrySidebar from "components/FoodTable/PantrySidebar";
import PantryRow from "components/FoodTable/PantrySidebar/PantryRow";
import FakeForm from "components/FakeForm";

import { get_input_by_name, expect_text, set_text, data, mock_func, expect_json_equivalent } from "test/test_utils";

const ingredients = [
  {id: 123, base_food: data[0], units: 1, quantity: 0.8},  // parmigiano
  {id: 456, base_food: data[2], units: 0, quantity: 2},    // onion
];

const total = {id: 12345, name: 'asdf', cals: 490.40000000000003, carbs: 40.0, protein: 30.400000000000002, fat: 24.0, sugar: 18.0, alcohol: 0.0, water: 0.0, mass: 520.0, density: 1, price: 353.20000000000005, cook_ratio: 1, fundamental_level: 1, date: null};

fetchMock.mock(/\/recipe\/get_available_ingredients/, {status: 200, body: data});
fetchMock.mock(/\/pantry\/as_json/, () => ({status: 200, body: get_ingredients()}));


/***************************** Helper functions *****************************/

/**
 * Create a copy of the `ingredients` array.
 * It's buggy for some reason if you don't copy it (probably react-test-renderer's fault).
 */
function get_ingredients() {
  return JSON.parse(JSON.stringify(ingredients));
}

/**
 * Assert that an ingredient (table row) has the right name
 */
function expect_ingredient_name(ingredient, name) {
  expect(ingredient.findByType(NameCell).findByType("a").children[0]).toBe(name);
}

/**
 * Assert that an ingredient (table row) has the right calories
 */
function expect_has_cals(ingredient, cals) {
  expect(ingredient.findAllByType("td")[3].children[0]).toBe(`${cals}`);
}

/**
 * Assert that an ingredient (table row) has the right protein
 */
function expect_has_protein(ingredient, protein) {
  expect(ingredient.findAllByType("td")[5].children[0]).toBe(`${protein}`);
}

/**
 * Assert that an ingredient (table row) has the right protein
 */
function expect_has_price(ingredient, price) {
  expect(ingredient.findByProps({className: "price-cell"}).children[0]).toBe(`${price}`);
}

/**
 * Assert that an ingredient (table row) has a dollar sign
 */
function expect_has_dollar_sign(ingredient, truth) {
  const exp = expect(ingredient.findByProps({className: "price-cell"}).children[0]);
  (truth ? exp : exp.not).toContain("$");
}

/***************************** Tests begin here *****************************/

describe("FoodTable component", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const TRs = dom_root.findAllByType("tr");

  test("FoodTable renders", () => {
    // 5 rows: 1 header, 2 ingredients, 1 total, 1 percentages
    expect(TRs).toHaveLength(5);
  });

  test("Correct distribution of dollar signs", () => {
    // row 0 is header
    expect_has_dollar_sign(TRs[1], true);
    expect_has_dollar_sign(TRs[2], false);
    expect_has_dollar_sign(TRs[3], true);
  });

  test("Correct distribution of ingredient links", () => {
    expect(TRs[1].findAllByType("td")[2].children[0].type).toBe("a");
    expect(TRs[2].findAllByType("td")[2].children[0].type).toBe("a");
    expect(TRs[3].findAllByType("td")[2].children[0].type).not.toBe("a");
  });

  test("Should be a splat button", () => {
    const TDs = TRs[4].children;
    expect(TDs[3].children).toHaveLength(1);
    expect(TDs[3].children[0].type).toBe("a");
    expect(TDs[3].children[0].children[0]).toBe("Splat");
    expect(TDs[3].children[0].props.href).toBe("/recipe/12345/splat");
  });

  test("Should be an iterate button", () => {
    const TDs = TRs[4].children;
    expect(TDs[1].children).toHaveLength(1);
    expect(TDs[1].children[0].type).toBe("a");
    expect(TDs[1].children[0].children[0]).toBe("Iterate");
    expect(TDs[1].children[0].props.href).toBe("/recipe/12345/iterate");
  });

  test("Save button is initially disabled", () => {
    const fakeform = dom_root.findByType(FakeForm);
    expect(fakeform.props.is_enabled).toBe(false);
  });

  test("Percentages render properly", () => {
    const TDs = TRs[4].findAllByType("td");
    expect(TDs[4].children).toHaveLength(1);
    expect(TDs[5].children).toHaveLength(1);
    expect(TDs[6].children).toHaveLength(1);
    expect(TDs[7].children).toHaveLength(1);

    expect(TDs[4].children[0]).toBe("(33%)");
    expect(TDs[5].children[0]).toBe("(25%)");
    expect(TDs[6].children[0]).toBe("(44%)");
    expect(TDs[7].children[0]).toBe("(45%)");
  });

  test("Correct data in rows", () => {
    const TDs = TRs[1].findAllByType("td");  // Parmigiano
    expect(TDs[1].type).toBe("td");
    expect(TDs[1].children).toHaveLength(1);
    expect(TDs[1].children[0].type).toBe("input");
    expect(TDs[1].children[0].props.value).toBe("80g");
    expect(TDs[2].children[0].children[0]).toBe("parmigiano");
    expect(TDs[3].children[0]).toBe("314"); // cals
    expect(TDs[4].children[0]).toBe("0");   // carbs
    expect(TDs[5].children[0]).toBe("26");  // protein
    expect(TDs[6].children[0]).toBe("24");  // fat
    expect(TDs[7].children[0]).toBe("0");   // sugar
  });
});


describe("Modify quantities", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");
  const row1 = tbody.children[0];  // parmigiano
  const input1 = row1.findByType("input");

  const row_total = tbody.findByProps({className: "total-row"});
  const row_percentages = tbody.findByProps({className: "percentages-row"});

  Object.defineProperty(window, 'location', {writable: true, value: {pathname: `/recipe/${total.id}`} });

  test("Changing an amount should work", () => {
    for (const test_case of ["", "0", "0g", "0.", "0.2", "0.235g", "1.", "-1.523g", ".48", "-1000", "80g"]) {
      set_text(input1, test_case);
      expect_text(input1, test_case);
    }
  });

  test("Changing the amount to something invalid should be ignored", () => {
    for (const test_case of ["a", "2.a", "..", "^2", "2.2.2", "2f", "3-", "3-g"]) {
      set_text(input1, test_case);
      expect_text(input1, "80g");
    }
  });

  test("Changing an amount should change that row, the total, and percentages", () => {
    expect_text(input1, "80g");
    expect_has_cals(row1, 314);
    expect_has_cals(row_total, 490);
    expect_has_protein(row_percentages, "(25%)");

    set_text(input1, "100g");
    expect_text(input1, "100g");
    expect_has_cals(row1, 393);
    expect_has_cals(row_total, 569);
    expect_has_protein(row_percentages, "(26%)");
  });

  test("Title should not be affected", () => {
    // Regression test
    expect(component.getInstance().state.total.name).toBe(total.name);
  });

  test("FakeForm should be populated and enabled", () => {
    const form = dom_root.findByType("form");
    expect(form.props.action).toBe("/recipe/12345");
    const expected_form_data = {
      ingredient_changes: {
        [data[0].id]: {
          quantity: 1,
          units: 1,
        },
      },
    };
    expect_text(get_input_by_name(dom_root, "data"), JSON.stringify(expected_form_data));
    expect(dom_root.findByType(FakeForm).props.is_enabled).toBe(true);
  });
});


describe("Reorder ingredients", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />, {createNodeMock: mock_func});
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");
  const fakeform = dom_root.findByType(FakeForm);

  test("Move an ingredient down", () => {
    expect_ingredient_name(tbody.children[0], "parmigiano");
    const row_count = tbody.children.length;

    // Move the row
    const input = tbody.children[0].findByType(AmountCell).findByType("input");
    input.props.onKeyDown({key: "ArrowDown", ctrlKey: true, shiftKey: true});

    // Rows should have changed places.  The amount of rows should still be the same
    expect_ingredient_name(tbody.children[0], "onion");
    expect(tbody.children.length).toBe(row_count);

    const fakeform_data = fakeform.props.data.ingredient_changes;
    expect(Object.keys(fakeform_data)).toContain("1");
    expect(Object.keys(fakeform_data)).toContain("35");
    expect(fakeform_data["1"].reorder).toBe(1);
    expect(fakeform_data["35"].reorder).toBe(0);
  });

  test("Move an ingredient up", () => {
    expect_ingredient_name(tbody.children[1], "parmigiano");
    const row_count = tbody.children.length;

    const input = tbody.children[1].findByType(AmountCell).findByType("input");
    input.props.onKeyDown({key: "ArrowUp", ctrlKey: true, shiftKey: true});

    expect_ingredient_name(tbody.children[1], "onion");
    expect(tbody.children.length).toBe(row_count);

    const fakeform_data = fakeform.props.data.ingredient_changes;
    expect(Object.keys(fakeform_data)).toContain("1");
    expect(Object.keys(fakeform_data)).toContain("35");
    expect(fakeform_data["1"].reorder).toBe(0);
    expect(fakeform_data["35"].reorder).toBe(1);
  });

  test("Can't move an ingredient up past the top of the list", () => {
    expect_ingredient_name(tbody.children[0], "parmigiano");
    const row_count = tbody.children.length;

    const input = tbody.children[0].findByType(AmountCell).findByType("input");
    input.props.onKeyDown({key: "ArrowUp", ctrlKey: true, shiftKey: true});

    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children.length).toBe(row_count);
  });

  test("Can't move an ingredient down past the bottom of the list", () => {
    expect_ingredient_name(tbody.children[1], "onion");
    const row_count = tbody.children.length;

    const input = tbody.children[1].findByType(AmountCell).findByType("input");
    input.props.onKeyDown({key: "ArrowDown", ctrlKey: true, shiftKey: true});

    expect_ingredient_name(tbody.children[1], "onion");
    expect(tbody.children.length).toBe(row_count);
  });
});


describe("Delete ingredient", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");
  const delete_button = tbody.children[0].findByProps({className: "delete-button"});  // parmigiano
  const fakeform = dom_root.findByType(FakeForm);

  test("Deleting an ingredient should remove the row and update the totals", () => {
    const row_count = tbody.children.length;
    expect_has_cals(tbody.findByProps({className: "total-row"}), 490);
    expect_ingredient_name(tbody.children[0], "parmigiano");

    delete_button.props.onClick();

    expect(tbody.children.length).toBe(row_count - 1);
    expect_has_cals(tbody.findByProps({className: "total-row"}), 176);
    expect_ingredient_name(tbody.children[0], "onion");
  });

  test("Deleting an ingredient updates the FakeForm correctly", () => {
    const fakeform_data = fakeform.props.data.ingredient_changes;
    expect(Object.keys(fakeform_data)).toContain("1");
    expect(fakeform_data["1"].delete).toBe(true);
    expect(Object.keys(fakeform_data)).toContain("35");
    expect(fakeform_data["35"].reorder).toBe(0);
  });
});


describe("Splat table", () => {
  const no_id_total = JSON.parse(JSON.stringify(total));
  no_id_total.id = null;
  const component = create(<FoodTable ingredients={get_ingredients()} total={no_id_total} />);
  const dom_root = component.root;
  const row_percentages = dom_root.findAllByType("tr")[4];

  test("Should be no buttons on a splat table", () => {
    // Save button should also be checked, but once again due to Jest, can't reliably set
    // `window.location.pathname` (it seems to be preserved / overwritten between tests).
    // So we skip testing for now.
    expect(row_percentages.children[1].children).toHaveLength(0);  // Iterate button
    expect(row_percentages.children[3].children).toHaveLength(0);  // Splat button
  });
});


describe("Create a new row", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");
  const amount_input = tbody.children[0].findByType(AmountCell).findByType("input");
  const fakeform = dom_root.findByType(FakeForm);

  test("Initial state", () => {
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "onion");
  });

  test("Pressing <Enter> in an amount cell creates a new row", () => {
    const row_count = tbody.children.length;

    amount_input.props.onKeyDown({key: "Enter"});

    expect(tbody.children).toHaveLength(row_count + 1);

    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(NewIngredient);
    expect(tbody.children[2].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[2], "onion");

    // No operation with NewIngredients should trigger a FakeForm update
    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Changing the amount in a new ingredient works", () => {
    const new_row = dom_root.findByType(NewIngredient).findByType("tr");
    const new_amount_input = new_row.findByType(AmountCell).findByType("input");

    expect_text(new_amount_input, "");

    for (const test_case of ["10g", "10"]) {
      act(() => set_text(new_amount_input, test_case));
      expect_text(new_amount_input, test_case);
    }

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Changing the name in a new ingredient works", () => {
    const new_row = dom_root.findByType(NewIngredient).findByType("tr");
    const new_name_input = new_row.findByProps({className: "name-cell"}).findByType("input");

    for (const test_case of ["a", "afw", "gioer", ""]) {
      act(() => set_text(new_name_input, test_case));
      expect_text(new_name_input, test_case);
    }
    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Changing name to an existing ingredient does not update the row", () => {
    const new_row = dom_root.findByType(NewIngredient).findByType("tr");
    const new_name_input = new_row.findByProps({className: "name-cell"}).findByType("input");
    act(() => set_text(new_name_input, "onion"));

    expect_has_cals(new_row, "--");
    expect_has_price(new_row, "--");

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Changing name to an ingredient updates the ingredient", () => {
    const new_row = dom_root.findByType(NewIngredient).findByType("tr");
    const new_name_input = new_row.findByProps({className: "name-cell"}).findByType("input");
    act(() => set_text(new_name_input, "chicken breast"));

    expect_has_cals(new_row, 1650);
    expect_has_price(new_row, "22.00");

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Harden a new ingredient", () => {
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(NewIngredient);
    expect(tbody.children[2].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[2], "onion");

    const new_row = dom_root.findByType(NewIngredient).findByType("tr");
    const new_name_input = new_row.findByProps({className: "name-cell"}).findByType("input");
    expect(Object.keys(fakeform.props.data.ingredient_changes)).toHaveLength(0);
    act(() => new_name_input.props.onKeyDown({key: "Enter"}));

    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "chicken breast");
    expect(tbody.children[2].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[2], "onion");
  });

  test("Should have updated the fakeform", () => {
    const { ingredient_changes } = fakeform.props.data;
    expect(Object.keys(ingredient_changes)).toContain(`${[data[1].id]}`);
    expect_json_equivalent(ingredient_changes[data[1].id], {created: true, quantity: 10, units: 0, reorder: 1});

    expect(Object.keys(ingredient_changes)).toContain(`${[data[2].id]}`);
    expect_json_equivalent(ingredient_changes[data[2].id], {reorder: 2});
  });
});


describe("Move and delete a new row", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />, {createNodeMock: mock_func});
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");
  const row = dom_root.findAllByType("tr")[2];  // Second row (onion); row 0 is header
  const amount_input = row.find(x => x.props.className === "amount-cell").findByType("input");  // have to use callback because it's not mounted yet
  const fakeform = dom_root.findByType(FakeForm);

  test("Create the new row", () => {
    const row_count = tbody.children.length;
    amount_input.props.onKeyDown({key: "Enter"});
    expect(tbody.children).toHaveLength(row_count + 1);

    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "onion");
    expect(tbody.children[2].type).toBe(NewIngredient);

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Can't move the new row down", () => {
    const new_row_amount_input = tbody.findByType(NewIngredient).findByProps({name: "amount"});
    new_row_amount_input.props.onKeyDown({key: "ArrowDown", ctrlKey: true, shiftKey: true});

    // Check nothing changed
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "onion");
    expect(tbody.children[2].type).toBe(NewIngredient);

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Move the new row up", () => {
    const new_row_amount_input = tbody.findByType(NewIngredient).findByProps({name: "amount"});
    new_row_amount_input.props.onKeyDown({key: "ArrowUp", ctrlKey: true, shiftKey: true});

    // Should have moved the new row
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(NewIngredient);
    expect(tbody.children[2].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[2], "onion");

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Move the new row down", () => {
    const new_row_amount_input = tbody.findByType(NewIngredient).findByProps({name: "amount"});
    new_row_amount_input.props.onKeyDown({key: "ArrowDown", ctrlKey: true, shiftKey: true});

    // Should have moved the new row back to where it was
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "onion");
    expect(tbody.children[2].type).toBe(NewIngredient);

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });

  test("Delete the new row", () => {
    const delete_button = tbody.findByType(NewIngredient).findByProps({className: "delete-button"});
    const row_count = tbody.children.length;
    delete_button.props.onClick();
    expect(tbody.children).toHaveLength(row_count - 1);

    // New row should be gone
    expect(tbody.children[0].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[0], "parmigiano");
    expect(tbody.children[1].type).toBe(Ingredient);
    expect_ingredient_name(tbody.children[1], "onion");

    expect_json_equivalent(fakeform.props.data.ingredient_changes, {});
  });
});


describe("An empty FoodTable", () => {
  const component = create(<FoodTable ingredients={[]} total={total} />);
  const dom_root = component.root;
  const tbody = dom_root.findByType("tbody");

  test("Should auto-insert a new row", () => {
    expect(tbody.children.length).toBe(1 + 2);  // NewIngredient, total, percentages
    expect(tbody.children[0].type).toBe(NewIngredient);
  });
});


describe("Change the recipe title", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const title_input = dom_root.findByType(FoodTableHeader).findByType("input");
  const fakeform = dom_root.findByType(FakeForm);

  test("Should update the title and the fake form", () => {
    expect_text(title_input, total.name);
    expect(fakeform.props.is_enabled).toBe(false);

    for (const test_case of ["", "wefoijo", "F K L # * )"]) {
      set_text(title_input, test_case);
      expect_text(title_input, test_case);

      expect(fakeform.props.is_enabled).toBe(true);
      expect(fakeform.props.data.change_title).toBe(test_case);
    }
  });
});


describe("Add an item from the pantry", () => {
  const component = create(<FoodTable ingredients={get_ingredients()} total={total} />);
  const dom_root = component.root;
  const pantry_sidebar = dom_root.findByType(PantrySidebar);
  const num_foodtable_rows = dom_root.findAllByType(Ingredient).length;


  test("There should be a pantry", () => {
    expect(pantry_sidebar).not.toBeNull();
    expect(dom_root.findAllByType(PantryRow)).toHaveLength(ingredients.length);
  });

  test("Clicking an item in the pantry adds it to the recipe", () => {
    // pantry is populated asynchronously; can't initialize it outside the test
    const pantry_rows = dom_root.findAllByType(PantryRow);
    const plus_button = pantry_rows[0].findAllByType("span")[0];
    plus_button.props.onClick();

    const ingredient_rows = dom_root.findAllByType(Ingredient);
    expect(ingredient_rows).toHaveLength(num_foodtable_rows + 1);

    const new_row = ingredient_rows[ingredient_rows.length - 2];  // Don't forget the Total row
    expect(new_row.props.ingredient.base_food).toBe(pantry_rows[0].props.ingredient.base_food);
  });
});
