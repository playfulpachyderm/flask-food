/**
 * Tests of the PantrySidebar component.
 */

jest.mock("react-jss");

import React from "react";
import { create } from "react-test-renderer";

import PantrySidebar from "components/FoodTable/PantrySidebar";
import PantryRow from "components/FoodTable/PantrySidebar/PantryRow";
import { data } from "test/test_utils";

describe("PantrySidebar component", () => {
  const ingredients = [
    {
      base_food: data[0],
      units: 1,
      quantity: 0.5,
      amount_string: "50g",
      inputRef: {current: null},
    },
    {
      base_food: data[1],
      units: -1,
      quantity: 1,
      amount_string: "",
      inputRef: {current: null},
    },
    {
      base_food: data[2],
      units: 0,
      quantity: 2,
      amount_string: "2",
      inputRef: {current: null},
    },
  ];
  const component = create(<PantrySidebar ingredients={ingredients} on_click={() => {}} />);
  const dom_root = component.root;
  const rows = dom_root.findAllByType(PantryRow);

  test("Renders properly", () => {
    expect(rows).toHaveLength(3);
    ["50g parmigiano", "chicken breast", "2 onion"].forEach((v, i) => {
      expect(rows[i].findAllByType("span")[1].children[0].type).toBe("a");
      expect(rows[i].findAllByType("span")[1].children[0].children[0]).toBe(v);
    });
  });
});
