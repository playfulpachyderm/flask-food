// We are using node's native package 'path'
// https://nodejs.org/api/path.html
const path = require('path');

// Constant with our paths
const paths = {
  DIST: path.resolve(__dirname, '..', 'static', 'js'),
  SRC: path.resolve(__dirname, 'src'),
};

// Webpack configuration
module.exports = {
  resolve: {
    // Resolve imports by looking in these base paths
    modules: [paths.SRC, 'node_modules'],
    // Look for files with these extensions
    extensions: [".js", ".jsx"],
  },

  entry: {
    show_recipe: path.join('views', "Recipe.jsx"),
    iterations_list: path.join('views', "Iterations.jsx"),
  },
  output: {
    path: paths.DIST,
    filename: '[name].js',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
};
