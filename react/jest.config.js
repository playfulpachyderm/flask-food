const path = require('path');

module.exports = {
  moduleDirectories: ["src", __dirname, "node_modules"],
  testMatch: [path.join(__dirname, "test/**/?(*.)+(spec|test).[jt]s?(x)")],
};
