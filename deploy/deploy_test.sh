set -e

(cd /app/deploy && docker-compose -f docker-compose-test.yaml down)
rm -rf /food-data/test/*

(cd /app/deploy && docker-compose -f docker-compose-test.yaml up -d)
sleep 3s
ENVIRONMENT=test python /app/food/sql_init/data.py
