set -e

(cd /app/deploy && docker-compose down)
rm -rf /food-data/app/*

(cd /app/deploy && docker-compose up -d)
sleep 3s
python /app/food/sql_init/data.py
