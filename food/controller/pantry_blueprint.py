"""
Routes for paths under `/pantry`
"""

import os
import json

from flask import Blueprint, render_template, jsonify, request

from models.pantry import Pantry

from .auth_blueprint import require_login

blueprint_pantry = Blueprint('pantry',
    __name__,
    template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views/recipes")
)
blueprint_pantry.before_request(require_login)


@blueprint_pantry.route("/", methods=["GET"])
def show_pantry():
    """Singleton route to show the pantry as a recipe"""
    return render_template("recipe_show.html", data=Pantry.instance().to_dict())

@blueprint_pantry.route("/", methods=["POST"])
def update_pantry():
    """Singleton route to show the pantry as a recipe"""
    pantry = Pantry.instance()
    pantry.update(json.loads(request.form["data"]))
    pantry.save()

    return show_pantry()


# JSON API
# --------

@blueprint_pantry.route("/as_json")
def get_pantry():
    """Get contents of the pantry in JSON"""
    return jsonify(Pantry.instance().to_dict()["ingredients"])
