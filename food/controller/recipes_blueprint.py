"""
Routes for paths under `/recipes`
"""

import os
import json

from flask import Blueprint, render_template, request, abort, redirect, jsonify
from sqlalchemy.orm.exc import NoResultFound

from models import Recipe
from models.pantry import Pantry

from .auth_blueprint import require_login


blueprint_recipes = Blueprint('recipes',
    __name__,
    template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views/recipes")
)

blueprint_recipes.before_request(require_login)


# Helpers
# -------

def recipe_or_404(recipe_id):
    """Look up a recipe by its id, aborting with 404 if one is not found"""
    try:
        return Recipe.by_id(recipe_id)
    except NoResultFound:
        abort(404)


# /recipe
# -------

@blueprint_recipes.route("/")
def recipes_index():
    """
    List all the recipes, not including daily logs.
    By default, excludes iterations; they can be included by adding `?all_iterations` to
    query string.
    """
    # pylint: disable=singleton-comparison
    show_all_iters = "all_iterations" in request.args
    if show_all_iters:
        all_recipes = Recipe.x_all()
    else:
        all_recipes = Recipe.x_all(Recipe.base_recipe == None)
    ret = [[r.name, Recipe.url_for(r)] for r in all_recipes]
    return render_template("recipe_index.html", recipes=ret, showing_all_iters=show_all_iters)


@blueprint_recipes.route("/new", methods=["GET"])
def new_recipe():
    """New recipe form"""
    return render_template("recipe_show.html",
        data=Recipe("").to_dict(),
        title="New recipe"
    )

@blueprint_recipes.route("/new", methods=["POST"])
def save_recipe():
    """Save a submitted recipe"""
    r = Recipe("")
    r.update(json.loads(request.form['data']))

    pantry = Pantry.instance()
    pantry.add(r)
    r.save()
    pantry.save()
    return redirect(Recipe.url_for(r))


# /recipe/<id>
# -----------

@blueprint_recipes.route("/<int:recipe_id>")
def show_recipe(recipe_id):
    """Recipe page"""
    recipe = recipe_or_404(recipe_id)
    data = recipe.to_dict()
    data.update(Recipe.nav_for(recipe))
    return render_template("recipe_show.html", data=data)


@blueprint_recipes.route("/<int:recipe_id>", methods=["POST"])
def update_recipe(recipe_id):
    """Modify a recipe by applying a set of changes"""
    r = recipe_or_404(recipe_id)
    r.update(json.loads(request.form['data']))

    r.save()
    return show_recipe(recipe_id)


@blueprint_recipes.route("/<int:recipe_id>/splat")
def splat_recipe(recipe_id):
    """Show a recipe's splat"""
    recipe = recipe_or_404(recipe_id)
    splat = recipe.splat()
    return render_template("recipe_show.html", data=splat.to_dict())


@blueprint_recipes.route("/<int:recipe_id>/iterate", methods=["GET"])
def new_iteration(recipe_id):
    """Make a new iteration of this recipe"""
    recipe = recipe_or_404(recipe_id)
    iteration = recipe.iterate()
    return render_template("recipe_show.html", data=iteration.to_dict())

@blueprint_recipes.route("/<int:recipe_id>/iterate", methods=["POST"])
def save_iteration(recipe_id):
    """Save an iteration, applying any changes made"""
    recipe = recipe_or_404(recipe_id)
    iteration = recipe.iterate()
    pantry = Pantry.instance()
    pantry.add(iteration)
    iteration.save()
    pantry.save()

    update_recipe(iteration.id)
    return redirect(Recipe.url_for(iteration))

@blueprint_recipes.route("/<int:recipe_id>/iterations")
def view_iterations(recipe_id):
    """Save an iteration, applying any changes made"""
    recipe = recipe_or_404(recipe_id)
    base_recipe = recipe.base_recipe or recipe
    iterations = [base_recipe] + Recipe.where(Recipe.base_recipe == base_recipe)
    return render_template("iterations_show.html",
        title=recipe.name,
        data=[i.to_dict() for i in iterations],
        highlight=recipe_id
    )



# JSON API
# ========

@blueprint_recipes.route("/get_available_ingredients")
def get_available_ingredients():
    """Get all ingredients that could be added to a recipe"""
    result = Recipe.available_ingredients_as_json()
    return jsonify(result)
