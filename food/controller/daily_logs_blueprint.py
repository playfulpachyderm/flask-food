"""
Routes for paths under `/daily_log`
"""

import os
import json
from datetime import date

from flask import Blueprint, render_template, request, abort, redirect
from sqlalchemy.orm.exc import NoResultFound

from models import DailyLog

from .auth_blueprint import require_login

blueprint_daily_logs = Blueprint('daily_logs',
    __name__,
    template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views/daily_logs")
)

blueprint_daily_logs.before_request(require_login)


def daily_log_or_404(date_slug):
    """Look up a daily log by its slug, aborting with 404 if one is not found"""
    try:
        return DailyLog.by_slug(date_slug)
    except NoResultFound:
        abort(404)
    except ValueError:
        abort(400, f"Invalid date: {date_slug}")


@blueprint_daily_logs.route("/")
def daily_logs_index():
    """List all the daily logs"""
    all_days = DailyLog.all()
    ret = [[r.name, DailyLog.url_for(r)] for r in reversed(all_days)]
    return render_template("daily_log_index.html", daily_logs=ret)


@blueprint_daily_logs.route("/<date_slug>")
def show_daily_log(date_slug):
    """Show a daily log"""
    daily_log = daily_log_or_404(date_slug)
    data = daily_log.to_dict()
    data.update(DailyLog.nav_for(daily_log))
    return render_template("recipe_show.html", data=data)

@blueprint_daily_logs.route("/<date_slug>/splat")
def splat_daily_log(date_slug):
    """Show a daily log's splat"""
    daily_log = daily_log_or_404(date_slug)
    splat = daily_log.splat()
    return render_template("recipe_show.html", data=splat.to_dict())


@blueprint_daily_logs.route("/new", methods=["GET"])
def new_daily_log():
    """New daily log form"""
    day = date.today()
    return render_template("recipe_show.html",
        data=DailyLog(day).to_dict(),
        title="New daily log"
    )


@blueprint_daily_logs.route("/new", methods=["POST"])
def save_daily_log():
    """Save a submitted daily log"""
    daily_log = DailyLog(date.today())

    try:
        daily_log.update(json.loads(request.form["data"]))
    except ValueError:
        abort(422, "Date format is no good")

    daily_log.save()
    return redirect(DailyLog.url_for(daily_log))


@blueprint_daily_logs.route("/<date_slug>/iterate", methods=["GET"])
def new_iteration(date_slug):
    """Make a new iteration of this daily log"""
    daily_log = daily_log_or_404(date_slug)
    iteration = daily_log.iterate()
    return render_template("recipe_show.html",
        data=iteration.to_dict(),
        title=f"New daily log, from {daily_log.name}"
    )

@blueprint_daily_logs.route("/<date_slug>/iterate", methods=["POST"])
def save_iteration(date_slug):
    """Save an iteration, applying any changes made"""
    daily_log = daily_log_or_404(date_slug)
    iteration = daily_log.iterate()
    iteration.save()

    update_daily_log(iteration.get_slug())
    return redirect(DailyLog.url_for(iteration))


@blueprint_daily_logs.route("/<date_slug>", methods=["POST"])
def update_daily_log(date_slug):
    """Modify a daily log by applying a set of changes"""
    daily_log = daily_log_or_404(date_slug)

    try:
        daily_log.update(json.loads(request.form['data']))
    except ValueError:
        abort(422, "Date format is no good")

    daily_log.save()
    return redirect(DailyLog.url_for(daily_log))


@blueprint_daily_logs.route("/today")
def today():
    """Redirects to today's daily log, or creates a new one if it doesn't exist"""
    try:
        return redirect(DailyLog.url_for(DailyLog.today()))
    except NoResultFound:
        return redirect(DailyLog.url_path("new"))
