"""
Base module for routes
"""

import os
import json
import traceback

from flask import Flask, render_template, abort, request
from werkzeug.exceptions import HTTPException, InternalServerError, NotFound

from application_factory import create_application

from models import Food, Recipe, DailyLog, Pantry, session
from .recipes_blueprint import blueprint_recipes
from .foods_blueprint import blueprint_foods
from .daily_logs_blueprint import blueprint_daily_logs
from .pantry_blueprint import blueprint_pantry
from .auth_blueprint import blueprint_auth

app = create_application()

@app.route("/")
def index():
    """Index page of website"""
    return render_template("index.html")

app.register_blueprint(blueprint_recipes, url_prefix=Recipe.url_path())
app.register_blueprint(blueprint_foods, url_prefix=Food.url_path())
app.register_blueprint(blueprint_daily_logs, url_prefix=DailyLog.url_path())
app.register_blueprint(blueprint_pantry, url_prefix=Pantry.url_path())
app.register_blueprint(blueprint_auth, url_prefix="/login")


@app.route("/explicit_404")
def explicit_404():
    """Cause an explicit 404 error"""
    msg = "This page is specifically designed not to exist.  You're welcome!"
    raise NotFound(msg)

@app.route("/explicit_500")
def explicit_500():
    """Produce an explicit 500 error"""
    msg = "This page deliberately triggers a server error for testing purposes.  Here you go!"
    raise InternalServerError(msg)

@app.route("/implicit_500")
def implicit_500():
    """Produce an implicit (uncaught) server error"""
    Food.by_name("fwjekfj")


@app.errorhandler(Exception)
def handle_error(error):
    """Catch-all error handler"""
    session.rollback()

    if isinstance(error, HTTPException):
        # Deliberately raised exception
        return render_template("error_pages/known_error.html", error=error), error.code

    new_err = InternalServerError("Something went wrong in the application...")
    new_err.__cause__ = error
    tb = "".join(traceback.format_exception(type(new_err), new_err, new_err.__traceback__))

    if request.json:
        data = request.json, "request.json"
    elif request.form:
        data = request.form, "request.form"
    elif request.data:
        data = request.data, "request.data"
    else:
        data = None

    # Log the error
    print("----------------------------")
    print(f"Error when trying to {request.method} {request.url}")
    print()
    print(tb)
    if data:
        print("--")
        print(f"Request included the following {data[1]}:")
        print(data[0])
        print()

    if app.config["DEBUG"]:
        # If we are in debug mode, show the error with traceback
        return render_template("error_pages/unknown_error.html",
            error_type=type(error),
            error=error,
            traceback=tb,
            data=data
        ), new_err.code

    return render_template("error_pages/known_error.html", error=new_err), new_err.code

@app.template_filter('static_url')
def static_url_for(s):
    """
    Jinja helper function to generate a full url for a static asset by joining it with the
    static root URL
    """
    return app.config["static_root_url"] + s
