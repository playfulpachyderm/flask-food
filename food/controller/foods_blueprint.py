"""
Routes for paths under `/food`
"""

import os

from flask import Blueprint, render_template
from models import Food

from .auth_blueprint import require_login

blueprint_foods = Blueprint('foods',
    __name__,
    template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views/foods")
)

blueprint_foods.before_request(require_login)


@blueprint_foods.route("/")
def foods_index():
    """List all the base foods"""
    all_foods = Food.x_all()
    ret = [[r.name, Food.url_for(r)] for r in all_foods]
    return render_template("food_index.html", foods=ret)

@blueprint_foods.route("/<int:food_id>")
def show_food(food_id):
    """Food page"""
    return render_template('food_show.html', food=Food.by_id(food_id))
