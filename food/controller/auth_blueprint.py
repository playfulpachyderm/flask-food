"""
Methods of and relating to login and authentication
"""

from urllib.parse import urlencode

from flask import make_response, request, redirect, Blueprint, render_template, jsonify

from models.authentication import create_token, validate_token, authenticate_by_password

def require_login():
    """
    Guard method to be applied as a `before_request` or called in a guarded route.
    Stops execution and redirects to login page if user is not authenticated.
    """
    token = request.cookies.get("jwt")

    validated, error = validate_token(token)
    if not validated:
        params = {
            "return_to": request.full_path,
            "error": error,
        }
        return redirect(f"/login?{urlencode(params)}")
    return None


blueprint_auth = Blueprint('login', __name__)


@blueprint_auth.route("/", methods=["GET"])
def login():
    """Login page.  Redirects immediately if already logged in"""
    return_to = request.args.get("return_to", "/")

    token = request.cookies.get("jwt")
    validated, error = validate_token(token)
    if validated:
        return redirect(return_to)

    return render_template("login.html", error=error, return_to=return_to)


@blueprint_auth.route("/", methods=["POST"])
def login_post():
    """Currently password-only authentication, since only 1 user (me)"""
    return_to = request.form.get("return_to", "/")
    if not authenticate_by_password(request.form["password"]):
        return render_template("login.html", error="Incorrect password", return_to=return_to), 401
    resp = make_response(redirect(return_to))
    resp.set_cookie("jwt", create_token(), secure=True, httponly=True, samesite="Strict")
    return resp


@blueprint_auth.route("/logout")
def logout():
    """Logout, deleting the token"""
    resp = make_response(redirect("/login"))
    resp.set_cookie("jwt", "", expires=0)
    return resp


@blueprint_auth.route("/test")
def login_test():
    """Dump the contents of the current JWT token, or decoding error if applicable"""
    token = request.cookies.get("jwt")
    data, error = validate_token(token)

    if error:
        return jsonify({"ERROR": error}), 401

    return jsonify(data)
