"""
Generate and configure a Flask application, based on the loaded config
"""

import os

from flask import Flask

from .config import config

def create_application():
    """Generate the application"""
    app = Flask(
        __name__,
        template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views")
    )
    app.config.update(config)
    app.url_map.strict_slashes = app.config["strict_slashes"]

    return app
