"""
The DailyFood model
"""

from datetime import datetime, timedelta, date

from .recipe import Recipe
from ._base import session, single_lookup


class DailyLog(Recipe):
    """
    Represents a daily food journal.
    This is a food item that has `fundamental_level = 2`.
    """

    SLUG_FORMAT = "%Y-%m-%d"
    NAME_FORMAT = "%b %-d, %Y"

    __mapper_args__ = {
        "polymorphic_identity": 2,
    }

    def __init__(self, _date, *args, **kwargs):
        self.date = _date
        super().__init__(DailyLog.to_name(self.date), *args, **kwargs)


    # Methods
    # -------

    def get_slug(self):
        """
        Get a representation of the date which is suitable for a URL.
        Example: `2020-01-01`
        """
        return DailyLog.to_slug(self.date)

    @classmethod
    def url_for(cls, daily_log):
        """Get the appropriate link to a daily log"""
        return "/".join([
            cls.url_path(),
            daily_log.get_slug()
        ])

    def iterate(self):
        ret = DailyLog(DailyLog.next_available_date())
        for i in self.ingredients:
            ret.add(i.times(1))
        return ret

    def update_date(self, new_date: str):
        """
        Takes a date string in either SLUG_FORMAT or NAME_FORMAT and updates this daily log
        """
        self.date = DailyLog.parse_date(new_date)
        self.name = DailyLog.to_name(self.date)  # in case `new_date` is in SLUG_FORMAT

    def update(self, patch):
        """
        Daily logs need special consideration for changes to the name, since it is associated
        with the date
        """
        super().update(patch)
        if "change_title" in patch:
            self.update_date(patch["change_title"])


    # Daily log getters
    # -----------------

    @classmethod
    @single_lookup
    def by_slug(cls, slug):
        """Look up a daily log by date slug"""
        return session.query(cls).filter_by(date=DailyLog.parse_date(slug)).one()

    @classmethod
    @single_lookup
    def by_date(cls, _date):
        """Look up a daily log by a date object"""
        return session.query(cls).filter_by(date=_date).one()

    @classmethod
    @single_lookup
    def today(cls):
        """Look up a daily log for today"""
        return cls.by_date(date.today())

    @classmethod
    def from_json(cls, json):
        json["title"] = DailyLog.parse_date(json["date"])
        return super(DailyLog, cls).from_json(json)

    @classmethod
    def date_range(cls, start, end):
        """Get a list of all daily logs from days within a date range"""
        return cls.where(start <= cls.date, cls.date <= end)


    # Date methods
    # ------------

    @classmethod
    def parse_date(cls, s: str) -> date:
        """Parse a date from an input string"""
        try:
            return datetime.strptime(s, cls.SLUG_FORMAT).date()
        except ValueError:
            return datetime.strptime(s, cls.NAME_FORMAT.replace("-", "")).date()

    @classmethod
    def to_slug(cls, d: date):
        """Format an input date as a slug"""
        return d.strftime(cls.SLUG_FORMAT)

    @classmethod
    def to_name(cls, d: date):
        """Format an input date as a name"""
        return d.strftime(cls.NAME_FORMAT)

    @classmethod
    def next_available_date(cls):
        """Get the day after the latest existing daily log"""
        return session.query(cls).order_by(cls.date.desc()).first().date + timedelta(days=1)


    # Splat methods
    # -------------

    @classmethod
    def splat_range(cls, start, end):
        """Aggregate all daily logs in a date range and splat them"""
        total = Recipe(f"Consumption, {start} to {end}")
        for day in cls.date_range(start, end):
            total.add(day)

        return total.splat()

    @classmethod
    def nav_for(cls, r):
        """DailyLog nav should be by date, instead of id"""
        next_daily_log = cls.next_in_sequence(r, cls.date)
        prev_daily_log = cls.prev_in_sequence(r, cls.date)
        return {"nav": {
            "next": cls.url_for(next_daily_log) if next_daily_log else None,
            "prev": cls.url_for(prev_daily_log) if prev_daily_log else None,
        }}
