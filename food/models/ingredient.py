"""
The Ingredient model
"""

from functools import wraps

from sqlalchemy import Column, Integer, Float, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.collections import MappedCollection

from ._base import Base
from .food import Food
from .recipe import Recipe

def can_be_food(f):
    """
    Modifies the interface of a function that takes a Food id (i.e., type(item) == int),
    to make it able to take Food objects as well.  Converts the Food instance to
    its id.
    """
    @wraps(f)
    def ret(self, item, *args, **kwargs):
        if isinstance(item, Food):
            item = item.id
        return f(self, item, *args, **kwargs)
    return ret


class IngredientList(MappedCollection):
    """
    Collection class of ingredients to be used by a recipe.
    Should behave like a list in most contexts, but we want to be able to look
    ingredients up by their base food id.
    """
    def __init__(self):
        """Initialize the keying function to use `base_food_id` attribute as the mapping key"""
        super().__init__(keyfunc=lambda x: x.base_food_id)

    def __iter__(self):
        """Should return the values (i.e., ingredients) not the keys (i.e., id numbers)"""
        return iter(self.values())

    @can_be_food
    def __contains__(self, item):
        """
        Should be able to check for the presence of:
            - an ingredient object in the values
            - the id of a food object, in the keys
            - a food object, in the keys (decorator converts to id)
        """
        return item in self.keys() or item in self.values()

    def at_position(self, position):
        """
        Find an ingredient by its position in the ingredients list.
        `IngredientList().at_position(x)` should behave like `list()[x]`.
        """
        return list(self.values())[position]

    @can_be_food
    def __getitem__(self, item):
        """Only here for the decorator; want to be able to check if a Food is in a recipe"""
        return super().__getitem__(item)

    @can_be_food
    def pop(self, value):
        """Only here for the decorator"""
        return super().pop(value)


class Ingredient(Base):
    """Represents an amount of a food item"""
    __tablename__ = "ingredients"

    id = Column(Integer, primary_key=True)
    base_food_id = Column(Integer, ForeignKey(Food.id))
    base_food = relationship("Food", foreign_keys="Ingredient.base_food_id")
    belongs_to = Column(Integer, ForeignKey(Recipe.id))
    recipe = relationship(Recipe,
        foreign_keys="Ingredient.belongs_to",
        backref=backref(
            "ingredients",
            cascade="all, delete-orphan",
            collection_class=IngredientList,
            order_by="Ingredient.list_position"
        )
    )
    list_position = Column(Integer)
    quantity = Column(Float)
    units = Column(Integer)
    sale_price = Column(Float)
    hidden = Column(Boolean)
    name = Column(String)

    def __init__(self, base_food, quantity=1, units=0, hidden=False, recipe=None, name=None):
        self.base_food = base_food
        self.base_food_id = base_food.id
        self.quantity = quantity
        self.units = units
        self.hidden = hidden
        self.recipe = recipe
        self.name = name

    def str_amount(self):
        """Returns: a string representation of an amount"""
        if self.units == 1:
            return "{}g".format(self.quantity * self.base_food.mass)
        else:
            return str(self.quantity)

    def __str__(self):
        beginning = self.name if self.name else "{} {}".format(
            self.str_amount(), self.base_food.name
        )
        return "{}<recipe: {}>".format(
            beginning,
            self.recipe.name if self.recipe else "None!"
        )

    def __repr__(self):
        return str(self)

    def to_dict(self):
        """Get this food item as a dictionary of attributes."""
        base_food = self.base_food
        if isinstance(base_food, Recipe):
            base_food = super(Recipe, base_food)

        return {
            "base_food": base_food.to_dict(),
            "quantity": self.quantity,
            "units": self.units,
            "id": self.id,
        }

    def times(self, n):
        """Ingredient of an ingredient (convenience method)"""
        return Ingredient(self.base_food,
            quantity=self.quantity * n,
            units=self.units,
            name=self.name
        )

    def set_price(self, price):
        """Set the price on this ingredient to be different than usual"""
        self.sale_price = price
        return self

    def free(self):
        """This ingredient was free (or stolen :P)"""
        return self.set_price(0)
