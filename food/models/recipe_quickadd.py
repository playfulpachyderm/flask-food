"""
The RecipeQuickadd model.

A quick-add recipe is one with a primary ingredient, and some others that are
tacked on at the end, like extra butter or cheese topping.
"""

from .recipe import Recipe
from .ingredient import Ingredient

class RecipeQuickadd(Recipe):
    """
    Represents a quick-add recipe.
    """
    __mapper_args__ = {
        "polymorphic_identity": 4
    }

    def __init__(self, primary, *args, **kwargs):
        """Identify the first ingredient as a primary ingredient"""
        if not isinstance(primary, Ingredient):
            raise TypeError(f"`primary` must be an Ingredient; got {type(primary)} instead")

        super().__init__(primary.base_food.name, *args, **kwargs)
        self.add(primary)

    def update_name(self):
        """Refresh the name of this recipe based on its current ingredient list"""
        ret = "{primary} w/ {extras}"
        primary = self.primary.base_food.name
        extras = [i.base_food.name
            for i in self.ingredients
            if not i.hidden and not i.list_position == 0
        ]
        if len(extras) == 0:
            return primary
        if len(extras) == 1:
            return ret.format(primary=primary, extras=extras[0])

        return ret.format(
            primary=primary,
            extras="{} and {}".format(", ".join(extras[:-1]), extras[-1])
        )

    def save(self):
        """Should not be able to save if there isn't at least one primary and one extra"""
        if len(self.ingredients) < 2:
            raise ValueError("No extras exist!")

        super().save()

    def add(self, ingredient):
        """Update this recipe's name on adding new ingredients"""
        super().add(ingredient)
        self.name = self.update_name()

    def add_hidden(self, ingredient):
        """No updating the name if the ingredient is hidden"""
        ingredient.hidden = True
        self.add(ingredient)

    @property
    def primary(self):
        """The primary food that this quick-add recipe is based on"""
        return self.ingredients.at_position(0)
