"""
Handles DB connection, and gibberish with SQL Alchemy
"""

from functools import wraps
import inspect
import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from application_factory import config

engine = create_engine("{}://{}:{}@{}:{}/{}".format(
    config["db"]["protocol"],
    config["db"]["username"],
    config["db"]["password"],
    config["db"]["hostname"],
    config["db"]["port"],
    config["db"]["database_name"]
))

Base = declarative_base(engine)

session = sessionmaker(bind=engine)()


# -----
# Add some generic methods to the base class
# pylint: disable=redefined-builtin

def single_lookup(f):
    """Decorator to add more information to `NoResultFound`s"""
    @wraps(f)
    def ret(cls, *args, **kwargs):
        try:
            return f(cls, *args, **kwargs)
        except (NoResultFound, MultipleResultsFound) as e:
            # Change the error message, leave everything else the same
            args_dict = dict(zip(inspect.getfullargspec(f).args[1:], args))
            new_msg = "{} such {}: {}".format(
                {NoResultFound: "No", MultipleResultsFound: "More than one"}[type(e)],
                cls.__name__,
                args_dict
            )
            e.args = (new_msg, ) + e.args[1:]
            raise

    return ret

def camel_to_snake(s):
    """Convert a camel-case string to snake-case"""
    return re.sub(r'(?<!^)(?=[A-Z])', '_', s).lower()

def all(cls, order_by=None):
    """Get all instances of this type.  Default ordering by id"""
    if not order_by:
        order_by = cls.id
    return session.query(cls).order_by(order_by).all()

def by_id(cls, id):
    """Lookup an item by its id"""
    return session.query(cls).filter_by(id=id).one()

def query(cls, *args, **kwargs):
    """Syntactic sugar"""
    if args:
        return session.query(cls).filter(
            cls.fundamental_level == cls.__mapper_args__["polymorphic_identity"],
            *args
        )
    else:
        return session.query(cls).filter_by(
            fundamental_level=cls.__mapper_args__["polymorphic_identity"],
            **kwargs
        )

def where(cls, *args, order_by=None, **kwargs):
    """Simulate a `where` clause in SQL"""
    if not order_by:
        order_by = cls.id
    if args:
        return session.query(cls).filter(*args).order_by(order_by).all()
    else:
        return session.query(cls).filter_by(*args, **kwargs).order_by(order_by).all()

def save(self):
    """Convenience method to save a model"""
    session.add(self)
    session.commit()

def url_path(cls, path=None):
    """The URL path that this model's API sits at"""
    base_path = "/{}".format(camel_to_snake(cls.__name__))
    if path is None:
        return base_path
    return "/".join([base_path, path])

def url_for(cls, item):
    """Get the appropriate link to a database item"""
    return "/".join([
        cls.url_path(),
        str(item.id)
    ])

Base.all = classmethod(all)
Base.by_id = classmethod(single_lookup(by_id))
Base.where = classmethod(where)
Base.query = classmethod(query)
Base.save = save
Base.url_path = classmethod(url_path)
Base.url_for = classmethod(url_for)
