"""
The Recipe model
"""

from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from .food import Food


# Read more: https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#many-to-many
# And: https://docs.sqlalchemy.org/en/13/orm/extensions/associationproxy.html
iterations_table = Table("iterations", Food.metadata,
    Column("base_recipe_id", Integer, ForeignKey("foods.id")),
    Column("derived_recipe_id", Integer, ForeignKey("foods.id"))
)


class Recipe(Food):
    """
    Represents a recipe.
    This is a food item that has `fundamental_level = 1`.
    """
    __mapper_args__ = {
        "polymorphic_identity": 1
    }

    base_recipe = relationship(
        "Recipe",
        secondary=iterations_table,
        primaryjoin="Recipe.id == iterations.c.derived_recipe_id",
        secondaryjoin="Recipe.id == iterations.c.base_recipe_id",
        backref="iterations",
        uselist=False
    )


    def __init__(self, *args, **kwargs):
        """Unlike a food, the default mass of a recipe should be 0"""
        if "mass" not in kwargs:
            kwargs["mass"] = 0
        super().__init__(*args, **kwargs)

    def add(self, ingredient):
        """Add an ingredient into this recipe"""
        if isinstance(ingredient, Food):
            ingredient = ingredient.times(1, units=-1)
        if ingredient.base_food in self.ingredients:
            raise ValueError(f"Food {ingredient} is already in this recipe ({self})!\
                \nIngredients include: {self.ingredients}"
            )

        for var in Food.additive_columns:
            current_val = getattr(self, var) or 0
            ingredient_val = getattr(ingredient.base_food, var) * ingredient.quantity
            setattr(self, var, current_val + ingredient_val)

        ingredient.list_position = len(self.ingredients)
        ingredient.recipe = self

    def add_or_update(self, ingredient):
        """
        Convenience method.  Adds the ingredient if it doesn't exist; otherwise increments
        the quantity.
        """
        if isinstance(ingredient, Food):
            ingredient = ingredient.times(1, units=-1)

        if ingredient.base_food in self:
            self.ingredients[ingredient.base_food_id].quantity += ingredient.quantity
            self.recalculate_total()
            return
        self.add(ingredient)

    def delete_item(self, ingredient):
        """
        Remove an item from this recipe.  Accepts an Ingredient, Food, or int (food id).
        Will raise a KeyError (`IngredientList#pop`) if the ingredient isn't in this recipe.
        """
        if hasattr(ingredient, "base_food_id"):
            ingredient = ingredient.base_food_id

        self.ingredients.pop(ingredient)
        self.recalculate_total()

    def recalculate_total(self):
        """Re-calculate the nutrition of this recipe, based on its current ingredient list"""
        for var in Food.additive_columns:
            total = 0
            for i in self.ingredients:
                total += getattr(i.base_food, var) * i.quantity
            setattr(self, var, total)

    @classmethod
    def recalculate_all_containing(cls, food):
        """
        Given an ingredient, recalculate the totals of all the recipes it belongs to.
        To be used as a callback when a food is changed
        """
        try:
            Ingredient
        except NameError:
            from . import Ingredient  # pylint: disable=import-outside-toplevel
        for i in Ingredient.where(Ingredient.base_food == food):
            i.recipe.recalculate_total()
            cls.recalculate_all_containing(i.recipe)

    def to_dict(self):
        return {
            "ingredients": [i.to_dict() for i in self.ingredients],
            "self": super().to_dict()
        }

    @classmethod
    def available_ingredients_as_json(cls):
        """
        Get all food items that could be used as ingredients in a new recipe, and
        return them as a JSON object.
        Daily foods cannot be added as ingredients.
        """
        return [i.to_dict() for i in Food.x_all()]

    @classmethod
    def from_json(cls, json):
        """Parse a recipe from a json object"""
        ret = cls(json['title'])
        for i in json['ingredients']:
            food = Food.by_name(i['name'])
            amount, units = Recipe.parse_amount(i['amount'])
            if units == 1:
                ret.add(food.grams(amount))
            else:
                ret.add(food.times(amount))
        return ret

    @staticmethod
    def parse_amount(amount):
        """Parse an amount from an amount string"""
        if amount[-1] == "g":
            return float(amount[:-1]), 1
        else:
            return float(amount), 0

    def update(self, patch):
        """
        Apply a list of changes to this recipe's ingredient list.
        Recalculate the total following the changes.
        TODO: Should do schema validation here
        """
        if "ingredient_changes" in patch:
            for i, v in patch["ingredient_changes"].items():
                if "created" in v:
                    self.add(Food.by_id(int(i)))

                ingredient = self.ingredients[int(i)]

                if "quantity" in v:
                    ingredient.quantity = v["quantity"]

                if "units" in v:
                    ingredient.units = v["units"]

                if "delete" in v:
                    self.delete_item(int(i))

                if "reorder" in v:
                    ingredient.list_position = v["reorder"]

            self.recalculate_total()

        if "change_title" in patch:
            self.name = patch["change_title"]


    def splat(self):
        """Recursively breaks this recipe down into its total fundamental ingredients"""
        ret = Recipe(f"{self.name} (splat)")

        for i1 in self.ingredients:
            base_food = i1.base_food
            if isinstance(base_food, Recipe):
                for i2 in base_food.splat().ingredients:
                    ret.add_or_update(i2.times(i1.quantity))
            else:
                ret.add_or_update(i1.times(1))

        ret.recalculate_total()
        return ret

    def iterate(self):
        """Create a new iteration of this recipe"""
        ret = Recipe(self.name)
        for i in self.ingredients:
            ret.add(i.times(1))
        ret.base_recipe = self
        return ret

    def __contains__(self, item):
        return item in self.ingredients

    @classmethod
    def next_in_sequence(cls, item, p):
        """Returns the next item, sorted by table column `p`"""
        return cls.query(p > getattr(item, p.name)).order_by(p).limit(1).one_or_none()

    @classmethod
    def prev_in_sequence(cls, item, p):
        """Returns the previous item, sorted by table column `p`"""
        return cls.query(p < getattr(item, p.name)).order_by(p.desc()).limit(1).one_or_none()

    @classmethod
    def nav_for(cls, r):
        """Returns JSON with links to "next" and "prev" recipes"""
        next_recipe = cls.next_in_sequence(r, cls.id)
        prev_recipe = cls.prev_in_sequence(r, cls.id)
        return {"nav": {
            "next": cls.url_for(next_recipe) if next_recipe else None,
            "prev": cls.url_for(prev_recipe) if prev_recipe else None,
        }}
