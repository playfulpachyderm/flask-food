"""
The Food model
"""
import copy

from sqlalchemy import Column, String, Integer, Float, Date

from ._base import Base, session, single_lookup

def format_number(n):
    """
    Convert numbers to strings in a readable way.
    Whole numbers display as integers.
    Floats are rounded to a maximum of 2 decimals.
    """
    if n == int(n):
        return f"{int(n)}"

    s = f"{n:.2f}".split(".")
    return ".".join([s[0], s[1].rstrip("0")])


class Food(Base):
    """Represents a food item"""
    __tablename__ = "foods"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    cals = Column(Float, default=0)
    carbs = Column(Float, default=0)
    protein = Column(Float, default=0)
    fat = Column(Float, default=0)
    sugar = Column(Float, default=0)
    alcohol = Column(Float, default=0)
    water = Column(Float, default=0)

    potassium = Column(Float, default=0)
    sodium = Column(Float, default=0)
    calcium = Column(Float, default=0)
    magnesium = Column(Float, default=0)
    phosphorus = Column(Float, default=0)
    iron = Column(Float, default=0)
    zinc = Column(Float, default=0)

    mass = Column(Float, default=100)
    density = Column(Float, default=1)
    price = Column(Float, default=0)
    cook_ratio = Column(Float, default=1)

    fundamental_level = Column(Integer, default=0)
    date = Column(Date)

    __mapper_args__ = {
        "polymorphic_on": fundamental_level,
        "polymorphic_identity": 0
    }

    additive_columns = [
        "cals", "carbs", "protein", "fat", "sugar", "alcohol", "water", "potassium", "sodium",
        "calcium", "magnesium", "phosphorus", "iron", "zinc", "mass", "price"
    ]

    def __init__(self, name, cals=0, carbs=0, protein=0, fat=0, sugar=0, alcohol=0, water=0,
                 potassium=0, sodium=0, calcium=0, magnesium=0, phosphorus=0, iron=0, zinc=0,
                 mass=100, density=1, price=0, cook_ratio=1):
        self.cals = cals
        self.carbs = carbs
        self.protein = protein
        self.fat = fat
        self.sugar = sugar
        self.alcohol = alcohol
        self.water = water

        self.potassium = potassium
        self.sodium = sodium
        self.calcium = calcium
        self.magnesium = magnesium
        self.phosphorus = phosphorus
        self.iron = iron
        self.zinc = zinc

        self.mass = mass
        self.density = density
        self.price = price

        self.name = name
        self.cook_ratio = cook_ratio

    def __str__(self):
        """
        Example: `bread(1)<289, 56, 12, 2, 3>[m=100]`
        """
        front = f"{self.name}({self.id})<"
        middle = ", ".join(format_number(x) for x in [
            self.cals, self.carbs, self.protein, self.fat, self.sugar
        ])
        back = f">[m={format_number(self.mass)}]"
        return front + middle + back

    def __repr__(self):
        return str(self)

    def save(self):
        """Update all recipes this food is in"""
        if self.id:
            Recipe.recalculate_all_containing(self)
        super().save()

    def __copy__(self):
        """
        Create a copy of this food.  It should not copy the id though!
        """
        ret = Food(self.name)
        for column in self.__table__.columns:
            if column.name == "id":
                continue
            setattr(ret, column.name, getattr(self, column.name))
        return ret

    @classmethod
    def x_all(cls, *args, **kwargs):
        """Class-exclusive all"""
        return cls.where(
            cls.fundamental_level == cls.__mapper_args__["polymorphic_identity"],
            *args,
            **kwargs
        )

    @classmethod
    @single_lookup
    def by_name(cls, name):
        """Lookup a food by its name"""
        return session.query(cls).filter_by(name=name).one()

    def times(self, mult, units=0, name=None):
        """
        Create an ingredient from this food.
        units:
        - 0: number
        - 1: grams
        """
        return Ingredient(self, quantity=mult, units=units, name=name)

    def grams(self, g):
        """
        Create an ingredient from this food.
        units:
        - 0: number
        - 1: grams
        """
        return self.times(g / self.mass, units=1)

    def to_dict(self):
        """Get this food item as a dictionary of attributes."""
        return {column.name: getattr(self, column.name) for column in self.__table__.columns}

    def sale(self):
        """Temporary implementation"""
        return self
    def set_price(self, price):
        """Create a new version of this food with a different price"""
        ret = copy.copy(self)
        ret.price = price
        return ret


from .ingredient import Ingredient  # pylint: disable=wrong-import-position
from .recipe import Recipe          # pylint: disable=wrong-import-position
