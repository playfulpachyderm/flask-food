"""
Helper methods for authentication and JWT token handling
"""

import hashlib
from datetime import datetime

import jwt
from jwt.exceptions import ExpiredSignatureError, DecodeError

from application_factory import config

def authenticate_by_password(password):
    """Dummy implementation"""
    salted_password = password + config["app_login"]["salt"]
    h = hashlib.sha256(salted_password.encode("latin-1"))
    return h.hexdigest() == config["app_login"]["hex_digest"]

def create_token(lifetime=86400):
    """Create a token which, by default, expires 1 day from now"""
    data = {
        'exp': int(datetime.now().timestamp()) + lifetime,
        "logged_in": True,
    }
    return jwt.encode(data, config["secret_key"])

def validate_token(token):
    """
    Checks if a token is valid.
    If it is missing, or invalid, generates an appropriate error message.
    Returns: 2-tuple => (bool, str)
        - bool: whether token is valid
        - str: error message, if applicable
    """
    if not token:
        return False, "Not logged in"
    try:
        data = jwt.decode(token, config["secret_key"], algorithms=["HS256"])
    except ExpiredSignatureError:
        return False, "Session expired"
    except DecodeError as e:
        return False, f"Token is invalid ({str(e)})"

    return data, None
