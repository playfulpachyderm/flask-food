"""
A singleton class
"""

from ._base import session
from .food import Food
from .recipe import Recipe

class Pantry(Recipe):
    """The pantry"""
    __mapper_args__ = {
        "polymorphic_identity": 5
    }

    def __init__(self):
        raise TypeError("You should not instantiate a new Pantry!")

    @classmethod
    def instance(cls):
        """Get the singleton"""
        return session.query(cls).one()

    def eat(self, ingredient):
        """Remove an ingredient from the pantry (it has been eaten)"""
        if isinstance(ingredient, Food):
            ingredient = ingredient.times(1, units=-1)

        if ingredient.base_food not in self:
            raise ValueError(f"Food {ingredient} isn't in the pantry")

        self.ingredients[ingredient.base_food].quantity -= ingredient.quantity
        if self.ingredients[ingredient.base_food].quantity == 0:
            self.ingredients.pop(ingredient.base_food)

        self.recalculate_total()
