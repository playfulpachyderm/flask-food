"""
Test routes under `/recipes`
"""

import json

import bs4
from pytest import approx

from controller import app

from models import Food, Recipe, session
from models.authentication import create_token

from helpers import redirected_to, friendly_forward

client = app.test_client()
client.set_cookie("", "jwt", create_token())


def test_recipes_index():
    """Recipes index page should list recipes"""
    page = client.get(Recipe.url_path())
    assert page.status_code == 200

    dom = bs4.BeautifulSoup(page.data.decode(), "html.parser")
    url = Recipe.url_for(Recipe.x_all()[0])
    assert dom.select("a[href='{}']".format(url))

    show_all_iters_form = dom.select("form[action='/recipe']")
    assert show_all_iters_form
    assert show_all_iters_form[0].select("input[type='hidden'][name='all_iterations']")

def test_recipes_index_all_iters():
    """Should have button to hide iterations"""
    page = client.get(Recipe.url_path(), query_string={"all_iterations": True})
    assert page.status_code == 200

    dom = bs4.BeautifulSoup(page.data.decode(), "html.parser")

    show_all_iters_form = dom.select("form[action='/recipe']")
    assert show_all_iters_form
    assert not show_all_iters_form[0].select("input[type='hidden'][name='all_iterations']")


def test_show_recipe():
    """Should show a recipe"""
    recipe = Recipe.x_all()[0]
    page = client.get(Recipe.url_for(recipe))
    assert page.status_code == 200

def test_recipe_404():
    """Nonexistent recipes should 404"""
    url = "/".join([Recipe.url_path(), "0"])
    page = client.get(url)
    assert page.status_code == 404

def test_recipes_require_login():
    """
    Should require an authentication token to view pages.
    This applies to all pages, but take a sample using the root page.
    """
    client_unauthed = app.test_client()
    page = client_unauthed.get(Recipe.url_path())
    assert redirected_to(page) == "/login"
    assert friendly_forward(page) == Recipe.url_path()

def test_recipe_splat():
    """Should show a splat page"""
    recipe = Recipe.x_all()[0]
    page = client.get(Recipe.url_for(recipe) + "/splat")
    assert page.status_code == 200

def test_recipe_splat_404():
    """Nonexistent recipe splats should 404"""
    url = "/".join([Recipe.url_path(), "0", "splat"])
    page = client.get(url)
    assert page.status_code == 404

def test_new_recipe_form():
    """Should display the new recipe form"""
    page = client.get(Recipe.url_path("new"))
    assert page.status_code == 200

def test_new_recipe_form_submit():
    """Should create a new recipe and redirect to it"""
    recipe_count = len(Recipe.x_all())
    potatoes = Food.by_name("potatoes")

    data = {
        "change_title": "potatoes w/ cheese",
        "ingredient_changes": {
            str(potatoes.id): {
                "created": True,
                "quantity": 3,
                "units": 0,
            },
        },
    }
    page = client.post(
        Recipe.url_path("new"),
        data={"data": json.dumps(data)},
        content_type='application/x-www-form-urlencoded'
    )

    assert len(Recipe.x_all()) == recipe_count + 1

    new_recipe = Recipe.x_all()[-1]
    assert new_recipe.name == "potatoes w/ cheese"
    assert redirected_to(page) == Recipe.url_for(Recipe.x_all()[-1])

    assert potatoes in new_recipe
    assert new_recipe.ingredients[potatoes].quantity == 3
    assert new_recipe.ingredients[potatoes].units == 0


def test_iterate_recipe_page():
    """Should display an iteration page"""
    base_recipe = Recipe.x_all()[0]
    page = client.get(Recipe.url_for(base_recipe) + "/iterate")
    assert page.status_code == 200

def test_iterate_recipe_save():
    """Should save an iteration"""
    base_recipe = Recipe.x_all()[0]
    i1 = base_recipe.ingredients.at_position(0)
    i2 = base_recipe.ingredients.at_position(1)
    iterations_count = len(base_recipe.iterations)

    # Switch the first two ingredients
    data = {
        "ingredient_changes": {
            i1.base_food_id: {"reorder": 1},
            i2.base_food_id: {"reorder": 0, "quantity": i2.quantity * 2},
        }
    }

    page = client.post(
        Recipe.url_for(base_recipe) + "/iterate",
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 302
    new_recipe_id = redirected_to(page).split("/")[-1]
    new_iteration = Recipe.by_id(new_recipe_id)
    assert new_iteration.ingredients.at_position(0).base_food_id == i2.base_food_id
    assert new_iteration.ingredients.at_position(1).base_food_id == i1.base_food_id
    assert new_iteration.ingredients.at_position(0).quantity == i2.quantity * 2

    # Refresh reference
    session.refresh(base_recipe)
    assert len(base_recipe.iterations) == iterations_count + 1
    assert new_iteration.base_recipe == base_recipe


def test_update_recipe():
    """Should modify the ingredients in a recipe and save it"""
    recipe = Recipe.x_all()[0]
    ingredient = recipe.ingredients.at_position(0)
    new_quantity = ingredient.quantity / 2
    target_cals = recipe.cals - ingredient.base_food.cals * new_quantity
    data = {
        "ingredient_changes": {
            str(ingredient.base_food_id): {
                "quantity": new_quantity
            }
        }
    }

    page = client.post(
        Recipe.url_for(recipe),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 200

    # Refresh reference
    session.refresh(recipe)
    session.refresh(ingredient)
    assert ingredient.quantity == approx(new_quantity)
    assert recipe.cals == approx(target_cals)


def test_update_recipe_with_deletion():
    """Should modify the ingredients in a recipe and save it"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)
    shake.save()

    data = {
        "ingredient_changes": {
            oats.id: {
                "delete": True
            }
        }
    }

    page = client.post(
        Recipe.url_for(shake),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 200

    # Refresh reference
    session.refresh(shake)
    assert oats not in shake


def test_view_iterations():
    """Should show an iterations listing"""
    recipe = Recipe.x_all()[0]
    page = client.get(Recipe.url_for(recipe) + "/iterations")
    assert page.status_code == 200


def test_get_available_ingredients():
    """Should get some json"""
    data = client.get(Recipe.url_path("get_available_ingredients"))
    assert data.status_code == 200
    assert data.headers["Content-Type"] == "application/json"
