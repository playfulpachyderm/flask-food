"""
Test the main page
"""

import json

import bs4

from controller import app, static_url_for
from models import Food, Recipe, DailyLog

client = app.test_client()


def test_index_page():
    """
    Home page should load
    Should contain the header bar links
    """
    page = client.get("/")
    assert page.status_code == 200
    dom = bs4.BeautifulSoup(page.data.decode(), "html.parser")

    for link in ["/", Recipe.url_path("new"), DailyLog.url_path("new"), Food.url_path()]:
        assert dom.select("a[href='{}']".format(link))


def test_explicit_404():
    """Should get a custom 404 page"""
    page = client.get("/explicit_404")
    assert page.status_code == 404
    body = page.data.decode()
    assert "Not Found" in body
    assert "specifically designed not to exist" in body

def test_implicit_404():
    """Should get a slightly less custom 404 page"""
    page = client.get("/awfjawlefkjalwkeflwek")
    assert page.status_code == 404
    body = page.data.decode()
    assert "Not Found" in body

def test_explicit_500():
    """Should get a default 500 page"""
    page = client.get("/explicit_500")
    assert page.status_code == 500
    body = page.data.decode()
    assert "Internal Server Error" in body

def test_implicit_500():
    """Should get a 500 page with debug info"""
    data = {"mlwewerk": 12}
    page = client.get("/implicit_500", data=data, content_type='application/x-www-form-urlencoded')
    assert page.status_code == 500
    body = page.data.decode()
    assert "<pre>" in body
    assert "mlwewerk" in body

def test_implicit_500_no_debug():
    """Debug info should not be visible when not in debug mode"""
    data = {"mlwewerk": 12}
    save_config = app.config["DEBUG"]
    app.config["DEBUG"] = False
    try:
        page = client.get("/implicit_500", data=json.dumps(data), content_type='application/json')
        assert page.status_code == 500
        body = page.data.decode()
        assert "<pre>" not in body
        assert "mlwewerk" not in body
    finally:
        app.config["DEBUG"] = save_config

def test_static_url_helper():
    """Should produce a static url based on the application config"""
    assert static_url_for("/potatoes") == "test://epicserver.com/potatoes"
    assert static_url_for("/js/script.js") == "test://epicserver.com/js/script.js"
