"""
Test food pages
"""

import bs4

from controller import app
from models import Food
from models.authentication import create_token

from helpers import redirected_to, friendly_forward


client = app.test_client()
client.set_cookie("", "jwt", create_token())


def test_food_index():
    """Should list foods"""
    page = client.get("/food")
    assert page.status_code == 200

    dom = bs4.BeautifulSoup(page.data.decode(), "html.parser")
    url = Food.url_for(Food.x_all()[0])
    assert dom.select("a[href='{}']".format(url))

def test_show_food():
    """Should show a food"""
    page = client.get("/food/1")
    assert page.status_code == 200

def test_show_food_404():
    """Nonexistent foods should 404"""
    page = client.get("/food/-1")
    assert page.status_code == 404

def test_recipes_require_login():
    """
    Should require an authentication token to view pages.
    This applies to all pages, but take a sample using the root page.
    """
    client_unauthed = app.test_client()
    page = client_unauthed.get(Food.url_path())
    assert redirected_to(page) == "/login"
    assert friendly_forward(page) == Food.url_path()
