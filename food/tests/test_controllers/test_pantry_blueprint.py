"""
Test routes under `/recipes`
"""

import json

from pytest import approx

from models import Pantry, Food
from models.authentication import create_token

from controller import app
from helpers import redirected_to, friendly_forward

client = app.test_client()
client.set_cookie("", "jwt", create_token())


def test_pantry_requires_login():
    """Pantry pages should be protected by require-login"""
    client_unauthed = app.test_client()
    page = client_unauthed.get(Pantry.url_path())
    assert redirected_to(page) == "/login"
    assert friendly_forward(page) == Pantry.url_path()


def test_show_pantry():
    """Should show a recipe"""
    page = client.get(Pantry.url_path())
    assert page.status_code == 200


def test_update_pantry():
    """Should modify the ingredients in the pantry and save it"""
    pantry = Pantry.instance()
    ingredient = pantry.ingredients.at_position(0)
    new_quantity = ingredient.quantity + 0.2
    target_cals = pantry.cals + ingredient.base_food.cals * 0.2

    data = {
        "ingredient_changes": {
            str(ingredient.base_food.id): {
                "quantity": new_quantity,
                "units": 0,
            }
        }
    }
    page = client.post(
        Pantry.url_path(),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 200
    assert pantry.cals == approx(target_cals)


def test_update_pantry_with_deletion():
    """Should remove an ingredient from the pantry"""
    pantry = Pantry.instance()
    oats = Food.by_name("oats")
    pantry.add(oats.times(20))
    pantry.save()

    assert oats in pantry

    data = {
        "ingredient_changes": {
            oats.id: {
                "delete": True
            }
        }
    }
    page = client.post(
        Pantry.url_path(),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 200
    assert oats not in pantry


# JSON API tests
# --------------

def test_get_as_json():
    """Should get a JSON response"""
    page = client.get(Pantry.url_path("as_json"))
    assert page.status_code == 200
