"""
Test the daily logs pages
"""

import json
from datetime import date

import bs4
from pytest import approx, raises
from sqlalchemy.orm.exc import NoResultFound

from controller import app
from models import DailyLog, session, Food
from models.authentication import create_token

from helpers import redirected_to, friendly_forward

client = app.test_client()
client.set_cookie("", "jwt", create_token())


def test_daily_log_index():
    """Should list daily logs"""
    page = client.get(DailyLog.url_path())
    assert page.status_code == 200

    dom = bs4.BeautifulSoup(page.data.decode(), "html.parser")
    url = DailyLog.url_for(DailyLog.all()[0])
    assert dom.select("a[href='{}']".format(url))

def test_show_daily_log():
    """Should show a daily log"""
    daily_log = DailyLog.all()[0]
    page = client.get(DailyLog.url_for(daily_log))
    assert page.status_code == 200

def test_show_daily_log_404():
    """Nonexistent day should 404"""
    page = client.get("/daily_log/1000-07-19")
    assert page.status_code == 404

def test_daily_logs_require_login():
    """
    Should require an authentication token to view pages.
    This applies to all pages, but take a sample using the root page.
    """
    client_unauthed = app.test_client()
    page = client_unauthed.get(DailyLog.url_path())
    assert redirected_to(page) == "/login"
    assert friendly_forward(page) == DailyLog.url_path()

def test_daily_log_splat():
    """Should show a daily log page"""
    daily_log = DailyLog.all()[0]
    page = client.get(DailyLog.url_for(daily_log) + "/splat")
    assert page.status_code == 200


def test_show_daily_log_non_date_slug_400():
    """Requesting a daily log by integer should 400 (NOT 500)"""
    page = client.get("/daily_log/1")
    assert page.status_code == 400

def test_new_daily_log_form():
    """Should display the new daily log form"""
    page = client.get(DailyLog.url_path("new"))
    assert page.status_code == 200

def test_new_daily_log_form_submit():
    """Should create a new daily log and redirect to it"""
    daily_log_count = len(DailyLog.all())
    potatoes = Food.by_name("potatoes")

    data = {
        "ingredient_changes": {
            str(potatoes.id): {
                "created": True,
                "quantity": 3,
                "units": 0,
            },
        },
        "change_title": "2000-10-10"
    }

    page = client.post(
        DailyLog.url_path("new"),
        data={"data": json.dumps(data)},
        content_type='application/x-www-form-urlencoded'
    )
    assert page.status_code == 302

    assert len(DailyLog.all()) == daily_log_count + 1

    new_daily_log = DailyLog.x_all()[-1]
    assert new_daily_log.date == date(2000, 10, 10)
    assert redirected_to(page) == DailyLog.url_for(new_daily_log)

    assert potatoes in new_daily_log
    assert new_daily_log.ingredients[potatoes].quantity == 3
    assert new_daily_log.ingredients[potatoes].units == 0

    # Cleanup

    session.delete(DailyLog.all()[-1])
    session.commit()

def test_new_daily_log_invalid_date():
    """Creating a new daily log with a bad date should 422"""
    data = {"change_title": "awefwefe"}

    page = client.post(
        DailyLog.url_path("new"),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )
    assert page.status_code == 422


def test_update_daily_log():
    """Should modify the ingredients in a recipe and save it"""
    daily_log = DailyLog.all()[-1]
    changed_ingredient = daily_log.ingredients.at_position(0)
    new_quantity = changed_ingredient.quantity / 2

    target_cals = (
        daily_log.cals
      - changed_ingredient.base_food.cals * new_quantity
    )

    target_date = DailyLog.next_available_date()

    data = {
        "ingredient_changes": {
            str(changed_ingredient.base_food_id): {
                "quantity": new_quantity
            },
        },
        "change_title": DailyLog.to_slug(target_date)
    }

    page = client.post(
        DailyLog.url_for(daily_log),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 302
    assert redirected_to(page) == DailyLog.url_path(DailyLog.to_slug(target_date))

    # Refresh reference
    session.refresh(daily_log)
    session.refresh(changed_ingredient)
    assert changed_ingredient.quantity == approx(new_quantity)
    assert daily_log.cals == approx(target_cals)
    assert daily_log.date == target_date

def test_update_invalid_date():
    """Updating a daily log with a bad date should 422"""
    daily_log = DailyLog.all()[-1]
    data = {"change_title": "awefwefe"}

    page = client.post(
        DailyLog.url_for(daily_log),
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )
    assert page.status_code == 422

def test_iterate_daily_log_page():
    """Should display an iteration page"""
    original_daily_log = DailyLog.all()[0]
    page = client.get(DailyLog.url_for(original_daily_log) + "/iterate")
    assert page.status_code == 200

def test_iterate_daily_log_save():
    """Should save an iteration"""
    original_daily_log = DailyLog.all()[0]
    i1 = original_daily_log.ingredients.at_position(0)
    i2 = original_daily_log.ingredients.at_position(1)

    target_date_slug = DailyLog.to_slug(DailyLog.next_available_date())

    # Switch the first two ingredients
    data = {
        "ingredient_changes": {
            i1.base_food_id: {"reorder": 1},
            i2.base_food_id: {"reorder": 0, "quantity": i2.quantity * 2},
        },
        "change_title": target_date_slug,
    }

    page = client.post(
        DailyLog.url_for(original_daily_log) + "/iterate",
        data={"data": json.dumps(data)},
        content_type="application/x-www-form-urlencoded"
    )

    assert page.status_code == 302
    assert redirected_to(page) == DailyLog.url_path(target_date_slug)

    new_iteration = DailyLog.by_slug(target_date_slug)
    assert new_iteration.ingredients.at_position(0).base_food_id == i2.base_food_id
    assert new_iteration.ingredients.at_position(1).base_food_id == i1.base_food_id
    assert new_iteration.ingredients.at_position(0).quantity == i2.quantity * 2

def test_today_link():
    """Should redirect to either today's daily log or the new daily log page"""
    with raises(NoResultFound):
        DailyLog.today()

    page = client.get(DailyLog.url_path("today"))
    assert redirected_to(page) == DailyLog.url_path("new")

    DailyLog(date.today()).save()

    # Check that it worked
    today_log = DailyLog.today()

    page = client.get(DailyLog.url_path("today"))
    assert redirected_to(page) == DailyLog.url_for(today_log)

    # Cleanup
    session.delete(today_log)
    session.commit()
