"""
Helpers pertaining to controller testing and HTTP operations
"""

from urllib.parse import urlparse, parse_qs


def friendly_forward(page):
    """Assert that friendly forwarding was recorded"""
    assert page.status_code == 302, page.status_code
    query_string = urlparse(page.headers["Location"]).query
    return_to = urlparse(parse_qs(query_string)["return_to"][0]).path
    return return_to

def redirected_to(page, path_only=True):
    """Assert status was 302 and redirection is to the expected location"""
    assert page.status_code >= 300 and page.status_code < 400
    assert "Location" in page.headers
    parsed_url = urlparse(page.headers["Location"])
    if path_only or not parsed_url.query:
        return parsed_url.path
    return f"{parsed_url.path}?{parsed_url.query}"
