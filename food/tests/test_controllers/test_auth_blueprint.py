"""
Test login and auth-related pages
"""

from werkzeug.http import parse_cookie
import bs4

from controller import app
from models.authentication import create_token

from helpers import redirected_to

client = app.test_client()

client_authed = app.test_client()
client_authed.set_cookie("", "jwt", create_token())

def test_login_page():
    """Should get the login page"""
    page = client.get("/login")
    assert page.status_code == 200

def test_login_page_when_already_logged_in():
    """Should get the login page"""
    page = client_authed.get("/login")
    assert redirected_to(page) == "/"

def test_login_post():
    """Should set a cookie and redirect to a given page"""
    return_to = "/blahblahblah?a=1"
    data = {"return_to": return_to, "password": "password"}
    page = client.post("/login",
        data=data,
        content_type="application/x-www-form-urlencoded"
    )
    assert redirected_to(page, path_only=False) == return_to
    assert "ey" in parse_cookie(page.headers["Set-Cookie"])["jwt"]

def test_login_post_failure():
    """
    Should return to the login page and preserve the friendly forward.

    The friendly-forward behavior is a bit complex, because on an unauthed page access, the FF is
    passed by query string; on an unsuccessful login, it's preserved in the login form hidden
    input field.
    """
    return_to = "/blahblahblah?a=1"
    data = {"return_to": return_to, "password": "wronganswer"}
    page = client.post("/login",
        data=data,
        content_type="application/x-www-form-urlencoded"
    )
    assert page.status_code == 401
    assert "Set-Cookie" not in page.headers

    html = page.data.decode()
    assert "Incorrect password" in html
    dom = bs4.BeautifulSoup(html, "html.parser")
    assert dom.find("input", attrs={"name": "return_to"})["value"] == return_to

def test_logout_page():
    """Should delete the cookie"""
    page = client.get("login/logout")
    assert redirected_to(page) == "/login"
    assert parse_cookie(page.headers["Set-Cookie"])["jwt"] == ""

def test_auth_check_endpoint_with_credentials():
    """Should echo back the auth cookie"""
    page = client_authed.get("/login/test")
    assert page.status_code == 200
    assert page.json["logged_in"]

def test_auth_check_endpoint_without_credentials():
    """Should return an error"""
    page = client.get("/login/test")
    assert page.status_code == 401
    assert page.json["ERROR"]
