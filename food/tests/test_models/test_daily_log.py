"""
Tests of and related to the DailyLog model.
"""

from datetime import date, timedelta

from models import session, Food, Recipe, DailyLog

def test_find_by_slug():
    """Should be able to find by slug"""
    x = DailyLog.by_slug("2018-11-10")
    assert x.date == date(2018, 11, 10)
    assert len(x.ingredients) > 0

def test_by_date():
    """Should get a daily log by date"""
    day = date(2018, 11, 10)
    x = DailyLog.by_date(day)
    assert x.date == day

def test_next_available_date():
    """Should get a date 1 day after the last available daily log"""
    next_date = DailyLog.next_available_date()
    assert len(DailyLog.where(DailyLog.date >= next_date)) == 0
    assert len(DailyLog.where(DailyLog.date >= (next_date - timedelta(days=1)))) != 0

def test_nav_for():
    """Should get links to next and prev available daily logs"""
    x = DailyLog.by_slug("2018-11-11")
    assert DailyLog.nav_for(x) == {"nav": {
        "next": "/daily_log/2018-11-12",
        "prev": "/daily_log/2018-11-10"
    }}

def test_nav_for_first_item():
    """Should give `None` for "prev" if this is the first existing daily log"""
    x = DailyLog.by_slug("2018-11-10")
    assert DailyLog.nav_for(x) == {"nav": {"next": "/daily_log/2018-11-11", "prev": None}}

def test_nav_for_last_item():
    """Should give `None` for "next" if this is the last existing daily log"""
    x = DailyLog.x_all()[-1]
    second_last = DailyLog.prev_in_sequence(x, DailyLog.date)
    assert DailyLog.nav_for(x) == {"nav": {"next": None, "prev": DailyLog.url_for(second_last)}}


def test_daily_log_creation():
    """Creating a daily log should name it appropriately"""
    day = date(2016, 11, 9)
    x = DailyLog(day)

    assert x.date == day
    assert x.name == "Nov 9, 2016"

    x.save()
    assert x.id is not None

    # cleanup
    session.delete(x)
    session.commit()

def test_daily_log_slug():
    """Slug should be the date in slug format"""
    slug = "2018-11-10"
    x = DailyLog.by_slug(slug)
    assert x.get_slug() == slug

def test_url_for():
    """Should provide a URL path using the slug, not id"""
    slug = "2018-11-10"
    x = DailyLog.by_slug(slug)

    assert Food.url_for(x) == "/food/" + str(x.id)
    assert Recipe.url_for(x) == "/recipe/" + str(x.id)
    assert DailyLog.url_for(x) == "/daily_log/" + slug

def test_daily_log_from_json():
    """Should correctly parse a daily log from a dictionary"""
    oats = Food.by_name("oats")
    eggs = Food.by_name("eggs")

    test_json = {"date": "1000-03-03", "ingredients": []}

    ingredients = [oats.grams(100), eggs.times(50)]
    for i in ingredients:
        test_json["ingredients"].append({"amount": i.str_amount(), "name": i.base_food.name})

    daily_log = DailyLog.from_json(test_json)
    assert daily_log.get_slug() == "1000-03-03"
    assert len(daily_log.ingredients) == 2
    assert daily_log.ingredients.at_position(0).base_food.name == "oats"

def test_date_range():
    """Should yield a list of daily logs between the specified dates"""
    daily_logs = DailyLog.date_range(date(2018, 11, 11), date(2018, 11, 12))
    assert len(daily_logs) == 2
    assert daily_logs[0].date == date(2018, 11, 11)
    assert daily_logs[1].date == date(2018, 11, 12)

def test_splat_range():
    """Should aggregate and splat all the daily logs within the range"""
    splat = DailyLog.splat_range(date(2018, 11, 11), date(2018, 11, 12))
    assert len(splat.ingredients) == 23

    daily_logs = DailyLog.date_range(date(2018, 11, 11), date(2018, 11, 12))
    splat1 = daily_logs[0].splat()
    splat2 = daily_logs[1].splat()

    def quantity_or_zero(log, base_food):
        if base_food in log:
            return log.ingredients[base_food].quantity
        else:
            return 0

    for i in splat.ingredients:
        assert i.quantity == (
            quantity_or_zero(splat1, i.base_food) + quantity_or_zero(splat2, i.base_food)
        )

def test_update_date_using_name_format():
    """Should accept a new date in NAME_FORMAT"""
    slug = "2018-11-10"
    x = DailyLog.by_slug(slug)

    assert x.name == "Nov 10, 2018"
    assert x.date == date(2018, 11, 10)

    x.update_date("Nov 10, 1018")

    assert x.name == "Nov 10, 1018"
    assert x.date == date(1018, 11, 10)

    # Reset the daily log object
    # (not sure why this is necessary, it seems to save itself somehow otherwise)
    session.rollback()

def test_update_date_using_slug_format():
    """Should accept a new date in SLUG_FORMAT"""
    slug = "2018-11-10"
    x = DailyLog.by_slug(slug)

    assert x.name == "Nov 10, 2018"
    assert x.date == date(2018, 11, 10)

    x.update_date("1018-05-3")

    assert x.name == "May 3, 1018"
    assert x.date == date(1018, 5, 3)

    # Reset the daily log object
    # (not sure why this is necessary, it seems to save itself somehow otherwise)
    session.rollback()

def test_update_with_patch():
    """Updating a daily log should work, and should update the date correctly"""
    x = DailyLog(date(1950, 1, 1))

    oats = Food.by_name("oats")
    whey = Food.by_name("whey")

    x.add(oats.times(1.70))
    x.add(whey.times(0.25))

    new_date = date(1951, 12, 5)

    x.update({
        "ingredient_changes": {
            str(oats.id): {
                "quantity": 2,
                "units": 1
            }
        },
        "change_title": DailyLog.to_name(new_date),
    })

    assert x.ingredients[oats].quantity == 2
    assert x.ingredients[oats].units == 1
    assert x.name == DailyLog.to_name(new_date)
    assert x.date == new_date

    new_date2 = date(1952, 11, 10)

    x.update({"change_title": DailyLog.to_slug(new_date2)})
    assert x.name == DailyLog.to_name(new_date2)
    assert x.date == new_date2


# Iterate
# -------

def test_iterate_daily_log():
    """
    Should copy the daily log.  All nutritional parameters should be the same (except id).
    Date should be advanced to the next available day.
    """
    day = date(2016, 11, 9)
    x = DailyLog(day)

    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        x.add(i)

    new_day = x.iterate()

    assert new_day.name != x.name
    assert oats in new_day
    assert new_day.ingredients[oats].quantity == 1.70
    assert whey in new_day
    assert pb in new_day

    assert new_day.cals == new_day.cals
    assert new_day.carbs == new_day.carbs
