"""
Tests of and related to the Pantry model.
"""


import pytest

from models import Food, Pantry

def test_cant_make_a_new_pantry():
    """Trying to create a new Pantry should cause an error"""
    with pytest.raises(TypeError):
        Pantry()

def test_get_instance():
    """Should get the pantry"""
    pantry = Pantry.instance()
    assert isinstance(pantry, Pantry)

def test_eat_from_pantry():
    """Eating a food should decrease its quantity in the pantry, or remove it if it's all gone"""
    pantry = Pantry.instance()
    oats = Food.by_name("oats")
    pantry.add(oats.grams(150))

    assert pantry.ingredients[oats].quantity == 1.5

    pantry.eat(oats)
    assert pantry.ingredients[oats].quantity == 0.5

    pantry.eat(oats.grams(50))
    assert oats not in pantry.ingredients

def test_cant_eat_food_that_doesnt_exist():
    """Trying to eat a food that isn't in the pantry should be a ValueError"""
    pb = Food.by_name("peanut butter")
    with pytest.raises(ValueError):
        Pantry.instance().eat(pb)
