"""
Tests on the base class / utils
"""

import pytest
from sqlalchemy.orm.exc import MultipleResultsFound

from models import Food, Recipe, DailyLog, session
from models._base import camel_to_snake

def test_camel_to_snake():
    """Camel-to-snake case conversion should give expected result"""
    assert camel_to_snake("Food") == "food"
    assert camel_to_snake("Recipe") == "recipe"
    assert camel_to_snake("DailyLog") == "daily_log"
    assert camel_to_snake("XmlHttpRequest") == "xml_http_request"


def test_url_path():
    """Should use a snake-case version of the model name"""
    assert Food.url_path() == "/food"
    assert Recipe.url_path() == "/recipe"
    assert DailyLog.url_path() == "/daily_log"

    assert Food.url_path("new") == "/food/new"
    assert Recipe.url_path("something") == "/recipe/something"
    assert DailyLog.url_path("make_graphs") == "/daily_log/make_graphs"

def test_exception_error_messages():
    """Should give informative error messages on MultipleResultsFound errors"""
    dupe_food = Recipe("oats")
    dupe_food.save()

    with pytest.raises(MultipleResultsFound, match="oats"):
        Food.by_name("oats")

    # Cleanup
    session.delete(dupe_food)
    session.commit()
