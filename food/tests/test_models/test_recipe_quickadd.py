"""
Tests of and related to the RecipeQuickadd model.
"""

from pytest import raises
from models import RecipeQuickadd, Food

oats = Food.by_name("oats")

def test_requires_primary():
    """All quick-add recipes need to have a primary ingredient"""
    with raises(TypeError):
        RecipeQuickadd()  # pylint: disable=no-value-for-parameter

    with raises(TypeError, match="must be an Ingredient"):
        RecipeQuickadd("asdf")

def test_fundamental_level():
    """Fundamental level: 4"""
    oats_w_honey = RecipeQuickadd(oats.grams(12))
    assert oats_w_honey.fundamental_level == RecipeQuickadd.__mapper_args__["polymorphic_identity"]

def test_add():
    """Adding ingredients should update the name"""
    oats_w_honey = RecipeQuickadd(oats.grams(12))
    assert oats_w_honey.name == "oats"

    oats_w_honey.add(Food.by_name("honey").grams(10))
    assert oats_w_honey.name == "oats w/ honey"

    oats_w_honey.add(Food.by_name("maple syrup").grams(10))
    assert oats_w_honey.name == "oats w/ honey and maple syrup"

    oats_w_honey.add(Food.by_name("milk").grams(10))
    assert oats_w_honey.name == "oats w/ honey, maple syrup and milk"

def test_add_hidden():
    """Adding a hidden ingredient should not change the name"""
    oats_w_honey = RecipeQuickadd(oats.grams(12))
    oats_w_honey.add_hidden(Food.by_name("honey").grams(10))
    assert oats_w_honey.name == "oats"

    oats_w_honey.add(Food.by_name("maple syrup").grams(10))
    assert oats_w_honey.name == "oats w/ maple syrup"

    oats_w_honey.add(Food.by_name("milk").grams(10))
    assert oats_w_honey.name == "oats w/ maple syrup and milk"

def test_must_have_extras():
    """A valid quick-add recipe should have at least 1 extra, otherwise it's just a food"""
    oats_w_honey = RecipeQuickadd(oats.grams(12))
    with raises(ValueError, match="extras"):
        oats_w_honey.save()

    assert oats_w_honey.id is None

    oats_w_honey.add(Food.by_name("honey").grams(10))
    oats_w_honey.save()
    assert oats_w_honey.id is not None
