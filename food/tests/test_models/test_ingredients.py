"""
Tests of and related to the Ingredient model.
"""

from pytest import raises

from models import Food, Ingredient, Recipe, session


def test_ingredient_creation():
    """
    Creating a new ingredient in a recipe should populate the ingredient's
    attributes and make it appear in its recipe's ingredients list
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    oats_50 = Ingredient(base_food=oats, recipe=shake, quantity=0.5, units=1, hidden=False)
    assert oats_50.quantity == 0.5
    assert oats_50.units == 1
    assert oats_50.base_food == oats
    assert oats_50.recipe == shake
    assert not oats_50.hidden

    assert isinstance(oats_50.recipe, Recipe)
    assert oats_50 in shake.ingredients

def test_ingredient_save():
    """
    Saving an ingredient should populate its foreign keys attributes (i.e.
    database level) from the SQLAlchemy relationship objects
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    oats_50 = Ingredient(base_food=oats, recipe=shake, quantity=0.5, units=1)
    oats_50.list_position = 0

    oats_50.save()

    assert oats_50.base_food_id == oats.id
    assert oats_50.belongs_to == shake.id

def test_ingredient_str():
    """For console debugging purposes"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    oats_50 = Ingredient(base_food=oats, quantity=0.5, units=1)
    assert str(oats_50) == "50.0g oats<recipe: {}>".format("None!")

    oats_50.recipe = shake
    assert str(oats_50) == "50.0g oats<recipe: {}>".format(shake.name)

    oats_50.units = 0
    assert str(oats_50) == "0.5 oats<recipe: {}>".format(shake.name)

    # `repr` is more commonly used in console
    assert repr(oats_50) == str(oats_50)

    # No clue why but the ingredient and its recipe get automatically added to
    # the session on creation
    session.rollback()

def test_ingredients_saving_thru_backref():
    """Saving a recipe should save all its ingredients automatically"""
    oats = Food.by_name("oats")
    shake = Recipe(name="shake")
    oats_1 = oats.times(1)
    oats_1.recipe = shake
    oats_1.list_position = 0

    session.add(shake)
    assert oats_1 in session

    assert oats_1.id is None
    shake.save()
    assert oats_1.id is not None

def test_ingredients_deleting_thru_backref():
    """Deleting a recipe should delete all its ingredients automatically"""
    oats = Food.by_name("oats")
    shake = Recipe(name="shake")
    oats_1 = oats.times(1)
    oats_1.recipe = shake
    oats_1.list_position = 0

    shake.save()
    session.delete(shake)
    assert shake in session.deleted
    assert oats_1 in session.deleted
    session.commit()

def test_save_new_food_by_ingredient():
    """Saving a recipe should create foods that were added to it as ingredients"""
    new_food = Food("squid ink")
    taleb = Recipe(name="taleb")
    taleb.add(new_food.grams(12))

    session.add(taleb)
    assert new_food in session

def test_ingredient_to_dict():
    """Should produce a dictionary of an ingredient's attributes"""
    oats = Food.by_name("oats")
    shake = Recipe(name="shake")

    ingredient = oats.grams(50)
    shake.add(ingredient)

    d = ingredient.to_dict()
    assert d["base_food"] == oats.to_dict()
    assert d["quantity"] == ingredient.quantity
    assert d["units"] == ingredient.units

def test_ingredient_times():
    """Should iteratively change the ingredient's amount"""
    oats = Food.by_name("oats")
    ingredient = oats.grams(50)

    double_ingredients = ingredient.times(2)
    assert double_ingredients.base_food == ingredient.base_food
    assert double_ingredients.units == ingredient.units
    assert double_ingredients.quantity == ingredient.quantity * 2

def test_lookup_ingredient_by_food():
    """
    Find an ingredient in a recipe's ingredient list by the base food expected.
    Should raise a KeyError if no such ingredient exists.
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")

    oats_i = oats.times(1.70)
    for i in [oats_i, whey.times(0.25), pb.times(1.3)]:
        shake.add(i)
    shake.save()

    assert shake.ingredients[oats] == oats_i
    assert shake.ingredients[oats.id] == oats_i

    fake_id = 1000000
    with raises(KeyError, match=str(fake_id)):
        shake.ingredients[fake_id]  # pylint: disable=pointless-statement
    milk = Food.by_name("milk")
    with raises(KeyError, match=str(milk.id)):
        shake.ingredients[Food.by_name("milk")]  # pylint: disable=expression-not-assigned

def test_ingredient_order():
    """Ingredients should appear in the proper order when iterated over"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    x = iter(shake.ingredients)
    assert next(x).base_food == oats
    assert next(x).base_food == whey
    assert next(x).base_food == pb
    with raises(StopIteration):
        next(x)

def test_ingredient_order_change():
    """Ingredients should update order if `list_position` is changed"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    oat_i = shake.ingredients[oats]
    oat_i.list_position = 7

    shake.save()
    x = iter(shake.ingredients)
    assert next(x).base_food == whey
    assert next(x).base_food == pb
    last_ingredient = next(x)
    assert last_ingredient.base_food == oats
    assert last_ingredient.list_position == 7

    with raises(StopIteration):
        next(x)

def test_ingredient_list_contains():
    """Should be able to check for presence of a Food, an Ingredient, or an int"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    oats_i = oats.times(1.70)
    for i in [oats_i, whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert Food.by_name("oats") in shake
    assert oats in shake
    assert oats.id in shake
    assert oats_i in shake

    milk = Food.by_name("milk")

    assert milk not in shake
    assert milk.id not in shake
    assert milk.grams(10) not in shake
    assert 0 not in shake
