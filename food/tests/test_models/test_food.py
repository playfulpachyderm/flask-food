"""
Tests of and related to the Food model.
"""

import copy

import pytest
from sqlalchemy.orm.exc import NoResultFound

from models import Food, session

# Database setup
# --------------

def test_food_find_by_name():
    """Should be able to look up a food by its name"""
    oats = Food.by_name("oats")
    assert oats.name == "oats"
    assert oats.cals == 400
    assert oats.density == 0.44

def test_food_find_by_name_invalid():
    """Looking up a nonexistent food by name should produce a NoResultFound"""
    gibberish = "fwjkellklk"
    with pytest.raises(NoResultFound, match=gibberish):
        Food.by_name(gibberish)

def test_food_by_id():
    """Should be able to look up a food by its id"""
    oats1 = Food.by_name("oats")
    oats2 = Food.by_id(oats1.id)
    assert oats2.name == "oats"
    assert oats2.cals == 400
    assert oats2.density == 0.44

def test_food_all():
    """Should be able to get all foods"""
    all_foods = Food.all()
    assert len(all_foods) > 50
    for f in all_foods:
        assert isinstance(f, Food)

def test_food_where():
    """Should be able to execute a `where` query"""
    a = Food.where(cals=0)
    assert len(a) >= 3

    b = Food.where(Food.name.like(r"%pizza%"))
    assert len(b) >= 15

def test_food_save():
    """A food should save properly; it should persist any changes"""
    walnuts = Food.by_name("walnuts")
    assert walnuts.cals == 654

    walnuts.cals = 300
    session.refresh(walnuts)
    assert walnuts.cals == 654

    walnuts.cals = 300
    walnuts.save()
    session.refresh(walnuts)
    assert walnuts.cals == 300

    # teardown
    walnuts.cals = 654
    walnuts.save()


# Ingredient spawning
# -------------------

def test_food_times():
    """Should create an ingredient from a base food"""
    oats = Food.by_name("oats")
    oats_60 = oats.times(0.6)
    assert oats_60.quantity == 0.6
    assert oats_60.units == 0  # units: number
    assert oats_60.base_food == oats
    assert oats_60.recipe is None

def test_food_grams():
    """Should create an ingredient from a base food, but with units of grams"""
    oats = Food.by_name("oats")
    oats_70 = oats.grams(70)
    assert oats_70.quantity == 0.7
    assert oats_70.units == 1  # units: grams
    assert oats_70.base_food == oats
    assert oats_70.recipe is None

def test_no_add_on_fundamental_food():
    """Should not be able to 'add' something to a food that isn't a recipe"""
    oats = Food.by_name("oats")
    assert not hasattr(oats, "add")


# Helper methods
# --------------

def test_food_str():
    """For console debugging purposes"""
    oats = Food.by_name("oats")
    assert str(oats) == f"oats({oats.id})<400, 67, 17, 7, 0>[m=100]"

    x = Food("awef", cals=3.89211, carbs=60000, protein=3, fat=2.0)
    assert str(x) == "awef(None)<3.89, 60000, 3, 2, 0>[m=100]"

    # `repr` is what's more used in the console
    assert str(x) == repr(x)

def test_food_copy():
    """
    Copying a food should behave as one would expect.
    You should get a new food with all the same attributes, but should not copy
    the `id` field.
    """
    oats = Food.by_name("oats")

    oats_copy = copy.copy(oats)
    assert oats_copy.cals == oats.cals
    assert oats_copy.carbs == oats.carbs
    assert oats_copy.price == oats.price
    assert oats_copy.mass == oats.mass
    assert oats_copy.id is None

    oats_copy.save()
    assert oats_copy.id is not None
    assert oats_copy.id != oats.id

    # cleanup
    session.delete(oats_copy)
    session.commit()

def test_food_to_dict():
    """Should produce a dictionary with all the attributes of the food"""
    oats = Food.by_name("oats")
    d = oats.to_dict()

    assert d['id'] == oats.id
    assert d['fat'] == oats.fat
    assert d['alcohol'] == oats.alcohol
    assert d['price'] == oats.price
    assert d['fundamental_level'] == oats.fundamental_level
    assert d['potassium'] == oats.potassium
    assert d['iron'] == oats.iron

def test_url_for():
    """Should give an URL path to a food item"""
    oats = Food.by_name("oats")
    assert Food.url_for(oats) == "/food/" + str(oats.id)
