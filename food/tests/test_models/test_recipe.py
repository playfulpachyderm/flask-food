"""
Tests of and related to the Recipe model.
"""

from pytest import approx, raises
from models import Food, Ingredient, Recipe, Pantry, DailyLog

# Database setup
# --------------

def test_fundamental_level():
    """Fundamental level of a recipe should be 1"""
    shake = Recipe(name="shake")
    assert shake.fundamental_level == Recipe.__mapper_args__["polymorphic_identity"]

def test_recipe_ingredients():
    """Ingredients in a recipe should show up in its ingredients list"""
    oats = Food.by_name("oats")
    shake = Recipe(name="shake")
    oats_50 = Ingredient(base_food=oats, recipe=shake, quantity=0.5, units=1)
    assert oats_50 in shake.ingredients

def test_base_recipe():
    """The `base_recipe` attribute should work"""
    shake1 = Recipe(name="shake1")
    shake2 = Recipe(name="shake2")

    assert len(shake1.iterations) == 0

    shake2.base_recipe = shake1
    shake2.save()

    assert len(shake1.iterations) == 1
    assert shake2 in shake1.iterations

def test_base_recipe_backref():
    """The `iterations` attribute should work"""
    shake1 = Recipe(name="shake1")
    shake2 = Recipe(name="shake2")

    assert shake2.base_recipe is None

    shake1.iterations.append(shake2)
    shake1.save()

    assert shake2.base_recipe == shake1


# Ingredient manipulation
# -----------------------

def test_recipe_add():
    """
    Adding an ingredient to a recipe should change the recipe's nutrition.
    The ingredient should also be added to the recipe's ingredient list.
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    oats_300 = oats.grams(300)
    shake.add(oats_300)
    assert shake.cals == oats.cals * 3
    assert shake.carbs == oats.carbs * 3
    assert shake.protein == oats.protein * 3
    assert shake.fat == oats.fat * 3
    assert shake.sugar == oats.sugar * 3
    assert shake.alcohol == oats.alcohol * 3
    assert shake.water == oats.water * 3
    assert shake.mass == oats.mass * 3
    assert shake.price == oats.price * 3
    assert shake.potassium == oats.potassium * 3
    assert shake.sodium == oats.sodium * 3
    assert shake.calcium == oats.calcium * 3
    assert shake.magnesium == oats.magnesium * 3
    assert shake.phosphorus == oats.phosphorus * 3
    assert shake.iron == oats.iron * 3
    assert shake.zinc == oats.zinc * 3

    assert oats_300 in shake.ingredients
    assert oats_300.list_position == 0

def test_recipe_add_duplicate():
    """Should not be able to add an ingredient if it already exists in the recipe"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    shake.add(oats.times(1))

    with raises(ValueError, match=oats.name):
        shake.add(oats.grams(3))

def test_recipe_add_multiple():
    """
    Same test as above, but with multiple ingredients.
    TODO: Could this be rolled into the previous test?
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25 + pb.cals * 1.3)
    assert shake.protein == approx(oats.protein * 1.70 + whey.protein * 0.25 + pb.protein * 1.3)
    assert shake.sugar == approx(oats.sugar * 1.70 + whey.sugar * 0.25 + pb.sugar * 1.3)
    assert shake.price == approx(oats.price * 1.70 + whey.price * 0.25 + pb.price * 1.3)
    assert shake.mass == approx(oats.mass * 1.70 + whey.mass * 0.25 + pb.mass * 1.3)
    assert shake.potassium == approx(oats.potassium * 1.70 + whey.potassium * 0.25 + pb.potassium * 1.3)  # pylint: disable=line-too-long

    shake.save()

    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25 + pb.cals * 1.3)
    assert shake.protein == approx(oats.protein * 1.70 + whey.protein * 0.25 + pb.protein * 1.3)
    assert shake.sugar == approx(oats.sugar * 1.70 + whey.sugar * 0.25 + pb.sugar * 1.3)
    assert shake.price == approx(oats.price * 1.70 + whey.price * 0.25 + pb.price * 1.3)
    assert shake.mass == approx(oats.mass * 1.70 + whey.mass * 0.25 + pb.mass * 1.3)
    assert shake.sodium == approx(oats.sodium * 1.70 + whey.sodium * 0.25 + pb.sodium * 1.3)

    assert {x.list_position for x in shake.ingredients} == {0, 1, 2}

def test_recipe_add_or_update():
    """Should add an ingredient if it doesn't exist; if it does, quantity should be incremented"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert shake.ingredients[oats].quantity == 1.7
    assert len(shake.ingredients) == 3
    shake.add_or_update(oats.times(0.3))
    assert shake.ingredients[oats].quantity == 2
    assert len(shake.ingredients) == 3

    milk = Food.by_name("milk")
    assert milk not in shake.ingredients
    assert len(shake.ingredients) == 3
    shake.add_or_update(milk)
    assert milk in shake.ingredients
    assert len(shake.ingredients) == 4
    assert shake.ingredients[milk].quantity == 1


def test_new_food_as_ingredient():
    """Creating a new food on the fly should not break `Recipe#add`"""
    new_food = Food(name="asdf")
    shake = Recipe(name="shake")
    assert shake.mass == 0
    shake.add(new_food)  # Should not TypeError!
    assert shake.mass == 100
    assert shake.cals == 0
    assert shake.price == 0
    assert shake.density == 1


def test_delete_ingredient():
    """Deleting an ingredient should update the recipe totals"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert oats in shake
    assert shake.price == approx(oats.price * 1.70 + whey.price * 0.25 + pb.price * 1.3)
    shake.delete_item(oats)
    assert oats not in shake
    assert shake.price == approx(whey.price * 0.25 + pb.price * 1.3)

    assert whey in shake
    shake.delete_item(shake.ingredients[whey])
    assert shake.price == approx(pb.price * 1.3)


def test_no_delete_nonexistent_ingredient():
    """Should not delete a nonexistent ingredient"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    fake_id = 248348
    with raises(KeyError, match=str(fake_id)):
        shake.delete_item(fake_id)


def test_recalculate_total():
    """Should re-calculate a recipe's total nutrition to suit its new ingredient list"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats").times(1.70)
    whey = Food.by_name("whey").times(0.25)
    pb = Food.by_name("peanut butter").times(1.3)
    for i in [oats, whey, pb]:
        shake.add(i)

    original_protein = shake.protein

    shake.protein = -12
    assert shake.protein == -12

    shake.recalculate_total()
    assert shake.protein == original_protein

    whey.quantity = 0.5
    shake.recalculate_total()
    assert shake.protein == original_protein + (whey.base_food.protein * 0.25)


def test_cascading_nutrition_update():
    """Modifying a food's nutrition should propagate the changes up the recipe hierarchy"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    fakefood = Food("fake", cals=100, carbs=10, protein=10, fat=10, sugar=10)
    for i in [oats.times(1.70), whey.times(0.25), fakefood.times(1.3)]:
        shake.add(i)
    shake.save()

    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25 + fakefood.cals * 1.3)
    assert shake.price == approx(oats.price * 1.70 + whey.price * 0.25 + fakefood.price * 1.3)

    fakefood.cals = fakefood.cals * 2
    fakefood.price = fakefood.price * 2
    fakefood.save()

    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25 + fakefood.cals * 1.3)
    assert shake.price == approx(oats.price * 1.70 + whey.price * 0.25 + fakefood.price * 1.3)



# Patching
# --------

def test_recipe_update_amount():
    """
    Updating the quantity of an ingredient should update the recipe's totals
    Should also update the ingredients
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats").times(1.70)
    whey = Food.by_name("whey").times(0.25)
    pb = Food.by_name("peanut butter").times(1.3)
    for i in [oats, whey, pb]:
        shake.add(i)

    assert shake.protein == approx(
        oats.base_food.protein * 1.70 + whey.base_food.protein * 0.25 + pb.base_food.protein * 1.3
    )
    assert shake.price == approx(
        oats.base_food.price * 1.70 + whey.base_food.price * 0.25 + pb.base_food.price * 1.3
    )
    assert shake.mass == approx(
        oats.base_food.mass * 1.70 + whey.base_food.mass * 0.25 + pb.base_food.mass * 1.3
    )

    shake.update({
        "ingredient_changes": {
            str(whey.base_food_id): {
                "quantity": 0.5
            },
            str(oats.base_food_id): {
                "quantity": 30,
                "units": 1
            },
        },
    })

    assert whey.quantity == 0.5
    assert whey.units == 0

    assert oats.quantity == 30
    assert oats.units == 1
    assert oats.str_amount() == "3000.0g"

    assert shake.protein == approx(
        oats.base_food.protein * 30 + whey.base_food.protein * 0.5 + pb.base_food.protein * 1.3
    )
    assert shake.price == approx(
        oats.base_food.price * 30 + whey.base_food.price * 0.5 + pb.base_food.price * 1.3
    )
    assert shake.mass == approx(
        oats.base_food.mass * 30 + whey.base_food.mass * 0.5 + pb.base_food.mass * 1.3
    )

def test_recipe_update_reorder():
    """Should reorder the ingredients in a recipe"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert shake.ingredients[oats].list_position == 0
    assert shake.ingredients[whey].list_position == 1
    assert shake.ingredients[pb].list_position == 2

    shake.update({
        "ingredient_changes": {
            oats.id: {
                "reorder": 2
            },
            whey.id: {
                "reorder": 3
            },
            pb.id: {
                "reorder": 1
            },
        },
    })
    assert shake.ingredients[oats].list_position == 2
    assert shake.ingredients[whey].list_position == 3
    assert shake.ingredients[pb].list_position == 1

def test_recipe_update_deletion():
    """Should delete an ingredient from a recipe and update its totals"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    assert pb in shake
    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25 + pb.cals * 1.3)

    shake.update({
        "ingredient_changes": {
            pb.id: {
                "delete": True
            },
        },
    })

    assert pb not in shake
    assert shake.cals == approx(oats.cals * 1.70 + whey.cals * 0.25)

def test_recipe_update_add_new_ingredient():
    """Should add an ingredient to the recipe"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")

    shake.update({
        "ingredient_changes": {
            oats.id: {
                "created": True,
                "quantity": 2,
                "units": 1,
            },
        }
    })
    assert oats in shake
    assert shake.cals == oats.cals * 2
    assert shake.ingredients[oats].quantity == 2
    assert shake.ingredients[oats].units == 1

def test_recipe_update_change_name():
    """Should rename recipe"""
    shake = Recipe(name="shake")

    new_name = "faweklgaboewjfksdkew"

    shake.update({"change_title": new_name})
    assert shake.name == new_name

# JSONify
# -------

def test_recipe_to_dict():
    """Should produce a dictionary of the recipe's attributes, including ingredients"""
    oats = Food.by_name("oats")
    pb = Food.by_name("peanut butter")
    shake = Recipe(name="shake")

    ingredients = [oats.grams(100), pb.grams(50)]
    for i in ingredients:
        shake.add(i)

    d = shake.to_dict()
    assert ingredients[0].to_dict() in d["ingredients"]
    assert ingredients[1].to_dict() in d["ingredients"]

def test_recipe_to_dict_with_recipe_as_ingredient():
    """A `to_dict` call should format recipes as if they were regular foods"""
    oats = Food.by_name("oats")
    pb = Food.by_name("peanut butter")
    shake = Recipe(name="shake")

    ingredients = [oats.grams(100), pb.grams(50)]
    for i in ingredients:
        shake.add(i)

    shake2 = Recipe(name="shake2")
    ingredients2 = [oats.grams(50), shake.times(0.5)]
    for i in ingredients2:
        shake2.add(i)

    d = shake2.to_dict()
    for ingredient in d["ingredients"]:
        assert "id" in ingredient["base_food"].keys(), ingredient

def test_recipe_from_json():
    """Should correctly parse a recipe from a dictionary"""
    oats = Food.by_name("oats")
    eggs = Food.by_name("eggs")

    test_json = {"title": "shake", "ingredients": []}

    ingredients = [oats.grams(100), eggs.times(50)]
    for i in ingredients:
        test_json["ingredients"].append({"amount": i.str_amount(), "name": i.base_food.name})

    shake = Recipe.from_json(test_json)
    assert shake.name == "shake"
    assert len(shake.ingredients) == 2
    assert shake.ingredients.at_position(0).base_food.name == "oats"
    assert shake.ingredients.at_position(0).quantity == 100 / oats.mass
    assert shake.ingredients.at_position(0).units == 1
    assert shake.ingredients.at_position(1).base_food.name == "eggs"
    assert shake.ingredients.at_position(1).quantity == 50
    assert shake.ingredients.at_position(1).units == 0


# Helper methods
# --------------

def test_parse_amount():
    """Should correctly parse an amount from an amount string"""
    expected = [(30, 1), (40, 1), (100, 0), (0, 0)]
    tests = ["30g", "40 g", "100", "0"]
    for i, v in enumerate(expected):
        result = Recipe.parse_amount(tests[i])
        assert result, "Expected {}; got {} instead".format(v, result == v)

def test_url_for():
    """Should give an URL path to a recipe"""
    shake = Recipe("shake")
    shake.save()

    assert Recipe.url_for(shake) == "/recipe/" + str(shake.id)
    assert Food.url_for(shake) == "/food/" + str(shake.id)

def test_available_ingredients():
    """Every ingredient should be in the list"""
    ingredients = Recipe.available_ingredients_as_json()
    test_ingredient = Food.by_name("oats")
    assert test_ingredient.to_dict() in ingredients

def test_xall():
    """Need a way to get Recipes with no complications"""
    just_recipes = Recipe.x_all()
    for r in just_recipes:
        assert not isinstance(r, Pantry)

def test_get_recipe_after_and_before():
    """
    Should get the previous recipe and the following recipe, respectively.
    Should still work if some IDs are not in sequence or missing.
    """
    assert isinstance(Food.by_id(289), DailyLog)  # Sanity check
    r = Recipe.by_id(288)
    assert Recipe.prev_in_sequence(r, Recipe.id).id == 287
    assert Recipe.next_in_sequence(r, Recipe.id).id == 290

def test_nav_for():
    """Should get links to next and prev recipes"""
    assert isinstance(Food.by_id(289), DailyLog)  # Sanity check

    r = Recipe.by_id(288)
    assert Recipe.nav_for(r) == {"nav": {"next": "/recipe/290", "prev": "/recipe/287"}}

def test_nav_for_first_item():
    """Should give `None` for "prev" if this is the first item"""
    r = Recipe.by_id(271)
    assert Recipe.nav_for(r) == {"nav": {"next": "/recipe/272", "prev": None}}

def test_nav_for_last_item():
    """Should give `None` for "next" if this is the last item"""
    r = Recipe.x_all()[-1]
    second_last = Recipe.prev_in_sequence(r, Recipe.id)
    assert Recipe.nav_for(r) == {"nav": {"next": None, "prev": Recipe.url_for(second_last)}}


# Iterate
# -------

def test_iterate_recipe():
    """
    Should create an iteration of the recipe.  All nutritional parameters should
    be the same (except id).  Iteration-tracking variables should be set.
    """
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    new_shake = shake.iterate()

    assert new_shake.name == shake.name
    assert oats in new_shake
    assert new_shake.ingredients[oats].quantity == 1.70
    assert whey in new_shake
    assert pb in new_shake

    assert new_shake.cals == shake.cals
    assert new_shake.carbs == shake.carbs

    assert new_shake.base_recipe == shake
    assert new_shake in shake.iterations


# Splat
# -----

def test_splat():
    """Should break a recipe down into base ingredients"""
    shake = Recipe(name="shake")
    oats = Food.by_name("oats")
    whey = Food.by_name("whey")
    pb = Food.by_name("peanut butter")
    for i in [oats.times(1.70), whey.times(0.25), pb.times(1.3)]:
        shake.add(i)

    epic_recipe = Recipe(name="epic recipe")
    epic_recipe.add(shake)
    epic_recipe.add(whey.times(0.75))
    potatoes = Food.by_name("potatoes")
    epic_recipe.add(potatoes.grams(100))

    splat = epic_recipe.splat()
    assert shake not in splat.ingredients
    assert splat.name == "epic recipe (splat)"
    assert splat.ingredients[oats].quantity == 1.70
    assert splat.ingredients[whey].quantity == 0.25 + 0.75
    assert splat.ingredients[pb].quantity == 1.3
    assert splat.ingredients[potatoes].quantity == 100 / potatoes.mass
