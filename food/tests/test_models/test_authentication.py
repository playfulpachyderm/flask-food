"""
Tests for JWT and authentication helpers
"""

import jwt

from application_factory import config
from models.authentication import create_token, validate_token, authenticate_by_password

def test_create_token():
    """Should create a proper token"""
    token = create_token()
    data_decoded = jwt.decode(token, config["secret_key"], algorithms=["HS256"])
    assert isinstance(data_decoded, dict)

def test_validate_good_token():
    """Valid tokens should validate"""
    token = create_token()
    data, error = validate_token(token)
    assert error is None
    assert isinstance(data, dict)

def test_validate_missing_token():
    """Missing tokens should produce an error and not validate"""
    data, error = validate_token(None)
    assert not data
    assert error is not None

def test_validate_invalid_token():
    """Invalid tokens should produce an error and not validate"""
    token = jwt.encode({"logged_in": True}, "FAKE SECRET KEY")
    data, error = validate_token(token)
    assert not data
    assert "invalid" in error

def test_validate_expired_token():
    """Expired token should produce an error and not validate"""
    token = create_token(lifetime=-1)
    data, error = validate_token(token)
    assert not data
    assert error is not None

def test_authenticate_by_password():
    """Can't test the positive case"""
    assert not authenticate_by_password("")
    assert not authenticate_by_password("asdf")
    assert authenticate_by_password("password")
