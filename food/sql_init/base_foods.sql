\set ON_ERROR_STOP 1

-- Wholesome
------------

insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('bread', 289.0, 56.0, 12.0, 2.0, 3.0, 0, 100, 44, 0.381, 0.1, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('zehrs italian loaf', 260.0, 52.0, 10.0, 3.0, 2.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ancient grain', 265.0, 47.0, 12.0, 4.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('zehrs sourdough', 266.0, 52.0, 12.0, 1.7, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, fundamental_level) values ('oats', 400.0, 67.0, 17.0, 7.0, 0.0, 0, 100, 33, 0.44, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('pasta', 360.0, 74.0, 13.0, 1.5, 3.0, 0, 100, 25, 2, 0.7, 0.22, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pasta_unico', 360.0, 74.0, 13.0, 1.5, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('potatoes', 97.0, 21.0, 2.67, 0.0, 1.0, 0, 100, 12, 0.01, 0.535, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('banana', 90.0, 23.0, 1.0, 0.3, 12.0, 0, 101, 23, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('rice', 365.0, 80.0, 7.0, 1.0, 0.0, 0, 100, 14, 0.003, 0.187, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('rice cooked', 130.0, 29.0, 2.4, 0.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, fundamental_level) values ('whey', 379.0, 7.0, 86.0, 0.0, 0.0, 0, 100, 334, 0.45, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('whey1', 387.0, 3.0, 87.0, 3.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('whey2', 365.0, 7.0, 73.0, 6.0, 3.0, 0, 100, 220, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('whey3', 379.0, 7.0, 86.0, 0.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('eggs', 80.0, 1.0, 7.0, 6.0, 0.0, 0, 53, 29, 0.065, 0.0625, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('egg whites', 48.0, 1.0, 11.0, 0.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('peanut butter', 600.0, 20.0, 27.0, 47.0, 7.0, 0, 100, 66, 0.007, 0.667, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('almond butter', 633.0, 20.0, 20.0, 60.0, 0.0, 0, 100, 242, 0.011, 0.667, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('olive oil', 884.0, 0.0, 0.0, 100.0, 0.0, 0, 100, 141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('avocado oil', 884.0, 0.0, 0.0, 100.0, 0.0, 0, 100, 155, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, sodium, potassium, fundamental_level) values ('flour', 364.0, 76.0, 10.0, 1.3, 0.0, 0, 100, 24, 0.53, 0.002, 0.107, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, sodium, potassium, fundamental_level) values ('flour2', 367.0, 70.0, 13.3, 1.7, 0.0, 0, 100, 16, 0.53, 0.002, 0.107, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sugar', 400.0, 100.0, 0.0, 0.0, 100.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('honey', 300.0, 85.0, 0.0, 0.0, 80.0, 0, 100, 89, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('tomatoes', 20.0, 5.0, 0.3, 0.0, 3.0, 0, 100, 44, 0.1, 0.32, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('carrots', 37.0, 9.0, 1.0, 0.0, 4.5, 0, 100, 105, 0.069, 0.32, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('peppers', 31.0, 6.0, 1.0, 0.3, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('avocado', 160.0, 9.0, 2.0, 15.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('almonds', 576.0, 22.0, 21.0, 49.0, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('walnuts', 654.0, 14.0, 15.0, 65.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('pine nuts', 720.0, 14.0, 14.0, 68.0, 4.0, 0, 100, 800, 0.002, 0.597, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('kalamata', 167.0, 7.0, 0.0, 13.0, 0.0, 0, 100, 300, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sundried tomato', 258.0, 56.0, 14.0, 3.0, 38.0, 0, 100, 290, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('celery', 16.0, 3.0, 1.0, 0.0, 2.0, 0, 100, 50, 0.08, 0.26, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('ginger', 80.0, 18.0, 2.0, 1.0, 2.0, 0, 100, 66, 0.013, 0.415, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('onion', 88.0, 20.0, 2.0, 0.0, 9.0, 0, 220, 17, 0.62, 0.008, 0.312, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('red onion', 88.0, 20.0, 2.0, 0.0, 9.0, 0, 220, 36.6, 0.008, 0.312, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('leeks', 61.0, 14.0, 1.5, 0.3, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('green onion', 32.0, 7.0, 1.8, 0.0, 2.0, 0, 11, 18, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('garlic', 149.0, 30.0, 6.0, 0.5, 1.0, 0, 100, 60, 0.017, 0.4, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('shallots', 72.0, 17.0, 3.0, 0.0, 8.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('ground beef medium', 250.0, 0.0, 19.0, 20.0, 0.0, 0, 100, 110, 0.75, 0.065, 0.323, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('ground beef lean', 215.0, 0.0, 19.0, 15.0, 0.0, 0, 100, 100, 0.75, 0.065, 0.323, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('ground beef extra lean', 170.0, 0.0, 21.0, 9.0, 0.0, 0, 100, 132, 0.065, 0.323, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ground_trio', 240.0, 0.0, 19.0, 18.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('top sirloin roast', 213.0, 1.0, 27.0, 11.0, 0.0, 0, 100, 88, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('striploin steak', 196.0, 0.0, 24.0, 11.0, 0.0, 0, 100, 132, 0.7, 0.055, 0.342, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('prime rib', 271.0, 0.0, 25.0, 19.0, 0.0, 0, 100, 0, 0.055, 0.342, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('flank steak', 150.0, 0.0, 22.0, 6.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('beef tenderloin', 180.0, 0.0, 28.0, 7.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('filet mignon', 205.0, 0.0, 28.0, 11.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('beef chuck', 130.0, 0.0, 21.0, 5.0, 0.0, 0, 100, 132, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ribeye', 207.0, 0.0, 24.0, 12.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('short ribs', 261.0, 0.0, 18.0, 21.0, 0.0, 0, 100, 242, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('beef shanks', 182.0, 0.0, 21.0, 10.0, 0.0, 0, 100, 110, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('oxtail', 262.0, 0.0, 30.0, 15.0, 0.0, 0, 100, 250, 0.047, 0.43, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('lamb leg', 200.0, 0.0, 22.0, 12.0, 0.0, 0, 100, 160, 0.054, 0.286, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, fundamental_level) values ('ground pork', 230.0, 1.0, 18.0, 17.0, 0.0, 0, 100, 88, 0.75, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, fundamental_level) values ('ground pork lean', 220.0, 1.0, 19.0, 15.0, 0.0, 0, 100, 88, 0.75, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pancetta', 288.0, 2.0, 16.0, 27.0, 2.0, 0, 100, 449, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sausage', 380.0, 0.0, 26.0, 32.0, 0.0, 0, 160, 81, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('sausage2', 300.0, 0.0, 16.0, 26.0, 0.0, 0, 100, 133, 0.71, 0.35, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('kirkland sausage honey garlic', 310.0, 5.0, 25.0, 22.0, 3.0, 0, 160, 123, 1.04, 0.39, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('pork shoulder', 290.0, 0.0, 19.0, 23.0, 0.0, 0, 100, 69, 0.073, 0.362, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ribs', 351.0, 0.0, 23.0, 28.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pork chops', 194.0, 0.0, 20.0, 12.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pork loin', 145.0, 0.0, 20.0, 6.0, 0.0, 0, 100, 44, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pork tenderloin', 136.0, 0.0, 21.0, 5.5, 0.0, 0, 100, 64, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('black forest ham', 140.0, 4.0, 24.0, 4.0, 2.0, 0, 100, 0, 0.8, 0.649, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('turkey', 196.0, 0.0, 29.0, 8.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ground turkey', 130.0, 0.0, 20.0, 6.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('shrimp', 62.0, 0.0, 15.0, 1.0, 0.0, 0, 100, 200, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('salmon', 208.0, 0.0, 22.0, 12.0, 0.0, 0, 100, 192, 0.061, 0.384, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('chicken breast', 165.0, 0.0, 31.0, 4.0, 0.0, 0, 100, 220, 0.72, 0.074, 0.259, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, cook_ratio, sodium, potassium, fundamental_level) values ('chicken thighs', 177.0, 0.0, 24.0, 8.0, 0.0, 0, 100, 170, 0.72, 0.1, 0.27, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chicken thighs w skin', 230.0, 0.0, 22.0, 15.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ground chicken', 160.0, 0.0, 18.0, 10.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('stuffing', 386.0, 76.0, 11.0, 3.4, 8.3, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('gravy', 50.0, 5.0, 1.0, 3.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sliced turkey breast', 50.0, 0.0, 11.0, 0.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('milk', 36.0, 4.8, 3.6, 0.0, 4.8, 0, 100, 11, 0.05, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('milk skim', 36.0, 4.8, 3.6, 0.0, 4.8, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('milk 1', 44.0, 4.8, 3.6, 1.0, 4.8, 0, 100, 11, 0.05, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('milk 2', 52.0, 4.8, 3.6, 2.0, 4.8, 0, 100, 11, 0.05, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('milk homo', 64.0, 4.8, 3.6, 3.2, 4.8, 0, 100, 13, 0.05, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('kefir', 64.0, 4.8, 3.2, 3.2, 4.8, 0, 100, 0, 0.05, 0.16, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('milk chocolate', 64.0, 11.0, 3.0, 1.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('buttermilk', 44.0, 4.8, 3.6, 1.0, 4.8, 0, 100, 38, 0.05, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('cream', 333.0, 6.0, 2.0, 33.0, 6.0, 0, 100, 70, 0.027, 0.0667, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sour cream', 193.0, 3.0, 2.0, 20.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, sodium, potassium, fundamental_level) values ('butter', 714.0, 0.0, 1.0, 81.0, 0.0, 0, 100, 95, 0.911, 0.7, 0.02, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('yogurt', 69.0, 3.0, 9.0, 2.0, 2.0, 0, 100, 80, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chocolate', 535.0, 59.0, 8.0, 30.0, 52.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('liberte greek yogurt', 91.0, 3.4, 8.6, 5.1, 3.4, 0, 100, 0, 0.04, 0.141, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, fundamental_level) values ('yogurt whey', 25.4, 5.0, 0.8, 0.35, 5.0, 0, 100, 0, 0.053, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('mozzarella', 233.0, 3.0, 30.0, 13.0, 0.0, 0, 100, 155, 0.627, 0.076, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('mozzarella hifat', 333.0, 3.0, 23.0, 26.0, 0.0, 0, 100, 200, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('italiano', 333.0, 3.0, 27.0, 27.0, 0.0, 0, 100, 175, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cheddar', 402.0, 1.0, 25.0, 33.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('parmigiano', 393.0, 0.0, 33.0, 30.0, 0.0, 0, 100, 399, 1.529, 0.125, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('asiago', 382.0, 3.0, 29.0, 29.0, 0.0, 0, 100, 229, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('blue cheese', 353.0, 2.0, 21.0, 29.0, 0.0, 0, 100, 479, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('texmex cheese', 366.0, 7.0, 23.0, 30.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('gruyere', 413.0, 0.4, 30.0, 32.0, 0.4, 0, 100, 549, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('feta', 300.0, 0.0, 17.0, 27.0, 0.0, 0, 100, 147, 1.112, 0.062, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cream cheese', 266.0, 6.6, 6.6, 23.0, 6.6, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ricotta', 90.0, 5.0, 5.0, 6.0, 3.0, 0, 55, 121, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('jarlsberg', 366.0, 1.0, 27.0, 28.0, 1.0, 0, 100, 190, 0.433, 0.077, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('salami', 333.0, 0.0, 27.0, 27.0, 0.0, 0, 100, 333, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('vincenzos sausage', 300.0, 0.0, 20.0, 23.0, 0.0, 0, 150, 297, 0.71, 0.35, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('water', 0.0, 0.0, 0.0, 0.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('orange juice', 48.0, 11.0, 1.0, 0.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('lemon juice', 25.0, 7.0, 0.4, 0.3, 2.4, 0, 100, 0, 0.001, 0.124, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('red wine vinegar', 10.0, 2.0, 0.0, 0.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('maple syrup', 367.0, 90.0, 0.0, 0.0, 77.0, 0, 100, 0, 0, 0.333, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('wine', 110.0, 3.0, 0.0, 0.0, 0.0, 14.0, 100, 0, 0);


-- Canned / Packaged
--------------------

insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('black beans', 104.0, 18.0, 7.0, 1.0, 1.0, 0, 100, 16, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('dried red beans', 290.0, 30.0, 22.0, 2.0, 2.0, 0, 100, 36.5, 0, 1.5, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('corn', 81.0, 19.0, 3.0, 1.0, 4.0, 0, 100, 25, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('canned tomatoes', 20.0, 5.0, 0.3, 0.0, 3.0, 0, 100, 14, 0.1, 0.32, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('mixed veggies', 71.0, 13.0, 3.5, 0.0, 3.5, 0, 100, 27, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('canned olives', 167.0, 7.0, 0.0, 13.0, 0.0, 0, 100, 150, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('frozen peas', 59.0, 14.0, 4.7, 0.0, 4.7, 0, 100, 27.5, 0.072, 0.11, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sliced bread', 100.0, 14.0, 4.0, 2.5, 2.0, 0, 38, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pecan butter tarts', 360.0, 49.0, 3.0, 17.0, 24.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza', 1920.0, 222.0, 90.0, 72.0, 30.0, 0, 100, 499, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza2', 1380.0, 162.0, 66.0, 48.0, 12.0, 0, 100, 300, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza3', 1260.0, 150.0, 54.0, 54.0, 12.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza4', 1150.0, 105.0, 50.0, 55.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza stuffed', 1600.0, 152.0, 88.0, 72.0, 16.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza vintage', 1360.0, 156.0, 60.0, 54.0, 12.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza gv', 1560.0, 162.0, 66.0, 72.0, 30.0, 0, 100, 333, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza gv rc', 1980.0, 222.0, 84.0, 84.0, 12.0, 0, 100, 444, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meat pizza mikes', 2320.0, 232.0, 104.0, 112.0, 24.0, 0, 100, 700, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('canadian_pizza', 1250.0, 110.0, 65.0, 55.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cheese pizza', 1680.0, 222.0, 90.0, 54.0, 30.0, 0, 100, 450, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cheese pizza2', 1200.0, 162.0, 60.0, 36.0, 12.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cheese pizza gv', 1320.0, 162.0, 60.0, 54.0, 24.0, 0, 100, 333, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('mikes bbq chicken pizza', 2160.0, 256.0, 88.0, 88.0, 56.0, 0, 100, 599, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('spicy_chicken_pizza', 1680.0, 222.0, 90.0, 48.0, 30.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('kirkland pizza', 1080.0, 148.0, 56.0, 32.0, 24.0, 0, 100, 300, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('tandoori_pizza', 1320.0, 176.0, 56.0, 32.0, 12.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ravioli', 220.0, 23.0, 14.0, 8.0, 0.0, 0, 100, 119, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('lasagna', 300.0, 33.0, 16.0, 12.0, 1.0, 0, 227, 130, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('panko', 366.0, 77.0, 10.0, 2.0, 7.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('doritos', 500.0, 57.0, 7.0, 28.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('bacon cheeseburger', 1980.0, 228.0, 90.0, 84.0, 30.0, 0, 801, 499, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('tortilla', 294.0, 50.0, 9.0, 7.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('t and t dumplings', 50.0, 7.6, 2.2, 1.4, 0.4, 0, 100, 15, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('dumplings2', 220.0, 38.0, 9.0, 3.0, 1.0, 0, 100, 70, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('perogies', 60.0, 10.3, 1.67, 1.17, 0.67, 0, 30, 8.33, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('perogies2', 51.0, 10.0, 2.0, 0.5, 1.0, 0, 30, 7.335, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('perogies3', 60.0, 10.0, 1.5, 1.5, 1.0, 0, 40, 36, 0.0775, 0.0666, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('onion rings', 45.0, 5.75, 0.5, 2.25, 0.75, 0, 21, 20, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('popcorn chicken', 26.7, 1.67, 1.17, 1.67, 0.0, 0, 10, 0, 0.0367, 0.05, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pepperoni', 90.0, 2.0, 7.0, 7.0, 0.0, 0, 40, 44, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('classico sauce', 225.0, 40.0, 10.0, 2.5, 25.0, 0, 650, 199, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('janes chicken', 240.0, 23.0, 11.0, 13.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('croissants', 190.0, 22.0, 4.0, 10.0, 2.0, 0, 58, 50, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('kirkland croissants', 300.0, 30.0, 6.0, 17.0, 4.0, 0, 69, 50, 0.36, 0.05, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chips', 540.0, 60.0, 6.0, 30.0, 2.0, 0, 100, 159, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('meatballs', 196.0, 7.0, 19.6, 10.7, 7.0, 0, 100, 56, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('bacon', 190.0, 1.0, 5.0, 18.0, 0.0, 0, 44, 45, 0.24, 0.08, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ice cream sandwich', 180.0, 31.0, 2.0, 5.0, 16.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('tenderflake', 880.0, 80.0, 8.0, 56.0, 8.0, 0, 100, 300, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('bbq sauce', 170.0, 41.0, 1.0, 0.0, 32.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('burger bun', 160.0, 29.0, 6.0, 2.0, 3.0, 0, 60, 41, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('brioche bun', 160.0, 23.0, 6.0, 4.0, 6.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('burger', 370.0, 4.0, 22.0, 30.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('thin sausage bun', 130.0, 25.0, 5.0, 1.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('sausage bun', 210.0, 36.0, 7.0, 4.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pc smokies sausages', 290.0, 6.0, 15.0, 22.0, 5.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('onion straws', 642.0, 43.0, 6.0, 50.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ketchup', 133.0, 33.0, 2.0, 0.0, 27.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('ranch', 467.0, 7.0, 3.0, 53.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, fundamental_level) values ('mayo', 600.0, 0.0, 1.0, 66.0, 0.0, 0, 100, 0, 0.9, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('pop', 39.0, 10.0, 0.0, 0.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('gatorade', 150.0, 38.0, 0.0, 0.0, 35.0, 0, 591, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('gatorade crystals', 375.0, 94.0, 0.0, 0.0, 90.0, 0, 100, 100, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('mini caramel cakes', 240.0, 33.0, 2.0, 11.0, 20.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('kraken rum', 329.0, 0.0, 0.0, 0.0, 0.0, 47.0, 100, 479, 0);


-- Fast foods
-------------

insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('harveys double angus', 756.0, 52.0, 41.0, 43.0, 6.0, 0, 100, 891, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('harveys large fries', 550.0, 75.0, 7.0, 25.0, 0.0, 0, 100, 371, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('harveys two originals', 940.0, 84.0, 37.0, 52.0, 4.0, 0, 100, 678, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('shawarma', 929.0, 69.0, 42.0, 52.0, 0.0, 0, 100, 791, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('double junior chicken', 500.0, 44.0, 20.0, 28.0, 5.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('junior chicken', 370.0, 36.0, 13.0, 20.0, 5.0, 0, 100, 377, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('mcdonalds large fries', 560.0, 74.0, 6.0, 27.0, 0.0, 0, 100, 360, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('parmesan fries', 420.0, 47.0, 5.0, 23.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('double hamburger', 340.0, 31.0, 21.0, 14.0, 6.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes sandwich', 600.0, 55.0, 25.0, 33.0, 3.0, 0, 100, 619, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes fries', 268.0, 33.0, 4.0, 14.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes popcorn shrimp', 390.0, 28.0, 14.0, 25.0, 0.0, 0, 100, 539, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes blackened sauce', 118.0, 2.0, 1.0, 13.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes mustard', 95.0, 5.0, 1.0, 9.0, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes wicked chicken', 830.0, 72.0, 35.0, 44.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes chicken breast', 380.0, 16.0, 35.0, 20.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes tenders', 148.0, 9.7, 12.6, 7.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes large fries', 804.0, 97.0, 10.0, 42.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes mashed', 330.0, 54.0, 9.0, 12.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('popeyes mac and cheese', 655.0, 70.0, 24.0, 28.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle tortilla', 320.0, 50.0, 8.0, 9.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle chicken', 180.0, 0.0, 32.0, 7.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle rice', 210.0, 36.0, 4.0, 6.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle beans', 130.0, 22.0, 8.0, 1.5, 2.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle cheese', 110.0, 1.0, 6.0, 8.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle sour cream', 110.0, 2.0, 2.0, 9.0, 2.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chipotle guac', 230.0, 8.0, 2.0, 22.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('aw papa burger', 580.0, 37.0, 34.0, 33.0, 8.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('aw habanero chicken', 550.0, 51.0, 21.0, 33.0, 8.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('aw fries', 340.0, 51.0, 6.0, 14.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('aw onion rings', 350.0, 45.0, 5.0, 16.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('wendys_lemonade_medium', 330.0, 86.0, 0.0, 0.0, 81.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('wendys_junior_bacon', 380.0, 25.0, 19.0, 22.0, 5.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('wendys_chili_fries', 500.0, 53.0, 16.0, 25.0, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('fries', 320.0, 41.0, 3.4, 15.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('south st bacon burger', 724.0, 42.0, 43.0, 41.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('south st fries', 430.0, 57.0, 6.0, 20.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('keg sliders', 735.0, 54.0, 39.0, 40.0, 8.0, 0, 358, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('keg prime rib', 721.0, 18.0, 75.0, 40.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('keg twice baked', 452.0, 52.0, 9.0, 24.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('salmon sashimi', 35.0, 0.0, 4.0, 2.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chicken dumpling', 40.0, 2.5, 2.0, 2.5, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cheese wonton', 57.0, 5.0, 2.0, 3.5, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('shrimp tempura', 58.0, 3.5, 3.7, 3.1, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('shrimp tempura roll', 64.0, 8.0, 2.5, 2.5, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chinese chicken', 254.0, 22.0, 10.0, 15.0, 10.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('chinese spring roll', 215.0, 27.0, 4.0, 10.0, 5.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('red lobster biscuit', 160.0, 16.0, 3.0, 10.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('red lobster seafood dip', 1170.0, 92.0, 36.0, 74.0, 7.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('red lobster pizza', 700.0, 59.0, 39.0, 35.0, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('red lobster loaded fries', 770.0, 72.0, 22.0, 44.0, 4.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('the works patty', 370.0, 1.0, 44.0, 22.0, 0.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('the works multigrain', 270.0, 40.0, 7.0, 8.0, 3.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('the works man cave', 346.0, 23.0, 14.0, 25.0, 16.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('the works fries', 620.0, 80.0, 9.0, 31.0, 1.0, 0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('tim_hortons_belt', 530.0, 61.0, 24.0, 24.0, 11.0, 0, 100, 0, 0);


-- Spices
---------

insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, sodium, fundamental_level) values ('salt', 0.0, 0.0, 0.0, 0.0, 0.0, 0, 100, 17, 2.2, 40, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, potassium, fundamental_level) values ('KCL', 0.0, 0.0, 0.0, 0.0, 0.0, 0, 100, 176, 52.4, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('pepper', 255.0, 65.0, 11.0, 3.0, 1.0, 0, 100, 0, 0.044, 1.26, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('oregano', 265.0, 48.0, 9.0, 4.3, 4.0, 0, 100, 162, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('garlic powder', 331.0, 68.0, 13.0, 1.0, 2.0, 0, 100, 107, 0.026, 1.101, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('onion powder', 347.0, 75.0, 10.0, 1.0, 35.0, 0, 100, 152, 0.053, 0.943, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, density, sodium, potassium, fundamental_level) values ('chili flakes', 307.0, 28.0, 12.0, 16.0, 10.0, 0, 100, 300, 0.357, 0.03, 1.9, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('habanero powder', 335.0, 66.0, 16.0, 2.2, 42.0, 0, 100, 624, 0.055, 2.855, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('ancho powder', 281.0, 29.0, 12.0, 8.0, 0.0, 0, 100, 460, 0.043, 2.411, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('paprika', 289.0, 19.0, 15.0, 13.0, 10.0, 0, 100, 141, 0.034, 2.344, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('chipotle powder', 300.0, 35.0, 13.0, 6.0, 30.0, 0, 100, 526, 0.01, 2.15, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('thai chilies', 40.0, 9.0, 2.0, 0.0, 5.0, 0, 100, 0, 0.009, 0.322, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('basil', 251.0, 41.0, 14.0, 4.0, 2.0, 0, 100, 500, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('cinnamon', 247.0, 80.0, 4.0, 3.0, 2.0, 0, 100, 110, 0.01, 0.431, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('cocoa powder', 228.0, 58.0, 20.0, 14.0, 2.0, 0, 100, 230, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('cumin', 375.0, 33.0, 18.0, 22.0, 2.0, 0, 100, 169, 0.168, 1.788, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('coriander', 298.0, 13.0, 12.0, 18.0, 0.0, 0, 100, 64, 0.035, 1.267, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('sriracha', 93.0, 18.6, 2.0, 1.0, 15.0, 0, 100, 0, 1.5, 0.4, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('light soy', 60.0, 5.0, 11.0, 0.0, 2.0, 0, 100, 0, 2.4, 0.212, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('soy sauce', 60.0, 5.0, 11.0, 0.0, 2.0, 0, 100, 0, 6.4, 0.212, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, fundamental_level) values ('maggi', 66.0, 6.0, 11.0, 0.0, 4.0, 0, 100, 0, 9.6, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('worcestershire', 78.0, 19.0, 0.0, 0.0, 10.0, 0, 100, 0, 0.98, 0.8, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, fundamental_level) values ('vanilla extract', 290.0, 12.6, 0.0, 0.0, 12.6, 35.0, 100, 0, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, fundamental_level) values ('mustard', 0.0, 0.0, 0.0, 0.0, 0.0, 0, 100, 0, 0.88, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('fennel', 345.0, 40.0, 16.0, 15.0, 0.0, 0, 100, 180, 0.088, 1.694, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('sage', 315.0, 40.0, 11.0, 13.0, 2.0, 0, 100, 210, 0.011, 1.07, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('nutmeg', 525.0, 44.0, 6.0, 36.0, 28.0, 0, 100, 246, 0.016, 0.35, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('turmeric', 354.0, 60.0, 8.0, 10.0, 3.0, 0, 100, 0, 0.038, 2.525, 0);
insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('cloves', 323.0, 55.0, 6.0, 20.0, 2.0, 0, 100, 0, 0.243, 1.102, 0);

insert into foods (name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('kefir', 64.0, 4.8, 3.2, 3.2, 4.8, 0, 100, 0, 0.05, 0.16, 0);

-- Pantry
---------

insert into foods(name, cals, carbs, protein, fat, sugar, alcohol, mass, price, sodium, potassium, fundamental_level) values ('Pantry', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5);



-- =========
-- Minerals
-- =========

-- Wholesome
------------
update foods set calcium=0.151, phosphorus=0.099, iron=0.0037, magnesium=0.023, zinc=0.0007 where name='bread' and fundamental_level=0;
update foods set calcium=0.021, phosphorus=0.189, iron=0.0013, magnesium=0.053, zinc=0.0014 where name='pasta' and fundamental_level=0;
update foods set calcium=0.015, phosphorus=0.07, iron=0.0011, magnesium=0.028, zinc=0.0004 where name='potatoes' and fundamental_level=0;
update foods set calcium=0.055, phosphorus=0.156, iron=0.0013, magnesium=0.027, zinc=0.001 where name='rice' and fundamental_level=0;
update foods set calcium=0.0265, phosphorus=0.096, iron=0.0009, magnesium=0.006, zinc=0.0006 where name='eggs' and fundamental_level=0;
update foods set calcium=0.043, phosphorus=0.358, iron=0.0019, magnesium=0.154, zinc=0.0029 where name='peanut butter' and fundamental_level=0;
update foods set calcium=0.27, phosphorus=0.523, iron=0.0037, magnesium=0.303, zinc=0.003 where name='almond butter' and fundamental_level=0;
update foods set calcium=0.015, phosphorus=0.108, iron=0.0046, magnesium=0.022, zinc=0.0007 where name='flour' and fundamental_level=0;
update foods set calcium=0.015, phosphorus=0.108, iron=0.0046, magnesium=0.022, zinc=0.0007 where name='flour2' and fundamental_level=0;
update foods set calcium=0.01, phosphorus=0.024, iron=0.0003, magnesium=0.011, zinc=0.0002 where name='tomatoes' and fundamental_level=0;
update foods set calcium=0.033, phosphorus=0.035, iron=0.0003, magnesium=0.012, zinc=0.0002 where name='carrots' and fundamental_level=0;
update foods set calcium=0.016, phosphorus=0.575, iron=0.0055, magnesium=0.251, zinc=0.0064 where name='pine nuts' and fundamental_level=0;
update foods set calcium=0.04, phosphorus=0.024, iron=0.0002, magnesium=0.011, zinc=0.0001 where name='celery' and fundamental_level=0;
update foods set calcium=0.016, phosphorus=0.034, iron=0.0006, magnesium=0.043, zinc=0.0003 where name='ginger' and fundamental_level=0;
update foods set calcium=0.023, phosphorus=0.029, iron=0.0002, magnesium=0.01, zinc=0.0002 where name='onion' and fundamental_level=0;
update foods set calcium=0.023, phosphorus=0.029, iron=0.0002, magnesium=0.01, zinc=0.0002 where name='red onion' and fundamental_level=0;
update foods set calcium=0.181, phosphorus=0.153, iron=0.0017, magnesium=0.025, zinc=0.0014 where name='garlic' and fundamental_level=0;
update foods set calcium=0.009, phosphorus=0.212, iron=0.0019, zinc=0.0036 where name='striploin steak' and fundamental_level=0;
update foods set calcium=0.009, phosphorus=0.212, iron=0.0019, zinc=0.0036 where name='prime rib' and fundamental_level=0;
update foods set calcium=0.012, iron=0.002, magnesium=0.021, zinc=0.004 where name='lamb leg' and fundamental_level=0;
update foods set calcium=0.022, iron=0.0013, magnesium=0.024, zinc=0.0032 where name='pork shoulder' and fundamental_level=0;
update foods set calcium=0.006, phosphorus=0.288, iron=0.0007, magnesium=0.021, zinc=0.0019 where name='black forest ham' and fundamental_level=0;
update foods set calcium=0.015, phosphorus=0.252, iron=0.0003, magnesium=0.03, zinc=0.0004 where name='salmon' and fundamental_level=0;
update foods set calcium=0.015, phosphorus=0.228, iron=0.001, magnesium=0.029, zinc=0.001 where name='chicken breast' and fundamental_level=0;
update foods set calcium=0.01, phosphorus=0.168, iron=0.001, magnesium=0.024, zinc=0.0019 where name='chicken thighs' and fundamental_level=0;
update foods set calcium=0.113, phosphorus=0.091, iron=0, magnesium=0.01, zinc=0.0004 where name='milk' and fundamental_level=0;
update foods set calcium=0.113, phosphorus=0.091, iron=0, magnesium=0.01, zinc=0.0004 where name='milk 1' and fundamental_level=0;
update foods set calcium=0.113, phosphorus=0.091, iron=0, magnesium=0.01, zinc=0.0004 where name='milk 2' and fundamental_level=0;
update foods set calcium=0.113, phosphorus=0.091, iron=0, magnesium=0.01, zinc=0.0004 where name='milk homo' and fundamental_level=0;
update foods set calcium=0.225 where name='kefir' and fundamental_level=0;
update foods set calcium=0.113, phosphorus=0.091, iron=0, magnesium=0.01, zinc=0.0004 where name='buttermilk' and fundamental_level=0;
update foods set calcium=0.065, phosphorus=0.062, iron=0, magnesium=0.007, zinc=0.0002 where name='cream' and fundamental_level=0;
update foods set calcium=0.024, phosphorus=0.024, iron=0, magnesium=0.002, zinc=0.0001 where name='butter' and fundamental_level=0;
update foods set calcium=0.505, phosphorus=0.354, iron=0.0004, magnesium=0.02, zinc=0.0029 where name='mozzarella' and fundamental_level=0;
update foods set calcium=1.109, phosphorus=0.729, iron=0.0009, magnesium=0.038, zinc=0.0039 where name='parmigiano' and fundamental_level=0;
update foods set calcium=0.493, phosphorus=0.337, iron=0.0007, magnesium=0.019, zinc=0.0029 where name='feta' and fundamental_level=0;
update foods set calcium=0.791, phosphorus=0.567, iron=0.0002, magnesium=0.038, zinc=0.0044 where name='jarlsberg' and fundamental_level=0;
update foods set calcium=0.007, magnesium=0.006 where name='lemon juice' and fundamental_level=0;

-- Canned / Packaged
--------------------
update foods set calcium=0.15, phosphorus=0.2, iron=0.007, magnesium=0.12, zinc=0.002 where name='dried red beans' and fundamental_level=0;
update foods set calcium=0.01, phosphorus=0.024, iron=0.0003, magnesium=0.011, zinc=0.0002 where name='canned tomatoes' and fundamental_level=0;
update foods set calcium=0.024, phosphorus=0.077, iron=0.0035, magnesium=0.022, zinc=0.0007 where name='frozen peas' and fundamental_level=0;
update foods set calcium=0.00167, iron=0.000125 where name='popcorn chicken' and fundamental_level=0;
update foods set calcium=0.04, phosphorus=0.051, iron=0.0018, magnesium=0.01, zinc=0.00032 where name='kirkland croissants' and fundamental_level=0;
update foods set calcium=0.0012, phosphorus=0.078, iron=0.0002, magnesium=0.005, zinc=0.0005 where name='bacon' and fundamental_level=0;

-- Spices
---------
update foods set calcium=0.437, phosphorus=0.173, iron=0.0289, magnesium=0.194, zinc=0.0014 where name='pepper' and fundamental_level=0;
update foods set calcium=0.08, phosphorus=0.417, iron=0.0027, magnesium=0.058, zinc=0.0026 where name='garlic powder' and fundamental_level=0;
update foods set calcium=0.363, phosphorus=0.34, iron=0.0026, magnesium=0.122, zinc=0.0023 where name='onion powder' and fundamental_level=0;
update foods set calcium=0.0177, phosphorus=0.345, iron=0.0236, magnesium=0.185, zinc=0.0041 where name='paprika' and fundamental_level=0;
update foods set calcium=0.014, phosphorus=0.043, iron=0.001, magnesium=0.023, zinc=0.0003 where name='thai chilies' and fundamental_level=0;
update foods set calcium=1.002, phosphorus=0.064, iron=0.0083, magnesium=0.06, zinc=0.0018 where name='cinnamon' and fundamental_level=0;
update foods set calcium=0.931, phosphorus=0.499, iron=0.0664, magnesium=0.366, zinc=0.0048 where name='cumin' and fundamental_level=0;
update foods set calcium=0.107, phosphorus=0.06, iron=0.0053, magnesium=0.013, zinc=0.0002 where name='worcestershire' and fundamental_level=0;
update foods set calcium=1.196, iron=0.019, magnesium=0.385, zinc=0.0037 where name='fennel' and fundamental_level=0;
update foods set calcium=1.652, iron=0.028, magnesium=0.428, zinc=0.0047 where name='sage' and fundamental_level=0;
update foods set calcium=0.184, iron=0.003, magnesium=0.183, zinc=0.0021 where name='nutmeg' and fundamental_level=0;
update foods set calcium=0.183, phosphorus=0.268, iron=0.041, magnesium=0.193, zinc=0.0043 where name='turmeric' and fundamental_level=0;
update foods set calcium=0.646, phosphorus=0.105, iron=0.0087, magnesium=0.264, zinc=0.0011 where name='cloves' and fundamental_level=0;
