\set ON_ERROR_STOP 1

create table foods (id serial primary key,
    created_at timestamp not null default now(),
    updated_at timestamp,

    cals double precision not null,
    carbs double precision not null,
    protein double precision not null,
    fat double precision not null,

    sugar double precision not null,
    alcohol double precision not null default 0,
    water double precision not null default 0,

    potassium double precision not null default 0,
    sodium double precision not null default 0,
    calcium double precision not null default 0,
    magnesium double precision not null default 0,
    phosphorus double precision not null default 0,
    iron double precision not null default 0,
    zinc double precision not null default 0,

    name varchar(40) not null,
    mass double precision not null default 100,
    price double precision not null default 0,
    density double precision not null default -1,
    sale_price double precision,
    cook_ratio double precision,
    fundamental_level integer default 0,
    "date" date unique
);

create table iterations (id serial primary key,
    created_at timestamp not null default now(),
    updated_at timestamp,

    base_recipe_id integer references foods,
    derived_recipe_id integer references foods,
    unique(derived_recipe_id)
);


create view groceries as select * from foods where fundamental_level = 0;
create view recipes as select * from foods where fundamental_level = 1;


create table ingredients (id serial primary key,
    created_at timestamp not null default now(),
    updated_at timestamp,

    base_food_id integer references foods not null,
    quantity double precision not null default 1,
    units integer not null default 0, -- Display purposes only; 0=count, 1=grams
    belongs_to integer references foods on delete cascade not null,
    list_position integer not null,
    sale_price double precision,
    hidden boolean not null default 'f',
    name varchar(60),
    unique (base_food_id, belongs_to),
    unique (list_position, belongs_to) deferrable initially deferred
);

-- "([^"]+)":\s*\[\s*"([\d.]+)",\s*"([\d.]+)",\s*"([\d.]+)",\s*"([\d.]+)",\s*"([\d.]+)"],
-- insert into foods (name, cals, carbs, protein, fat, sugar, fundamental) values ('\1', \2, \3, \4, \5, \6, 't');
