\set ON_ERROR_STOP 1

create or replace function set_timestamp_created()
    returns trigger as
    $$
    begin
        NEW.created_at = NOW();
        return NEW;
    end;
    $$
language plpgsql;

create or replace function set_timestamp_updated()
    returns trigger as
    $$
    begin
        NEW.updated_at = NOW();
        return NEW;
    end;
    $$
language plpgsql;


create trigger on_foods_insert before insert on foods
    for each row
        execute procedure set_timestamp_created();

create trigger on_foods_update before update on foods
    for each row
        execute procedure set_timestamp_updated();


create trigger on_ingredients_insert before insert on ingredients
    for each row
        execute procedure set_timestamp_created();

create trigger on_ingredients_update before update on ingredients
    for each row
        execute procedure set_timestamp_updated();
