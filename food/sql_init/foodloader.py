# pylint: disable=undefined-variable

import json
import glob

from models import *

def load():
    foods = session.query(Food).all()
    return { food.name.replace(" ", "_"): food for food in foods }
