#!/usr/bin/env python

#pylint: disable=undefined-variable

import re
import datetime
import sys
import os

os.path.dirname(__file__)

sys.path += [os.path.dirname(os.path.dirname(__file__))]


from sql_init.foodloader import load
from models import *

from application_factory import config

Date = datetime.date

try:
    drained_fat
except NameError:
    drained_fat = Food("drained fat", cals=884, fat=100)
    drained_fat.save()

def add(*foods, name=""):
    recipe = Recipe(name)
    for food in foods:
        recipe.add_or_update(food)
    # print(recipe.to_dict()["self"])
    recipe.save()
    return recipe

def to_recipe(*foods, name="", with_alc=False):
    return add(*foods, name=name)

def to_day(date, *foods, with_alc=False):
    daily_log = DailyLog(date)
    for i in foods:
        f = i.times(1, units=-1) if isinstance(i, Food) else i
        def matches(a, b):
            if isinstance(a, Ingredient): a = a.base_food
            if isinstance(b, Ingredient): b = b.base_food
            if a.id is None or b.id is None:
                return False
            return a.id == b.id
        existing = [x for x in daily_log.ingredients if matches(x, f)]
        if existing:
            assert len(existing) == 1, (existing, f)
            print("Merging:", f)
            existing[0].quantity += f.quantity
            if existing[0].units == f.units == -1:
                existing[0].units = 0
        else:
            daily_log.add(f)
    print(daily_log.to_dict()["self"], daily_log.name)
    daily_log.save()
    assert daily_log.date == date, f"Expected {daily_log.date} but got {date} instead"
    return daily_log

def add_no_diff(i1, i2):
    if not isinstance(i1, RecipeQuickadd):
        if isinstance(i1, Food):
            i1 = i1.times(1, units=-1)
        i1 = RecipeQuickadd(i1)
        i1.add_hidden(i2)
        i1.save()
    else:
        i1.add_hidden(i2)
    print("add_no_diff:", i1)
    return i1

def combine(i1, i2, grams):
    if not isinstance(i1, RecipeQuickadd):
        if isinstance(i1, Food):
            i1 = i1.times(1, units=-1)
        i1 = RecipeQuickadd(i1)
        i1.add(i2.grams(grams))
        i1.save()
    else:
        i1.add(i2.grams(grams))
    print("Combined:", i1)
    return i1

def drain_fat(i1, amount):
    return i1.add_no_diff(drained_fat.grams(amount))

Ingredient.add_no_diff = add_no_diff
Ingredient.combine = combine
Ingredient.drain_fat = drain_fat
Food.add_no_diff = add_no_diff
Food.combine = combine
Food.drain_fat = drain_fat

if __name__ == "__main__":
    globals().update(load())

    def oat_whey_pb(o, w, pb, m=0, *args):
        name = " / ".join(map(lambda i: ("oat", "whey", "pb")[i], filter(lambda i: (o, w, pb)[i], range(3)))) + " shake" + " w/ milk"*bool(m)
        name = name[0].upper() + name[1:]
        return add(oats.grams(o), whey2.grams(w), peanut_butter.grams(pb), milk.grams(m), name=name)

    def pizza_w_mozza(pizza, mozza):
        pizza1 = add(pizza, mozzarella.grams(mozza), name=pizza.name + " w/ mozza" * bool(mozza))
        def slices(n):
            return pizza1.times(n/6, name="{} slices {}".format(n, pizza1.name))
        pizza1.slices = slices
        return pizza1

    def pizza_w_italiano(pizza, ital):
        pizza1 = add(pizza, italiano.grams(ital), name=pizza.name + " w/ italiano" * bool(ital))
        def slices(n):
            return pizza1.times(n/6, name="{} slices {}".format(n, pizza1.name))
        pizza1.slices = slices
        return pizza1

    def omelette_w_mozza(num_eggs, mozza):
        return add(eggs.times(num_eggs), mozzarella.grams(mozza), butter.grams(6), name="Omelette w/ mozza")
    def omelette_w_italiano(num_eggs, ital):
        return add(eggs.times(num_eggs), italiano.grams(ital), butter.grams(6), name="Omelette w/ italiano")

    # assert oat_whey_pb(120,20,44, 597) == add(oats.grams(120), whey2.grams(20), peanut_butter.grams(44), milk.grams(597), name="Oat / whey / pb shake w/ milk")
    # assert oat_whey_pb(0,20,44) == add(whey2.grams(20), peanut_butter.grams(44), name="Whey / pb shake")
    # assert pizza_w_mozza(bacon_cheeseburger, 120).slices(3) == add(bacon_cheeseburger, mozzarella.grams(120)).times(1/2, name="3 slices bacon cheeseburger w/ mozza")
    # assert pizza_w_mozza(meat_pizza, 0).slices(1) == meat_pizza.times(1/6, name="1 slices meat pizza")

    alfredo = add(
        butter.grams(32),
        olive_oil.grams(29),
        garlic.grams(21),
        cream.grams(250),
        milk.grams(150),
        parmigiano.grams(75),
        asiago.grams(75),
        mozzarella.grams(40),
        name="Alfredo sauce"
    )

    def oat_whey_pb(o, w, pb, m=0, *args):  # pylint: disable=function-redefined
        name = " / ".join(map(lambda i: ("oat", "whey", "pb")[i], filter(lambda i: (o, w, pb)[i], range(3)))) + " shake" + " w/ milk"*bool(m)
        name = name[0].upper() + name[1:]
        return add(oats.grams(o), whey.grams(w), peanut_butter.grams(pb), milk.grams(m), name=name)



    meat_sauce = add(
        canned_tomatoes.grams(796*2),
        onion.grams(1193 - 735),
        olive_oil.grams(20),
        butter.grams(20),
        garlic.grams(30),
        ground_beef_lean.grams(454),
        name="Meat sauce"
    )
    meat_sauce.mass = 2247

    rice_chili_soup = add(
        onion.grams(300),
        butter.grams(20),
        olive_oil.grams(20),
        ground_beef_extra_lean.grams(500),
        rice.grams(300),
        canned_tomatoes.grams(540),
        corn.grams(341),
        black_beans.grams(540),
        name="Mexican rice soup"
    )
    rice_chili_soup.name="Rice chili semisoup"
    rice_chili_soup.mass = 185 + 660 + 690 + 650 + 660 + 213

    chicken_parm_soup = add(
        onion.grams(385),
        butter.grams(27),
        olive_oil.grams(22),
        garlic.grams(24),
        garlic_powder.grams(12),
        chili_flakes.grams(5),
        canned_tomatoes.grams(400),
        chicken_breast.sale().grams(375),
        pasta.grams(250),
        italiano.grams(120),
        water.grams(950),
        salt.grams(2),
        name="Chicken parmigiano soup"
    )
    chicken_parm_soup.mass = 2065
    chicken_parm_soup.name="Chicken parmigiano soup"

    stew = add(
        onion.times(4),
        olive_oil.grams(20),
        butter.grams(23),
        garlic.grams(40),
        beef_chuck.grams(817),
        flour2.grams(100),
        carrots.grams(170).free(),
        garlic_powder.grams(15),
        salt.grams(12),
        chili_flakes.grams(5),
        canned_tomatoes.grams(400),
        name="Beef stew"
    )
    stew.mass = 4500 + 452 - 2927 + 980

    dominos_equivalent = add(
        Food(cals=180, carbs=32, protein=6, fat=2.5, sugar=1, name="crust"),
        Food(cals=28, carbs=3, protein=1, fat=0, sugar=1, name="sauce"),
        Food(cals=70, carbs=1, protein=6, fat=5, sugar=0, name="cheese"),
        Food(cals=25, carbs=0, protein=3, fat=5, sugar=0, name="bacon"),
        Food(cals=50, carbs=1, protein=3, fat=4.5, sugar=0, name="sausage"),
        Food(cals=35, carbs=0, protein=2, fat=4, sugar=0, name="pepperoni"),
        name="Delivery pizza"
    )

    pasta_sauce = add(
        onion.grams(1205 - 735),
        butter.grams(30),
        olive_oil.grams(20),
        garlic.grams(40),
        canned_tomatoes.grams(796 * 2),
        garlic_powder.grams(15),
        chili_flakes.grams(12),
        salt.grams(11),
        oregano.grams(6.5),
        name="Pasta sauce"
    )
    pasta_sauce.mass = 4710 - 2927


    posta = add(pasta.grams(200), pasta_sauce.grams(300), chicken_thighs.sale().grams(340), butter.grams(26), olive_oil.grams(15), name="Pasta w/ chicken breast and sauce")
    posta.name = "Pasta w/ chicken breast and sauce"
    posta.mass = 3892 - 2927

    foods10 = [
        add(eggs.times(4), bacon.times(4), drained_fat.grams(48), onion.grams(150), name="The Breakfast"),
        add(rice.grams(120), stew.grams(300), name="Rice w/ beef stew"),
        milk_2.grams(1350),
        posta.grams(645),
    ]
    to_day(Date(2018, 11, 10), *foods10)

    homemade_bread = add(
        flour2.grams(400),
        water.grams(150),
        butter.grams(10),
        honey.grams(20),
        eggs.times(2),
        name="Egg bread"
    )

    posta2 = add(pasta.grams(200), pasta_sauce.grams(300), chicken_breast.sale().grams(323), butter.grams(35), olive_oil.grams(20), name="Pasta w/ chicken breast and sauce")
    posta2.mass = 3847 - 2927
    posta2.name = "Pasta w/ chicken breast and sauce"
    foods11 = [
        posta.grams(posta.mass - 645),
        milk_2.grams(1350),
        oat_whey_pb(34, 21, 98),
        homemade_bread.times(1/4),
        posta2.grams(400)
    ]
    to_day(Date(2018, 11, 11), *foods11)

    foods12 = [
        add(eggs.times(4), bacon.times(4), drained_fat.grams(53), onion.grams(89), homemade_bread.times(1/4), butter.grams(10), name="The Breakfast"),
        milk_2.grams(1350),
        oat_whey_pb(90, 10, 70),
        posta2.grams(350),
    ]
    to_day(Date(2018, 11, 12), *foods12)

    foods13 = [
        posta2.grams(posta2.mass - 350 - 400),
        meat_pizza_gv.times(2/3),
        milk_2.grams(1350),
        oat_whey_pb(30, 20, 80),
        chicken_parm_soup.grams(470),
    ]
    to_day(Date(2018, 11, 13), *foods13)

    if config["ENVIRONMENT"] == "test":
        """Only load a little bit of data; makes tests faster"""
        pantry = Pantry.instance()
        pantry.add(homemade_bread)
        pantry.save()
        sys.exit(0)



    foods14 = [
        meat_pizza_gv.times(1/3),
        milk_2.grams(1350),
        sausage.times(2), drained_fat.grams(25),
        oat_whey_pb(50, 30, 60),
        add(homemade_bread.times(1/4), butter.grams(10), name="Egg bread w/ butter"),
        rice_chili_soup.grams(350),
    ]
    to_day(Date(2018, 11, 14), *foods14)

    posta3 = add(
        pasta.grams(230),
        pasta_sauce.grams(300),
        chicken_thighs.sale().grams(303),
        butter.grams(20),
        olive_oil.grams(15),
        name="Pasta w/ chicken thighs"
    )
    posta3.mass = 3894 - 2927
    posta3.name = "Pasta w/ chicken thighs"

    foods15 = [
        rice_chili_soup.grams(300),
        posta3.grams(493),
        milk_2.grams(1350),
        oat_whey_pb(54, 30, 127),
        oat_whey_pb(30, 0, 20),
        # add(homemade_bread.times(3/16), butter.grams(10), name="Egg bread w/ butter")
    ]
    to_day(Date(2018, 11, 15), *foods15)

    foods16 = [
        add(bacon.times(4), drained_fat.grams(51), eggs.times(5), onion.grams(90), name="The Breakfast"),
        oat_whey_pb(50,30,100),
        milk_2.grams(1350),
        posta3.grams(posta3.mass - 493)
    ]
    to_day(Date(2018, 11, 16), *foods16)

    foods17 = [
        pizza_w_italiano(bacon_cheeseburger, 100).slices(3),
        oat_whey_pb(42,20,80),
        milk_2.grams(450),
        add(the_works_patty, the_works_multigrain, the_works_man_cave, the_works_fries, name="Man Cave from The Works w/ fries").set_price(1883)
    ]
    to_day(Date(2018, 11, 17), *foods17)

    pizza = add(
        flour2.grams(200),
        honey.grams(10),
        salt.grams(3),
        pasta_sauce.grams(70),
        italiano.grams(125),
        bacon.times(3), drained_fat.grams(37),
        name="Pizza w/ cheese and bacon"
    )
    pizza.name="Pizza w/ cheese and bacon"

    foods18 = [
        pizza_w_italiano(bacon_cheeseburger, 100).slices(3),
        oat_whey_pb(40, 35, 110),
        milk_2.grams(900),
        pizza.times(1/2)
    ]
    to_day(Date(2018, 11, 18), *foods18)

    posta4 = add(pasta.grams(180), pasta_sauce.grams(200), sausage.times(2), butter.grams(15), name="Pasta w/ sausage")
    posta4.mass = 707

    foods19 = [
        meat_pizza_gv.times(1/2),
        oat_whey_pb(50, 35, 108),
        milk_2.grams(1350),
        posta4.grams(424)
    ]
    to_day(Date(2018, 11, 19), *foods19)

    foods20 = [
        meat_pizza_gv.times(1/2),
        oat_whey_pb(40, 30, 110),
        posta4.grams(707 - 424),
        pizza_w_italiano(bacon_cheeseburger, 105).slices(3)
    ]
    to_day(Date(2018, 11, 20), *foods20)

    foods21 = [
        pizza_w_italiano(bacon_cheeseburger, 105).slices(3),
        oat_whey_pb(50,30,99),
        milk_2.grams(1350),
        oat_whey_pb(60, 10, 50),
    ]
    to_day(Date(2018, 11, 21), *foods21)

    foods22 = [
        bacon.times(3), drained_fat.grams(112 / 7 * 2),
        add(eggs.times(5), butter.grams(6), name="Scrambled eggs"),
        oat_whey_pb(60, 40, 100),
        milk_2.grams(1350),
        add(pasta.grams(180), meat_sauce.grams(230), butter.grams(24), olive_oil.grams(10), name="Pasta w/ meat sauce"),
    ]
    to_day(Date(2018, 11, 22), *foods22)

    foods23 = [
        sausage.times(3), drained_fat.grams(51),
        milk_2.grams(1350),
        oat_whey_pb(30, 30, 100),
        add(pasta.grams(180), pasta_sauce.grams(230), butter.grams(23), olive_oil.grams(7), name="Pasta w/ meat sauce"),
    ]
    to_day(Date(2018, 11, 23), *foods23)

    foods24 = [
        pizza_w_italiano(bacon_cheeseburger, 105).slices(3),
        oat_whey_pb(80, 35, 100),
        milk_2.grams(1350),
        oat_whey_pb(30, 10, 30)
    ]
    to_day(Date(2018, 11, 24), *foods24)

    foods25 = [
        pizza_w_italiano(bacon_cheeseburger, 105).slices(3),
        milk_2.grams(1350),
        add(pasta.grams(180), pasta_sauce.grams(220), sausage.times(2), drained_fat.grams(24), butter.grams(11), name="Pasta w/ sauce and sausage").times(0.5),
        oat_whey_pb(60, 30, 60)
    ]
    to_day(Date(2018, 11, 25), *foods25)

    homemade_bread = add(
        flour2.grams(420),
        water.grams(250),
        butter.grams(25),
        salt.grams(5),
        honey.grams(20),
        name="Bread w/ rosemary"
    )

    foods26 = [
        add(pasta.grams(180), pasta_sauce.grams(220), sausage.times(2), drained_fat.grams(24), butter.grams(11), name="Pasta w/ sauce and sausage").times(0.5),
        milk_2.grams(1350),
        oat_whey_pb(60, 30, 100),
        homemade_bread.times(3/10),
        omelette_w_italiano(4.5, 21),

    ]
    to_day(Date(2018, 11, 26), *foods26)

    posta5 = to_recipe(pasta.grams(300), pasta_sauce.grams(50), meat_sauce.grams(200), chicken_breast.sale().grams(333), butter.grams(35), olive_oil.grams(25), name="Pasta w/ sauce and chicken")
    posta5.mass = 4069 - 2927
    posta5.name = "Pasta w/ sauce and chicken breast"

    foods27 = [
        meat_pizza_gv.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(60, 30, 100),
        posta5.grams(440),
    ]
    to_day(Date(2018, 11, 27), *foods27)

    foods28 = [
        meat_pizza_gv.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(60, 30, 78),
        posta5.grams(500),
    ]
    to_day(Date(2018, 11, 28), *foods28)

    foods29 = [
        posta5.grams(posta5.mass - 440 - 500),
        milk_2.grams(1350),
        oat_whey_pb(62, 30, 100),
        add(homemade_bread.times(3/10), butter.grams(15), name="Bread w/ butter"),
        add(eggs.times(6), italiano.grams(30), onion.times(1/2), butter.grams(6), name="Omelette w/ onion and italiano"),
    ]
    to_day(Date(2018, 11, 29), *foods29)

    croque = to_recipe(
        homemade_bread.times(2/10),
        bacon.times(4), drained_fat.grams(112 / 7 * 4),
        add(flour2.grams(20), butter.grams(25), name="roux"),
        italiano.grams(30),
        name="Croque monsieur"
    )

    foods30 = [
        meat_pizza2.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(60, 30, 100),
        croque,
        oat_whey_pb(30, 10, 0),
    ]
    to_day(Date(2018, 11, 30), *foods30)

    buttermilk_chicken = to_recipe(
        chicken_thighs_w_skin.grams(951 - 146).set_price(500),
        buttermilk.grams(200),
        flour.grams(20),
        butter.grams(25),
        name="Buttermilk chicken"
    )
    buttermilk_chicken.mass=3773 - 2927
    buttermilk_chicken.name = "Cajun buttermilk chicken"

    # Dec 2018

    foods1 = [
        meat_pizza2.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(30, 30, 80),
        # add(pasta.grams(150), meat_sauce.grams(200), butter.grams(15), olive_oil.grams(10), name="Pasta w/ meat sauce"),
        buttermilk_chicken.grams(272),
        add(homemade_bread.times(1/4), butter.grams(20), name="Bread w/ butter"),
    ]
    to_day(Date(2018, 12, 1), *foods1)

    posta6 = to_recipe(
        pasta.grams(150),
        meat_sauce.grams(200),
        chicken_breast.sale().grams(213),
        butter.grams(30),
        olive_oil.grams(15),
        name="Pasta w/ chicken breast"
    )
    posta6.name="Pasta w/ chicken breast"

    pork_rice = to_recipe(
        rice.grams(150),
        butter.grams(30),
        pork_loin.grams(261),
        mixed_veggies.grams(80),
        name="Pork stir fry"
    )

    foods2 = [
        posta6,
        milk_2.grams(1350),
        pork_rice.times(3/4),
    ]
    to_day(Date(2018, 12, 2), *foods2)

    foods3 = [
        meat_pizza_mikes.times(1/2),
        pork_rice.times(1/4),
        milk_2.grams(1350),
        oat_whey_pb(90, 50, 100),
        # add(rice.grams(100), stew.grams(300), name="Rice w/ beef stew"),
        # omelette_w_italiano(6, 50)
    ]
    to_day(Date(2018, 12, 3), *foods3)

    foods4 = [
        meat_pizza_mikes.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(50, 30, 100),
        oat_whey_pb(30, 30, 50),

    ]
    to_day(Date(2018, 12, 4), *foods4)

    meat_sauce2 = to_recipe(
        onion.grams(1227 - 24 - 6 - 735),
        garlic.grams(35),
        butter.grams(30),
        olive_oil.grams(22),
        ground_beef_extra_lean.sale().grams(465),
        ground_pork_lean.sale().grams(350),
        canned_tomatoes.grams(796 * 2),
        oregano.grams(9.5),
        garlic_powder.grams(12),
        name="Meat sauce"
    )
    meat_sauce2.name = "Meat sauce"
    meat_sauce2.mass = 334 + 691 + 647 + 709

    homemade_bread2 = to_recipe(
        flour2.grams(807),
        butter.grams(50),
        honey.grams(40),
        salt.grams(10),
        name="Homemade bread"
    ).times(1/2)
    homemade_bread2.name = "Homemade bread"


    posta7 = add(pasta.grams(200), meat_sauce2.grams(250), butter.grams(30), olive_oil.grams(15), name="Pasta w/ meat sauce")
    posta7.mass = 3580 - 2927

    foods5 = [
        add(rice.grams(100), buttermilk_chicken.grams(295), name="Rice w/ buttermilk chicken"),
        milk_2.grams(1350),
        oat_whey_pb(50, 30, 50),
        posta7.grams(400),
    ]
    to_day(Date(2018, 12, 5), *foods5)

    foods6 = [
        posta7.grams(posta7.mass - 400),
        Food("bowls of turkey chili", cals=224, carbs=20, protein=20, fat=8, sugar=6).times(2),
        oat_whey_pb(50, 30, 100),
        milk_2.grams(1350),
        buttermilk_chicken.grams(112),
        add(homemade_bread2.times(1/4), butter.grams(10), name="Bread w/ butter"),
    ]
    to_day(Date(2018, 12, 6), *foods6)

    foods7 = [
        buttermilk_chicken.grams(137),
        add(homemade_bread2.times(1/4), butter.grams(15), name="Bread w/ butter"),
        add(pasta.grams(180), meat_sauce2.grams(210), chicken_breast.sale().grams(216), butter.grams(26 + 10), olive_oil.grams(20), name="Pasta w/ chicken_breast"),
        milk_2.grams(1350),
        oat_whey_pb(30, 7, 0),
    ]
    to_day(Date(2018, 12, 7), *foods7)

    foods8 = [
        meat_pizza2.times(1/2),
        sausage.times(2),
        milk_2.grams(1350),
    ]
    to_day(Date(2018, 12, 8), *foods8)

    foods11 = [
        meat_pizza_gv,
        milk_2.grams(1350),
        oat_whey_pb(50, 55, 100),
    ]
    to_day(Date(2018, 12, 11), *foods11)

    foods12 = [
        buttermilk_chicken.grams(200),
        oat_whey_pb(50, 50, 100),
        milk_2.grams(1350),
        t_and_t_dumplings.times(12),
    ]
    to_day(Date(2018, 12, 12), *foods12)

    foods13 = [
        t_and_t_dumplings.times(10),
        add(rice.grams(100), stew.grams(300), name="Rice w/ beef stew"),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 100),
        sausage.times(2), drained_fat.grams(28),
    ]
    to_day(Date(2018, 12, 13), *foods13)

    puttanesca = to_recipe(
        canned_tomatoes.grams(796*2),
        garlic.grams(41),
        canned_olives.grams(87 + 111),
        olive_oil.grams(30),
        salt.grams(5),
        chili_flakes.grams(10),
        oregano.grams(8),
        name="Puttanesca"
    )
    puttanesca.mass = 753 + 740 - 60*2 + 250

    posta8 = to_recipe(
        pasta.grams(250),
        chicken_breast.sale().grams(323),
        butter.grams(15),
        puttanesca.grams(250),
        olive_oil.grams(43),
        name="Pasta w/ chicken and puttanesca"
    )
    posta8.mass = 1000
    posta8.name = "Pasta w/ chicken and puttanesca"

    foods14 = [
        sausage.times(2), drained_fat.grams(30),
        homemade_bread2.times(1/4),
        milk_2.grams(1350),
        posta8.grams(700),
    ]
    to_day(Date(2018, 12, 14), *foods14)

    posta9 = to_recipe(
        pasta.grams(180),
        meat_sauce2.grams(212),
        meatballs.grams(200),
        butter.grams(15),
        olive_oil.grams(25),
        name="Pasta w/ meatballs"
    )
    posta9.mass = 720
    posta9.name="Pasta w/ meatballs"

    foods15 = [
        posta8.grams(posta8.mass - 700),
        sausage, drained_fat.grams(15),
        milk_2.grams(500),
        oat_whey_pb(0, 40, 100),
        add(homemade_bread2.times(1/4), butter.grams(15), name="Bread w/ butter"),
        posta9.grams(450),
    ]
    to_day(Date(2018, 12, 15), *foods15)

    foods16 = [
        posta9.grams(720 - 450),
        milk_2.grams(1350),
        # PPC meetup burger
        # Dinner at G&G's
    ]
    to_day(Date(2018, 12, 16), *foods16)

    foods17 = [
        add(rice.grams(102), stew.grams(400), name="Rice w/ beef stew"),
        milk_2.grams(1350),
        omelette_w_italiano(7, 62),
        add(homemade_bread2.times(1/4), butter.grams(15), name="Bread w/ butter"),
        oat_whey_pb(0, 20, 80),
    ]
    to_day(Date(2018, 12, 17), *foods17)

    posta10 = to_recipe(
        pasta.grams(200),
        meat_sauce2.grams(234),
        meatballs.grams(330),
        butter.grams(17),
        olive_oil.grams(35),
        name="Pasta w/ meatballs"
    )

    buttermilk_chicken2 = to_recipe(
        chicken_thighs.sale().grams(415 + 455),
        buttermilk.grams(340),
    )
    buttermilk_chicken2.mass=3811 - 2927
    buttermilk_chicken2.name = "Southwest buttermilk chicken"

    foods18 = [
        posta10.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 100),
    ]
    to_day(Date(2018, 12, 18), *foods18)

    foods19 = [
        posta10.times(1/2),
        milk_2.grams(1350),
        oat_whey_pb(50, 30, 60),
        add(t_and_t_dumplings.times(12), butter.grams(12), name="T&T dumplings")
    ]
    to_day(Date(2018, 12, 19), *foods19)

    # Made new batch of homemade bread (same recipe as homemade bread 2)
    # ==================================================================

    foods20 = [
        add(t_and_t_dumplings.times(10), butter.grams(12), name="T&T dumplings"),
        milk_2.grams(1350),
        oat_whey_pb(50, 30, 100),
        homemade_bread2.times(1/4),
        buttermilk_chicken2.grams(300),
    ]
    to_day(Date(2018, 12, 20), *foods20)

    chicken_parm_soup2 = to_recipe(
        butter.grams(50),
        chicken_breast.sale().grams(472),
        onion.grams(444),
        olive_oil.grams(20),
        oregano.grams(6.3),
        garlic.grams(38),
        garlic_powder.grams(10),
        canned_tomatoes.grams(500),
        chili_flakes.grams(8),
        pasta.grams(392),
        name="Chicken parmigano soup"
    )
    chicken_parm_soup2.name = "Chicken parmigiano soup"
    chicken_parm_soup2.mass = 4820 - 2927 + 1172


    foods21 = [
        add(buttermilk_chicken2.grams(295), homemade_bread2.times(1/4), butter.grams(18), name="Buttermilk chicken and bread"),
        oat_whey_pb(55, 25, 100),
        milk_2.grams(1350),
        chicken_parm_soup2.grams(550),
    ]
    to_day(Date(2018, 12, 21), *foods21)

    posta11 = to_recipe(
        pasta.grams(200),
        meatballs.grams(200),
        meat_sauce2.grams(230),
        butter.grams(20),
        olive_oil.grams(25),
        name="Pasta w/ meatballs"
    )
    posta11.mass = 3731 - 2927
    posta11.name = "Pasta w/ meatballs"

    foods22 = [
        chicken_parm_soup2.grams(685 - 429),
        milk_2.grams(1350),
        buttermilk_chicken2.grams(buttermilk_chicken2.mass - 300 - 295),
        homemade_bread2.times(1/4),
        posta11.grams(550),
    ]
    to_day(Date(2018, 12, 22), *foods22)

    posta12 = to_recipe(
        pasta.grams(220),
        puttanesca.grams(200),
        chicken_breast.sale().grams(253),
        butter.grams(17),
        olive_oil.grams(37),
        name="Pasta w/ chicken and puttanesca"
    )
    posta12.mass = 3770 - 2927
    posta12.name = "Pasta w/ chicken and puttanesca"

    foods27 = [
        pizza_w_italiano(bacon_cheeseburger, 96).slices(3),
        milk_2.grams(1350),
        oat_whey_pb(50, 20, 60),
        posta12.grams(353 + 60),
    ]
    to_day(Date(2018, 12, 27), *foods27)

    posta13 = to_recipe(
        pasta.grams(220),
        meat_sauce2.grams(246),
        meatballs.grams(240),
        butter.grams(25),
        olive_oil.grams(20),
        name="Pasta w/ meatballs and meat sauce"
    )
    posta13.mass = 3767 - 2927
    posta13.name="Pasta w/ meatballs and meat sauce"

    foods30 = [
        pizza_w_italiano(bacon_cheeseburger, 100).slices(3),
        milk_2.grams(1350),
        oat_whey_pb(45, 30, 100),
        posta13.grams(300),
    ]
    to_day(Date(2018, 12, 30), *foods30)

    foods3 = to_day(Date(2019, 1, 3),
        eggs.times(8),
        milk_2.grams(1350),
        posta13.grams(posta13.mass - 300),
        oat_whey_pb(30, 20, 50),
    )

    posta14 = to_recipe(
        pasta.grams(250),
        chicken_breast.sale().grams(252),
        butter.grams(15),
        puttanesca.grams(250),
        olive_oil.grams(30),
        name="Pasta w/ chicken and puttanesca"
    )
    posta14.mass=3804 - 2927
    posta14.name="Pasta w/ chicken and puttanesca"

    foods4 = to_day(Date(2019, 1, 4),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(1),
        oat_whey_pb(50, 30, 120),
        milk_2.grams(1350),
        posta14.grams(323 + 200),
    )

    pot_roast = to_recipe(
        onion.grams(1251 - 735),
        butter.grams(28 + 19),
        garlic.grams(30),
        garlic_powder.grams(20),
        canned_tomatoes.grams(250),
        olive_oil.grams(12),
        top_sirloin_roast.grams(765),
        name="Pot roast"
    )
    pot_roast.mass= 533
    pot_roast.name = "pot roast"

    foods5 = to_day(Date(2019, 1, 5),
        posta14.grams(posta14.mass - 323 - 200),
        oat_whey_pb(60, 30, 100),
        milk_2.grams(1350),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2),
    )

    posta15 = to_recipe(
        pasta.grams(200),
        puttanesca.grams(200),
        olive_oil.grams(20),
        name="Pasta w/ puttanesca"
    )

    foods6 = to_day(Date(2019, 1, 6),
        oat_whey_pb(60, 30, 100),
        milk_2.grams(1350),
        pot_roast.grams(174),
        posta15.times(0.8)
    )

    foods7 = to_day(Date(2019, 1, 7),
        pot_roast.grams(70),
        posta15.times(0.2),
        oat_whey_pb(60, 30, 100),
        add(Food(cals=230, carbs=3, protein=32, fat=10, sugar=0, name="chicken"), Food(cals=140, carbs=23, protein=3, fat=5, sugar=0, name="Potatoes"), Food(cals=250, carbs=34, protein=4, fat=11, sugar=0, name="lentil rice"), Food(cals=35, carbs=2, protein=1, fat=2.5, sugar=0, name="hummus"), name="Villa Madina chicken shawarma").set_price(1034),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(1.5),
    )

    foods8 = to_day(Date(2019, 1, 8),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2.5),
        oat_whey_pb(80, 30, 112),
        milk_2.grams(1350),
        pot_roast.grams(60),
        oat_whey_pb(40, 10, 32),
    )

    foods9 = to_day(Date(2019, 1, 9),
        chicken_parm_soup2.grams(345 + 265),
        oat_whey_pb(80, 30, 120),
        milk_2.grams(1350),
        mikes_bbq_chicken_pizza.times(1/2),
    )

    posta16 = to_recipe(
        meat_sauce2.grams(300),
        pasta.grams(230),
        butter.grams(20),
        olive_oil.grams(20),
        name="Pasta w/ meat sauce"
    )
    posta16.mass=760

    foods10 = to_day(Date(2019, 1, 10),
        mikes_bbq_chicken_pizza.times(1/2),
        oat_whey_pb(65, 30, 120),
        milk_2.grams(1350),
        posta16.grams(285),
        whey.grams(20),
    )

    foods11 = to_day(Date(2019, 1, 11),
        posta16.grams(posta16.mass - 285),
        milk_2.grams(1350),
        oat_whey_pb(100, 35, 120),
        pot_roast.grams(80),
        add(dumplings2.grams(237), butter.grams(15), name="Dumplings"),
    )

    foods12 = to_day(Date(2019,1,12),
        milk_2.grams(1350),
        oat_whey_pb(100, 30, 120),
        pot_roast.grams(pot_roast.mass - 174 - 70 - 60 - 80),
        dumplings2.grams(250),
        rice.grams(80),
    )

    foods13 = to_day(Date(2019, 1, 13),
        milk_2.grams(1350),
        oat_whey_pb(100, 30, 106),
        flank_steak.grams(300),
        potatoes.grams(95),
        add(sliced_bread, butter.grams(15), name="sourdough w/ butter"),
        add(pasta.grams(162), peanut_butter.grams(30), butter.grams(20), name="Asian peanut butter pasta").times(0.6),
    )

    foods14 = to_day(Date(2019, 1, 14),
        chicken_parm_soup2.grams(625),
        oat_whey_pb(100, 40, 120),
        milk_2.grams(1350),
        mikes_bbq_chicken_pizza.times(1/3),
    )

    omelette_w_rice = to_recipe(eggs.times(5), butter.grams(5), rice.grams(40), name="Omelette w/ rice")

    foods15 = to_day(Date(2019, 1, 15),
        mikes_bbq_chicken_pizza.times(2/3),
        oat_whey_pb(100, 40, 120),
        milk_2.grams(1350),
        mikes_bbq_chicken_pizza.times(1/3),

        omelette_w_rice.times(1/3),
    )

    foods16 = to_day(Date(2019, 1, 16),
        omelette_w_rice.times(2/3),
        oat_whey_pb(100, 40, 120),
        milk_2.grams(1350),
        add(pasta.grams(162), peanut_butter.grams(30), butter.grams(20), name="Asian peanut butter pasta").times(0.4),
        # costco chicken pot pie
    )

    foods17 = to_day(Date(2019, 1, 17),
        oat_whey_pb(100, 40, 120),
        milk_2.grams(1350),
        Food(cals=756, carbs=99, protein=42, fat=24, sugar=12, name="3 slices City Pizza pizza"),
        meat_pizza_mikes.times(1/3),
    )

    foods18 = to_day(Date(2019, 1, 18),
        meat_pizza_mikes.times(1/3),
        oat_whey_pb(100, 40, 125),
        milk_2.grams(1350 - 400),
        meat_pizza_mikes.times(1/3),
    )

    foods19 = to_day(Date(2019, 1, 19),
        milk_2.grams(1350 + 400),
        oat_whey_pb(100, 45, 120),
        meat_pizza_mikes.times(2/3),

    )

    foods20 = to_day(Date(2019, 1, 20),
        meat_pizza_mikes.times(1/3),
        milk_2.grams(1350),
        oat_whey_pb(95, 40, 120),
        milk_2.grams(200),
        mikes_bbq_chicken_pizza.times(1/3),
    )

    tuscan_chicken = to_recipe(
        chicken_breast.sale().grams(1508),
        Food(cals=90, carbs=4, protein=2, fat=8, sugar=1, name="PC canned alfredo", mass=60).grams(410).set_price(349),
        butter.grams(40),
        olive_oil.grams(27),
        onion.times(3),
        sundried_tomato.grams(150),
        cream.grams(60),
        name="Tuscan Chicken Alfredo"
    )
    tuscan_chicken.name="Tuscan Chicken Alfredo"
    tuscan_chicken.mass = 554 - 60 +  524 - 60 + 500 - 60 + 305

    foods21 = to_day(Date(2019, 1, 21),
        mikes_bbq_chicken_pizza.times(1/3),
        oat_whey_pb(100, 45, 120),
        milk_2.grams(1350 - 200 + 600),

        mikes_bbq_chicken_pizza.times(1/3),
    )

    posta17 = to_recipe(
        pasta.grams(330),
        tuscan_chicken.grams(305),
        butter.grams(18),
        olive_oil.grams(15),
        name="Pasta w/ chicken alfredo"
    )
    posta17.name="Pasta w/ chicken alfredo"
    posta17.mass = 3885 - 2927

    foods22 = to_day(Date(2019, 1, 22),
        oat_whey_pb(100, 45, 155),
        milk_2.grams(1350 - 600 + 800),
        posta17.grams(450),
    )

    foods23 = to_day(Date(2019, 1, 23),
        oat_whey_pb(100, 45, 155),
        milk_2.grams(1350 - 800 + 1350),
        posta17.grams(posta17.mass - 450 - 130),
    )

    foods24 = to_day(Date(2019, 1, 24),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 + 600),
        posta17.grams(130),
        mikes_bbq_chicken_pizza.times(1/3),
    )

    foods25 = to_day(Date(2019, 1, 25),
        mikes_bbq_chicken_pizza.times(1/3),
        shawarma,
        milk_2.grams(1350 - 600 + 700),
        mikes_bbq_chicken_pizza.times(1/3),
        oat_whey_pb(15, 40, 60).times(3/3),
    )

    foods26 = to_day(Date(2019, 1, 26),
        shawarma,
        milk_2.grams(1350 - 700 + 1350),
        oat_whey_pb(100, 45, 155)
    )

    foods27 = to_day(Date(2019, 1, 27),
        meat_pizza_mikes.times(1/2),
        milk_2.grams(1350 + 200),
        oat_whey_pb(100, 40, 120),
    )

    posta18 = to_recipe(
        pasta.grams(250),
        tuscan_chicken.grams(250),
        cream.grams(57),
        name="Pasta w/ chicken alfredo"
    )
    posta18.name="Pasta w/ chicken alfredo"
    posta18.mass = 3692 - 2927

    foods28 = to_day(Date(2019, 1, 28),
        meat_pizza_mikes.times(1/3),
        milk_2.grams(1350 - 200),
        banana,
        oat_whey_pb(100, 40, 130),
        posta18.grams(300),
        banana,
    )

    foods29 = to_day(Date(2019, 1, 29),
        meat_pizza_mikes.times(1/6),
        banana,
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        posta18.grams(340),
    )

    foods30 = to_day(Date(2019, 1, 30),
        posta18.grams(posta18.mass - 300 - 340),
        banana,
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 120),
        banana,
        add(rice.grams(110), tuscan_chicken.grams(120), name="Rice w/ chicken alfredo"),
        banana
    )

    foods31 = to_day(Date(2019, 1, 31),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1000),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2),
        banana
    )

    foods1 = to_day(Date(2019, 2, 1),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 + 350),
        add(dumplings2.grams(407), butter.grams(10), name="Dumplings").times(12/22),
    )

    posta19 = to_recipe(
        pasta.grams(250),
        tuscan_chicken.grams(225),
        cream.grams(50),
        butter.grams(27),
        name="Pasta w/ chicken alfredo"
    )
    posta19.mass = 3640 - 2927

    foods2 = to_day(Date(2019, 2, 2),
        add(dumplings2.grams(407), butter.grams(10), name="Dumplings").times(10/22),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 135),
        posta19.grams(350),
    )

    foods3 = to_day(Date(2019, 2, 3),
        posta19.grams(posta19.mass - 350),
        milk_2.grams(1350),
        chips.grams(90),
        bread.grams(107),
        ribeye.grams(259 + 57),
        filet_mignon.grams(32),
    )

    foods4 = to_day(Date(2019, 2, 4),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 + 400),
        meat_pizza_mikes.times(1/2),
    )

    posta20 = to_recipe(
        pasta.grams(150),
        peanut_butter.grams(30),
        cream.grams(20),
        # plate: 358
        # pasta: 345
    )
    posta20.mass = 345
    posta20.name = "Pasta w/ peanut sauce"

    foods5 = to_day(Date(2019, 2, 5),
        meat_pizza_mikes.times(1/3),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 - 400 + 800),
        banana,
        posta20.grams(150),
    )

    foods6 = to_day(Date(2019, 2, 6),
        meat_pizza_mikes.times(1/6),
        milk_2.grams(1350 - 800 + 1350),
        banana,
        oat_whey_pb(100, 40, 150),
        posta20.grams(posta20.mass - 150),
        pizza_w_italiano(mikes_bbq_chicken_pizza, 55).times(1/8),
    )

    foods7 = to_day(Date(2019, 2, 7),
        pizza_w_italiano(mikes_bbq_chicken_pizza, 55).times(3/8),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        pizza_w_italiano(mikes_bbq_chicken_pizza, 55).times(2/8),
    )


    posta21 = to_recipe(
        pasta.grams(250),
        chicken_breast.sale().grams(182),
        butter.grams(15),
        puttanesca.grams(200),
        olive_oil.grams(25),
    )
    posta21.name = "Pasta w/ chicken and puttanesca"
    posta21.mass = 306 + 117 + 345

    foods8 = to_day(Date(2019, 2, 8),
        pizza_w_italiano(mikes_bbq_chicken_pizza, 55).times(2/8),
        milk_2.grams(1350),
        banana,
        oat_whey_pb(100, 40, 150),
        posta21.grams(306 + 117),
    )

    posta22 = to_recipe(
        pasta.grams(275),
        tuscan_chicken.grams(227),
        cream.grams(30),
        olive_oil.grams(30),
        butter.grams(15),
    )
    posta22.mass= 3690 - 2927
    posta22.name = "Pasta w/ chicken alfredo"

    foods10 = to_day(Date(2019, 2, 10),
        milk_2.grams(1350 + 500),
        oat_whey_pb(100, 40, 150),
        banana,
        posta22.times(1/2)
    )

    foods11 = to_day(Date(2019, 2, 11),
        posta22.times(1/4),
        milk_2.grams(1350 - 500 + 800),
        oat_whey_pb(67, 40, 150),
        posta22.times(1/4),
        banana,
    )

    foods12 = to_day(Date(2019, 2, 12),
        # korean fried chicken pieces,
        oat_whey_pb(100, 45, 150),
        milk_2.grams(1350 - 800 + 800),
        pizza_w_italiano(bacon_cheeseburger, 105).slices(3),
    )

    foods13 = to_day(Date(2019, 2, 13),
        # korean fried chicken pieces,
        pizza_w_italiano(bacon_cheeseburger, 105).slices(2),
        milk_2.grams(1350 - 800 + 1000),
        oat_whey_pb(100, 40, 150),
        banana,
        pizza_w_italiano(bacon_cheeseburger, 105).slices(1),
    )

    foods14 = to_day(Date(2019, 2, 14),
        oat_whey_pb(100, 40, 130),
        banana,
        milk_2.grams(350 + 1350),
        Food(cals=100, carbs=10, protein=8, fat=1.5, sugar=8, name="Yogurt").times(2),
        # Security Pizza talk
        meat_pizza_mikes.times(1/8),
    )

    foods15 = to_day(Date(2019, 2, 15),
        meat_pizza_mikes.times(3/8),
        banana,
        oat_whey_pb(105, 40, 130),
        milk_2.grams(1350),
        meat_pizza_mikes.times(1/8),
    )

    posta23 = to_recipe(
        pasta.grams(330),
        chicken_breast.sale().grams(251),
        butter.grams(12),
        puttanesca.grams(290),
        olive_oil.grams(30),
    )
    posta23.name = "Pasta w/ chicken and puttanesca"
    posta23.mass = 387 + 3630-2927

    foods16 = to_day(Date(2019, 2, 16),
        meat_pizza_mikes.times(3/8),
        milk_2.grams(1350),
        oat_whey_pb(50, 40, 130),
        posta23.grams(387)
    )

    foods17 = to_day(Date(2019, 2, 17),
        posta23.grams(posta23.mass - 387),
        milk_2.grams(1350),
        oat_whey_pb(30, 30, 60),
        meat_pizza_mikes.times(1/4),
    )

    foods18 = to_day(Date(2019, 2, 18),
        meat_pizza_mikes.times(3/8),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 130),
        omelette_w_italiano(6, 50).times(2/3),
    )

    posta24 = add(pasta.grams(200), puttanesca.grams(150), olive_oil.grams(20), name="Pasta w/ puttanesca")
    posta24.mass = 515

    foods19 = to_day(Date(2019, 2, 19),
        meat_pizza_mikes.times(3/8),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 140),
        omelette_w_italiano(6, 50).times(1/3),
        posta24.grams(150),
    )

    foods20 = to_day(Date(2019, 2, 20),
        posta24.grams(posta24.mass - 150),
        oat_whey_pb(110, 40, 157),
        milk_2.grams(1350),
        omelette_w_italiano(6, 50).times(2/3),
    )

    foods21 = to_day(Date(2019, 2, 21),
        omelette_w_italiano(6, 50).times(1/3),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 110),
        # 4 pieces PizzaTech pizza
    )

    pulled_pork = add(
        pork_tenderloin.grams(1218),
        honey.grams(75),
        onion.times(2),
        garlic.grams(35),
        oregano.grams(5),
        cream.grams(100),
        name="Pulled pork"
    )
    pulled_pork.mass = 1603 - 60*3 + 171*2

    foods22 = to_day(Date(2019, 2, 22),
        shawarma,
        milk_2.grams(1350 - 600), # spilled milk everywhere

        add(
            burger_bun,
            pulled_pork.grams(171),
            mayo.grams(14),
            name="Pulled pork sandwich"
        ).times(2),
    )

    foods23 = to_day(Date(2019, 2, 23),
        shawarma,
        milk_2.grams(1350),
        Food(cals=130, carbs=11, protein=3, fat=7, sugar=1, name="croissants").times(3),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(130),
            mayo.grams(10),
            name="Pulled pork sandwich"
        ).times(1),
        pulled_pork.grams(45),
        oat_whey_pb(30, 40, 50)
    )

    foods24 = to_day(Date(2019, 2, 24),
        Food(cals=130, carbs=11, protein=3, fat=7, sugar=1, name="croissants").times(6),
        milk_2.grams(1350),
        oat_whey_pb(60, 40, 140),
    )

    foods25 = to_day(Date(2019, 2, 25),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(115),
            mayo.grams(10),
            italiano.grams(25),
            name="Pulled pork sandwich"
        ).times(2),
    )

    posta25 = to_recipe(
        meatballs.grams(310),
        pasta.grams(323),
        meat_sauce2.grams(340),
        butter.grams(10),
        olive_oil.grams(20),
        name="Pasta w/ meatballs and meat sauce"
    )
    posta25.mass = 4069 - 2927 + 15
    posta25.name = "Pasta w/ meatballs and meat sauce"

    foods26 = to_day(Date(2019, 2, 26),
        milk_2.grams(1350 + 400),
        oat_whey_pb(100, 40, 150),
        posta25.grams(500),
    )

    foods27 = to_day(Date(2019, 2, 27),
        posta25.grams(posta25.mass/2 - 250),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        posta25.grams(posta25.mass/2 - 250),
    )

    foods28 = to_day(Date(2019, 2, 28),
        milk_2.grams(1350 + 400),
        oat_whey_pb(100, 40, 150),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(115),
            mayo.grams(12),
            italiano.grams(25),
            name="Pulled pork sandwich"
        ).times(2),
    )

    to_day(Date(2019, 3, 1),
        milk_2.grams(1350 - 400 + 400),
        oat_whey_pb(100, 40, 150),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(3),
    )

    posta26 = to_recipe(
        pasta.grams(323),
        chicken_breast.sale().grams(240),
        meat_sauce2.grams(340),
        butter.grams(20),
        olive_oil.grams(30),
        name="Pasta w/ chicken and meat sauce"
    )
    posta26.mass = 4033 - 2927
    posta26.name = "Pasta w/ chicken and meat sauce"

    to_day(Date(2019, 3, 2),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(3),
        milk_2.grams(1350 - 400 + 800),
        posta26.grams(650),
    )

    to_day(Date(2019, 3, 3),
        posta26.grams(posta26.mass - 650),
        milk_2.grams(1350 - 800 + 1350),
        oat_whey_pb(100, 40, 150),
        # Half a 4-meat Famoso pizza
    )

    to_day(Date(2019, 3, 4),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        # Striploin steak,
        # ~80g cheetos
        # Greek salad, 1 piece white bread, big slice of butter
    )

    to_day(Date(2019, 3, 5),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        meatballs.grams(292),
        banana,
        dumplings2.grams(327 - 100),
    )

    to_day(Date(2019, 3, 6),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 165),
        Food(cals=130, carbs=11, protein=3, fat=7, sugar=1, name="croissants").times(3.5),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(158),
            mayo.grams(20),
            butter.grams(6),
            italiano.grams(30),
            name="Pulled pork sandwich"
        ),
    )

    to_day(Date(2019, 3, 7),
        milk_2.grams(1350),
        oat_whey_pb(85, 40, 150),
        Food(cals=130, carbs=11, protein=3, fat=7, sugar=1, name="croissants").times(3),
        # Pizza Tech

    )

    to_day(Date(2019, 3, 8),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        banana,
        Food(cals=130, carbs=11, protein=3, fat=7, sugar=1, name="croissants").times(2),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(1),
    )

    to_day(Date(2019, 3, 11),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        shawarma
    )

    butter_chicken = add(
        chicken_breast.sale().grams(1100),
        yogurt.grams(410),
        canned_tomatoes.grams(550),
        milk_2.grams(160),
        honey.grams(25),
        butter.grams(70),
        name="Butter chicken"
    )

    to_day(Date(2019, 3, 12),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 + 500),
        add(rice.grams(140), butter_chicken.grams(200), butter.grams(20), name="Rice w/ butter chicken")
    )

    to_day(Date(2019, 3, 13),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        add(rice.grams(130), butter_chicken.grams(250), butter.grams(20), name="Rice w/ butter chicken")
    )

    to_day(Date(2019, 3, 14),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        # Pies
        banana,
        add(rice.grams(130), butter_chicken.grams(323), name="Rice w/ butter chicken").times(3/4),
    )

    pasta_sauce = to_recipe(
        butter.grams(30),
        onion.times(3),
        garlic.grams(55),
        canned_tomatoes.grams(796*2 + 146),
        oregano.grams(12),
        chili_flakes.grams(12),
        salt.grams(10),
        garlic_powder.grams(12),
        name="Pasta sauce"
    )

    to_day(Date(2019, 3, 15),
        add(rice.grams(130), butter_chicken.grams(323), name="Rice w/ butter chicken").times(1/4),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        banana,
        dumplings2.grams(230),
        meatballs.grams(180),
    )

    posta27 = to_recipe(
        pasta.grams(300),
        pasta_sauce.grams(350),
        chicken_breast.sale().grams(220),
        butter.grams(15),
        olive_oil.grams(45),
        name="Pasta w/ chicken breast"
    )
    posta27.mass = 3963 - 2927

    to_day(Date(2019, 3, 16),
        oat_whey_pb(100, 40, 150),
        dumplings2.grams(90),
        milk_2.grams(1350),
        posta27.grams(500),
    )

    posta28 = to_recipe(
        pasta.grams(180),
        pasta_sauce.grams(200),
        butter.grams(15),
        olive_oil.grams(15),
        name="Pasta w/ sauce"
    )

    to_day(Date(2019, 3, 19),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350 + 500),
        mikes_bbq_chicken_pizza.times(1/2),
        # posta28
    )

    pizza = to_recipe(
        flour.grams(220),
        butter.grams(20),
        honey.grams(10),
        pasta_sauce.grams(154),
        bacon.times(7), drained_fat.grams(12 * 7),
        italiano.grams(180),
        name="Pizza w/ bacon"
    )
    pizza.name="Pizza w/ bacon"

    to_day(Date(2019, 3, 24),
        add(rice.grams(160), butter_chicken.grams(396), butter.grams(30), name="Rice w/ butter chicken").times(7/10),
        milk_2.grams(1350),
        oat_whey_pb(50, 40, 100),
        pizza.times(1/3),
    )

    to_day(Date(2019, 3, 25),
        milk_2.grams(1350 + 400),
        oat_whey_pb(100, 40, 150),
        banana,
        add(rice.grams(160), butter_chicken.grams(396), butter.grams(30), name="Rice w/ butter chicken").times(3/10),
        add(pasta.grams(150), pasta_sauce.grams(220), butter.grams(10), olive_oil.grams(15), name="Pasta w/ sauce"),
    )

    to_day(Date(2019, 3, 26),
        milk_2.grams(1350 + 500),
        oat_whey_pb(100, 40, 150),
        pizza.times(1/3),
        banana,
        add(dumplings2.grams(200), butter.grams(15), name="Dumplings").times(10/13),
    )

    posta29 = to_recipe(
        pasta.grams(330),
        pasta_sauce.grams(400),
        chicken_breast.sale().grams(293),
        butter.grams(20),
        olive_oil.grams(25),
        name="Pasta w/ chicken breast"
    )
    posta29.name="Pasta w/ chicken breast"
    posta29.mass = 4248 - 2927

    to_day(Date(2019, 3, 27),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 150),
        banana,
        posta29.grams(650),
    )

    to_day(Date(2019, 3, 30),
        mikes_bbq_chicken_pizza.times(1/2),
        milk_2.grams(1350 + 600),
        oat_whey_pb(100, 40, 150),
        posta29.grams(200),
    )

    to_day(Date(2019, 3, 31),
        mikes_bbq_chicken_pizza.times(1/2),
        milk_2.grams(1350 + 750),
        sausage.times(2), drained_fat.grams(25),
        banana,
        dumplings2.grams(400), butter.grams(15),
        # whey.grams(10),
        # add(pasta.grams(170), pasta_sauce.grams(230), butter.grams(15), olive_oil.grams(15), name="Pasta w/ sauce"),
    )

    posta30 = to_recipe(
        pasta.grams(210),
        pasta_sauce.grams(271),
        bacon, drained_fat.grams(12).times(4),
        butter.grams(20),
        name="Pasta w/ bacon and sauce"
    )
    posta30.mass = 713
    posta30.name="Pasta w/ bacon and sauce"

    to_day(Date(2019, 4, 1),
        milk_2.grams(1350 + 800),
        oat_whey_pb(125, 40, 150),
        croissants.times(0.7),

        posta30.grams(350)
    )

    to_day(Date(2019, 4,2),
        milk_2.grams(550 + 1350 + 600),
        oat_whey_pb(120, 40, 150),
        posta30.grams(posta30.mass - 350),
    )

    chicken_parm_soup3 = to_recipe(
        butter.grams(33),
        olive_oil.grams(21),
        onion.grams(600),
        canned_tomatoes.grams(650),
        chicken_breast.sale().grams(330),
        pasta.grams(366),
        italiano.grams(220),
        name="Chicken parmigiano soup"
    )
    chicken_parm_soup3.mass=3530 - 2927 + 2100

    to_day(Date(2019, 4, 3),
        milk_2.grams(750 + 600),
        oat_whey_pb(130, 40, 150),
        sausage, drained_fat.grams(12).times(2),
        banana,
        dumplings2.grams(300)
    )

    to_day(Date(2019, 4, 4),
        milk_2.grams(1350 - 600),
        oat_whey_pb(125, 40, 150),
        dominos_equivalent.times(3),
        chicken_parm_soup3.grams(946 - 500)
    )

    to_day(Date(2019, 4, 5),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1350),
        chicken_parm_soup3.grams(610),
    )

    to_day(Date(2019, 4, 8),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1000 + 1350),
        banana,
        # add(rice.grams(100), stew.grams(250), name='Rice w/ beef stew'),
        omelette_w_italiano(6, 60).times(1/2),
        banana,
    )

    to_day(Date(2019, 4, 9),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1000 + 1350),
        add(rice.grams(140), stew.grams(300), name='Rice w/ beef stew'),
        # bacon, drained_fat.grams(11).times(3)
    )

    to_day(Date(2019, 4, 11),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1350),
        banana,
        croissants,
        pizza_w_italiano(meat_pizza_stuffed, 130).slices(3)
    )

    to_day(Date(2019, 4, 12),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1350 + 300),
        pizza_w_italiano(meat_pizza_stuffed, 130).slices(3)
    )

    to_day(Date(2019, 4, 13),
        oat_whey_pb(145, 20, 50),
        milk_2.grams(1350 - 300),
        dumplings2.grams(375),
        bread.grams(42 + 37),
        butter.grams(10+10+6+8),
        flank_steak.grams((863 - 665 + 130)/0.7)
    )

    jamaican_curry = to_recipe(
        onion.grams(3885 - 3530),
        garlic.grams(40),
        ground_beef_extra_lean.sale().grams(475),
        butter.grams(25),
        olive_oil.grams(10),
        potatoes.grams(450),
        name="Jamaican beef curry"
    )
    jamaican_curry.mass = 4535 - 2927

    to_day(Date(2019, 4, 14),
        milk_2.grams(1350 + 400),
        oat_whey_pb(160, 30, 80),
        bacon, drained_fat.grams(12).times(5),
        jamaican_curry.grams(400),
        add(rice.grams(120), stew.grams(440), name="Rice w/ beef stew").times(0.5)
    )

    to_day(Date(2019, 4, 15),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1350 - 400 + 1350),
        add(rice.grams(120), stew.grams(440), name="Rice w/ beef stew").times(0.5),
        peanut_butter.grams(40)
    )

    to_day(Date(2019, 4, 16),
        oat_whey_pb(130, 40, 150),
        milk_2.grams(1350 + 1000),
        add(bread.grams(150), butter.grams(15), name="Bread w/ butter and sriracha")
    )

    to_day(Date(2019, 4, 17),
        oat_whey_pb(150, 40, 120),
        milk_2.grams(1350 - 1000 + 1350),
        chicken_parm_soup3.grams(400),
        add(bread.grams(183), butter.grams(22), name="Bread w/ butter and sriracha")
    )

    to_day(Date(2019, 4, 18),
        oat_whey_pb(150, 40, 120),
        milk_2.grams(1350 + 200),
        dominos_equivalent.times(2),
        add(bread.grams(150), butter.grams(27), name="Bread w/ butter and sriracha")
    )

    to_day(Date(2019, 4, 19),
        bacon, drained_fat.grams(12).times(5),
        oat_whey_pb(150, 40, 120),
        jamaican_curry.grams(440),
        milk_2.grams(1350 - 200),

    )

    to_day(Date(2019, 4, 20),
        oat_whey_pb(150, 40, 120),
        milk_2.grams(1350),
        chicken_parm_soup3.grams(200),
        # Famoso pizza + spanish chorizo and polenta
    )

    to_day(Date(2019, 4, 21),
        oat_whey_pb(100, 40, 150),
        milk_2.grams(1350),
        chips.grams(60),
        kraken_rum.grams(77),
        potatoes.grams(77),
        prime_rib.grams(270 * 1.1),
        bread.grams(137),
        # lobster tail 372 - 294g
        # probably ~20-30g garlic butter
    )

    to_day(Date(2019, 4, 22),
        oat_whey_pb(120, 40, 150),
        milk_2.grams(1350 + 900),
        dumplings2.grams(300)
    )

    posta31 = add(pasta.grams(173), pasta_sauce.grams(250), butter.grams(15), olive_oil.grams(15), name="Pasta w/ sauce")
    posta31.mass = 2614 - 2050

    to_day(Date(2019, 4, 23),
        oat_whey_pb(130, 40, 130),
        milk_2.grams(450 + 1350),
        # potluck at the office (probably heavy on carbs)
        posta31.grams(400)
    )

    to_day(Date(2019, 4, 24),
        oat_whey_pb(140, 40, 130),
        milk_2.grams(1350 + 400),
        posta31.grams(posta31.mass - 400),
        pizza_w_italiano(bacon_cheeseburger, 100).slices(2)
    )

    to_day(Date(2019, 4, 29),
        oat_whey_pb(140, 40, 130).times(1/2),
        milk_2.grams(1350 * 1.5),
        chicken_parm_soup3.grams(500)
    )

    to_day(Date(2019, 5, 2),
        oat_whey_pb(130, 40, 140),
        milk_2.grams(1350 + 500),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(90),
            italiano.grams(30),
            mayo.grams(10),
            name="Pulled pork sandwich"
        ).times(2),
    )

    posta32 = add(
        pasta.grams(300),
        tuscan_chicken.grams(200),
        butter.grams(36),
        olive_oil.grams(40),
        name="Pasta w/ tuscan_chicken"
    )

    to_day(Date(2019, 5, 3),
        milk_2.grams(1350),
        oat_whey_pb(130, 40, 140),
        posta32.times(412 / 806),
    )


    posta33 = add(
        chicken_thighs.sale().grams(466),
        pasta.grams(350),
        pasta_sauce.grams(500),
        butter.grams(25),
        olive_oil.grams(35),
        name="Pasta w/ chicken thighs and sauce"
    )
    posta33.mass = 4300 - 2927

    to_day(Date(2019, 5, 6),
        milk_2.grams(1350),
        oat_whey_pb(130, 40, 150),
        posta33.grams(600)
    )

    to_day(Date(2019, 5, 7),
        milk_2.grams(1350),
        oat_whey_pb(125, 40, 150),
        posta33.grams(posta33.mass - 600)
    )

    fried_rice = to_recipe(
        rice.grams(250),                  # really should be done night before
        chicken_breast.sale().grams(260), # about right
        eggs.times(3),                    # needs more (maybe 5?)
        olive_oil.grams(30),
        butter.grams(15),
        mixed_veggies.grams(120),         # about right
        name="Fried rice"
    )
    fried_rice.name="Fried rice"
    fried_rice.mass = 4217 - 2927

    to_day(Date(2019, 5, 8),
        milk_2.grams(1350),
        oat_whey_pb(135, 40, 150),
        fried_rice.grams(650)
    )

    to_day(Date(2019, 5, 9),
        milk_2.grams(1350),
        oat_whey_pb(135, 40, 150),
        fried_rice.grams(400),
        dumplings2.grams(300),
    )

    to_day(Date(2019, 5, 10),
        fried_rice.grams(fried_rice.mass - 400 - 650),
        milk_2.grams(1350),
        oat_whey_pb(100, 30, 120),
        dumplings2.grams(140),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(90),
            italiano.grams(30),
            mayo.grams(10),
            name="Pulled pork sandwich"
        ).times(2),
    )

    posta34 = to_recipe(
        chicken_breast.sale().grams(176),
        pasta.grams(200),
        pasta_sauce.grams(250),
        butter.grams(20), olive_oil.grams(30),
        name="Pasta w/ chicken breast"
    )

    to_day(Date(2019, 5, 13),
        milk_2.grams(1350),
        oat_whey_pb(130, 40, 150),
        add(
            burger_bun.times(230 / 160),
            pulled_pork.grams(90),
            italiano.grams(30),
            mayo.grams(10),
            name="Pulled pork sandwich"
        ),
        posta34.times(1/2)
    )

    to_day(Date(2019, 5, 15),
        oat_whey_pb(80, 80, 100),
        milk_2.grams(700),
        omelette_w_italiano(6, 60),
    )

    posta35 = to_recipe(
        pasta.grams(200),
        tuscan_chicken.grams(200),
        butter.grams(30),
        olive_oil.grams(40),
        name="Pasta w/ tuscan_chicken"
    )

    fried_rice2 = to_recipe(
        rice.grams(250),
        chicken_breast.sale().grams(260),
        eggs.times(5),
        olive_oil.grams(40),
        butter.grams(10),
        mixed_veggies.grams(120),
        name="Fried rice"
    )
    fried_rice2.name="Fried rice"
    fried_rice2.mass = 4180 - 2927

    to_day(Date(2019, 5, 21),
        oat_whey_pb(0, 40, 130),
        milk_2.grams(1350),
        fried_rice2.grams(375)
    )

    honey_chicken = to_recipe(
        chicken_thighs.sale().grams(400),
        olive_oil.grams(10),
        butter.grams(30),
        honey.grams(55),
        garlic.grams(25),
        name="Honey garlic chicken"
    )
    honey_chicken.name = "Honey garlic chicken"
    honey_chicken.mass = 3282 - 2927

    to_day(Date(2019, 5, 22),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 130),
        fried_rice2.grams(300),
        honey_chicken.grams(150)
    )

    to_day(Date(2019, 5, 23),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 130),
        honey_chicken.grams(honey_chicken.mass - 150),
        fried_rice2.grams(300),
    )

    to_day(Date(2019, 5, 24),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 130),
        fried_rice2.grams(278),
        meatballs.grams(320)
    )

    stroganoff = to_recipe(
        pasta.grams(340),
        ground_beef_extra_lean.sale().grams(460),
        cheddar.grams(120),
        Food(cals=100, carbs=8, protein=14, fat=1.5, sugar=6, name="Cottage cheese").grams(225),
        cream_cheese.grams(115),
        canned_tomatoes.grams(400),
        garlic.grams(30),
        onion.grams(350),
        butter.grams(10),
        name="Beef stroganoff"
    )
    stroganoff.mass = 4921 - 2927

    to_day(Date(2019, 5, 25),
        milk_2.grams(1350),
        popeyes_tenders.times(5),
        popeyes_blackened_sauce.times(2),
        popeyes_fries.times(1/2),
        chips.grams(50),
        stroganoff.grams(410),
    )

    puttanesca2 = add(
        canned_tomatoes.grams(796*2 + 400),
        olive_oil.grams(10 + 40),
        kalamata.grams(300),
        garlic.grams(51),
        name="Puttanesca"
    )

    posta36 = add(
        puttanesca2.grams(225),
        pasta.grams(150),
        chicken_breast.sale().grams(289),
        olive_oil.grams(40),
        name="Pasta w/ puttanesca and chicken"
    )

    to_day(Date(2019, 5, 31),
        oat_whey_pb(0, 50, 150),
        milk_2.grams(1350),
        posta36.times(3/4),
    )

    to_day(Date(2019, 6, 3),
        oat_whey_pb(0, 50, 170),
        milk_2.grams(1350),
        posta36.times(1/4),
        meatballs.grams(250)
    )

    honey_garlic_chicken = add(
        butter.grams(67),
        olive_oil.grams(20),
        chicken_thighs.sale().grams(1250),
        honey.grams(200),
        garlic.grams(67),
        name="Honey garlic chicken"
    )
    honey_garlic_chicken.mass = 4482 - 2927

    to_day(Date(2019, 6, 5),
        oat_whey_pb(0, 45, 150),
        milk_2.grams(1350),
        omelette_w_italiano(4, 0),
        honey_garlic_chicken.grams(250)
    )

    meatballz = add(
        ground_beef_lean.sale().grams(580),
        garlic_powder.grams(10),
        eggs.times(1),
        name="Meatballs"
    )

    to_day(Date(2019, 6, 7),
        oat_whey_pb(0, 45, 150),
        milk_2.grams(1350),
        honey_garlic_chicken.grams(330),
        meatballz.times(1/3),

    )


    to_day(Date(2019, 6, 8),
        # oat_whey_pb(0, 30, 100),
        milk_2.grams(1350),
        honey_garlic_chicken.grams(200),
        meatballz.times(1/3),
    )

    to_day(Date(2019, 6, 10),
        honey_garlic_chicken.grams(320),
        milk_2.grams(1000),
        oat_whey_pb(0, 30, 100)
    )

    to_day(Date(2019, 6, 11),
        milk_2.grams(1350),
        omelette_w_italiano(6, 70),
        # Swine and Vine:
        # - tartine
        # - duck 3 ways
        # - croque monsieur w/ pork belly
    )

    to_day(Date(2019, 6, 12),
        add(whey.grams(30), almond_butter.grams(50), peanut_butter.grams(50), name="Whey / almond / pb shake"),
        milk_2.grams(1350),
        omelette_w_italiano(6, 70).times(2/3),
    )

    to_day(Date(2019, 6, 13),
        oat_whey_pb(0, 40, 100),
        milk_2.grams(1350),
        omelette_w_italiano(6, 70).times(1/3),
        # add(pasta.grams(80), puttanesca2.grams(100), olive_oil.grams(15), name="Pasta w/ puttanesca")
        almond_butter.grams(50),
    )

    to_day(Date(2019, 6, 14),
        oat_whey_pb(0, 40, 100),
        milk_2.grams(1350),
        add(pasta.grams(180), puttanesca2.grams(220), chicken_breast.grams(304), butter.grams(15), olive_oil.grams(40), name="Pasta w/ puttanesca").times(1/2),
    )

    to_day(Date(2019, 6, 15),
        oat_whey_pb(0, 20, 80),
        milk_2.grams(1350),
    )


    # fridge = [
    # ]
    # to_recipe(*fridge, name="Fridge")

    fried_chicken = add(
        honey.grams(80),
        olive_oil.grams(50 + 100),
        chicken_thighs.grams(1131),
        flour2.grams(200),
        eggs.times(2),
        name="Fried chicken"
    )
    fried_chicken.mass = 4363 - 2927

    to_day(Date(2019, 6, 17),
        milk_2.grams(1350),
        add(honey.grams(25), peanut_butter.grams(54), name="Kale smoothie"),
        croissants,
        fried_chicken.grams(360),
    )


    to_day(Date(2019, 6, 18),
        milk_2.grams(1350),
        oat_whey_pb(0, 42, 105),
        fried_chicken.grams(200),
    )

    to_day(Date(2019, 6, 19),
        milk_2.grams(1350),
        add(whey.grams(42), almond_butter.grams(100), name="Whey / almond shake"),
        fried_chicken.grams(250),
    )

    to_day(Date(2019, 6, 20),
        add(whey.grams(41), almond_butter.grams(100), name="Whey / almond shake"),
        fried_chicken.grams(303),
        croissants,
        milk_2.grams(1000),
    )

    posta37 = add(
        pasta.grams(450),
        chicken_breast.sale().grams(270),
        olive_oil.grams(60),
        puttanesca2.grams(380),
        name="Pasta w/ puttanesca"
    )
    posta37.mass = 4179 - 2927

    to_day(Date(2019, 6, 21),
        milk_2.grams(1350 + 350),
        oat_whey_pb(0, 40, 125),
        croissants,
        posta37.grams(400)
    )

    to_day(Date(2019, 6, 22),
        oat_whey_pb(80, 40, 100),
        milk_2.grams(1350),
    )

    def garlic_bread(brd, bttr, grlic=0):
        name= "garlic " * bool(grlic) + "buttered bread"
        name = name[0].upper() + name[1:]
        return add(bread.grams(brd), butter.grams(bttr), garlic.grams(grlic), name=name)

    to_day(Date(2019, 6, 23),
        milk_2.grams(1350),
        Food(name="Lancaster pork, brisket, onion rings", cals=1000, carbs=60, protein=55, fat=60, sugar=48),
        butter_chicken.grams(225),
        garlic_bread(71 + 42 + 100, 50, 30),
    )

    to_day(Date(2019, 6, 24),
        posta37.grams(280),
        milk_2.grams(1350),
        garlic_bread(250, 40, 30),
        butter_chicken.grams(330),
    )

    def oat_whey_almond(o, w, a, m=0, *args):  # pylint: disable=function-redefined
        name = " / ".join(map(lambda i: ("oat", "whey", "almond butter")[i], filter(lambda i: (o, w, a)[i], range(3)))) + " shake" + " w/ milk"*bool(m)
        name = name[0].upper() + name[1:]
        return add(oats.grams(o), whey.grams(w), almond_butter.grams(a), milk.grams(m), name=name)

    to_day(Date(2019, 6, 25),
        milk_2.grams(1350 - 400),
        oat_whey_almond(100, 20, 100),
        posta37.grams(212), butter.grams(22),
        bread.grams(150),
    )

    bred = garlic_bread(200, 70, 38)

    to_day(Date(2019, 6, 26),
        milk_2.grams(400),
        Food(cals=160, carbs=16, protein=3, fat=9, sugar=2, name="Mini cheese croissants", price=25).times(5),
        oat_whey_pb(100, 30, 100),
        milk_2.grams(1350),
        croissants,
        bred.times(1/3),
    )

    to_day(Date(2019, 6, 27),
        milk_2.grams(1350),
        Food(cals=160, carbs=16, protein=3, fat=9, sugar=2, name="Mini cheese croissants", price=25).times(5),
        bred.times(2/3),
        striploin_steak.grams(505 - 130),
    )

    to_day(Date(2019, 6, 29),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 100),
        dominos_equivalent.times(1.5),
        add(bread.grams(100), butter.grams(23), name="Bread w/ butter"),
        croissants
    )

    to_day(Date(2019, 6, 30),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 100),
        add(bread.grams(70), butter.grams(15), name="Bread w/ butter"),
    )

    to_day(Date(2019, 7, 1),
        meat_pizza.times(1/2),
        milk_2.grams(1350),
        # add(pasta.grams(140), puttanesca2.grams(150), butter.grams(10), olive_oil.grams(20), name="Pasta w/ puttanesca"),
        oat_whey_pb(120, 25, 100),
    )

    to_day(Date(2019, 7, 2),
        meat_pizza.times(1/2),
        milk_2.grams(1350),
        # add(pasta.grams(140), puttanesca2.grams(150), butter.grams(10), olive_oil.grams(20), name="Pasta w/ puttanesca"),
        oat_whey_pb(120, 25, 100),
    )

    posta38 = to_recipe(
        pasta.grams(450),
        puttanesca2.grams(380),
        chicken_breast.sale().grams(250),
        butter.grams(20),
        olive_oil.grams(50),
        name="Pasta w/ puttanesca and chicken"
    )
    posta38.mass = 4259 - 2927

    to_day(Date(2019, 7, 3),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 105),
        croissants,
        posta38.grams(400),
    )

    to_day(Date(2019, 7, 4),
        milk_2.grams(1350),
        oat_whey_pb(100, 40, 100),
        posta38.grams(400),
    )

    to_day(Date(2019, 7, 5),
        posta38.grams(250 + 200),
        milk_2.grams(1350),
        oat_whey_pb(0, 40, 100),
    )

    fried_rice3 = to_recipe(
        rice.grams(300),
        chicken_breast.sale().grams(270),
        eggs.times(6), # maggi.grams(5)
        olive_oil.grams(50),
        butter.grams(10),
        mixed_veggies.grams(120),
        # sriracha.grams(25),
        # light_soy.grams(70),
        # maggi.grams(8)
        # salt unknown
        # pepper unknown
        name="Fried rice"
    )
    fried_rice3.name="Fried rice"
    fried_rice3.mass = 4507 - 2927

    to_day(Date(2019, 7, 6),
        milk_2.grams(1350),
        oat_whey_pb(100, 30, 100),
        garlic_bread(114, 11),
        fried_rice3.grams(260 + 250),
    )

    butter_chicken2 = to_recipe(
        butter.grams(95),
        garlic.grams(47),  # <-- too much :( maybe try 30 next time
        # ginger.grams(15), # <-- a bit more would be good, perhaps 20-25
        canned_tomatoes.grams(300),
        chicken_breast.sale().grams(730),  # not enough sauce for this much chicken
        buttermilk.grams(75),
        milk.grams(80),
        honey.grams(30),
        name="Butter chicken"
        # Marinate chicken longer in buttermilk!
        # use very slightly less cumin next time
    )
    butter_chicken2.mass = 3169 - 2052
    butter_chicken2.name="Butter chicken"

    to_day(Date(2019, 7, 7),
        milk_2.grams(1350),
        oat_whey_pb(100, 30, 100),
        # garlic_bread(114, 11),
        # fried_rice3.grams(260 + 250),
        butter_chicken2.grams(150),
        garlic_bread(192, 37)
    )

    to_day(Date(2019, 7, 8),
        milk_2.grams(1350),
        oat_whey_pb(130, 30, 105),
        fried_rice3.grams(211),
        garlic_bread(200, 40)

    )

    to_day(Date(2019, 7, 9),
        milk_2.grams(1350),
        oat_whey_pb(130, 30, 100),
        add(bread.grams(153), olive_oil.grams(15), name="Bread w/ oil and vinegar"),
        perogies.grams(300).add_no_diff(butter.grams(30)).combine(italiano, 25),
    )

    to_day(Date(2019, 7, 10),
        milk_2.grams(1350),
        oat_whey_pb(130, 30, 100),
        eggs.times(2).add_no_diff(butter.grams(10)),
    )

    to_day(Date(2019, 7, 11),
        milk_2.grams(1350),
        oat_whey_pb(130, 30, 100),
        perogies.grams(300).add_no_diff(butter.grams(20)).combine(italiano, 35),
        croissants,
        add(bread.grams(153), olive_oil.grams(15), name="Bread w/ oil and balsamic"),
    )

    to_day(Date(2019, 7, 12),
        milk_2.grams(1350),
        butter_chicken2.grams(200).combine(bread, 193).add_no_diff(butter.grams(30)).add_no_diff(cream.grams(30)),
        oat_whey_pb(80, 25, 84),
        add(pasta.grams(100), puttanesca2.grams(120), butter.grams(20), name="Pasta w/ puttanesca"),
    )

    fresh_bread = add(
        flour2.grams(400),
        honey.grams(30),
        butter.grams(21),
        name="Fresh bread"
    )
    fresh_bread.mass = 600


    to_day(Date(2019, 7, 13),
        milk_2.grams(1350),
        oat_whey_pb(130, 35, 100),
        add(fresh_bread.grams(200), butter.grams(26), name="Buttered bread"),
        perogies.grams(200)
        # butter_chicken2.grams(200).combine(bread, 193).add_no_diff(butter.grams(30)).add_no_diff(cream.grams(30)),
    )

    to_day(Date(2019, 7, 14),
        milk_2.grams(1350),
        oat_whey_pb(80, 30, 100),
        perogies.grams(100)
    )


    to_day(Date(2019, 7, 15),
        milk_2.grams(1350),
        oat_whey_pb(130, 30, 100),
        add(gruyere.grams(75), bread.grams(50 * 2), onion.times(2), name="French onion soup"),
        garlic_bread(170, 30),
    )

    habanero_sauce = to_recipe(
        onion.grams(500),
        canned_tomatoes.grams(796 * 2 + 500),
        butter.grams(25),
        olive_oil.grams(25),
        garlic.grams(50),
        salt.grams(12),
        oregano.grams(3.5),
        # fresh_basil.grams(25),
        name="habanero sauce"
    )
    habanero_sauce.name="habanero sauce"
    habanero_sauce.mass = 1800

    # pasta_sauce = add(
    #     onion.grams(1205 - 735),
    #     butter.grams(30),
    #     olive_oil.grams(20),
    #     garlic.grams(40),
    #     canned_tomatoes.grams(796 * 2),
    #     garlic_powder.grams(15),
    #     chili_flakes.grams(12),
    #     salt.grams(11),
    #     oregano.grams(6.5),
    #     name="Pasta sauce"
    # )

    to_day(Date(2019, 7, 16),
        milk_2.grams(1350),
        oat_whey_almond(130, 30, 100),
        banana,
        parmigiano.grams(30),
        add(pasta.grams(85 + 122), habanero_sauce.grams(210), butter.grams(25), olive_oil.grams(15), name="Pasta w/ habanero sauce"),
    )

    fresh_bread2 = to_recipe(
        flour2.grams(500),
        honey.grams(40),
        olive_oil.grams(30),
        name="Oregano bread w/ olive oil"
    )
    fresh_bread2.name="oregano bread"
    fresh_bread2.mass = 769


    to_day(Date(2019, 7, 17),
        milk_2.grams(1350),
        oat_whey_almond(130, 30, 100),
        butter_chicken2.grams(110).combine(fresh_bread2, 280).add_no_diff(butter.grams(15)).add_no_diff(cream.grams(12))
    )

    to_day(Date(2019, 7, 18),
        milk_2.grams(1350),
        oat_whey_almond(140, 20, 50),
        chips.grams(60).free(),
        butter_chicken2.grams(150).combine(fresh_bread2, 150).add_no_diff(butter.grams(15)).add_no_diff(cream.grams(12)),
        # add(pasta.grams(100), habanero_sauce.grams(120), butter.grams(20), olive_oil.grams(10), name="Pasta w/ habanero sauce"),
        perogies.times(6).add_no_diff(butter.grams(6)),
    )

    to_day(Date(2019, 7, 19),
        perogies.times(6).add_no_diff(butter.grams(6)),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 20, 100),
        banana,
        butter_chicken2.grams(165).combine(fresh_bread2, 170).add_no_diff(butter.grams(15)).add_no_diff(cream.grams(12)),
        # potatoes.grams(500).add_no_diff(olive_oil.grams(20)),
        eggs.times(2),
        # add(pasta.grams(100), habanero_sauce.grams(120), butter.grams(20), olive_oil.grams(10), name="Pasta w/ habanero sauce"),
    )

    posta39 = add(
        pasta.grams(220),
        tuscan_chicken.grams(230),
        cream.grams(37),
        olive_oil.grams(15),
        name="Pasta w/ tuscan chicken"
    )
    posta39.mass = 710

    to_day(Date(2019, 7, 20),
        milk_2.grams(1350 / 2),
        oat_whey_pb(130, 20, 100),
        banana,
        potatoes.grams(878-222).add_no_diff(olive_oil.grams(30)).times(0.5),
        posta39.grams(480),
    )

    to_day(Date(2019, 7, 21),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 20, 100),
        banana,
        potatoes.grams(878-222).add_no_diff(olive_oil.grams(30)).times(0.5),
        posta39.grams(710 - 480),
    )

    posta40 = add(
        pasta.grams(250),
        tuscan_chicken.grams(250),
        cream.grams(30),
        olive_oil.grams(25),
        name="Pasta w/ tuscan chicken"
    )
    posta40.mass = 2842-2052

    to_day(Date(2019, 7, 22),
        milk_2.grams(1350 / 2),
        oat_whey_pb(66, 20, 85),
        fresh_bread2.grams(200).combine(butter, 20),
        posta40.grams(550),
    )

    to_day(Date(2019, 7, 23),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 20, 100),
        posta40.grams(posta40.mass - 550),
        perogies.times(14).add_no_diff(butter.grams(10)),
    )

    posta41 = add(
        pasta.grams(285),
        habanero_sauce.grams(293),
        butter.grams(20),
        cream.grams(30),
        name="Pasta w/ habanero sauce and cream"
    )
    posta41.mass = 2904-2052

    to_day(Date(2019, 7, 24),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 20, 100),
        chips.grams(50),
        # dumplings2.grams(300),
        omelette_w_italiano(4, 50),
        posta41.grams(373 + 227),
    )

    to_day(Date(2019, 7, 25),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 30, 100),
        posta41.grams(posta41.mass - 600),
        dumplings2.grams(450),
        eggs.times(2).add_no_diff(butter.grams(10)),
    )

    to_day(Date(2019, 7, 26),
        milk_2.grams(1350 / 2),
        oat_whey_pb(144, 30, 100),
        gruyere.grams(42),
        almonds.grams(60),
        shawarma,
    )

    rosemary_potatoes = add(
        potatoes.grams(650),
        garlic.grams(30),     # <= needs more next time, maybe 40
        butter.grams(50),
        # 1 sprig rosemary      <= about right
        # 1 heaping tsp in 2500 mL water <= bit too much
        name="Garlic and rosemary potatoes"
    )

    to_day(Date(2019, 7, 27),
        # milk_2.grams(1350 / 2),
        shawarma,
        rosemary_potatoes,
        oat_whey_pb(0, 80, 80),
    )


    fried_rice4 = to_recipe(
        rice.grams(350),
        chicken_breast.sale().grams(180),
        eggs.times(7), # maggi.grams(8)
        olive_oil.grams(50),
        butter.grams(30),
        mixed_veggies.grams(140),
        # sriracha.grams(25),
        # light_soy.grams(80),
        # maggi.grams(10),
        # salt.grams(15),
        # pepper.grams(7)
        name="Fried rice"
    )
    fried_rice4.mass = 4676 - 2927
    fried_rice4.name = "Fried rice"

    to_day(Date(2019, 7, 28),
        milk_2.grams(1350 / 2),
        oat_whey_pb(120, 30, 100),
        fried_rice4.grams(322 + 231),
    )

    to_day(Date(2019, 7, 29),
        milk_2.grams(1350 / 2),
        oat_whey_pb(120, 40, 100),
        fried_rice4.grams(300),
        # dumplings2.grams(350),
        pizza_w_italiano(meat_pizza_mikes, 50).slices(2),
    )

    mashed_potatoes = add(
        potatoes.grams(781),
        # half tsp baking soda, 75g salt in 2200mL boiling water
        # fresh_rosemary.grams(4)
        butter.grams(53),
        cream.grams(50),
        name="Garlic mashed potatoes"
    )

    to_day(Date(2019, 7, 30),
        milk_2.grams(1350),
        pizza_w_italiano(meat_pizza_mikes, 50).slices(2),
        mashed_potatoes.times(1/4),
        pizza_w_italiano(meat_pizza_mikes, 50).slices(2),
        # oat_whey_pb(120, 40, 100),

    )

    to_day(Date(2019, 7, 31),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 40, 100),
        fried_rice4.grams(250),
        # Sole carbonara
    )

    to_day(Date(2019, 8, 1),
        milk_2.grams(1350 / 2),
        oat_whey_pb(54, 30, 100),
        fried_rice4.grams(220),
        eggs.times(4),
        add(pasta.grams(280), habanero_sauce.grams(290), butter.grams(25), cream.grams(25), olive_oil.grams(14), name="Pasta w/ habanero cream"),
    )

    to_day(Date(2019, 8, 2),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 35, 100),
        add(bread.grams(292), butter.grams(15), olive_oil.grams(10), name="Bread w/ butter and olive oil"),
        pizza_w_italiano(bacon_cheeseburger, 0).slices(2)
    )

    to_day(Date(2019, 8, 3),
        milk_2.grams(1350 / 2),
        oat_whey_pb(140, 35, 100),
        add(bread.grams(202), butter.grams(15), olive_oil.grams(10), name="Bread w/ butter and olive oil"),
        pizza_w_italiano(bacon_cheeseburger, 0).slices(2),
        mashed_potatoes.times(1/3),
    )

    to_day(Date(2019, 8, 4),
        milk_2.grams(1350 / 2),
        pizza_w_italiano(bacon_cheeseburger, 0).slices(2),
        mashed_potatoes.times(1/3),
        oat_whey_pb(50, 30, 30),
    )

    cajun_chicken = to_recipe(
        buttermilk.grams(300),
        chicken_thighs_w_skin.grams(1218 - 0).set_price(535),
        name="Cajun chicken"
    )

    to_day(Date(2019, 8, 7),
        pizza_w_italiano(bacon_cheeseburger, 130).slices(3),
        milk_2.grams(1350 / 2),
        perogies2.grams(450),
        cajun_chicken.times(1/6),
    )

    to_day(Date(2019, 8, 8),
        pizza_w_italiano(bacon_cheeseburger, 130).slices(3),
        oat_whey_pb(0, 50, 130),
        perogies2.grams(450).combine(italiano, 0),

        # dumplings2.grams(450),
    )

    to_day(Date(2019, 8, 19),
        oat_whey_pb(100, 25, 100),
        milk_2.grams(1350),
        zehrs_sourdough.grams(200).combine(butter, 16),
        perogies2.grams(381).add_no_diff(butter.grams(30)).combine(italiano, 50),
    )

    to_day(Date(2019, 8, 20),
        oat_whey_pb(100, 20, 100),
        milk_2.grams(1350),
        zehrs_sourdough.grams(375).combine(butter, 30),
        dumplings2.grams(230),
    )

    to_day(Date(2019, 8, 21),
        oat_whey_pb(100, 20, 100),
        milk_2.grams(1350),
        dumplings2.grams(230),
        perogies2.grams(400).add_no_diff(olive_oil.grams(30)).combine(garlic, 15).combine(italiano, 50),
    )

    to_day(Date(2019, 8, 22),
        oat_whey_pb(100, 20, 100),
        milk_2.grams(1350),
        zehrs_sourdough.grams(200).combine(butter, 30),
        zehrs_sourdough.grams(283).combine(butter, 20),
    )

    rosemary_potatoes2 = add(
        potatoes.grams(598),
        garlic.grams(40),
        butter.grams(50),
        # 1 sprig rosemary
        # 1 short tsp in 2500 mL water
        name="Garlic and rosemary potatoes"
    )

    to_day(Date(2019, 8, 23),
        perogies2.grams(310),
        striploin_steak.grams(533 - 116), drained_fat.grams(20),
        milk_2.grams(1350),
        rosemary_potatoes2,
        zehrs_sourdough.grams(150).combine(butter, 30),
    )

    posta42 = add(
        pasta.grams(360),
        habanero_sauce.grams(415),
        sausage.times(1.5),
        olive_oil.grams(21),
        butter.grams(20),
        name="Pasta w/ habanero sauce and sausage"
    )
    posta42.mass = 4247 - 2927


    to_day(Date(2019, 8, 24),
        milk_2.grams(1350),
        oat_whey_pb(33, 30, 100),
        posta42.grams(500 + 250),

        zehrs_sourdough.grams(91).combine(olive_oil, 12),
    )

    posta43 = add(
        pasta.grams(310),
        habanero_sauce.grams(340),
        butter.grams(20),
        olive_oil.grams(20),
        name="Pasta w/ habanero sauce"
    )
    posta43.mass = 2990 - 2050

    to_day(Date(2019, 8, 26),
        milk_2.grams(1350 + 650),
        oat_whey_pb(100, 30, 100),
        zehrs_italian_loaf.grams(150).combine(olive_oil, 15),
        banana,
        posta43.grams(400),
    )

    to_day(Date(2019, 8, 27),
        milk_2.grams(1350),
        posta43.grams(posta43.mass - 400),
        striploin_steak.grams(300),
        # rosemary_potatoes2
        zehrs_italian_loaf.grams(300).combine(olive_oil, 30),
    )

    to_day(Date(2019, 8, 29),
        milk_2.grams(1350 + 650),
        oat_whey_pb(100, 25, 100),
        zehrs_italian_loaf.grams(200).combine(butter, 38),
    )

    posta44 = add(
        pasta.grams(450),
        puttanesca2.grams(400),
        olive_oil.grams(50),
        name="Pasta w/ puttanesca"
    )
    posta44.mass=4083 - 2927

    to_day(Date(2019, 9, 2),
        oat_whey_pb(100, 25, 100),
        milk_2.grams(575 + 575 + 500),
        posta44.grams(455),
    )

    bolognese = to_recipe(
        pancetta.grams(158),
        milk_2.grams(200),
        cream.grams(50),
        ground_beef_medium.grams(561),
        ground_trio.grams(497).set_price(710),
        onion.times(3), # one of which is red
        carrots.grams(80), # 1 carrot
        # celery.times(1),
        olive_oil.grams(15),
        butter.grams(15),
        garlic.grams(30),
        canned_tomatoes.grams(250),
        # white_wine.grams(250),
        # 2-3 bay leaves
        # some black pepper
        # some beef stock
        # tiny bit of nutmeg
        # thyme.grams(about 1)
        name="Bolognese sauce"
    )
    bolognese.mass = 400 + 655 + 714 + 69

    posta45 = add(
        pasta.grams(400),
        bolognese.grams(400),
        name="Pasta w/ bolognese"
    )
    posta45.mass = 4220 - 2927

    to_day(Date(2019, 9, 8),
        bacon, drained_fat.grams(12).times(3),
        milk_2.grams(650 + 550*2),
        omelette_w_italiano(6, 62),
        croissants.times(3),
        posta45.grams(591),
    )

    to_day(Date(2019, 9, 9),
        milk_2.grams(550 + 650*2),
        oat_whey_pb(117, 25, 100),
        add(sliced_bread.times(2), avocado.grams(57), name="Avocado toast"),
        garlic_bread(180, 25, 10),
    )

    posta46 = add(
        pasta.grams(400),
        bolognese.grams(400),
        cream.grams(52),
        name="Pasta w/ bolognese"
    )
    posta46.mass = 4183 - 2927

    to_day(Date(2019, 9, 10),
        milk_2.grams(550+500),
        oat_whey_pb(125, 28, 40),
        almonds.grams(80),
        add(sliced_bread.times(2), avocado.grams(57), name="Avocado toast"),
        posta46.grams(700),
    )

    posta47 = add(
        pasta.grams(450),
        bolognese.grams(450),
        cream.grams(60),
        name="Pasta w/ bolognese"
    )
    posta47.mass = posta46.mass * 9/8

    to_day(Date(2019, 9, 11),
        milk_2.grams(550 + 700 + 330),
        oat_whey_pb(125, 28, 100),
        almonds.grams(50),
        bread.grams(100),
        banana,
        bread.grams(150).combine(butter, 18),
        posta47.grams(350)
    )

    greek_dressing = add(
        olive_oil.grams(240),
        water.grams(150 + 50),
        garlic.grams(12),
        honey.grams(10),
        name="Greek dressing"
    )

    greek_salad = add(
        tomatoes.grams(278), # slightly less than 1 tomato
        onion.grams(125), # ~4 slices
        kalamata.grams(100),
        greek_dressing.grams(100),
        feta.grams(54),
        name="Greek salad"
    )

    to_day(Date(2019, 9, 12),
        milk_2.grams(550 + 400),
        oat_whey_pb(125, 28, 100),
        almonds.grams(80),
        bread.grams(150),
        banana,
        greek_salad.times(1/2).combine(bread, 185),
    )

    bakes = add(
        potatoes.grams(1038),
        butter.grams(95),
        garlic.grams(30),
        name="Potato bakes w/ thyme and rosemary"
    )

    to_day(Date(2019, 9, 13),
        milk_2.grams(550 + 600 + 500 + 300),
        oat_whey_pb(135, 28, 80),
        almonds.grams(100),
        bread.grams(100).combine(greek_dressing, 20),
        bakes.times(2/5),
    )

    to_day(Date(2019, 9, 14),
        add(eggs.times(3), bacon, drained_fat.grams(12).times(3), name="Bacon w/ eggs"),
        milk_2.grams(400 + 550),
        oat_whey_pb(125, 28, 80),
        # Restaurant Bonaparte
    )

    chicken_noodle_soup = add(
        pasta.grams(300),
        chicken_thighs.grams(150),
        carrots.grams(80),
        butter.grams(60),
        name="Chicken noodle soup"
    )

    to_day(Date(2019, 9, 15),
        milk_2.grams(550 + 600 + 600),
        oat_whey_pb(125, 28, 80),
        greek_salad.times(1/3).combine(bread, 160),
        chicken_noodle_soup.times(1/2),
    )

    posta48 = add(
        pasta.grams(400),
        bolognese.grams(400),
        cream.grams(80),
        name="Pasta w/ bolognese"
    )

    to_day(Date(2019, 9, 16),
        milk_2.grams(550 + 300 + 700 + 630),
        oat_whey_pb(130, 28, 120),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(cream_cheese, 30).times(2),
        add(sliced_bread.times(2), Food(cals=81, carbs=0, protein=9, fat=5, sugar=0, name="salami"), mayo.grams(15), name="Sandwich"),
        banana,
        posta48.times(640 / (posta46.mass + 20)).times(4/5),
    )

    chicken_noodle_soup2 = to_recipe(
        pasta.grams(450 - 110),
        butter.grams(50),
        chicken_thighs.grams(225),
        carrots.grams(80),
        # 1 celery,
        garlic.grams(20),
        name="Chicken noodle soup"
    )
    chicken_noodle_soup2.mass = 4854 - 2927 + 515
    print(chicken_noodle_soup2.mass)

    to_day(Date(2019, 9, 17),
        milk_2.grams(550 + 300 + 500),
        oat_whey_pb(130, 31, 120),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(cream_cheese, 30),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(15), name="Sandwich"),
        posta48.times(640 / (posta46.mass + 20)).times(1/5),
        bread.grams(70).combine(butter, 25),
        banana,
        chicken_noodle_soup2.times(1/4).add_no_diff(butter.grams(25)),
    )

    to_day(Date(2019, 9, 18),
        milk_2.grams(550 + 300 + 550),
        oat_whey_pb(135, 28, 120),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(cream_cheese, 30),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(15), name="Sandwich"),
        bread.grams(100).combine(olive_oil, 20),
        chicken_noodle_soup2.times(1/5).add_no_diff(butter.grams(25)).combine(bread, 80),

    )

    chicken_parm_stew = to_recipe(
        onion.grams(350),
        garlic.grams(20),
        butter.grams(20),
        chicken_thighs.grams(300),
        canned_tomatoes.grams(600),
        pasta.grams(340),
        italiano.grams(220),
        name="Chicken parmigiano stew"
    )
    chicken_parm_stew.mass = 4780 + 735 - 2927

    to_day(Date(2019, 9, 19),
        milk_2.grams(550 + 400 + 400 + 600),
        oat_whey_pb(115, 28, 80),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(cream_cheese, 30).times(2),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(15), name="Sandwich"),
        bread.grams(100).combine(olive_oil, 20),
        chicken_parm_stew.grams(570),

    )

    to_day(Date(2019, 9, 20),
        milk_2.grams(550 + 300 + 500),
        oat_whey_pb(125, 28, 90),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(cream_cheese, 30).times(2),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(15), name="Sandwich"),
        almonds.grams(100),
        banana,
        chicken_parm_stew.grams(600),
    )

    mirepoix_sauce = add(
        canned_tomatoes.times(796*2),
        onion.grams(400),
        carrots.grams(70),
        # celery.grams(60),
        garlic.grams(14),
        butter.grams(60),
        name="mirepoix sauce"
    )

    posta49 = add(
        pasta.grams(350),
        mirepoix_sauce.grams(380),
        butter.grams(20),
        olive_oil.grams(28),
        name="Pasta w/ mirepoix sauce"
    )
    posta49.mass = 3098 - 2050

    to_day(Date(2019, 9, 21),
        milk_2.grams(550 + 200 + 600 + 650 + 700),
        bread.grams(115).combine(butter, 20),
        add(bacon, drained_fat.grams(12).times(4), eggs.times(5), name="Bacon w/ eggs"),
        chicken_parm_stew.grams(330),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel"),
        posta49.grams(330),
    )

    cannelloni = to_recipe(
        pasta.grams(225),
        mirepoix_sauce.grams(450),
        ground_trio.set_price(88.8).grams(500),
        italiano.grams(280),
        butter.grams(20),
        Food(cals=163, carbs=3.6, protein=9, fat=12.7, sugar=3.6, name="ricotta").grams(300),
        name="Cannelloni"
    )

    to_day(Date(2019, 9, 22),
        milk_2.grams(550 + 450 + 100),
        oat_whey_pb(135, 30, 80),
        posta49.grams(170),
        bread.grams(150).combine(greek_dressing, 40),
        cannelloni.times(4/14)
    )

    to_day(Date(2019, 9, 23),
        milk_2.grams(550 + 450 + 570),
        oat_whey_pb(135, 30, 80),
        croissants,
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), cream_cheese.grams(30), name="Sandwich").times(2),
        almonds.grams(40),
        bread.grams(180).combine(butter, 30),
    )

    greek_salad2 = add(
        tomatoes.grams(280),
        kalamata.grams(88),
        onion.grams(88),
        feta.grams(100),
        greek_dressing.grams(106),
        name="Greek salad"
    )

    to_day(Date(2019, 9, 24),
        milk_2.grams(550 + 300 + 500),
        oat_whey_pb(130, 30, 90),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), cream_cheese.grams(30), name="Sandwich").times(2),
        bread.grams(40).combine(butter, 4),
        greek_salad2.times(1/2).combine(bread, 120)
    )

    to_day(Date(2019, 9, 25),
        milk_2.grams(550 + 300 + 350),
        oat_whey_pb(130, 30, 90),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), cream_cheese.grams(30), name="Sandwich").times(2),
        bread.grams(150).combine(butter, 15),
        banana,
        add(pasta.grams(180), bolognese.grams(180), cream.grams(30), name="Pasta w/ bologese")
    )

    bakes2 = to_recipe(
        potatoes.grams(1250),
        butter.grams(42 + 18 + 10),
        garlic.grams(20),
        italiano.grams(150),
        name="Bakes"
    )

    to_day(Date(2019, 9, 26),
        milk_2.grams(550 + 300 + 450),
        oat_whey_pb(135, 30, 90),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), cream_cheese.grams(30), name="Sandwich"),
        banana,
        bread.grams(100).combine(olive_oil, 20),
        bakes2.times(1/2),
    )

    posta50 = add(
        pasta.grams(450),
        mirepoix_sauce.grams(420),
        butter.grams(30),
        olive_oil.grams(40),
        parmigiano.grams(16),
        name="Pasta w/ mirepoix sauce"
    )
    posta50.mass = 4169 - 2927


    to_day(Date(2019, 9, 27),
        milk_2.grams(550 + 300 + 250),
        oat_whey_pb(135, 30, 90),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), avocado.grams(57), name="Sandwich"),
        bread.grams(100).combine(olive_oil, 20),
        banana,
        posta50.grams(555 + 749 - 583),
    )

    # cook 15 min at 450
    meatballz2 = add(
        ground_trio.grams(994),
        panko.grams(40),
        eggs.times(2),
        milk.grams(125),
        garlic_powder.grams(20),
        onion_powder.grams(30),
        chili_flakes.grams(4),
        parmigiano.grams(20),
        pepper.grams(2),
        oregano.grams(2),
        # thyme.grams(1),
        worcestershire.grams(20),
        salt.grams(10),
        name="Italian meatballs"
    ).times(1/26, name="Italian meatballs")

    mirepoix_sauce2 = add(
        onion.grams(450),
        carrots.grams(120),
        #celery.grams(120),
        garlic.grams(45),
        canned_tomatoes.grams(796*2),
        butter.grams(60),
        olive_oil.grams(30),
        oregano.grams(6),
        # dried_basil.grams(2),
        # thyme.grams(2),
        worcestershire.grams(24),
        salt.grams(11),
        name="mirepoix sauce"
    )

    posta51 = add(
        pasta.grams(400),
        mirepoix_sauce2.grams(490),
        meatballz2.times(4),
        butter.grams(20),
        olive_oil.grams(20),
        name="Spaghetti with meatballs"
    )

    to_day(Date(2019, 9, 28),
        milk_2.grams(550 + 170 + 450 + 200),
        oat_whey_pb(135, 30, 110),
        bakes2.times(1/8),
        meatballz2.times(2),
        posta51.times(2/5)
    )

    pulled_pork2 = to_recipe(
        pork_tenderloin.grams(877),
        butter.grams(80),
        onion.grams(450),
        garlic.grams(60),
        # 1 can tomato paste,
        # cider_vinegar.grams(160) <= way too much
        honey.grams(220),
        # dijon.grams(30),
        worcestershire.grams(24),
        # bay leaf,
        flour2.grams(20),
        name="Pulled pork"
    )
    pulled_pork2.mass = 1600

    bread2 = to_recipe(
        flour2.grams(300/4),
        honey.grams(25/4),
        butter.grams(25/4),
        eggs.times(1/4),
        milk.grams(160/4),
        name="Burger buns"
    )


    to_day(Date(2019, 9, 29),
        posta51.times(1/5),
        milk_2.grams(550 + 170 + 600 + 485),
        oat_whey_pb(135, 22, 80),
        bread.grams(67).combine(butter, 6),
        add(pulled_pork2.grams(100), bread2, butter.grams(12), name="Pulled pork sandwich").times(2)
    )

    posta52 = to_recipe(
        pasta.grams(400),
        mirepoix_sauce2.grams(480),
        meatballz2.times(9),
        butter.grams(25),
        olive_oil.grams(30),
        name="Pasta w/ meatballs"
    )

    to_day(Date(2019, 9, 30),
        milk_2.grams(550 + 200 +360),
        oat_whey_pb(135, 22, 100),
        croissants.times(1.5),
        almonds.grams(20),
        chips.grams(80),
        posta52.times(600 / 1650)
    )

    chicken_noodle_soup3 = to_recipe(
        pasta.grams(620 - 290),
        chicken_thighs.grams(150),
        carrots.grams(90),
        # celery.grams(100),
        garlic.grams(20),
        # stock.grams(1300),
        name="Chicken noodle soup"
    )
    chicken_noodle_soup3.mass = 4700 - 2927

    to_day(Date(2019, 10, 1),
        milk_2.grams(550 + 200 + 450),
        oat_whey_pb(135, 25, 110),
        almonds.grams(80),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(20), name="Sandwich"),
        posta52.times(1/5),
        chicken_noodle_soup3.grams(570).add_no_diff(butter.grams(20))
    )

    to_day(Date(2019, 10, 2),
        milk_2.grams(550 + 200 + 950),
        oat_whey_pb(135, 28, 110),
        bread.grams(150).combine(olive_oil, 20),
        chicken_noodle_soup3.times(1/4).add_no_diff(butter.grams(20)),
        banana,
        add(Food(cals=240, carbs=49, protein=8, fat=1.5, sugar=0, name="ciabatta"), pulled_pork2.grams(75), butter.grams(15), name="Pulled pork sandwich").times(2)
    )

    posta53 = add(
        pasta.grams(415),
        mirepoix_sauce2.grams(430),
        butter.grams(40),
        olive_oil.grams(30),
        name="Pasta w/ sauce"
    )
    posta53.mass = 4268 - 2927

    to_day(Date(2019, 10, 3),
        milk_2.grams(550 + 200 + 1020),
        oat_whey_pb(135, 28, 115),
        almonds.grams(100),
        banana,
        bread.grams(100).combine(butter, 20),
        posta53.grams(540).combine(parmigiano, 12)
    )

    posta54 = add(
        pasta.grams(410),
        mirepoix_sauce2.grams(415),
        butter.grams(8),
        olive_oil.grams(54),
        name="Pasta w/ sauce"
    )
    posta54.mass = 4020 - 2927

    to_day(Date(2019, 10, 4),
        milk_2.grams(550 + 200 + 400 + 400),
        oat_whey_pb(135, 28, 120),
        almonds.grams(100),
        posta53.grams(130),
        bread.grams(124).combine(butter, 30),
        posta54.grams(424),
    )


    to_day(Date(2019, 10, 5),
        milk_2.grams(550 + 340 + 400 + 500),
        oat_whey_pb(190, 36, 120),
        posta54.grams(100),
        # bread.grams(124).combine(butter, 30),
    )

    chicken_noodle_soup4 = add(
        pasta.grams(330),
        chicken_thighs.sale().grams(200),
        carrots.grams(80),
        butter.grams(20),
        # celery.grams(some),
        # peas.grams(80),
        garlic.grams(15),
        name="Chicken noodle soup"
    )

    to_day(Date(2019, 10, 6),
        milk_2.grams(550 + 200),
        oat_whey_pb(195, 19, 140),
        bread.grams(176).combine(greek_dressing, 60),
        almonds.grams(80),
        chicken_noodle_soup4.times(1/3).add_no_diff(butter.grams(31)),
    )

    buns1 = add(flour2.grams(250/4), name="bun")

    to_day(Date(2019, 10, 7),
        milk_2.grams(830 + 550 + 580),
        oat_whey_pb(200, 25, 150),
        croissants.times(1.5),
        chicken_noodle_soup4.times(1/5),
        add(buns1, pulled_pork2.grams(60), butter.grams(10), name="Pulled pork sandwich").times(1)
    )

    to_day(Date(2019, 10, 8),
        milk_2.grams(550 + 322 + 370),
        oat_whey_pb(135, 28, 100),
        almonds.grams(100),
        croissants.times(0.5),
        Food(cals=120, carbs=4, protein=5, fat=10, sugar=2, name="Pepperoni stick"),
        buns1.combine(butter, 20),
    )

    chicken_noodle_soup5 = add(
        pasta.grams(360),
        chicken_thighs.sale().grams(200),
        carrots.grams(80),
        butter.grams(20),
        # celery.times(1),
        # peas.grams(80),
        garlic.grams(15),
        name="Chicken noodle soup"
    )

    to_day(Date(2019, 10, 9),
        milk_2.grams(550 + 200 + 1100 + 460),
        oat_whey_pb(135, 28, 120),
        almonds.grams(100),
        banana,
        chicken_noodle_soup5.times(1/3).add_no_diff(butter.grams(22)),
    )

    pasta_sauce2 = add(
        canned_tomatoes.grams(796*2),
        onion.grams(460),
        garlic.grams(52),
        # wine.grams(250),
        chili_flakes.grams(12),
        oregano.grams(6),
        # dried_basil.grams(1),
        # worcestershire.grams(11),
        salt.grams(10.5),
        butter.grams(30),
        olive_oil.grams(30),
        name="Pasta sauce"
    )

    posta55 = add(
        pasta.grams(450),
        pasta_sauce2.grams(450),
        butter.grams(30),
        olive_oil.grams(20),
        name="Pasta w/ sauce"
    )
    posta55.mass = 3280 - 1860

    to_day(Date(2019, 10, 10),
        milk_2.grams(550 + 1100 + 440),
        oat_whey_pb(135, 28, 120),
        almonds.grams(60),
        banana,
        chicken_noodle_soup5.times(1/5),
        posta55.grams(450)
    )

    posta56 = add(
        pasta.grams(403),
        pasta_sauce2.grams(450),
        butter.grams(30),
        olive_oil.grams(30),
        name="Pasta and sauce"
    )
    posta56.mass = 3150 - 1860

    to_day(Date(2019, 10, 11),
        milk_2.grams(550 + 300 + 456 + 370),
        oat_whey_pb(140, 25, 120),
        almonds.grams(80),
        posta55.grams(2165 + 65 - 1860),
        banana,
        add(posta56.grams(450), meatballz2.times(2), name="Pasta w/ meatballs")
    )

    to_day(Date(2019, 10, 12),
        milk_2.grams(500 + 1000),
        oat_whey_pb(140, 25, 120),
        banana,
        posta56.grams(75),
    )

    pizza1 = to_recipe(
        flour.grams(310),
        pasta_sauce2.grams(140),
        olive_oil.grams(20),
        salami.grams(110),
        mozzarella.grams(110),
        kalamata.grams(60),
        onion.grams(60),
        name="Pizza"
    )

    to_day(Date(2019, 10, 13),
        milk_2.grams(550 + 200 + 150 + 1630),
        oat_whey_pb(200, 25, 120),
        banana,
        salami.grams(20),
        pizza1.times(1/2),
    )

    alfredo2 = add(
        butter.grams(60),
        garlic.grams(50),
        cream.grams(350),
        milk_2.grams(100),
        flour.grams(15),
        italiano.grams(100), # <-- too much.  try 50g
        parmigiano.grams(50),
        # sage.grams(0.5),
        # pepper.grams(2),  # <-- not enough, try 3
        name="Alfredo"
    )
    alfredo2.mass = 550

    posta57 = to_recipe(
        pasta.grams(400),
        alfredo2.grams(200),
        chicken_thighs.grams(150),
        butter.grams(20),
        olive_oil.grams(20),
        name="Pasta w/ chicken alfredo"
    )
    posta57.mass = 3073 - 1860

    to_day(Date(2019, 10, 14),
        milk_2.grams(550 + 200 + 550),
        oat_whey_pb(145, 22, 120),
        croissants.times(3),
        posta57.grams(550),
    )

    posta58 = add(
        pasta.grams(450),
        pasta_sauce2.grams(500),
        butter.grams(40),
        olive_oil.grams(30),
        name="Pasta w/ sauce"
    )

    to_day(Date(2019, 10, 15),
        milk_2.grams(550 + 200 + 400),
        oat_whey_pb(140, 22, 90),
        chips.grams(56),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast"), mayo.grams(20), name="Sandwich"),
        almonds.grams(70),
        banana,
        posta58.times(1/3),
        milk_2.grams(625)
    )

    to_day(Date(2019, 10, 16),
        milk_2.grams(550 + 600),
        oat_whey_pb(120, 33, 120),
        add(sliced_bread.times(2), Food(cals=110, carbs=0, protein=8, fat=8, sugar=0, name="salami"), cream_cheese.grams(30), name="Sandwich"),
        gruyere.grams(50).free(),
        t_and_t_dumplings.times(2).add_no_diff(butter.grams(20)),
        posta58.times(1/3),
    )

    white_pizza = to_recipe(
        flour.grams(320),
        # water.grams(230),
        italiano.grams(100),
        alfredo2.grams(80),  # <--- not enough, try 100
        chicken_thighs.grams(200),
        butter.grams(10),
        garlic.grams(20),
        red_onion.grams(65),
        name="White pizza"
    )

    to_day(Date(2019, 10, 17),
        milk_2.grams(550 + 420 + 500),
        oat_whey_pb(145, 22, 110),
        almonds.grams(80),
        chips.grams(28),
        banana,
        croissants,
        white_pizza.times(1/2)
    )

    buns2 = add(flour.grams(250/4), name="bun")

    to_day(Date(2019, 10, 18),
        milk_2.grams(550 + 200 + 500),
        oat_whey_pb(145, 22, 110),
        almonds.grams(70),
        chips.grams(28),
        banana,
        croissants.times(2),
        add(buns2, pulled_pork2.grams(110 * (570 / 470)), butter.grams(5), name="Pulled pork sandwich").times(2),
    )

    posta59 = add(
        pasta.grams(220),
        alfredo2.grams(100),
        cream.grams(30),
        name="Pasta w/ alfredo"
    )
    posta59.mass = 3475 - 2927

    to_day(Date(2019, 10, 19),
        milk_2.grams(550 + 350 + 1000),
        oat_whey_pb(145, 22, 120),
        banana,
        posta59.grams(100),
    )

    bolognese2 = to_recipe(
        pancetta.grams(150),
        milk_2.grams(250),
        cream.grams(100),
        ground_beef_medium.grams(1461 + 500),
        onion.grams(530), # one of which is red
        carrots.grams(110), # 1 carrot
        celery.times(1),
        olive_oil.grams(30),
        butter.grams(60),
        garlic.grams(52),
        canned_tomatoes.grams(250),
        # white_wine.grams(250),
        # 2-3 bay leaves
        # some black pepper
        # some beef stock
        # nutmeg.grams(0.5),
        # thyme.grams(about 1)
        name="Bolognese sauce"
    )
    bolognese2.mass = 4537 + 1024 - 1860


    lasagna = to_recipe(
        pasta.grams(450),
        mozzarella_hifat.grams(375),
        bolognese2.grams(bolognese2.mass - 4277 + 1860),
        italiano.grams(100),
        name="Lasagna"
    )

    to_day(Date(2019, 10, 20),
        milk_2.grams(550 + 350 + 350 + 585),
        oat_whey_pb(160, 22, 114),
        garlic_bread(130 + 60, 25 + 15),
        lasagna.grams(400),
    )

    to_day(Date(2019, 10, 21),
        milk_2.grams(550 + 545 + 528),
        oat_whey_pb(140, 27, 120),
        almonds.grams(80),
        chips.grams(40),
        mini_caramel_cakes,
        add(bread.grams(100), olive_oil.grams(15), name="Bread w/ oil and balsamic"),
        banana,
        lasagna.grams(290),
    )

    to_day(Date(2019, 10, 22),
        milk_2.grams(550 + 370),
        oat_whey_pb(145, 27, 125),
        add(buns1, ground_beef_lean.grams(227), mayo.grams(20), avocado.grams(20), bacon, drained_fat.grams(11).times(2), name="Madison grill burger"),
        mini_caramel_cakes,
        lasagna.grams(290),
        add(bread.grams(77), butter.grams(10), name="Bread w/ butter")
    )

    pizza2 = to_recipe(
        flour.grams(305 + 10),
        bolognese2.grams(160),
        mozzarella_hifat.grams(100),
        salami.grams(30),
        name="Pizza w/ bolognese"
    )

    to_day(Date(2019, 10, 23),
        milk_2.grams(550 + 550 + 550),
        oat_whey_pb(145, 25, 125),
        mini_caramel_cakes.times(1/2),
        Food(cals=190, carbs=32, protein=7, fat=4.5, sugar=1, name="Bean chips"),
        add(sliced_bread.times(2), Food(cals=110, carbs=0, protein=8, fat=8, sugar=0, name="salami"), cream_cheese.grams(30), name="Sandwich"),
        pizza2.times(9/16)
    )

    posta60 = add(
        pasta.grams(454),
        bolognese2.grams(480),
        cream.grams(81),
        name="Pasta w/ bolognese"
    )
    posta60.mass = 4384 - 2927

    to_day(Date(2019, 10, 24),
        milk_2.grams(700 + 520 + 431),
        oat_whey_pb(145, 25, 120),
        mini_caramel_cakes.times(3/4),
        almonds.grams(80),
        banana,
        posta60.grams(560),
    )

    posta61 = add(
        pasta.grams(454),
        bolognese2.grams(455),
        cream.grams(81),
        name="Pasta w/ bolognese"
    )
    posta61.mass = 4256 - 2927

    to_day(Date(2019, 10, 25),
        milk_2.grams(550 + 500),
        oat_whey_pb(145, 25, 120),
        almonds.grams(80),
        banana,
        posta60.grams(300),
        posta61.grams(500),
    )

    to_day(Date(2019, 10, 26),
        milk_2.grams(550 + 600 + 470),
        oat_whey_pb(150, 25, 125),
        banana,
        posta61.grams(100),
        fries.grams(180),
        garlic_bread(160, 40),
        croissants,
    )

    posta62 = add(
        pasta.grams(330),
        alfredo2.grams(160),
        cream.grams(75),
        name="Pasta w/ alfredo"
    )
    posta62.mass = 3827 - 2927

    to_day(Date(2019, 10, 27),
        croissants,
        milk_2.grams(550),
        oat_whey_pb(150, 25, 120),
        croissants.times(2),
    )

    to_day(Date(2019, 10, 28),
        milk_2.grams(550 + 670 + 440),
        oat_whey_pb(150, 27, 120),
        croissants.times(2),
        add(chips.grams(50), avocado.grams(57), name="Nachos w/ guac"),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2.5), mayo.grams(20), name="Sandwich"),
        posta62.grams(170),
    )

    to_day(Date(2019, 10, 29),
        milk_2.grams(550 + 410 + 300),
        oat_whey_pb(150, 25, 120),
        add(bread.grams(110), olive_oil.grams(20), name="Bread w/ balsamic and oil"),
        chips.grams(28),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1.5), mayo.grams(20), name="Sandwich"),
        posta61.grams(100),
        croissants,
        posta61.grams(256),
    )

    fried_rice5 = add(
        rice.grams(400),
        chicken_thighs.grams(270),
        eggs.times(7), # maggi.grams(8)
        butter.grams(20),
        mixed_veggies.grams(180), # carrots.grams(90), peas.grams(90),
        # sriracha.grams(30),
        # light_soy.grams(80),
        # maggi.grams(12),
        salt.grams(5),
        # pepper.grams(10),
        olive_oil.grams(71),
        name="Fried rice"
    )
    fried_rice5.mass = 3957 - 1860

    to_day(Date(2019, 10, 30),
        milk_2.grams(550 + 440 + 480 + 330),
        oat_whey_pb(85, 25, 125),
        add(bread.grams(120), olive_oil.grams(20), name="Bread w/ balsamic and oil"),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1.5), cream_cheese.grams(35), name="Sandwich"),
        banana,
        posta61.grams(100),
        croissants,
        fried_rice5.grams(206),
    )

    to_day(Date(2019, 10, 31),
        milk_2.grams(600 + 440 + 480 + 300),
        oat_whey_pb(155, 25, 125),
        chips.grams(56),
        add(bread.grams(120), olive_oil.grams(30), name="Bread w/ balsamic and oil"),
        fried_rice5.grams(500),
    )

    pizza3 = add(
        flour.grams(330),
        bolognese2.grams(170),
        cream.grams(20),
        mozzarella_hifat.grams(110),
        name="Bolognese pizza"
    )

    to_day(Date(2019, 11, 1),
        milk_2.grams(600 + 422),
        oat_whey_pb(155, 25, 120),
        chips.grams(56),
        Food(cals=100, carbs=13, protein=1, fat=4.5, sugar=1, name="Crackers").times(3),
        mini_caramel_cakes,
        fried_rice5.grams(250),
        pizza3.times(1/2),
    )

    greek_salad3 = to_recipe(
        tomatoes.grams(170),
        feta.grams(100),
        kalamata.grams(100),
        red_onion.grams(70),
        greek_dressing.grams(45),
        olive_oil.grams(20),
        name="Greek salad"
    )

    bread3 = add(flour.grams(450), name="bread")
    bread3.mass = 660

    to_day(Date(2019, 11, 2),
        milk_2.grams(600 + 500),
        oat_whey_pb(165, 25, 120),
        greek_salad3.times(1/2).combine(bread3, 122 + 83)
    )

    fried_rice6 = add(
        rice.grams(400),
        chicken_thighs.grams(225),
        eggs.times(8), # maggi.grams(10)
        butter.grams(20),
        mixed_veggies.grams(200), # carrots.grams(109), peas.grams(90),
        # sriracha.grams(30),
        # light_soy.grams(80),
        # maggi.grams(12),
        salt.grams(5),
        # pepper.grams(10),
        olive_oil.grams(75),
        name="Fried rice"
    )
    fried_rice6.mass = 3958 - 1860

    to_day(Date(2019, 11, 4),
        milk_2.grams(600 + 320),
        oat_whey_pb(165, 22, 130),
        croissants,
        add(chips.grams(75), canned_tomatoes.grams(250).free(), name="Nachos w/ salsa"),
        mini_caramel_cakes,
        add(bread.grams(120), olive_oil.grams(30), name="Bread w/ balsamic and oil"),
        fried_rice6.grams(300)
    )

    to_day(Date(2019, 11, 5),
        milk_2.grams(800 + 500),
        oat_whey_pb(160, 25, 130),
        add(chips.grams(75), canned_tomatoes.grams(300).free(), name="Nachos w/ salsa"),
        almonds.grams(60),
    )

    to_day(Date(2019, 11, 6),
        milk_2.grams(750 + 150 + 500),
        oat_whey_pb(160, 25, 130).combine(honey, 20),
        mini_caramel_cakes,
        chips.grams(28),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), avocado.grams(57), olive_oil.grams(20), name="Sandwich"),
        banana,
        fried_rice6.grams(450),
    )

    chicken_noodle_soup6 = add(
        pasta.grams(355),
        chicken_thighs.grams(315),
        carrots.grams(110),
        # peas.grams(85),
        # celery.grams(100),
        # stock.grams(1300),
        butter.grams(30),
        olive_oil.grams(20),
        garlic.grams(15),
        name="Chicken noodle soup"
    )
    chicken_noodle_soup6.mass = 4982 - 1866

    to_day(Date(2019, 11, 7),
        milk_2.grams(800 + 150 + 436 + 430 + 460),
        oat_whey_pb(160, 25, 114).combine(honey, 40),
        Food(cals=240, carbs=33, protein=2, fat=11, sugar=20, name="Mini caramel cakes").times(0.5),
        chips.grams(28),
        walnuts.grams(30),
        sliced_bread.times(2).combine(canned_tomatoes, 250),
        chicken_noodle_soup6.grams(565 + 112).add_no_diff(butter.grams(24 + 6))
    )

    posta63 = add(
        pasta.grams(400),
        butter.grams(65),
        olive_oil.grams(10),
        shrimp.grams(400),
        garlic.grams(25),
        salt.grams(5),
        # pepper.grams(1) <- not enough, try 2
        name="Lemon garlic shrimp pasta"
    )
    posta63.mass = 4072 - 2927

    to_day(Date(2019, 11, 8),
        milk_2.grams(750 + 430 + 385),
        oat_whey_pb(95, 28, 0),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(3), canned_tomatoes.grams(120), olive_oil.grams(20), name="Sandwich"),
        Food(cals=240, carbs=33, protein=2, fat=11, sugar=20, name="Mini caramel cakes").times(0.5),
        walnuts.grams(60),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(3), canned_tomatoes.grams(120), olive_oil.grams(20), name="Sandwich"),
        chicken_noodle_soup6.grams(350),
        posta63.grams(483 - 80)
    )

    to_day(Date(2019, 11, 9),
        milk_2.grams(800 + 430 + 422),
        oat_whey_pb(210, 22, 130).combine(honey, 40),
        bread.grams(128).combine(butter, 30),
    )

    pasta_sauce3 = to_recipe(
        canned_tomatoes.grams(796),
        onion.times(2),
        olive_oil.grams(15),
        butter.grams(30),
        name="sauce"
    )

    to_day(Date(2019, 11, 10),
        milk_2.grams(800 + 430 + 500),
        oat_whey_pb(165, 22, 130).combine(honey, 50),
        croissants.times(3),
        banana,
    )

    to_day(Date(2019, 11, 11),
        milk_2.grams(750 + 500 + 567 + 506),
        oat_whey_pb(165, 20, 100).combine(honey, 65),
        chips.grams(75).combine(canned_tomatoes, 250).combine(avocado, 57),
        croissants.times(1),
    )

    posta64 = add(
        pasta.grams(450),
        pasta_sauce3.grams(450),
        butter.grams(35),
        olive_oil.grams(35),
        name="Pasta w/ sauce"
    )
    posta64.mass = 3372 - 1866

    to_day(Date(2019, 11, 12),
        milk_2.grams(500 + 300 + 315),
        oat_whey_pb(165, 20, 130).combine(honey, 40),
        chips.grams(50).combine(avocado, 57*2),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), avocado.grams(57), olive_oil.grams(15), name="Sandwich"),
        posta64.grams(575),
    )

    to_day(Date(2019, 11, 13),
        milk_2.grams(500 + 300 + 360),
        oat_whey_pb(160, 20, 130).combine(honey, 40),
        chips.grams(75).combine(canned_tomatoes, 250).combine(avocado, 57),
        posta64.grams(250),
        bread.grams(106).combine(butter, 36),
        milk_chocolate.grams(700),
    )

    to_day(Date(2019, 11, 14),
        milk_2.grams(500 + 280 + 325 + 300),
        oat_whey_pb(165, 20, 130).combine(honey, 40),
        chips.grams(50).combine(canned_tomatoes, 250).combine(avocado, 57),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), avocado.grams(57), olive_oil.grams(15), name="Sandwich"),
        milk_chocolate.grams(150 + 460),
        banana,
        bread.grams(80 - 18).combine(butter, 12),
        posta64.grams(165),
    )

    puttanesca3 = add(
        canned_tomatoes.grams(796*2),
        garlic.grams(55),
        olive_oil.grams(25),
        butter.grams(30),
        # capers.spoons(3),
        # anchovies.times(4),
        # pepper.grams(7),
        salt.grams(6),
        chili_flakes.grams(10),
        oregano.grams(3),
        name="Puttanesca"
    )

    posta65 = add(
        pasta.grams(400),
        puttanesca3.grams(380),
        olive_oil.grams(40),
        name="Pasta w/ puttanesca"
    )
    posta65.mass=3260 - 1866

    to_day(Date(2019, 11, 15),
        milk_2.grams(250),
        oat_whey_pb(165, 20, 130).combine(honey, 20),
        milk_chocolate.grams(250 + 150 + 260 + 270),
        chips.grams(75).combine(canned_tomatoes, 250).combine(avocado, 57),
        croissants.times(2.5),
        posta65.grams(518),
    )


    to_day(Date(2019, 11, 16),
        milk_2.grams(500 + 500),
        oat_whey_pb(170, 20, 130).combine(honey, 40),
        milk_chocolate.grams(500),
        posta65.grams(300),
        croissants,
    )

    buns3 = add(flour2.grams(310 / 4), name="burger bun")
    burgerz = to_recipe(
        panko.grams(20),
        eggs.times(1),
        onion_powder.grams(20),
        garlic_powder.grams(17),
        # worcestershire.grams(10),
        ground_beef_medium.grams(1060),
        name="Burger"
    ).times(1/8)

    blue_cheese_sauce = to_recipe(
        onion.times(3),
        butter.grams(100),
        blue_cheese.grams(125),
        name="Blue cheese sauce"
    )

    to_day(Date(2019, 11, 17),
        milk_2.grams(500 + 200 + 420),
        oat_whey_pb(200, 28, 130).combine(honey, 40),
        milk_chocolate.grams(550),
        croissants,
        add(buns3, burgerz, blue_cheese_sauce.times(1/10), name="Burger w/ blue cheese sauce").times(2),
    )

    posta66 = add(
        pasta.grams(400),
        puttanesca3.grams(385),
        olive_oil.grams(40),
        name="Pasta w/ puttanesca"
    )
    posta66.mass = 3035 - 1866

    to_day(Date(2019, 11, 18),
        milk_2.grams(500 + 280 + 200 + 450),
        oat_whey_pb(160, 28, 130).combine(honey, 40),
        chips.grams(75).combine(canned_tomatoes, 250).combine(avocado, 57),
        croissants,
        posta66.grams(460),
    )

    to_day(Date(2019, 11, 19),
        milk_2.grams(500 + 280 + 300 + 580),
        oat_whey_pb(163, 25, 130).combine(honey, 20),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), avocado.grams(57), olive_oil.grams(15), name="Sandwich"),
        milk_chocolate.grams(500),
        croissants,
        posta66.grams(225),
        croissants,
    )

    to_day(Date(2019, 11, 22),
        milk_2.grams(500 + 250 + 200 + 275),
        oat_whey_pb(163, 25, 130).combine(honey, 40),
        chips.grams(60).combine(canned_tomatoes, 200).combine(avocado, 57),
        Food(cals=210, carbs=38, protein=8, fat=3, sugar=2, name="bagel").combine(mayo, 15),
        milk_chocolate.grams(350),
        croissants,
        add(buns3, burgerz, blue_cheese_sauce.times(1/10), name="Burger w/ blue cheese sauce")
    )

    poutine1 = to_recipe(
        potatoes.grams(1200),
        butter.grams(100 + 16 + 27),
        flour.grams(35),
        garlic.grams(30),
        italiano.grams(130),
        name="Poutine"
    )

    to_day(Date(2019, 11, 23),
        milk_2.grams(500 + 275 + 400 + 475),
        oat_whey_pb(160, 25, 130).combine(honey, 30),
        add(pasta.grams(150), puttanesca3.grams(125), butter.grams(5), olive_oil.grams(15), name="Pasta w/ puttanesca"),
        poutine1.times(1/2),
    )

    mirepoix_sauce3 = add(
        onion.times(3),
        carrots.grams(130),
        # celery.grams(100),
        garlic.grams(50),
        canned_tomatoes.grams(796*2),
        butter.grams(50),
        oregano.grams(6),
        # dried_basil.grams(2),
        # worcestershire.grams(20),
        # red_wine.grams(250),
        salt.grams(13),
        name="mirepoix sauce"
    )
    mirepoix_sauce3.mass = 4591 - 2927 + 400

    posta67 = add(
        pasta.grams(400),
        mirepoix_sauce3.grams(400),
        butter.grams(25),
        olive_oil.grams(25),
        name="Pasta w/ mirepoix sauce"
    )
    posta67.mass = 3000 - 1866

    to_day(Date(2019, 11, 24),
        milk_2.grams(500 + 400 + 300),
        oat_whey_pb(165, 25, 130).combine(honey, 40),
        poutine1.times(1/12),
        croissants.times(1.5),
        posta67.grams(350).add_no_diff(parmigiano.grams(5)),
    )

    to_day(Date(2019, 11, 25),
        milk_2.grams(500 + 280 + 450 + 450 + 510),
        oat_whey_pb(160, 25, 130).combine(honey, 40),
        milk_chocolate.grams(150),
        Food(cals=240, carbs=33, protein=2, fat=11, sugar=20, name="Mini butter tarts"),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), mozzarella_hifat.grams(30), avocado.grams(57), olive_oil.grams(10), name="Sandwich"),
        banana,
        posta67.grams(230),
    )

    to_day(Date(2019, 11, 26),
        milk_2.grams(500 + 280 + 200 + 475),
        oat_whey_pb(160, 25, 130).combine(honey, 40),
        milk_chocolate.grams(150),
        chips.grams(60).combine(canned_tomatoes, 200).combine(avocado, 57),
        banana,
        posta67.grams(2324 - 1870),
        croissants.times(2/3),
    )

    to_day(Date(2019, 11, 27),
        milk_2.grams(500 + 280 + 200 + 475 + 400 + 430),
        oat_whey_pb(160, 25, 130).combine(honey, 40),
        milk_chocolate.grams(200),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), avocado.grams(57), olive_oil.grams(15), name="Sandwich"),
        banana,
        croissants.times(2/3),
        posta67.grams(130),
        almonds.grams(30),
    )

    calzone = to_recipe(
        flour.grams(350/4),
        italiano.grams(25),
        ricotta.grams(50),
        mirepoix_sauce3.grams(50),
        pepperoni.grams(20),
        olive_oil.grams(8),
        name="Calzone"
    )

    to_day(Date(2019, 11, 28),
        milk_2.grams(500 + 500 + 380),
        oat_whey_pb(160, 25, 130).combine(honey, 40),
        milk_chocolate.grams(375),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), mozzarella_hifat.grams(30), avocado.grams(57), olive_oil.grams(15), name="Sandwich"),
        Food(cals=240, carbs=33, protein=2, fat=11, sugar=20, name="Mini caramel cakes"),
        calzone.times(1.5),
    )

    posta68 = add(
        pasta.grams(450),
        mirepoix_sauce3.grams(450),
        butter.grams(40),
        olive_oil.grams(55),
        name="Pasta w/ mirepoix sauce"
    )
    posta68.mass = 3264 - 1866

    to_day(Date(2019, 11, 29),
        milk_2.grams(500 + 310 + 500 + 385),
        oat_whey_pb(160, 25, 130).combine(honey, 40),
        milk_chocolate.grams(200 + 380),
        add(sliced_bread.times(2), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), mayo.grams(20), name="Sandwich"),
        posta68.grams(431)
    )

    dumplingz1 = to_recipe(
        flour.grams(400 - 175*2/3),
        ground_beef_medium.grams(450),
        garlic.grams(30),
        green_onion.times(3),
        # ginger.grams(18),
        salt.grams(4),
        # light_soy.grams(30),
        # sriracha.grams(15),
        carrots.grams(100),
        # celery.grams(75),
        # black_pepper.grams(1.5),
        butter.grams(45),
        name="Dumplings"
    ).times(1/25)

    to_day(Date(2019, 11, 30),
        milk_2.grams(500 + 380 + 385 + 450),
        oat_whey_pb(210, 25, 130).combine(honey, 40),
        croissants.times(1.5),
        dumplingz1.times(10),
    )

    pizza4 = to_recipe(
        flour2.grams(225),  # hydration 71%
        mirepoix_sauce3.grams(130),
        italiano.grams(142),
        feta.grams(70),
        garlic.grams(10),
        name="Feta pizza"
    )

    pizza5 = to_recipe(
        flour2.grams(225),  # hydration 71%
        mirepoix_sauce3.grams(130),
        red_onion.grams(60),
        kalamata.grams(50),
        mozzarella.grams(130),
        pepperoni.grams(82),
        garlic.grams(6),
        name="Pepperoni pizza deluxe"
    )

    to_day(Date(2019, 12, 1),
        milk_2.grams(500 + 300 + 583 + 477 + 513),
        oat_whey_pb(172, 25, 130).combine(honey, 40),
        milk_chocolate.grams(350),
        pepperoni.grams(36),
        pizza4.times(3/8),
        pizza5.times(1/8),
    )

    to_day(Date(2019, 12, 2),
        milk_2.grams(500 + 300 + 250 + 500),
        oat_whey_pb(172, 25, 130).combine(honey, 40),
        chips.grams(60).combine(canned_tomatoes, 200).combine(avocado, 57),

        milk_chocolate.grams(400),
        pizza5.times(1/4).combine(olive_oil, 10),
        pizza4.times(1/8),
        bread.grams(50).combine(butter, 8),
    )

    mirepoix_sauce4 = add(
        onion.times(2.5),
        carrots.grams(130),
        # celery.grams(150),
        garlic.grams(45),
        canned_tomatoes.grams(796*2),
        butter.grams(50),
        olive_oil.grams(30),
        oregano.grams(3),
        # dried_basil.grams(2),
        # worcestershire.grams(24),
        # red_wine.grams(375),
        salt.grams(13),
        name="mirepoix sauce"
    )
    mirepoix_sauce4.mass = 4717 - 2927 + 364

    posta69 = add(
        pasta.grams(364),
        mirepoix_sauce4.grams(364),
        butter.grams(20),
        olive_oil.grams(32),
        name="Pasta w/ mirepoix sauce"
    )
    posta69.mass = 2881 - 1866

    to_day(Date(2019, 12, 3),
        milk_2.grams(500 + 250 + 250 + 600),
        oat_whey_pb(86, 25, 130).combine(honey, 40),
        bread.grams(80).combine(avocado, 57),
        add(sliced_bread.times(1.7), mayo.grams(15), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), name="Sandwich").times(2),
        milk_chocolate.grams(234),
        bread.grams(70).combine(butter, 10),
        posta69.grams(414),
    )

    posta70 = add(
        pasta.grams(450),
        mirepoix_sauce4.grams(450),
        butter.grams(30),
        olive_oil.grams(40),
        name="Pasta w/ mirepoix sauce"
    )
    posta69.mass = 2881 - 1866

    to_day(Date(2019, 12, 4),
        milk_2.grams(500 + 250 + 250),
        oat_whey_pb(170, 25, 130).combine(honey, 40),
        add(sliced_bread.times(1.7), avocado.grams(57), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), name="Sandwich"),
        milk_chocolate.grams(700),
        posta69.grams(550),
    )

    to_day(Date(2019, 12, 5),
        milk_2.grams(600 + 250 + 250 + 470),
        oat_whey_pb(46, 25, 170).combine(honey, 40),
        add(sliced_bread.times(1.7), avocado.grams(57), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), name="Sandwich"),
        posta69.grams(200),
        bread.grams(100).combine(butter, 20),
        bread.grams(109).combine(butter, 30),
    )


    # cook 15 min at 450
    meatballz3 = add(
        ground_trio.grams(994/28),
        panko.grams(50/28),
        eggs.times(2/28),
        milk.grams(130/28),
        garlic_powder.grams(20/28),
        onion_powder.grams(30/28),
        chili_flakes.grams(5/28),
        parmigiano.grams(30/28),
        # black_pepper.grams(2/28),
        # oregano.grams(2/28),
        # thyme.grams(1/28),
        # worcestershire.grams(25/28),
        salt.grams(12/28),
        name="Italian meatballs"
    )

    posta71 = add(
        pasta.grams(360),
        mirepoix_sauce4.grams(300),
        butter.grams(15),
        olive_oil.grams(35),
        name="Pasta w/ mirepoix sauce"
    )
    posta71.mass = 3122 - 1866 - 35*3

    to_day(Date(2019, 12, 6),
        milk_2.grams(600 + 250 + 250),
        oat_whey_pb(172, 25, 130).combine(honey, 40),
        milk_chocolate.grams(400),
        add(sliced_bread.times(1.7), mayo.grams(20), mozzarella_hifat.grams(20), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(2), name="Sandwich"),
        bread.grams(90).combine(butter, 30),
        add(posta71.grams(290), meatballz3.times(2), name="Pasta w/ meatballs")
    )

    to_day(Date(2019, 12, 7),
        milk_2.grams(500 + 410),
        oat_whey_pb(155, 25, 130).combine(honey, 20),
        milk_chocolate.grams(437),
        add(posta71.grams(100), meatballz3.times(1), name="Pasta w/ meatballs")
    )

    pan_pizza = to_recipe(
        flour2.grams(350 + 20),  # hydration 90% (!)
        butter.grams(10),        # <- can use more!  Maybe 20g
        olive_oil.grams(10),
        red_onion.grams(100),
        pepperoni.grams(100),
        meatballz3.times(2),
        mozzarella_hifat.grams(208),
        mirepoix_sauce4.grams(250),
        name="Pan pizza"
    )

    to_day(Date(2019, 12, 8),
        milk_2.grams(500 + 250 + 390),
        oat_whey_pb(210, 25, 130).combine(honey, 40),
        milk_chocolate.grams(400),
        banana,
        add(posta71.grams(100), meatballz3.times(1), name="Pasta w/ meatballs"),
        pan_pizza.times(2.5/6),
    )

    posta72 = add(
        pasta.grams(450),
        mirepoix_sauce4.grams(320),
        butter.grams(32),
        olive_oil.grams(34),
        name="Pasta w/ mirepoix sauce"
    )
    posta72.mass = 3355 - 1866 - 35*6

    to_day(Date(2019, 12, 9),
        milk_2.grams(500 + 250 + 250 + 340),
        oat_whey_pb(170, 25, 130).combine(honey, 50),
        add(sliced_bread.times(1.7), mozzarella_hifat.grams(30), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1), avocado.grams(57), name="Sandwich"),
        banana,
        milk_chocolate.grams(390),
        meatballz3,
        add(posta72.grams(400), meatballz3.times(1), name="Pasta w/ meatballs")
    )

    posta73 = to_recipe(
        pasta.grams(300),
        mirepoix_sauce4.grams(225),
        butter.grams(32),
        olive_oil.grams(36),
        name="Pasta w/ sauce"
    )
    posta73.mass = 3959 - 2927 - 35*6

    to_day(Date(2019, 12, 10),
        milk_2.grams(500 + 250 + 250 + 250 + 400),
        oat_whey_pb(170, 25, 130).combine(honey, 45),
        add(sliced_bread.times(2), mozzarella_hifat.grams(30), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1), mayo.grams(15), name="Sandwich"),
        milk_chocolate.grams(300),
        posta72.grams(200),
        add(posta73.grams(200), meatballz3.times(1), name="Pasta w/ meatballs"),
    )

    poutine2 = to_recipe(
        potatoes.grams(1078),
        butter.grams(100),
        flour2.grams(15),
        garlic.grams(37),
        pepperoni.grams(71),
        # pepper.grams(4),
        # thyme.grams(0.6),
        # salt: 3%,
        # baking soda 5g / 3000mL
        # 250mL stock
        name="Poutine"
    )
    poutine2.mass = 2490 - 1870

    to_day(Date(2019, 12, 12),
        oat_whey_pb(170, 25, 130).combine(honey, 45),
        milk_2.grams(500 + 250 + 350),
        milk_chocolate.grams(400),
        croissants.times(0.7),
        add(posta73.grams(200), meatballz3.times(2), name="Pasta w/ meatballs"),
        poutine2.grams(200),
    )

    mirepoix_sauce5 = add(
        canned_tomatoes.grams(796*2),
        butter.grams(50),
        oregano.grams(6),
        # basil.grams(2),
        # red_wine.grams(320),
        onion.grams(480),
        carrots.grams(180),
        # celery.grams(160),
        garlic.grams(50),
        salt.grams(14),
        # worcestershire.grams(20),
        name="mirepoix sauce"
    )

    posta74 = add(
        pasta.grams(450),
        mirepoix_sauce5.grams(430),
        butter.grams(32),
        olive_oil.grams(29),
        name="Pasta w/ mirepoix_sauce"
    )
    posta74.mass = 2250 - 1870 + 500 + 454

    to_day(Date(2019, 12, 14),
        oat_whey_pb(170, 25, 130).combine(honey, 0),
        milk_2.grams(500 + 250),
        milk_chocolate.grams(250 + 400),
        poutine2.grams(80),
        posta74.grams(500),
        meatballz3.times(2),
    )

    calzone = to_recipe(
        flour.grams(350/4),
        italiano.grams(25),
        ricotta.grams(55), # <-- this was too much.  stick to 40-50g
        mirepoix_sauce3.grams(60), # <-- too much, bottoms got soggy.  Stay with 50
        pepperoni.grams(40),  # <-- too much.  Try 30g
        olive_oil.grams(8),
        name="Calzone"
    )

    to_day(Date(2019, 12, 15),
        oat_whey_pb(170, 25, 130).combine(honey, 0),
        milk_2.grams(500 + 250 + 530 + 400),
        posta74.grams(100),
        calzone.times(1.5),
    )

    to_day(Date(2019, 12, 16),
        oat_whey_pb(170, 25, 145).combine(honey, 30),
        milk_2.grams(500 + 250 + 250),
        croissants,
        milk_chocolate.grams(415),
        chips.grams(60).combine(canned_tomatoes, 300),
        calzone.times(0.5),
        bread.grams(120).combine(butter, 40),
    )

    to_day(Date(2019, 12, 17),
        oat_whey_pb(170, 25, 145),
        milk_2.grams(500 + 250 + 250 + 330 + 100),
        mini_caramel_cakes,
        add(sliced_bread.times(2), mozzarella_hifat.grams(35), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1.5), olive_oil.grams(5), name="Sandwich"),
        banana,
        milk_chocolate.grams(415),
        calzone,
    )

    to_day(Date(2019, 12, 18),
        oat_whey_pb(170, 25, 145),
        milk_2.grams(500 + 250 + 250 + 300),
        mini_caramel_cakes,
        add(sliced_bread.times(2), mozzarella_hifat.grams(35), salami.grams(40), name="Sandwich"),
        banana,
        milk_chocolate.grams(425),
        bread.grams(130).combine(butter, 43),
    )


    to_day(Date(2019, 12, 19),
        oat_whey_pb(170, 30, 145),
        milk_2.grams(500 + 250 + 250 + 330),
        mini_caramel_cakes,
        add(sliced_bread.times(2), mozzarella_hifat.grams(35), Food(cals=50, carbs=0, protein=11, fat=0, sugar=0, name="turkey breast").times(1.5), mayo.grams(15), name="Sandwich"),
        bread.grams(40).combine(butter, 16),
        calzone
    )

    posta75 = to_recipe(
        pasta.grams(450),
        mirepoix_sauce5.grams(460),
        butter.grams(80),
        name="Pasta w/ sauce"
    )
    posta75.mass = 3335 - 1870

    to_day(Date(2019, 12, 20),
        oat_whey_pb(170, 30, 145),
        milk_2.grams(500 + 250 + 250),
        posta75.grams(505+303),
        posta75.grams(306),
    )

    guac = add(
        red_onion.grams(60),
        olive_oil.grams(16),
        avocado.grams(162 - 60 - 16),
        name="guac"
    )

    to_day(Date(2019, 12, 21),
        oat_whey_pb(170, 30, 150),
        milk_2.grams(500),
        posta75.grams(351).combine(pepperoni, 60),
        bread.grams(113).combine(butter, 10),
        add(bread.grams(214), guac, name="Bread w/ guac")
    )

    to_day(Date(2019, 12, 22),
        oat_whey_pb(170, 20, 135),
        milk_2.grams(500 + 250),
        to_recipe(bread.grams(180), butter.grams(35), pepperoni.grams(60), name="Bread w/ butter and pepperoni"),
        almonds.grams(70),
    )

    to_day(Date(2019, 12, 27),
        croissants,
        oat_whey_pb(170, 0, 130).combine(honey, 20),
        milk_2.grams(1350),
        pizza_w_italiano(bacon_cheeseburger, 50).slices(3),
    )

    mirepoix_sauce6 = to_recipe(
        onion.grams(188 + 175 + 160),
        #celery.grams(150),
        carrots.grams(100),
        garlic.grams(44),
        butter.grams(60),
        olive_oil.grams(32),
        oregano.grams(8),
        salt.grams(13),
        # worcestershire.grams(25),
        canned_tomatoes.grams(796 * 2),
        name="mirepoix sauce"
    )

    posta76 = to_recipe(
        pasta.grams(300),
        mirepoix_sauce6.grams(300),
        butter.grams(15),
        olive_oil.grams(15),
        chicken_thighs.grams(256),
        name="Pasta w/ chicken thighs"
    )
    posta76.mass = 1030

    to_day(Date(2019, 12, 28),
        croissants.times(2),
        oat_whey_pb(200, 0, 130).combine(maple_syrup, 20),
        milk_2.grams(1350),
        posta76.grams(470 + 105),
    )

    to_day(Date(2019, 12, 29),
        oat_whey_pb(200, 0, 140).combine(maple_syrup, 20),
        milk_2.grams(1350),
        croissants.times(2),
        posta76.grams(posta76.mass - (470 + 105)),
    )

    braised_shortrib = to_recipe(
        mirepoix_sauce6.grams(230),
        maple_syrup.grams(21),
        garlic.grams(21),
        onion.times(1),
        short_ribs.grams(488 - 70),
        name="Braised shortrib"
    )

    bolognese3 = to_recipe(
        ground_beef_medium.grams(1180).set_price(779),
        onion.grams(540),
        red_onion.grams(150),
        carrots.grams(124),
        celery.grams(150),
        olive_oil.grams(32),
        butter.grams(32),
        garlic.grams(52),
        # white_wine.grams(250),
        milk_2.grams(200),
        cream.grams(130),
        # bay_leaves.times(3),
        # thyme.grams(1),
        # black_pepper.grams(6),
        canned_tomatoes.grams(796),
        name="bolognese sauce"
    )
    print(2183 + 3515 - 2927)
    print(5009 - 2927)

    to_day(Date(2019, 12, 30),
        croissants.times(2),
        oat_whey_pb(200, 0, 130).combine(maple_syrup, 20),
        milk_2.grams(1350),
        braised_shortrib.times(1/2),
    )

    neapolitan_sauce = to_recipe(
        Food(cals=220, carbs=0, protein=16, fat=17, sugar=0, name="Sausage", mass=100).times(5),
        beef_shanks.grams(538 + 470 - 185),
        canned_tomatoes.grams(796 * 2),
        onion.times(3),
        garlic.grams(59),
        butter.grams(50),
        oregano.grams(8),
        salt.grams(10),
        chili_flakes.grams(12),
        # red_wine.grams(430),
        # bay_leaves.times(4),
        # fennel_seeds.teaspoons(~2),
        name="Neapolitan ragu"
    )
    neapolitan_sauce.name="Neapolitan ragu"
    neapolitan_sauce.mass = 5130 - 2927


    to_day(Date(2020, 1, 2),
        oat_whey_pb(200, 0, 135).combine(maple_syrup, 35),
        milk_2.grams(1350),
        croissants.times(1),
        braised_shortrib.times(1/2),
    )

    posta77 = to_recipe(
        pasta.grams(180),
        bolognese3.grams(180),
        cream.grams(30),
        name="Pasta w/ bolognese"
    )

    to_day(Date(2020, 1, 3),
        oat_whey_pb(155, 0, 130).combine(maple_syrup, 28),
        milk_2.grams(1350),
        posta77,
        croissants,
        garlic_bread(106, 30).combine(italiano, 30)
    )

    posta78 = to_recipe(
        pasta.grams(300),
        neapolitan_sauce.grams(350),
        butter.grams(30),
        olive_oil.grams(30),
        name="Pasta w/ neapolitan sauce"
    )
    posta78.mass = 2986 - 2050

    to_day(Date(2020, 1, 4),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 20),
        milk_2.grams(1350),
        garlic_bread(135, 40).combine(italiano, 50),
        posta78.grams(414),
    )

    to_day(Date(2020, 1, 5),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 20),
        milk_2.grams(1350),
        garlic_bread(116, 40).combine(italiano, 50),
        posta78.grams(posta78.mass - 414),
    )

    posta79 = to_recipe(
        pasta.grams(415),
        neapolitan_sauce.grams(500),
        butter.grams(42),
        olive_oil.grams(42),
        name="Pasta w/ neapolitan sauce"
    )
    posta79.mass = 4348 - 2927

    to_day(Date(2020, 1, 6),
        oat_whey_pb(150, 0, 120),
        milk_2.grams(1350),
        garlic_bread(70, 30).combine(italiano, 30),
        posta79.grams(500),
    )

    to_day(Date(2020, 1, 7),
        oat_whey_pb(150, 0, 120),
        milk_2.grams(1350),
        posta79.grams(457),
        posta79.grams(347 + 97),
    )

    posta80 = to_recipe(
        pasta.grams(275),
        mirepoix_sauce6.grams(275),
        chicken_thighs.grams(110),
        butter.grams(30),
        olive_oil.grams(30),
        name="Pasta w/ mirepoix sauce and chicken"
    )

    to_day(Date(2020, 1, 8),
        oat_whey_pb(150, 0, 80).combine(maple_syrup, 38).add_no_diff(cinnamon.grams(1)),
        milk_2.grams(1350),
        posta80.times(1/2),
        posta80.times(1/2),
    )

    # Changed purchase
    light_soy = soy_sauce


    fried_rice7 = to_recipe(
        rice.grams(350),
        chicken_thighs.grams(220),
        eggs.times(8), # maggi.grams(10)
        butter.grams(20),
        carrots.grams(98),
        frozen_peas.grams(100),
        sriracha.grams(30), # <-- try more sriracha and less pepper
        light_soy.grams(80), # <- not low sodium
        # maggi.grams(12),
        salt.grams(5),
        pepper.grams(10),  # <-- try more sriracha and less pepper
        olive_oil.grams(60),
        name="Fried rice"
    )
    fried_rice7.mass = 1306 + 3488 - 2927

    posta81 = to_recipe(
        pasta.grams(275),
        bolognese3.grams(275),
        cream.grams(15),
        butter.grams(10),
        olive_oil.grams(10),
        name="Pasta w/ bolognese"
    )
    posta81.mass = 2923 - 2050

    to_day(Date(2020, 1, 9),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 20).add_no_diff(cinnamon.grams(1)),
        milk_2.grams(1350),
        fried_rice7.grams(410),
        posta81.grams(522),
    )

    to_day(Date(2020, 1, 10),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 20).add_no_diff(cinnamon.grams(1)),
        milk_2.grams(1350),
        posta81.grams(posta81.mass - 522),
        to_recipe(perogies2.times(16), onion_rings.times(5), butter.grams(20), sriracha.grams(30), name="Perogies and onion rings"),
    )

    to_day(Date(2020, 1, 11),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 25).add_no_diff(cinnamon.grams(1)),
        milk_2.grams(1350),
        banana,
        pecan_butter_tarts,
        fried_rice7.grams(343 + 250),
        # Soham's chicken, mashed potatoes, gravy and veggies
    )

    to_day(Date(2020, 1, 12),
        oat_whey_pb(150, 0, 120).combine(maple_syrup, 30),
        milk_2.grams(1350 - 226),
        add(eggs.times(4), milk_2.grams(226), cream.grams(20), maple_syrup.grams(30), cinnamon.grams(1), name="Egg milkshake"),
        cheese_pizza2.add_no_diff(butter.grams(30))
    )

    to_day(Date(2020, 1, 13),
        add(oat_whey_pb(100, 0, 120), maple_syrup.grams(31), eggs.times(2), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350),
        fried_rice7.grams(250),
        # dinner at G&G's
    )

    to_day(Date(2020, 1, 14),
        add(oat_whey_pb(150, 0, 120), maple_syrup.grams(20), eggs.times(1), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350 - 280),
        add(eggs.times(5), milk_2.grams(280), cream.grams(26), maple_syrup.grams(25), cocoa_powder.grams(2), name="Egg milkshake"),
        banana,
        add(pasta.grams(208), neapolitan_sauce.grams(260), butter.grams(15), olive_oil.grams(15), name="Pasta w/ neapolitan_sauce"),
    )

    to_day(Date(2020, 1, 15),
        add(oat_whey_pb(150, 0, 120), maple_syrup.grams(20), eggs.times(1), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350 - 280),
        fried_rice7.grams(487 - 373),
        add(eggs.times(5), milk_2.grams(280), maple_syrup.grams(25), cocoa_powder.grams(2), name="Egg milkshake"),
        pecan_butter_tarts,
        add(perogies2.times(10), onion_rings.times(5), butter.grams(14), sriracha.grams(30), name="Perogies and onion rings")
    )

    posta82 = to_recipe(
        pasta.grams(425),
        bolognese3.grams(503),
        olive_oil.grams(25),
        butter.grams(25),
        name="Pasta w/ bolognese"
    )
    posta82.mass = 4459 - 2927

    to_day(Date(2020, 1, 16),
        pecan_butter_tarts,
        add(oat_whey_pb(150, 0, 100), maple_syrup.grams(20), eggs.times(2), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350),
        add(eggs.times(5), maple_syrup.grams(25), cocoa_powder.grams(2), name="Egg milkshake"),
        posta82.grams(600),
    )

    to_day(Date(2020, 1, 17),
        add(oat_whey_pb(150, 0, 120), maple_syrup.grams(15), eggs.times(2), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350),
        kirkland_croissants.times(3),
        add(eggs.times(5), maple_syrup.grams(25), cocoa_powder.grams(2), name="Egg milkshake"),
        posta82.grams(225)
    )

    to_day(Date(2020, 1, 18),
        add(oat_whey_pb(150, 0, 120), maple_syrup.grams(20), eggs.times(2), cocoa_powder.grams(4), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350),
        add(eggs.times(5), maple_syrup.grams(20), cinnamon.grams(1), name="Egg milkshake"),
        posta82.grams(530),
        kirkland_croissants.times(1),
    )

    to_day(Date(2020, 1, 19),
        pecan_butter_tarts,
        add(oat_whey_pb(150, 0, 120), maple_syrup.grams(20), eggs.times(2), cinnamon.grams(1), name="Oat / pb shake w/ maple syrup and eggs"),
        milk_2.grams(1350),
        kirkland_croissants.times(2),
        add(eggs.times(5), maple_syrup.grams(20), cinnamon.grams(1), name="Egg milkshake"),
    )

    to_day(Date(2020, 1, 20),
        kirkland_pizza,
        add(eggs.times(7), maple_syrup.grams(25), cinnamon.grams(2), name="Egg milkshake"),
        milk_2.grams(1350),
        kirkland_croissants.times(2),
        pecan_butter_tarts,
        add(perogies2.times(5), onion_rings.times(3), butter.grams(5), sriracha.grams(15), name="Perogies and onion rings")
    )

    posta83 = to_recipe(
        pasta.grams(400),
        neapolitan_sauce.grams(480),
        butter.grams(20),
        olive_oil.grams(20),
        name="Pasta w/ neapolitan sauce"
    )
    posta83.mass = 4350 - 2927

    to_day(Date(2020, 1, 21),
        add(eggs.times(7), maple_syrup.grams(20), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350),
        add(perogies2.times(8), onion_rings.times(4), sriracha.grams(15), name="Perogies and onion rings"),
        kirkland_croissants.times(2),
        posta83.grams(513 + 200),
    )

    to_day(Date(2020, 1, 22),
        add(eggs.times(7), maple_syrup.grams(22), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350),
        kirkland_croissants.times(2),
        posta83.grams(posta83.mass - 513 - 200),
        add(perogies2.times(7), onion_rings.times(5), butter.grams(5), sriracha.grams(15), name="Perogies and onion rings"),
    )

    posta84 = add(
        ravioli.grams(330),
        mirepoix_sauce6.grams(170),  # <-- too much!  Try like 140 next time
        olive_oil.grams(15),
        name="Ravioli w/ sauce"
    )


    to_day(Date(2020, 1, 23),
        add(eggs.times(7), maple_syrup.grams(25), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350),
        add(perogies2.times(7), onion_rings.times(5), butter.grams(5), sriracha.grams(15), name="Perogies and onion rings"),
        posta84,
        kirkland_pizza.add_no_diff(butter.grams(15)).times(4/6)
    )

    to_day(Date(2020, 1, 24),
        add(eggs.times(7), maple_syrup.grams(30), cinnamon.grams(2), name="Egg milkshake"),
        milk_2.grams(1350),
        kirkland_pizza.add_no_diff(butter.grams(15)).times(2/6),
        oat_whey_pb(100, 0, 50).combine(maple_syrup, 30),
        banana,
        add(perogies2.times(12), onion_rings.times(11), butter.grams(25), sriracha.grams(15), name="Perogies and onion rings"),
    )

    pan_pizza2 = to_recipe(
        flour2.grams(350),
        water.grams(320),  # about 90% ish
        # yeast.grams(5),
        salt.grams(5),
        butter.grams(25),  # <-- try spreading with wax paper instead of melting
        mirepoix_sauce6.grams(250),
        olive_oil.grams(10),
        italiano.grams(200),

        red_onion.grams(100),
        pepperoni.grams(120),

        # 375 for 25 min  <-- not long enough; try 30 min at 400
        # broil 5 min
        name="Pan pizza"
    )

    to_day(Date(2020, 1, 25),
        add(eggs.times(6), maple_syrup.grams(40), cinnamon.grams(2), name="Egg milkshake"),
        milk_2.grams(1350),
        banana.times(2),
        pan_pizza2.times(1/2),
        milk_homo.grams(920),
    )

    to_day(Date(2020, 1, 26),
        add(eggs.times(8), maple_syrup.grams(57), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(430 + 400),
        pan_pizza2.times(1/6),
        # Dinner with Oma&Opa at red lobster
    )

    posta85 = add(
        ravioli.grams(330),
        mirepoix_sauce6.grams(140),
        olive_oil.grams(15),
        # pasta_water.grams(25),
        name="Ravioli w/ sauce"
    )

    to_day(Date(2020, 1, 27),
        add(eggs.times(8), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(930),
        pan_pizza2.times(1/3),
        posta85,
        add(perogies2.times(2), onion_rings.times(4), butter.grams(10), sriracha.grams(15), name="Perogies and onion rings"),
    )

    to_day(Date(2020, 1, 28),
        add(eggs.times(8), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350),
        add(perogies2.times(4), onion_rings.times(4), butter.grams(10), sriracha.grams(15), name="Perogies and onion rings"),
        kirkland_pizza
    )


    posta86 = to_recipe(
        pasta.grams(105),
        neapolitan_sauce.grams(305),
        chicken_thighs.grams(190),
        butter.grams(20),
        olive_oil.grams(15),
        name="3-meat pasta"
    )


    to_day(Date(2020, 1, 30),
        add(eggs.times(8), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        milk_2.grams(900),
        milk_homo.grams(1350),
        posta86.times(1/2),
        add(ravioli.grams(370), mirepoix_sauce6.grams(140), olive_oil.grams(20), water.grams(20), name="Ravioli w/ sauce"),
    )

    mirepoix_sauce7 = to_recipe(
        onion.grams(450),
        #celery.grams(200),
        carrots.grams(130),
        garlic.grams(50),
        butter.grams(50),
        oregano.grams(10),
        salt.grams(15),
        # worcestershire.grams(28),
        # red_wine.grams(300),
        canned_tomatoes.grams(1600),
        name="Mirepoix sauce w/ red"
    )

    pan_pizza3 = to_recipe(
        flour2.grams(350),
        water.grams(320),  # about 90% ish
        # yeast.grams(8),
        salt.grams(5),
        butter.grams(28),  # <-- try spreading with wax paper instead of melting
        mirepoix_sauce7.grams(200),
        olive_oil.grams(0),
        italiano.grams(220),

        red_onion.grams(60),
        pepperoni.grams(85),

        # 400 crust only for 5 min; then 30 min at 400
        name="Pan pizza"
    )

    egg_whites.water = 90

    to_day(Date(2020, 1, 31),
        add(eggs.times(8), egg_whites.grams(-130), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350 + 900),
        pan_pizza3.times(2/6),
    )

    homemade_bread3 = to_recipe(
        flour2.grams(400),
        egg_whites.grams(130),
        water.grams(200),
        butter.grams(20),
        salt.grams(6),
        name="Bread"
    )
    homemade_bread3.mass = 695

    to_day(Date(2020, 2, 1),
        add(eggs.times(8), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        milk_homo.grams(1350),
        pan_pizza3.times(2/6),
        homemade_bread3.grams(95 + 60).combine(butter, 22)
    )

    posta87 = to_recipe(
        pasta.grams(400),
        mirepoix_sauce7.grams(420),
        butter.grams(6),
        olive_oil.grams(59),
        name="Pasta w/ sauce"
    )
    posta87.mass = 4352 - 2927

    to_day(Date(2020, 2, 2),
        pan_pizza3.times(2/6),
        milk_homo.grams(1350),
        add(eggs.times(8), maple_syrup.grams(50), cinnamon.grams(2), name="Egg milkshake"),
        posta87.grams(800),
    )

    to_day(Date(2020, 2, 3),
        add(eggs.times(8), orange_juice.grams(350), cream.grams(25), vanilla_extract.grams(10), name="OJ Egg shake"),
        milk_homo.grams(1350),
        posta87.grams(posta87.mass - 800),
        homemade_bread3.grams(280).combine(butter, 50)
    )

    posta88 = to_recipe(
        pasta.grams(350),
        mirepoix_sauce7.grams(350),
        butter.grams(20),
        olive_oil.grams(30),
        name="Pasta w/ sauce"
    )
    posta88.mass = 3913 - 2927


    to_day(Date(2020, 2, 4),
        add(eggs.times(8), orange_juice.grams(350), cream.grams(40), honey.grams(15), vanilla_extract.grams(15), name="OJ Egg shake"),
        milk_homo.grams(1350),
        kirkland_pizza,
        posta88.grams(500),
    )

    # cook 15 min at 450
    meatballz4 = add(
        ground_beef_lean.grams(1100 + 289),  # sirloin and oxtail
        panko.grams(75),
        eggs.times(2),
        milk.grams(90),
        garlic_powder.grams(26),
        onion_powder.grams(36),
        chili_flakes.grams(5),
        # black_pepper.grams(2),
        # oregano.grams(2),
        # thyme.grams(1),
        # worcestershire.grams(25),
        salt.grams(14),
        name="Italian meatballs"
    ).times(1/28, name="Italian meatballs")


    posta89 = add(
        pasta.grams(350),
        mirepoix_sauce7.grams(300),
        meatballz4.times(4),
        butter.grams(30),
        olive_oil.grams(30),
        name="Pasta w/ sauce"
    )
    posta89.mass = 4111 - 2927

    to_day(Date(2020, 2, 6),
        add(eggs.times(8), orange_juice.grams(350), cream.grams(50), vanilla_extract.grams(15), name="OJ Egg shake"),
        posta89.times(3/4),
        milk_homo.grams(1350),
    )

    to_day(Date(2020, 2, 7),
        add(eggs.times(6), orange_juice.grams(380), cream.grams(50), vanilla_extract.grams(15), name="OJ Egg shake"),
        posta89.times(1/4),
        milk_homo.grams(1350),
        homemade_bread3.grams(280).combine(butter, 66),
    )

    to_day(Date(2020, 2, 8),
        milk_homo.grams(1350),
        kirkland_croissants.times(3),
    )

    posta90 = to_recipe(
        pasta.grams(414),
        neapolitan_sauce.grams(200),
        mirepoix_sauce7.grams(200),
        butter.grams(20),
        olive_oil.grams(30),
        name="Pasta w/ mixed sauce"
    )
    posta90.mass = 4246 - 2927

    to_day(Date(2020, 2, 10),
        kirkland_croissants.times(3),
        milk_homo.grams(1350),
        add(eggs.times(8), maple_syrup.grams(28), cinnamon.grams(2), name="Egg milkshake"),
        posta90.grams(653),
    )

    beef_curry = to_recipe(
        ground_beef_medium.grams(710),
        cream.grams(50),
        garlic.grams(20),
        ginger.grams(20),
        thai_chilies.grams(12),
        red_onion.grams(110),
        mirepoix_sauce7.grams(100),
        # lemon_juice.grams(10),
        light_soy.grams(20),
        butter.grams(60),
        salt.grams(10),
        pepper.grams(2),
        name="Beef curry"
    )
    beef_curry.mass = 810

    to_day(Date(2020, 2, 15),
        add(eggs.times(8), orange_juice.grams(380), cream.grams(50), vanilla_extract.grams(15), name="OJ Egg shake"),
        beef_curry.grams(240).combine(rice, 233 * (212 / (375 + 233))),
        milk_homo.grams(1350),
    )

    posta91 = to_recipe(
        pasta.grams(280),
        mirepoix_sauce7.grams(236),
        butter.grams(30),
        olive_oil.grams(15),
        cream.grams(40),
        meatballz4.times(4),
        name="Pasta w/ meatballs"
    )

    to_day(Date(2020, 2, 16),
        add(eggs.times(8), orange_juice.grams(300), cream.grams(50), vanilla_extract.grams(20), name="OJ Egg shake"),
        milk_homo.grams(1350),
        posta91.times(1/2),
    )

    to_day(Date(2020, 2, 17),
        milk_homo.grams(1350),
        posta91.times(1/2),
        beef_curry.grams(230).combine(rice, 230 * (212 / (375 + 233))),
    )

    perogies_mix = to_recipe(
        perogies3.times(10),
        italiano.grams(80),
        pepperoni.grams(65),
        name="Perogies with pepperoni and italiano"
    )

    to_day(Date(2020, 2, 18),
        add(eggs.times(8), egg_whites.grams(-260), orange_juice.grams(240), cream.grams(60), vanilla_extract.grams(20), name="OJ Egg shake"),
        milk_homo.grams(1350),
        beef_curry.grams(300).combine(rice, 165 * (212 / (375 + 233))),
        pepperoni.grams(30),
        perogies_mix.times(1/2),
    )


    # poutine2 = to_recipe(
    #     potatoes.grams(1078),
    #     butter.grams(100),
    #     flour2.grams(15),
    #     garlic.grams(37),
    #     pepperoni.grams(71),
    #     # pepper.grams(4),
    #     # thyme.grams(0.6),
    #     # salt: 3%,
    #     # baking soda 5g / 3000mL
    #     # 250mL stock
    #     name="Poutine"
    # )
    # poutine2.mass = 2490 - 1870

    poutine3 = to_recipe(
        potatoes.grams(860),
        garlic.grams(40),
        pepper.grams(1),
        butter.grams(70 + 27),
        flour2.grams(5.5),
        # stock.grams(100),  # <--- double this next time
        pepperoni.grams(90),
        italiano.grams(50),
        name="Poutine w/ pepperoni"
    )

    to_day(Date(2020, 2, 19),
        add(eggs.times(8), orange_juice.grams(300), cream.grams(60), vanilla_extract.grams(20), name="OJ Egg shake"),
        milk_homo.grams(1350),
        perogies_mix.times(1/2),
        poutine3.times(1/2),
    )

    tandoori_pizza_w_toppings = to_recipe(tandoori_pizza, italiano.grams(50), pepperoni.grams(80), butter.grams(15), name="Tandoori pizza w/ toppings")
    to_day(Date(2020, 2, 20),
        add(oat_whey_pb(63, 0, 108), eggs.times(2), name="Oat / pb shake w/ 2 eggs"),
        milk_homo.grams(1350),
        poutine3.times(1/2),
        tandoori_pizza_w_toppings.times(1/2),
    )

    # cook 15 min at 375
    meatballz5 = to_recipe(
        ground_beef_medium.grams(1046),  # brisket tip
        panko.grams(70),
        eggs.times(2),
        milk.grams(100),
        garlic_powder.grams(25),
        onion_powder.grams(25),
        chili_flakes.grams(5),
        # black_pepper.grams(2),
        # oregano.grams(4),
        # thyme.grams(2),
        # worcestershire.grams(30),
        salt.grams(14),
        name="Italian meatballs"
    ).times(1/35, name="Italian meatballs")



    to_day(Date(2020, 2, 21),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), name="Oat / pb shake w/ 2 eggs"),
        milk_homo.grams(1350),
        tandoori_pizza_w_toppings.times(1/2),
        meatballz5.times(5),
    )

    chili_garlic_sauce = to_recipe(
        canned_tomatoes.grams(1250),
        garlic.grams(65),
        oregano.grams(6),
        # thai_chilies.grams(14),
        butter.grams(35),
        name="Chili garlic pasta sauce"
    )

    posta92 = to_recipe(
        pasta.grams(360),
        chili_garlic_sauce.grams(360),
        meatballz5.times(10),
        olive_oil.grams(50),
        name="Pasta w/ meatballs"
    )
    posta92.mass = 4440 - 2927

    to_day(Date(2020, 2, 22),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(25), cinnamon.grams(1),  name="Oat / pb shake w/ 4 eggs"),
        milk_homo.grams(1350),
        posta92.grams(587)
    )

    to_day(Date(2020, 2, 23),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(25), cinnamon.grams(1), name="Oat / pb shake w/ 4 eggs"),
        milk_homo.grams(1350),
        posta92.grams(587)
    )

    to_day(Date(2020, 2, 25),
        kirkland_croissants.times(3),
        milk_homo.grams(1350),
        add(eggs.times(8), orange_juice.grams(300), cream.grams(60), vanilla_extract.grams(20), name="Pineapple Egg shake"),
        pepperoni.grams(55),
        kirkland_croissants.times(1),
        milk_homo.grams(350),
    )

    to_day(Date(2020, 2, 26),
        kirkland_croissants.times(2),
        milk_homo.grams(1350),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(25), cinnamon.grams(1), name="Oat / pb shake w/ 4 eggs"),
        kirkland_croissants.times(2),
        pepperoni.grams(65),
    )

    beef_tallow = Food(cals=900, carbs=0, protein=0, fat=100, sugar=0, name="Beef tallow")

    # poutine3 = to_recipe(
    #     potatoes.grams(860),
    #     garlic.grams(40),
    #     pepper.grams(1),
    #     butter.grams(70 + 27),
    #     flour2.grams(5.5),
    #     # stock.grams(100),  # <--- double this next time
    #     pepperoni.grams(90),
    #     italiano.grams(50),
    # )

    poutine4 = to_recipe(
        potatoes.grams(1050),
        butter.grams(79),
        garlic.grams(58),
        pepper.grams(1.5),
        beef_tallow.grams(42),

        # stock.grams(200),
        flour2.grams(10),

        pepperoni.grams(130),
        italiano.grams(110),
        egg_whites.grams(260),
        name="Poutine w/ pepperoni"
    )
    poutine4.mass = 4065 - 2928

    to_day(Date(2020, 2, 27),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(25), cinnamon.grams(1), name="Oat / pb shake w/ 4 eggs"),
        kirkland_croissants.times(2),
        milk_homo.grams(1350),
        poutine4.grams(305),
    )

    to_day(Date(2020, 2, 28),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 4 eggs"),
        kirkland_croissants.times(2),
        milk_homo.grams(1350),
        poutine4.grams(205),
    )

    to_day(Date(2020, 2, 29),
        add(oat_whey_pb(61, 0, 110), eggs.times(4), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 4 eggs"),
        milk_homo.grams(1350),
        poutine4.grams(poutine4.mass - 205 - 305),
    )

    posta93 = to_recipe(
        pasta.grams(375),
        chili_garlic_sauce.grams(375),
        meatballz5.times(9.5),
        olive_oil.grams(20),
        name="Pasta w/ meatballs"
    )
    posta93.mass = 3453 - 2050

    to_day(Date(2020, 3, 1),
        add(eggs.times(8), egg_whites.grams(-200), orange_juice.grams(300), cream.grams(65), vanilla_extract.grams(15), name="OJ egg shake"),
        posta93.times(1/2),
        milk_homo.grams(1350),
        # Ribs at G&G's
    )
    to_day(Date(2020, 3, 2),
        posta93.times(1/2),
        milk_homo.grams(1350),
        add(oat_whey_pb(61, 0, 110), eggs.times(3), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    whey = Food(cals=365, carbs=7.3, protein=85.4, fat=1.2, sugar=0, name="Kaizen whey")
    whey.potassium = 0.536
    whey.sodium = 0.170

    to_day(Date(2020, 3, 3),
        add(oat_whey_pb(61, 30, 110), eggs.times(3), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        milk_homo.grams(1350),
        pepperoni.grams(45),
    )

    posta94 = to_recipe(
        pasta.grams(300),
        chili_garlic_sauce.grams(300),
        meatballz5.times(11),
        pepperoni.grams(104),
        olive_oil.grams(30),
        name="Pasta w/ meatballs"
    )
    posta93.mass = 4080 - 2927


    to_day(Date(2020, 3, 4),
        add(oat_whey_pb(61, 30, 110), eggs.times(3), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        milk_homo.grams(1350),
        posta94.grams(380)
    )

    to_day(Date(2020, 3, 5),
        add(oat_whey_pb(61, 30, 110), eggs.times(3), maple_syrup.grams(20), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        milk_homo.grams(1350),
        posta94.grams(360)
    )

    to_day(Date(2020, 3, 6),
        add(oat_whey_almond(0, 30, 100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        add(perogies3.grams(390), butter.grams(65), italiano.grams(35), egg_whites.grams(200), name="Perogies").times(3/4),
    )


    beef_curry = to_recipe(
        ground_beef_medium.grams(900),
        cream.grams(70),
        garlic.grams(50),
        ginger.grams(30),
        thai_chilies.grams(12),
        chili_flakes.grams(5),
        red_onion.grams(110),
        mirepoix_sauce7.grams(100),
        # lemon_juice.grams(15),
        light_soy.grams(20),
        butter.grams(50),
        salt.grams(13),
        KCL.grams(3),
        pepper.grams(4),
        # turmeric.grams(2),
        rice.grams(300),
        name="Beef curry"
    )
    beef_curry.mass = 4022 - 2927 + 1000
    # rice.grams(300).add(KCL.grams(2)).add(salt.grams(1))

    to_day(Date(2020, 3, 8),
        add(oat_whey_almond(0, 30, 80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        add(perogies3.grams(390), butter.grams(65), italiano.grams(35), egg_whites.grams(200), name="Perogies").times(1/4),
        beef_curry.grams(450).times(5/6)
    )

    to_day(Date(2020, 3, 9),
        add(oat_whey_almond(0, 30, 80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        beef_curry.grams(450).times(35/45)
    )

    to_day(Date(2020, 3, 10),
        beef_curry.grams(420),
        add(oat_whey_almond(0, 50, 110), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
    )

    to_day(Date(2020, 3, 11),
        add(oat_whey_almond(0, 50, 110), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        beef_curry.grams(300),
    )

    to_day(Date(2020, 3, 12),
        add(oat_whey_almond(0, 50, 80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        beef_curry.grams(300).add_no_diff(butter.grams(15)),
    )

    # butter_chicken2 = to_recipe(
    #     butter.grams(95),
    #     garlic.grams(47),  # <-- too much :( maybe try 30 next time
    #     # ginger.grams(15), # <-- a bit more would be good, perhaps 20-25
    #     canned_tomatoes.grams(300),
    #     chicken_breast.sale().grams(730),  # not enough sauce for this much chicken
    #     buttermilk.grams(75),
    #     milk.grams(80),
    #     honey.grams(30),
    #     name="Butter chicken"
    #     # Marinate chicken longer in buttermilk!
    #     # use very slightly less cumin next time
    # )
    # butter_chicken2.mass = 3169 - 2052
    # butter_chicken2.name="Butter chicken"

    butter_chicken_seasoning = to_recipe(
        paprika.grams(12),
        oregano.grams(1 + 4),
        cumin.grams(4),
        garlic_powder.grams(4),
        chili_flakes.grams(1),
        coriander.grams(1.25),
        # turmeric.grams(0.25),
        salt.grams(2.5),
        KCL.grams(2.5),
        name="Butter chicken mix"
    )

    butter_chicken3 = to_recipe(
        buttermilk.grams(150),
        chicken_thighs.sale().grams(702),

        butter.grams(127),
        garlic.grams(40),
        ginger.grams(35),
        onion.grams(400),
        # white_wine.grams(200),
        canned_tomatoes.grams(300),
        # butter_chicken_mix.grams(45),
        maple_syrup.grams(30),
        KCL.grams(3.5),
        salt.grams(3.5),
        butter_chicken_seasoning,
        cream.grams(100),

        # rice.grams(200).add_no_diff(salt.grams(2)),
        name="Butter chicken"
    )
    butter_chicken3.mass = 4505 - 2927
    butter_chicken3.name = "Butter chicken"
    rice1 = rice.grams(200).add_no_diff(salt.grams(2))


    to_day(Date(2020, 3, 13),
        milk_homo.grams(700),
        sausage2.times(2),
        butter_chicken3.grams(371).combine(rice1, 200 / 3),
        whey.grams(32),
    )

    to_day(Date(2020, 3, 14),
        add(whey.grams(50), almond_butter.grams(80), eggs.times(3), maple_syrup.grams(15), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(650),
        butter_chicken3.grams(350).combine(rice1, 200 / 3),
    )

    to_day(Date(2020, 3, 15),
        add(whey.grams(50), almond_butter.grams(72), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(700),
        # butter_chicken3.grams(350).combine(rice1, 200 / 3),
        sausage2.times(2.5),
        add(perogies3.grams(103).times(1/2), italiano.grams(70), name="Perogies w/ cheese")
    )

    to_day(Date(2020, 3, 16),
        milk_homo.grams(650),
        # butter_chicken3.grams(350).combine(rice1, 200 / 3),
        sausage2.times(0.5),
        add(perogies3.grams(103).times(1/2), name="Perogies w/ cheese"),
        add(whey.grams(50), peanut_butter.grams(28), almond_butter.grams(55), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        butter_chicken3.grams(320).combine(rice1, 200 / 3),

    )

    to_day(Date(2020, 3, 17),
        milk_homo.grams(700),
        add(whey.grams(50), almond_butter.grams(89), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        butter_chicken3.grams(340).combine(rice, 100).add_no_diff(salt.grams(1)).add_no_diff(KCL.grams(1)),

    )

    chili = to_recipe(
        # - too many beans: try a quarter the weight of beef next time
        # - not spicy enough
        # - beef not fatty enough
        # - bacon is useless
        # - use worcestershire next time
        # - more pepper
        onion.grams(4026 - 3485),
        garlic.grams(50),
        # red_wine.grams(250),

        ground_beef_lean.grams(900),
        bacon.times(10),
        drained_fat.grams(150),
        butter.grams(150),
        cream.grams(100),
        canned_tomatoes.grams(796),
        dried_red_beans.grams(450),

        salt.grams(5+7.5),
        KCL.grams(5+2.5),
        oregano.grams(13),
        pepper.grams(5),
        ancho_powder.grams(12),
        habanero_powder.grams(8),
        chili_flakes.grams(5),
        paprika.grams(8),
        chipotle_powder.grams(5),
        garlic_powder.grams(5),
        onion_powder.grams(5),
        cumin.grams(1),
        # 5g oregano, 5g pepper, 5g ancho, 4g habanero, 5g chili flakes, 3g paprika, 40g concoction
        name="Chili"
    )
    chili.mass = 4011 - 700 # ?

    to_day(Date(2020, 3, 18),
        milk_homo.grams(650),
        add(whey.grams(50), almond_butter.grams(89), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        chili.grams(540)
    )

    to_day(Date(2020, 3, 19),
        chili.grams(300 + 200),
        milk_homo.grams(700),
        add(whey.grams(50), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    to_day(Date(2020, 3, 20),
        chili.grams(350 + 200),
        milk_homo.grams(650),
        add(whey.grams(50), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(5), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    to_day(Date(2020, 3, 21),
        chili.grams(300 + 300),
        milk_homo.grams(800),
        # sausage2.times(2),
        add(whey.grams(40), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(5), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    to_day(Date(2020, 3, 22),
        chili.grams(600),
        milk_homo.grams(550),
        sausage2.times(2),
        add(whey.grams(30), peanut_butter.grams(50), eggs.times(2), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    to_day(Date(2020, 3, 23),
        chili.grams(400),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    to_day(Date(2020, 3, 24),
        milk_homo.grams(1350),
        add(sausage2.times(3), soy_sauce.grams(5), sriracha.grams(5), maple_syrup.grams(5), name="Sausages w/ sauce"),
        add(whey.grams(40), peanut_butter.grams(40), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    oxtail_soup = to_recipe(
        # Even more onion!
        # Could add more butter, but not necessary
        # Maybe higher celery to carrot ratio?
        # Perhaps 30% more oxtail would be good
        # What about adding spinach?
        # More pepper?
        # Thicken with flour?
        onion.grams(500),
        garlic.grams(40),
        butter.grams(40 + 50),
        olive_oil.grams(20),
        canned_tomatoes.grams(400),
        carrots.grams(230),
        celery.grams(230),
        # stock.grams(~1600), # <-- salt included in total below
        # evaporated_red.grams(250),
        oxtail.grams(600),
        # 2 branches parsley
        # 2-3 branches thyme
        worcestershire.grams(20),
        # 4 bay leaves
        # sprinkle of oregano
        chili_flakes.grams(5),
        KCL.grams(4),
        salt.grams(6 + 12),
        name="Oxtail soup"
    )
    oxtail_soup.mass = 3588 + 1796 - 2927 + 50

    to_day(Date(2020, 3, 25),
        milk_homo.grams(1350),
        add(whey.grams(50), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        oxtail_soup.grams(430),
    )

    to_day(Date(2020, 3, 26),
        milk_homo.grams(1350),
        add(whey.grams(31), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        oxtail_soup.grams(430),
    )

    to_day(Date(2020, 3, 27),
        milk_homo.grams(1350),
        add(whey.grams(31), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        oxtail_soup.grams(500),
    )

    to_day(Date(2020, 3, 28),
        to_recipe(
            bacon.times(5),
            drained_fat.grams(60),
            sriracha.grams(12),
            kirkland_sausage_honey_garlic.times(2),
            add(sriracha.grams(15*((262 - 246) / (262-224))), mustard.grams(10*((262 - 246) / (262-224))), maple_syrup.grams(15 * ((262 - 246) / (262-224))), name="dipping sauce"),
            name="Bacon and sausages w/ dipping sauce"
        ),
        milk_homo.grams(1350),
        add(whey.grams(21), peanut_butter.grams(53), eggs.times(1), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    chicken_vindaloo = to_recipe(
        onion.grams(440 - 160),
        red_onion.grams(160),
        garlic.grams(40),
        ginger.grams(18),
        butter.grams(50 + 50),
        cream.grams(25 + 75),
        # evaporated_red.grams(250),
        chicken_thighs.grams(756),
        canned_tomatoes.grams(400),

        KCL.grams(3),
        salt.grams(5.5 + 4),
        worcestershire.grams(14),
        # red_wine_vinegar.grams(55),
        thai_chilies.grams(30),
        # cloves.tsp(0.5),
        paprika.grams(3.5),
        # turmeric.tsp(1.5),
        # mustard.tsp(0.5),
        # cumin.grams(0.75),
        cinnamon.grams(0.6),
        name="Chicken vindaloo"
    )
    chicken_vindaloo.mass = 4478 - 2927


    to_day(Date(2020, 3, 29),
        milk_homo.grams(1350),
        add(whey.grams(31), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        to_recipe(rice.grams(75), KCL.grams(0.75), chicken_vindaloo.grams(300), olive_oil.grams(10), name="Chicken vindaloo w/ rice"),
    )

    to_day(Date(2020, 3, 30),
        milk_homo.grams(1350),
        add(whey.grams(32), peanut_butter.grams(83), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        to_recipe(rice.grams(75), KCL.grams(0.75), chicken_vindaloo.grams(300), olive_oil.grams(15), name="Chicken vindaloo w/ rice"),
    )

    to_day(Date(2020, 3, 31),
        milk_homo.grams(1350),
        bacon.times(5).drain_fat(75).combine(sriracha, 20),
        add(whey.grams(32), peanut_butter.grams(90), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        to_recipe(rice.grams(80), KCL.grams(1.5), chicken_vindaloo.grams(200), butter.grams(15), name="Chicken vindaloo w/ rice"),
    )

    to_day(Date(2020, 4, 1),
        milk_homo.grams(1350),
        to_recipe(rice.grams(80), KCL.grams(1.5), chicken_vindaloo.grams(420), butter.grams(30), name="Chicken vindaloo w/ rice"),
        add(whey.grams(32), almond_butter.grams(80), eggs.times(3), maple_syrup.grams(0), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    lamb_meatballs = to_recipe(
        ground_beef_medium.grams(670),
        panko.grams(50),
        eggs.times(2),
        milk.grams(100),

        garlic_powder.grams(15),
        onion_powder.grams(20),
        cumin.grams(1),
        # turmeric.grams(0.5),
        paprika.grams(8),
        pepper.grams(2),
        chili_flakes.grams(5),
        oregano.grams(8),
        worcestershire.grams(30),
        salt.grams(11),
        name="Lamb meatballs"
    ).times(1/25)

    mint_sauce = to_recipe(
        olive_oil.grams(50),
        garlic.grams(20),
        liberte_greek_yogurt.grams(200),
        salt.grams(2),
        KCL.grams(2),
        name="mint sauce"
    )

    to_day(Date(2020, 4, 2),
        add(kirkland_sausage_honey_garlic.times(2), maple_syrup.grams(8), sriracha.grams(8), name="Sausages w/ maple syrup and sriracha"),
        kefir.grams(500),
        add(whey.grams(32), almond_butter.grams(80), eggs.times(3), maple_syrup.grams(0), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(1350),
        lamb_meatballs.times(2)
    )

    to_day(Date(2020, 4, 3),
        milk_homo.grams(1250),
        add(whey.grams(32), almond_butter.grams(80), eggs.times(4), maple_syrup.grams(0), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        lamb_meatballs.times(7).combine(mint_sauce, 60)
    )

    to_day(Date(2020, 4, 4),
        add(whey.grams(32), almond_butter.grams(60), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        milk_homo.grams(850),
        lamb_meatballs.times(5).combine(mint_sauce, 80),
        kirkland_sausage_honey_garlic.times(2),
    )

    to_day(Date(2020, 4, 5),
        milk_homo.grams(1350),
        add(kirkland_sausage_honey_garlic.times(3), sriracha.grams(20 * 27/40), soy_sauce.grams(10 * 27/40), honey.grams(10 * 27/40), name="Sausages w/ maple syrup and sriracha"),
        add(whey.grams(22), almond_butter.grams(50), eggs.times(2), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    homemade_yogurt = to_recipe(milk_homo.grams(500), yogurt_whey.grams(-180), sugar.grams(-7), name="Fresh yogurt")

    x = to_day(Date(2020, 4, 6),
        milk_homo.grams(1350),
        add(whey.grams(30), almond_butter.grams(80), eggs.times(3), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        lamb_meatballs.times(7).combine(mint_sauce, 60)
    )
    # from matplotlib import pyplot
    # pyplot.pie([x.ratio_c(), x.ratio_p(), x.ratio_f()], labels=["%d%%" % (x.ratio_c()*100), "%d%%" % (x.ratio_p()*100), "%d%%" % (x.ratio_f()*100)])
    # pyplot.legend(["Carbs (%dg)" % (x.c), "Protein (%dg)" % x.p, "Fat (%dg)" % x.f])
    # pyplot.show()

    to_day(Date(2020, 4, 7),
        lamb_meatballs.times(4).combine(mint_sauce, 20),
        milk_homo.grams(1350),
        homemade_yogurt.combine(maple_syrup, 14).combine(cinnamon, 0.5),
        perogies3.grams(330).combine(butter, 16).combine(italiano, 150),
        whey.grams(20)
    )

    to_day(Date(2020, 4, 8),
        kefir.grams(500),
        add(whey.grams(25), almond_butter.grams(70), eggs.times(3), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
        vincenzos_sausage.times(2),
        milk_homo.grams(1450),
    )

    def yogurt(milk_g):
        return add(
            milk_homo.grams(milk_g),
            yogurt_whey.grams(-0.5 * milk_g),
            sugar.grams(-0.25 * milk_g * 0.048),
            name="Fresh yogurt"
        )

    homemade_yogurt2 = yogurt(1250)

    to_day(Date(2020, 4, 9),
        kefir.grams(500),
        vincenzos_sausage.times(2),
        milk_homo.grams(350),
        homemade_yogurt2.combine(maple_syrup, 30).combine(cinnamon, 1),
        add(whey.grams(30), peanut_butter.grams(60), eggs.times(3), cinnamon.grams(1), name="Oat / almond shake w/ 3 eggs"),
    )

    fried_chicken2 = to_recipe(
        chicken_breast.grams(650),
        olive_oil.grams(54), # actually avocado oil
        buttermilk.grams(50),
        light_soy.grams(15),
        flour.grams(160),
        eggs.times(2),
        salt.grams(3),
        KCL.grams(3),
        pepper.grams(1.6),
        # white_pepper.grams(1.6),
        chili_flakes.grams(4),
        garlic_powder.grams(8),
        onion_powder.grams(8),
        oregano.grams(4),
        name="Fried chicken"
    )
    fried_chicken2.mass = 852

    sweet_sauce = to_recipe(
        garlic.grams(40),
        ginger.grams(20),
        KCL.grams(1),
        sriracha.grams(20),
        # louisiana.grams(20),
        butter.grams(50),
        light_soy.grams(78),
        honey.grams(80),
    )
    sweet_sauce.mass = 260

    to_day(Date(2020, 4, 13),
        bacon.times(5).drain_fat(55).combine(sriracha, 20),
        yogurt(1000).combine(maple_syrup, 20).combine(cinnamon, 1),
        fried_chicken2.grams(275).combine(sweet_sauce, 66),
        milk_homo.grams(675),
    )

    to_day(Date(2020, 4, 14),
        milk_homo.grams(675),
        add(whey.grams(30), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(14), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        fried_chicken2.grams(206).combine(sweet_sauce, 55),
        homemade_yogurt2.grams(310).combine(maple_syrup, 20).combine(cinnamon, 1),
    )

    bleh = to_recipe(
        popcorn_chicken.grams(200),
        butter.grams(20),
        sriracha.grams(17),
        light_soy.grams(9),
        KCL.grams(1),
        salt.grams(.5),
        mayo.grams(10),
        name="Popcorn chicken w/ sauce"
    )

    to_day(Date(2020, 4, 15),
        milk_homo.grams(1350),
        fried_chicken2.grams(186).combine(sweet_sauce, 60),
        add(whey.grams(30), peanut_butter.grams(110), eggs.times(3), maple_syrup.grams(14), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        bleh
    )

    to_day(Date(2020, 4, 16),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(14), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        bacon.times(7).drain_fat(103).combine(sriracha, 27),
        add(popcorn_chicken.grams(210), butter.grams(20), sriracha.grams(19), light_soy.grams(12), mayo.grams(10), mustard.grams(7), name="Popcorn chicken w/ sauce")
    )

    homemade_sausage = to_recipe(
        pork_shoulder.grams(800),
        garlic.grams(20),  # Don't actually remember how much, this is approx
        fennel.grams(3),
        sage.grams(0.5),
        oregano.grams(5),
        pepper.grams(3),
        chili_flakes.grams(5),
        paprika.grams(2),
        salt.grams(6),
        KCL.grams(3),
        name="Homemade sausage"
    )

    to_day(Date(2020, 4, 17),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(19), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        homemade_sausage.grams(300).combine(butter, 10).add_no_diff(salt.grams(0.6)).add_no_diff(KCL.grams(0.4)),
    )

    to_day(Date(2020, 4, 18),
        milk_homo.grams(1350),
        add(whey.grams(42), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(11), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        bacon.times(6).drain_fat(103 * 6/7).combine(sriracha, 20),
        homemade_sausage.grams(125).combine(butter, 10).add_no_diff(salt.grams(1)),
    )


    greek_dressing2 = to_recipe(
        olive_oil.grams(250),
        red_wine_vinegar.grams(100),
        lemon_juice.grams(67),
        oregano.grams(5),
        garlic.grams(12),
        honey.grams(40),
        feta.grams(25),
        pepper.grams(3),
        salt.grams(6.6),
        KCL.grams(3.3),
        name="Greek dressing"
    )

    # greek_salad3 = to_recipe(
    #     tomatoes.grams(170),
    #     feta.grams(100),
    #     kalamata.grams(100),
    #     red_onion.grams(70),
    #     greek_dressing.grams(45),
    #     olive_oil.grams(20),
    #     name="Greek salad"
    # )

    greek_lamb_bowl = to_recipe(
        lamb_leg.grams(680),
        rice.grams(250),
        salt.grams(5),
        KCL.grams(2.5),
        onion.grams(400),
        butter.grams(10),

        olive_oil.grams(30),
        tomatoes.grams(170),
        # red_pepper.times(1),
        feta.grams(100),
        kalamata.grams(100),
        red_onion.grams(161),
        garlic.grams(20),
        greek_dressing2.grams(100),
        name="Greek lamb rice bowl"
    )
    greek_lamb_bowl.mass = 1260 + 3970 - 2927

    to_day(Date(2020, 4, 19),
        milk_homo.grams(1350),
        homemade_sausage.grams(375).combine(butter, 10).add_no_diff(salt.grams(3)),
        greek_lamb_bowl.grams(550),
    )

    to_day(Date(2020, 4, 20),
        milk_homo.grams(1350),
        add(whey.grams(32), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(19), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        greek_lamb_bowl.grams(600).add_no_diff(olive_oil.grams(10)),
    )

    pasta_sauce4 = to_recipe(
        canned_tomatoes.grams(796 * 2),
        butter.grams(50),
        garlic.grams(45),
        thai_chilies.grams(30),
        worcestershire.grams(20),
        salt.grams(6),
        KCL.grams(5),
        basil.grams(10),
        name="Pasta sauce"
    )

    to_day(Date(2020, 4, 21),
        milk_homo.grams(1350),
        add(whey.grams(32), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(19), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        greek_lamb_bowl.grams(450).add_no_diff(olive_oil.grams(10)),
        add(pasta.grams(110), pasta_sauce4.grams(100), butter.grams(15), olive_oil.grams(15), name="Pasta w/ sauce"),
    )

    chicken_vindaloo2 = to_recipe(
        # too much of something, perhaps turmeric or mustard
        # didn't have the same sweetness from last time
        # update: reduce the ginger, reduce the mustard
        thai_chilies.grams(30),
        onion.grams(440 - 160),
        red_onion.grams(160),
        garlic.grams(40),
        ginger.grams(18),
        butter.grams(100 + 10),
        # evaporated_red.grams(250),
        chicken_breast.grams(670),
        canned_tomatoes.grams(400),

        KCL.grams(5),
        salt.grams(5 + 5),
        worcestershire.grams(15),
        # red_wine_vinegar.grams(20),
        buttermilk.grams(100),
        cloves.grams(2),
        paprika.grams(4),
        turmeric.grams(3),
        # mustard_powder.grams(1.5),
        cumin.grams(1),
        cinnamon.grams(1),
        name="Chicken vindaloo"
    )
    chicken_vindaloo2.mass = 4589 - 2927

    to_day(Date(2020, 4, 22),
        milk_homo.grams(1350),
        add(whey.grams(32), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(184), chicken_vindaloo2.grams(267), butter.grams(10), olive_oil.grams(30), salt.grams(20 * 0.03)),
    )

    to_day(Date(2020, 4, 23),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(188), chicken_vindaloo2.grams(288), olive_oil.grams(40), salt.grams(20 * 0.03)),
    )

    to_day(Date(2020, 4, 24),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(110), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(200), chicken_vindaloo2.grams(310), butter.grams(12), olive_oil.grams(30), salt.grams(20 * 0.03)),
    )

    to_day(Date(2020, 4, 25),
        milk_homo.grams(1350),
        greek_lamb_bowl.grams(430).add_no_diff(salt.grams(1)).add_no_diff(pepper.grams(1)),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(130), pasta_sauce4.grams(100), butter.grams(15), olive_oil.grams(15), salt.grams(15 * 0.03), name="Pasta w/ sauce"),
    )

    to_day(Date(2020, 4, 26),
        milk_homo.grams(1350),
        to_recipe(rice.grams(200), chicken_vindaloo2.grams(310), butter.grams(12), olive_oil.grams(35), salt.grams(1), KCL.grams(3)),
        add(whey.grams(30), peanut_butter.grams(80), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 4, 27),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(popcorn_chicken.grams(220), butter.grams(10), sriracha.grams(20), soy_sauce.grams(10), mayo.grams(10), mustard.grams(5), name="Popcorn chicken w/ sauce"),
        add(pasta.grams(100), pasta_sauce4.grams(90), butter.grams(15), olive_oil.grams(15), salt.grams(15 * 0.03), name="Pasta w/ sauce"),
    )

    to_day(Date(2020, 4, 28),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(180), pasta_sauce4.grams(150), butter.grams(17), olive_oil.grams(30), feta.grams(50), italiano.grams(30), salt.grams(25 * 0.03), name="Pasta w/ sauce and feta"),
    )

    to_day(Date(2020, 4, 29),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(180), pasta_sauce4.grams(150), chicken_thighs.grams(120), butter.grams(30), olive_oil.grams(15), salt.grams(25 * 0.02), name="Pasta w/ chicken"),
    )

    remaining_vindaloo = to_recipe(rice.grams(200), chicken_vindaloo2.grams(400), butter.grams(20), olive_oil.grams(20), salt.grams(2), KCL.grams(2), garlic_powder.grams(2), onion_powder.grams(2), name="Chicken vindaloo w/ rice")
    to_day(Date(2020, 4, 30),
        milk_homo.grams(1350),
        remaining_vindaloo.times(2/3),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        # add(popcorn_chicken.grams(220), butter.grams(10), sriracha.grams(20), soy_sauce.grams(10), mayo.grams(10), mustard.grams(5), name="Popcorn chicken w/ sauce"),
    )

    to_day(Date(2020, 5, 1),
        milk_homo.grams(1350),
        remaining_vindaloo.times(1/3),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(bread.grams(150), butter.grams(40), italiano.grams(60), garlic_powder.grams(2.5), pepper.grams(1.5), name="Garlic bread"),
    )

    homemade_sausage2 = to_recipe(
        pork_shoulder.grams(824),
        pork_tenderloin.grams(1060 - 298 - 370),
        fennel.grams(5),
        sage.grams(1.5),
        oregano.grams(8),
        pepper.grams(5),
        paprika.grams(6.5),
        chili_flakes.grams(8),
        salt.grams(9),
        KCL.grams(4),
        maggi.grams(17),
        name="Homemade sausage"
    )

    to_day(Date(2020, 5, 2),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(116), eggs.times(3), maple_syrup.grams(9), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(bread.grams(170), butter.grams(41), mozzarella_hifat.grams(68), garlic_powder.grams(2.5), pepper.grams(2), name="Garlic bread"),
    )

    to_day(Date(2020, 5, 3),
        milk_homo.grams(1350),
        add(homemade_sausage2.grams(150), eggs.times(1), butter.grams(5), feta.grams(10), bread.grams(57), sriracha.grams(4), name="Breakfast sandwich").times(2),
        to_recipe(bread.grams(130), butter.grams(30), mozzarella_hifat.grams(54), garlic_powder.grams(4), pepper.grams(2.5), name="Garlic bread"),
    )

    to_day(Date(2020, 5, 4),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(116), eggs.times(3), maple_syrup.grams(9),cinnamon.grams(1),  name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(180), pasta_sauce4.grams(150), homemade_sausage2.grams(150), cream.grams(30), butter.grams(15), olive_oil.grams(15), name="Pasta w/ sausage")
    )

    to_day(Date(2020, 5, 5),
        milk_homo.grams(1350),
        add(homemade_sausage2.grams(150), eggs.times(1), butter.grams(5), bread.grams(57), sriracha.grams(4), name="Breakfast sandwich").times(2),
        add(pasta.grams(180), pasta_sauce4.grams(160), feta.grams(50), cream.grams(30), olive_oil.grams(15), salt.grams(25 * 0.02), name="Pasta w/ chicken"),
    )

    to_day(Date(2020, 5, 6),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(16), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(burger_bun, ground_beef_lean.grams(161), sriracha.grams(8), mayo.grams(8), mustard.grams(4), maple_syrup.grams(2), name="Burgers").times(2),
    )

    to_day(Date(2020, 5, 7),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(homemade_sausage2.grams(150), eggs.times(1), butter.grams(5), bread.grams(57), sriracha.grams(4), name="Breakfast sandwich"),
    )

    to_day(Date(2020, 5, 8),
        milk_homo.grams(1350),
        add(bacon.drain_fat(10).times(5), eggs.times(3), sriracha.grams(25), name="Bacon and eggs"),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(5), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 5, 9),
        water.grams(3000)  # Full-day fast!
    )

    to_day(Date(2020, 5, 10),
        milk_homo.grams(1350),
        add(bacon.drain_fat(12).times(6), eggs.times(3), sriracha.grams(26), name="Bacon and eggs"),
        add(pasta.grams(140), pasta_sauce4.grams(110), chicken_breast.grams(167), cream.grams(30), butter.grams(22), olive_oil.grams(20), name="Pasta w/ chicken"),
        peanut_butter.grams(50),
    )

    chicken_noodle_soup7 = to_recipe(
        chicken_breast.grams(170),
        chicken_thighs.grams(175),
        pasta.grams(150),
        # stock.grams(1010),
        carrots.grams(91),
        celery.grams(125),
        frozen_peas.grams(75),
        garlic.grams(20),
        salt.grams(4.5),
        KCL.grams(2),
        pepper.grams(5),
        name="Chicken noodle soup"
    )

    to_day(Date(2020, 5, 11),
        milk_homo.grams(1350),
        bacon.drain_fat(12).times(6).combine(sriracha, 26),
        chicken_noodle_soup7.times(2/3).add_no_diff(butter.grams(50)),
    )

    to_day(Date(2020, 5, 12),
        milk_homo.grams(1350),
        chicken_noodle_soup7.times(1/3).add_no_diff(butter.grams(25)),
        striploin_steak.grams(360),
        to_recipe(bread.grams(180), butter.grams(50), garlic_powder.grams(8.5), pepper.grams(5), name="Garlic bread"),
    )

    to_day(Date(2020, 5, 13),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(bread.grams(150), feta.grams(100), tomatoes.grams(120), greek_dressing2.grams(75), name="Bruschetta"),
        whey.grams(15),
    )

    to_day(Date(2020, 5, 14),
        milk_homo.grams(1350),
        add(whey.grams(20), peanut_butter.grams(120), eggs.times(4), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(bread.grams(130), greek_dressing2.grams(70), tomatoes.grams(130), feta.grams(100), garlic_powder.grams(1.5), pepper.grams(6.5), name="Bruschetta"),
        whey.grams(15),
    )

    mirepoix_sauce8 = to_recipe(
        onion.grams(600),
        celery.grams(200),
        carrots.grams(100),
        garlic.grams(50),
        butter.grams(60),
        oregano.grams(10),
        salt.grams(12),
        KCL.grams(10),
        worcestershire.grams(28),
        canned_tomatoes.grams(2000),
        name="Mirepoix sauce w/ red"
    )
    mirepoix_sauce8.mass = 838 + 885

    posta95 = to_recipe(
        pasta.grams(400),
        mirepoix_sauce8.grams(350),
        butter.grams(30),
        olive_oil.grams(30),
        chicken_breast.grams(222),
        chicken_thighs.grams(222),
        salt.grams(0.02 * 50),
        name="Pasta w/ chicken"
    )
    posta95.mass = 4407 - 2927

    to_day(Date(2020, 5, 15),
        milk_homo.grams(1350),
        add(whey.grams(20), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        posta95.grams(600),
    )

    to_day(Date(2020, 5, 16),
        milk_homo.grams(1350),
        posta95.grams(posta95.mass - 600).combine(italiano, 120),
    )

    shakshuka = to_recipe(
        onion.grams(520),
        butter.grams(50),
        mirepoix_sauce8.grams(300),
        eggs.times(8),
        feta.grams(150),
        salt.grams(3),
        KCL.grams(3),
        name="Feta shakshuka"
    )

    to_day(Date(2020, 5, 17),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        shakshuka.times(3/5),
    )

    to_day(Date(2020, 5, 18),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(130), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        shakshuka.times(2/5),
    )

    to_day(Date(2020, 5, 19),
        milk_homo.grams(1350),
        striploin_steak.grams(553 - 24 - 100),
        add(pasta.grams(220), mirepoix_sauce8.grams(190), butter.grams(25), olive_oil.grams(25), name="Pasta w/ sauce")
    )

    to_day(Date(2020, 5, 20),
        milk_homo.grams(1350),
        striploin_steak.grams(100).add_no_diff(pepper.grams(2)).add_no_diff(salt.grams(2)),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(200), mirepoix_sauce8.grams(210), butter.grams(25), olive_oil.grams(25), parmigiano.grams(30), name="Pasta w/ sauce")
    )

    to_day(Date(2020, 5, 21),
        milk_homo.grams(1350),
        striploin_steak.grams(320),  # Cost: 17.37 / kg
    )

    to_day(Date(2020, 5, 22),
        milk_homo.grams(1350),
        striploin_steak.grams(150),
        kirkland_croissants.times(2),
        # add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(brioche_bun, ground_beef_medium.grams(150), garlic_powder.grams(1.5), worcestershire.grams(2.5), onion_powder.grams(1.5), salt.grams(1), pepper.grams(1), sriracha.grams(6), mayo.grams(8), mustard.grams(4), name="Hamburger").times(2),
    )

    to_day(Date(2020, 5, 23),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        vincenzos_sausage,
        add(pasta.grams(180), mirepoix_sauce8.grams(180), butter.grams(15), olive_oil.grams(15), cream.grams(25), salt.grams(40 * 0.02), parmigiano.grams(20), name="Pasta w/ sauce")
    )

    posta96 = add(pasta.grams(180), mirepoix_sauce8.grams(180), butter.grams(17), olive_oil.grams(30), cream.grams(40), salt.grams(50 * 0.02), parmigiano.grams(21), name="Pasta w/ sauce and parmigiano")

    to_day(Date(2020, 5, 24),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        vincenzos_sausage,
        posta96.times(2/3),
        # add(pasta.grams(180), mirepoix_sauce8.grams(180), butter.grams(17), olive_oil.grams(30), cream.grams(40), salt.grams(50 * 0.02)).times(2/3).add_no_diff(parmigiano.grams(10)), #parmigiano.grams(10), name="Pasta w/ sauce")
        whey.grams(20),
    )

    to_day(Date(2020, 5, 25),
        milk_homo.grams(1350),
        posta96.times(1/3),
        to_recipe(brioche_bun, mozzarella_hifat.grams(30), ground_beef_medium.grams(150), garlic_powder.grams(1.5), worcestershire.grams(2.5), onion_powder.grams(1.5), salt.grams(1), pepper.grams(1), sriracha.grams(6), mayo.grams(8), mustard.grams(4), name="Hamburger").times(2),
        add(whey.grams(20), peanut_butter.grams(50), eggs.times(2), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),

    )

    chili2 = to_recipe(
        ground_beef_medium.grams(1200), # <-- brown the beef in pan first, it makes a difference
        lamb_leg.grams(600),
        dried_red_beans.grams(450),
        canned_tomatoes.grams(796),

        onion.grams(500),
        garlic.grams(60),
        celery.grams(302),   # <-- cook them in pan before adding next time
        carrots.grams(230),  # <-- skip carrots next time

        butter.grams(0),
        cream.grams(300),

        salt.grams(14),
        KCL.grams(10),
        worcestershire.grams(55),
        cumin.grams(2),
        fennel.grams(2),
        ancho_powder.grams(15),
        habanero_powder.grams(8),
        chipotle_powder.grams(6),
        paprika.grams(20),
        pepper.grams(8),
        oregano.grams(12),
        chili_flakes.grams(8),
        garlic_powder.grams(10),
        onion_powder.grams(15),

        name="Brisket chili"
    )
    chili2.mass = 4648 - 621 + 900

    to_day(Date(2020, 5, 30),
        milk_homo.grams(1350),
        chili2.grams(400).combine(cheddar, 30).add_no_diff(butter.grams(15)),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 5, 31),
        milk_homo.grams(1350),
        chili2.grams(400).combine(cheddar, 30).add_no_diff(butter.grams(30)),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 6, 1),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(8).combine(sriracha, 22),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(pasta.grams(130), pasta_sauce4.grams(110), olive_oil.grams(35), name="Pasta w/ sauce"),
    )

    to_day(Date(2020, 6, 2),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(7).combine(sriracha, 21),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(2), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        chili2.grams(283).add_no_diff(butter.grams(40)),
    )

    to_day(Date(2020, 6, 3),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(7).combine(sriracha, 27),
        add(whey.grams(20), peanut_butter.grams(50), eggs.times(2), maple_syrup.grams(10), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(bread.grams(152), butter.grams(45), chili2.grams(190), cheddar.grams(60), garlic_powder.grams(4), pepper.grams(5.5), name="Garlic chili bread"),
    )

    to_day(Date(2020, 6, 4),
        milk_homo.grams(1350),
        striploin_steak.grams(260),
        add(bread.grams(190), butter.grams(55), italiano.grams(100), garlic_powder.grams(7.5), pepper.grams(5.5), name="Garlic bread"),
        add(pasta.grams(130), pasta_sauce4.grams(110), butter.grams(10), olive_oil.grams(22), parmigiano.grams(20), name="Pasta w/ sauce"),
    )

    gnocchi = to_recipe(
        potatoes.grams(500),
        flour2.grams(250),
        eggs.times(1)
    )

    to_day(Date(2020, 6, 5),
        milk_homo.grams(1350),
        striploin_steak.grams(228),
        add(bread.grams(127), butter.grams(30), pepper.grams(5.5), name="Pan-toasted bread"),
        # add(whey.grams(20), peanut_butter.grams(50), eggs.times(2), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
        # add(chili2.grams(600), eggs.times(4), name="Chili shakshuka").times(1/2),
        add(gnocchi.times(1/3), butter.grams(30), name="Gnocchi"),
        chili.grams(180),
    )

    to_day(Date(2020, 6, 7),
        milk_homo.grams(1350),
        striploin_steak.grams(240),
        add(gnocchi.times(1/3), butter.grams(30), sriracha.grams(5), soy_sauce.grams(8), name="Gnocchi"),
        add(pasta.grams(200), mirepoix_sauce8.grams(220), butter.grams(50), parmigiano.grams(50), name="Pasta w/ sacue")
    )

    to_day(Date(2020, 6, 8),
        milk_homo.grams(1350),
        add(whey.grams(37), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        chili2.grams(350).add_no_diff(butter.grams(20)).add_no_diff(olive_oil.grams(10)),
    )

    to_day(Date(2020, 6, 9),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(8).combine(sriracha, 22),
        to_recipe(bread.grams(163), butter.grams(45), cheddar.grams(60), garlic_powder.grams(6.6), pepper.grams(4.5), name="Garlic bread"),
        add(whey.grams(30), peanut_butter.grams(80), eggs.times(2), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 6, 10),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(7).combine(sriracha, 22),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(bread.grams(150), tomatoes.grams(140), greek_dressing2.grams(62), garlic_powder.grams(5.5), cheddar.grams(50), pepper.grams(6), name="Bruschetta")
    )

    to_day(Date(2020, 6, 11),
        milk_homo.grams(1350),
        chili2.grams(400).add_no_diff(butter.grams(30)).add_no_diff(olive_oil.grams(10)),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(bread.grams(160), tomatoes.grams(175), greek_dressing2.grams(77), garlic_powder.grams(10), cheddar.grams(62), pepper.grams(7.8), name="Bruschetta"),
    )

    posta97 = add(pasta.grams(198), mirepoix_sauce8.grams(178), butter.grams(20), parmigiano.grams(25), name="Pasta w/ sauce")
    to_day(Date(2020, 6, 12),
        milk_homo.grams(1350),
        bacon.drain_fat(5).times(7).combine(sriracha, 17),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        posta97.times(2/3),
    )

    homemade_bread4 = add(
        flour2.grams(400),
        butter.grams(50),
        salt.grams(4),
        KCL.grams(3),
        name="homemade bread"
    )

    to_day(Date(2020, 6, 14),
        milk_homo.grams(1350),
        striploin_steak.grams(270),
        chili2.grams(400).add_no_diff(butter.grams(30)).add_no_diff(olive_oil.grams(10)),
        homemade_bread4.times(1/3).combine(butter, 30),
    )

    garlic_bread1 = add(homemade_bread4.times(1/3), butter.grams(45), cheddar.grams(60), garlic_powder.grams(7), pepper.grams(7), name="Fresh garlic bread")
    to_day(Date(2020, 6, 15),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(5).combine(sriracha, 27),
        garlic_bread1.times(2/3),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 6, 16),
        milk_homo.grams(1350),
        bacon.drain_fat(10).times(6).combine(sriracha, 33),
        garlic_bread1.times(1/3),
        add(whey.grams(32), peanut_butter.grams(84), almond_butter.grams(15), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 6, 17),
        milk_homo.grams(1350),
        chili2.grams(360).add_no_diff(butter.grams(30)),
        add(whey.grams(32), almond_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    homemade_bread5 = add(flour.grams(400), water.grams(267), salt.grams(4), KCL.grams(3), name="homemade bread")
    homemade_bread5.mass = 589

    to_day(Date(2020, 6, 18),
        milk_homo.grams(1350),
        chili2.grams(190).add_no_diff(butter.grams(30)),
        bacon.drain_fat(10).times(5).combine(sriracha, 22),
        add(whey.grams(32), almond_butter.grams(100), eggs.times(3), maple_syrup.grams(7), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        homemade_bread5.grams(110).combine(butter, 20),
    )


    homemade_sausage3 = to_recipe(
        pork_shoulder.grams(666 + 126),
        pork_tenderloin.grams(280),
        garlic.grams(35),  # <-- this is excessive tbqh
        fennel.grams(6),
        sage.grams(0.8),
        oregano.grams(10),
        pepper.grams(5),
        paprika.grams(9),
        chili_flakes.grams(8),
        salt.grams(9),
        KCL.grams(8),
        maggi.grams(17),
        name="Homemade sausage"
    )

    to_day(Date(2020, 6, 19),
        milk_homo.grams(1350),
        add(whey.grams(32), almond_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        homemade_sausage3.grams(220),
        homemade_bread5.grams(124).combine(olive_oil, 18),
    )

    to_day(Date(2020, 6, 20),
        milk_homo.grams(1350),
        add(whey.grams(32), almond_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(homemade_bread5.times(3/12), tomatoes.grams(80), red_onion.grams(60), greek_dressing2.grams(60), garlic_powder.grams(7), cheddar.grams(50), pepper.grams(5), name="Bruschetta"),
        homemade_sausage3.grams(130),
    )

    to_day(Date(2020, 6, 21),
        milk_homo.grams(1350),
        homemade_sausage3.grams(260),
        add(whey.grams(25), almond_butter.grams(50), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(homemade_bread5.times(3/12), tomatoes.grams(80), red_onion.grams(80), greek_dressing2.grams(60), garlic_powder.grams(9), cheddar.grams(50), pepper.grams(6), name="Bruschetta"),
    )

    to_day(Date(2020, 6, 22),
        milk_homo.grams(1350),
        homemade_sausage3.grams(260),
        add(homemade_bread5.times(0.8 / 6), butter.grams(20), name="Toasted bread"),
        add(whey.grams(37), almond_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 6, 23),
        water.grams(2000) # Fasting!
    )

    to_day(Date(2020, 6, 24),
        milk_homo.grams(1350),
        homemade_sausage3.grams(260),
        add(potatoes.grams(560), butter.grams(40), cheddar.grams(72), name="Baked potato"),
        add(whey.grams(34), cream.grams(50), name="Oat / pb shake w/ 3 eggs"),
        # chili2.grams(350),
    )


    chili3 = to_recipe(
        ground_beef_medium.grams(3369 - 2927),
        dried_red_beans.grams(150),
        onion.grams(125 + 166 + 140),
        celery.grams(180),   # <-- cook them in pan before adding next time
        garlic.grams(25),
        butter.grams(50),

        salt.grams(8),
        KCL.grams(5),

        cream.grams(50),
        milk.grams(125),
        canned_tomatoes.grams(320),
        worcestershire.grams(20),

        cumin.grams(2),
        fennel.grams(1.5),
        ancho_powder.grams(8),
        habanero_powder.grams(2.5),
        chipotle_powder.grams(2.5),
        paprika.grams(10),
        pepper.grams(4),
        oregano.grams(6),
        chili_flakes.grams(5),
        garlic_powder.grams(5),
        onion_powder.grams(5),

        name="Rib finger chili"
    )
    chili3.mass = 4100 - 2927 + 350

    pesto = to_recipe(
        basil.grams(25),
        garlic.grams(7),
        olive_oil.grams(60),
        pine_nuts.grams(30),
        name="Pesto sauce"
    )

    to_day(Date(2020, 6, 25),
        milk_homo.grams(1350),
        striploin_steak.grams(280).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        chili3.grams(350).add_no_diff(butter.grams(15)),
        add(pasta.grams(150), pesto.grams(40), parmigiano.grams(20), olive_oil.grams(20), pepper.grams(5), name="Pasta w/ pesto"),
    )

    potatoes_and_chili = to_recipe(
        chili3.grams(350),
        butter.grams(55),
        potatoes.grams(450),
        italiano.grams(80),
        name="Baked potatoes w/ chili"
    )

    to_day(Date(2020, 6, 26),
        milk_homo.grams(1350),
        striploin_steak.grams(280).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        potatoes_and_chili.times(5/7),
    )

    to_day(Date(2020, 6, 27),
        milk_homo.grams(1350),
        potatoes_and_chili.times(2/7),
        striploin_steak.grams(222).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        add(pasta.grams(150), pesto.grams(40), parmigiano.grams(20), olive_oil.grams(20), pepper.grams(5), name="Pasta w/ pesto"),
    )

    to_day(Date(2020, 6, 29),
        milk_homo.grams(1350),
        add(whey.grams(34), almond_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        add(chili3.grams(300), potatoes.grams(300), butter.grams(30), avocado_oil.grams(10), KCL.grams(0.3), maggi.grams(7), name="Potatoes w/ chili"),
    )


    to_day(Date(2020, 6, 30),
        milk_homo.grams(1350),
        prime_rib.grams(222).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(1)).add_no_diff(KCL.grams(0.2)).add_no_diff(pepper.grams(1.5)),
        kirkland_croissants.times(2),
        add(whey.grams(30), almond_butter.grams(37), peanut_butter.grams(63), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 1),
        milk_homo.grams(1350),
        add(chili3.grams(325), potatoes.grams(300), butter.grams(27), KCL.grams(0.3), maggi.grams(7), name="Potatoes w/ chili"),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(4), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 2),
        milk_homo.grams(1350),
        prime_rib.grams(255).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(1)).add_no_diff(KCL.grams(0.2)).add_no_diff(pepper.grams(1.5)),
        kirkland_croissants.times(2),
        add(whey.grams(20), peanut_butter.grams(80), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 4),
        milk_homo.grams(1350),
        kirkland_croissants.times(3),
        prime_rib.grams(250 - 20).add_no_diff(avocado_oil.grams(15)).add_no_diff(salt.grams(1)).add_no_diff(KCL.grams(0.5)).add_no_diff(pepper.grams(1.5)),
        add(whey.grams(20), peanut_butter.grams(50), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    chicken_cordon_bleu_soup = to_recipe(
        chicken_breast.grams(225),
        onion_powder.grams(8),
        garlic_powder.grams(5),
        worcestershire.grams(15),
        mustard.grams(5),
        onion.grams(500),
        butter.grams(50),
        jarlsberg.grams(150),
        parmigiano.grams(40),
        black_forest_ham.grams(100),
        pasta.grams(270),
        cream.grams(60),
    )
    chicken_cordon_bleu_soup.mass = 4365 - 2927


    to_day(Date(2020, 7, 5),
        milk_homo.grams(1350),
        prime_rib.grams(250).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        chili3.grams(160).add_no_diff(butter.grams(15)),
        chicken_cordon_bleu_soup.grams(400).add_no_diff(olive_oil.grams(10)),
    )

    to_day(Date(2020, 7, 6),
        milk_homo.grams(1350),
        chicken_cordon_bleu_soup.grams(420),
        add(whey.grams(40), peanut_butter.grams(114), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 7),
        milk_homo.grams(1350),
        add(whey.grams(35), peanut_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        fried_rice7.grams(350).add_no_diff(olive_oil.grams(30)),
    )

    chicken_parm_soup4 = to_recipe(
        chicken_breast.grams(220),
        onion_powder.grams(12),
        garlic_powder.grams(6),
        worcestershire.grams(20),
        maggi.grams(10),
        canned_tomatoes.grams(300),
        onion.grams(200),
        garlic.grams(15),
        olive_oil.grams(15),
        butter.grams(21),
        pasta.grams(300),
        pepper.grams(8),
        italiano.grams(190),
        salt.grams(40 * 0.025),
        name="Chicken parmigiano soup"
    )
    chicken_parm_soup4.mass = 3962 - 2050

    to_day(Date(2020, 7, 8),
        milk_homo.grams(1350),
        add(whey.grams(33), peanut_butter.grams(100), eggs.times(3), cinnamon.grams(1), name="Oat / pb shake w/ 3 eggs"),
        chicken_parm_soup4.grams(700).add_no_diff(olive_oil.grams(30)).add_no_diff(pepper.grams(4)),
    )

    to_day(Date(2020, 7, 14),
        milk_homo.grams(1350 * 1.5),
        add(whey.grams(36), peanut_butter.grams(100), eggs.times(3), cocoa_powder.grams(2), name="Oat / pb shake w/ 3 eggs"),
        fried_rice7.grams(250).add_no_diff(olive_oil.grams(10)).add_no_diff(pepper.grams(3)),
        add(bread.grams(80), butter.grams(20), pepper.grams(2), garlic_powder.grams(2), name="Buttered bread")
        # to_recipe(pasta.grams(150), butter.grams(20), olive_oil.grams(30), cream.grams(20), garlic.grams(8), parmigiano.grams(20), name="Pasta w/ alfredo sauce").times(1/3),
    )

    to_day(Date(2020, 7, 15),
        milk_homo.grams(1350),
        salmon.grams(270).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(2)).add_no_diff(olive_oil.grams(8)),
        add(bread.grams(150), butter.grams(32), greek_dressing2.grams(30), pepper.grams(5), garlic_powder.grams(5), name="Buttered bread"),
        add(whey.grams(36), peanut_butter.grams(110), eggs.times(3), cocoa_powder.grams(2), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 16),
        salmon.grams(270).add_no_diff(salt.grams(1)).add_no_diff(pepper.grams(2)).add_no_diff(butter.grams(10)).add_no_diff(garlic_powder.grams(3)),
        milk_homo.grams(1350),
        to_recipe(pasta.grams(180), butter.grams(30), cream.grams(80), garlic.grams(10), parmigiano.grams(30), pepper.grams(7), salt.grams(20 * 0.02), olive_oil.grams(10), name="Pasta w/ alfredo sauce"),
        add(whey.grams(26), eggs.times(2), cocoa_powder.grams(2), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 7, 17),
        salmon.grams(270).add_no_diff(light_soy.grams(10)).add_no_diff(olive_oil.grams(8)),
        milk_homo.grams(1350),
        add(whey.grams(30), peanut_butter.grams(100), eggs.times(3), cocoa_powder.grams(2), name="Oat / pb shake w/ 3 eggs"),
    )

    cordon_bleu_marinade = add(worcestershire.grams(15), buttermilk.grams(100), KCL.grams(2), pepper.grams(2), onion_powder.grams(5), garlic_powder.grams(5), name="Chicken marinade")
    chicken_cordon_bleu = to_recipe(
        # This is the recipe for 1; I made 2
        chicken_breast.grams(220),
        black_forest_ham.grams(75),
        jarlsberg.grams(60),
        butter.grams(15),
    )

    white_sauce1 = to_recipe(
        butter.grams(60),
        parmigiano.grams(50),
        garlic.grams(40),
        cream.grams(213),
        pepper.grams(9.5),
        KCL.grams(1.5),
        name="White sauce"
    )

    posta98 = to_recipe(pasta.grams(163), white_sauce1.grams(80), salt.grams(25 * 0.02), name="Pasta w/ white sauce")

    to_day(Date(2020, 7, 18),
        add(whey.grams(36), peanut_butter.grams(60), eggs.times(3), cocoa_powder.grams(2), name="Oat / pb shake w/ 3 eggs"),
        milk_homo.grams(1350),
        kirkland_croissants.times(2),
        chicken_cordon_bleu.times(1/3),
        posta98.times(2/3),
    )

    to_day(Date(2020, 7, 19),
        milk_homo.grams(1350),
        add(bread.grams(81), butter.grams(25), black_forest_ham.grams(75), jarlsberg.grams(40), white_sauce1.grams(15), name="Ham and cheese sandwich"),
        add(bread.grams(70), butter.grams(25), jarlsberg.grams(43), white_sauce1.grams(20), name="Ham and cheese sandwich"),
        posta98.times(1/3),
        chicken_cordon_bleu.times(2/3),
    )

    to_day(Date(2020, 7, 20),
        kirkland_croissants.times(3),
        milk_homo.grams(1350),
        add(whey.grams(35), almond_butter.grams(100), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        chicken_cordon_bleu.times(1/2),
    )

    to_day(Date(2020, 7, 21),
        milk_homo.grams(1350 * 0.6),
        add(whey.grams(35), almond_butter.grams(60), peanut_butter.grams(40), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        kirkland_croissants.times(3),
        chicken_cordon_bleu.times(1/2),
    )

    posta99 = to_recipe(pasta.grams(182), white_sauce1.grams(103), salt.grams(30 * 0.02), name="Pasta w/ white sauce")
    to_day(Date(2020, 7, 22),
        milk_homo.grams(1350 * 1.4),
        kirkland_croissants.times(1),
        chicken_cordon_bleu.times(1/2),
        add(whey.grams(20), almond_butter.grams(30), peanut_butter.grams(30), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        posta99.times(1/2),
    )

    posta100 = to_recipe(
        pasta.grams(163),
        pasta_sauce4.grams(160),
        meatballs.times(2),
        butter.grams(15),
        olive_oil.grams(20),
        parmigiano.grams(40),
        salt.grams(0.03 * 20),
        name="Pasta w/ grandma's sauce and meatballs"
    )

    to_day(Date(2020, 7, 23),
        posta99.times(1/2),
        milk_homo.grams(1350),
        add(whey.grams(34), almond_butter.grams(50), peanut_butter.grams(50), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        posta100.times(1/2),
    )

    to_day(Date(2020, 7, 24),
        posta100.times(1/2),
        milk_homo.grams(1350),
        add(whey.grams(34), almond_butter.grams(50), peanut_butter.grams(80), eggs.times(5), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta101 = to_recipe(pasta.grams(220), pasta_sauce4.grams(220), butter.grams(40), olive_oil.grams(30), chicken_breast.grams(305), salt.grams(40 * 0.02), name="Pasta w/ chicken")

    to_day(Date(2020, 7, 25),
        milk_homo.grams(1350),
        add(whey.grams(34), almond_butter.grams(50), peanut_butter.grams(60), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        posta101.times(1/2).combine(parmigiano, 15),
    )

    posta102 = to_recipe(pasta.grams(160), cream.grams(80), butter.grams(20), parmigiano.grams(15), pepper.grams(2), maggi.grams(3), salt.grams(5 * 0.02),
        # thyme.grams(0.25),
        name="Pasta w/ white sauce")

    to_day(Date(2020, 7, 26),
        milk_homo.grams(1350),
        striploin_steak.grams(277 - 60).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        posta101.times(1/2).combine(parmigiano, 30),
        posta102.times(1/2),
    )

    to_day(Date(2020, 7, 27),
        milk_homo.grams(1350),
        posta102.times(1/2),
        striploin_steak.grams(277 - 50).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        add(whey.grams(30), almond_butter.grams(50), peanut_butter.grams(50), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    pan_pizza4 = to_recipe(
        flour.grams(330 + 20),
        water.grams(360),
        salt.grams(4),
        KCL.grams(1.5),
        butter.grams(30),
        # yeast.grams(8),

        black_forest_ham.grams(125),
        chicken_thighs.grams(220),
        jarlsberg.grams(180),
        red_onion.grams(75),

        cream.grams(180),
        butter.grams(50),
        garlic.grams(22 + 11),
        salt.grams(0.5),
        KCL.grams(1.5),
        parmigiano.grams(30),

        name="Chicken cordon bleu pizza"
    )

    to_day(Date(2020, 7, 28),
        milk_homo.grams(1350),
        striploin_steak.grams(260).add_no_diff(butter.grams(15)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        pan_pizza4.times(2/6),
        whey.grams(25),
    )

    to_day(Date(2020, 7, 29),
        pan_pizza4.times(2/6),
        milk_homo.grams(1350),
        striploin_steak.grams(266).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        kirkland_croissants.times(2),
    )

    to_day(Date(2020, 7, 30),
        kirkland_croissants.times(2),
        whey.grams(15),
        milk_homo.grams(1350),
        striploin_steak.grams(115).add_no_diff(butter.grams(10)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        add(whey.grams(34), almond_butter.grams(30), peanut_butter.grams(100), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta103 = to_recipe(
        pasta.grams(250),
        pasta_sauce4.grams(220),
        meatballs.times(2),
        butter.grams(40),
        olive_oil.grams(30),
        chicken_breast.grams(277),
        salt.grams(40 * 0.02),
        name="Pasta w/ chicken"
    )

    to_day(Date(2020, 7, 31),
        whey.grams(30),
        milk_homo.grams(1350),
        pan_pizza4.times(1.5/6),
        posta103.times(1/2),
    )

    to_day(Date(2020, 8, 1),
        milk_homo.grams(1350),
        whey.grams(30),
        pan_pizza4.times(.5/6),
        kirkland_croissants.times(3),
        striploin_steak.grams(370 - 80).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
    )

    to_day(Date(2020, 8, 2),
        milk_homo.grams(1350),
        whey.grams(26),
        kirkland_croissants.times(3),
        striploin_steak.grams(310).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(2)).add_no_diff(pepper.grams(1.5)),
        whey.grams(20),
    )

    to_day(Date(2020, 8, 3),
        milk_homo.grams(1350),
        posta103.times(1/2),
        kirkland_croissants.times(2),
        add(whey.grams(20), peanut_butter.grams(30), eggs.times(2), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    mirepoix_sauce9 = to_recipe(
        canned_tomatoes.grams(796*2),
        onion.grams(500),
        carrots.grams(250),
        celery.grams(180),
        garlic.grams(45),
        butter.grams(50),
        worcestershire.grams(33),

        salt.grams(10),
        KCL.grams(8),
        oregano.grams(3),
        # thyme.grams(1),
        # basil.grams(40),
        name="mirepoix sauce"
    )
    mirepoix_sauce9.mass = 1900

    to_day(Date(2020, 8, 4),
        milk_homo.grams(1350),
        add(whey.grams(34), almond_butter.grams(50), peanut_butter.grams(80), eggs.times(4), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        striploin_steak.grams(200).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)),
    )


    # cook 15 min at 375
    meatballz6 = to_recipe(
        ground_beef_medium.grams(605),  # rib fingers
        panko.grams(50),
        eggs.times(2),
        milk.grams(50),
        garlic_powder.grams(15),
        onion_powder.grams(15),
        chili_flakes.grams(2),
        pepper.grams(3),
        oregano.grams(3),
        # thyme.grams(1),
        worcestershire.grams(30),
        maggi.grams(5),
        salt.grams(10),
        KCL.grams(7),
        name="Italian meatballs"
    ).times(1/29, name="Italian meatballs")

    to_day(Date(2020, 8, 5),
        milk_homo.grams(1350),
        add(whey.grams(35), almond_butter.grams(50), peanut_butter.grams(80), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        to_recipe(pasta.grams(140), mirepoix_sauce9.grams(160), butter.grams(20), olive_oil.grams(10), meatballz6.times(6), name="Pasta w/ sauce"),
    )

    pan_pizza5 = to_recipe(
        flour.grams(320 + 20),
        water.grams(290),
        salt.grams(4),
        KCL.grams(2),
        butter.grams(20),
        # yeast.grams(8),

        mirepoix_sauce9.grams(200),
        mozzarella_hifat.grams(200),

        black_forest_ham.grams(225),
        red_onion.grams(100),
        garlic.grams(10),

        butter.grams(30),

        name="Pan pizza"
    )


    to_day(Date(2020, 8, 6),
        bacon.drain_fat(12).times(7).add_no_diff(sriracha.grams(25)),
        milk_homo.grams(1350),
        add(whey.grams(35), almond_butter.grams(50), peanut_butter.grams(80), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        pan_pizza5.times(1/6),
    )

    to_day(Date(2020, 8, 7),
        # Full day fast!
        water.grams(4000),
    )

    to_day(Date(2020, 8, 8),
        bacon.drain_fat(12).times(5.5).add_no_diff(sriracha.grams(25)),
        milk_homo.grams(1350),
        pan_pizza5.times(1.66/6),
        add(whey.grams(35), peanut_butter.grams(60), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta104 = add(pasta.grams(250), mirepoix_sauce9.grams(250), butter.grams(30), olive_oil.grams(30), meatballz6.times(2), name="Pasta w/ sauce")

    to_day(Date(2020, 8, 10),
        milk_homo.grams(1350),
        pan_pizza5.times(1.33/6).add_no_diff(butter.grams(20)),
        striploin_steak.grams(270).add_no_diff(butter.grams(20)).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)),
        posta104.times(1/2),
    )

    to_day(Date(2020, 8, 13),
        milk_homo.grams(1350),
        posta104.times(1/2),
        add(whey.grams(35), almond_butter.grams(52), peanut_butter.grams(87), eggs.times(4), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 8, 14),
        milk_homo.grams(1350),
        pan_pizza5.times(1/6).add_no_diff(butter.grams(15)),
        add(eggs.times(3), cocoa_powder.grams(4), name="Egg chocolate milkshake with coffee"),
        add(whey.grams(35), almond_butter.grams(52), peanut_butter.grams(80), eggs.times(4), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta105 = to_recipe(
        pasta.grams(222),
        mirepoix_sauce9.grams(240),
        butter.grams(30),
        beef_tallow.grams(65),
        olive_oil.grams(30),
        meatballz6.times(4),
        parmigiano.grams(40),
        name="Pasta w/ sauce"
    )

    to_day(Date(2020, 8, 15),
        milk_homo.grams(1350),
        add(whey.grams(35), almond_butter.grams(52), peanut_butter.grams(80), eggs.times(4), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
        posta105.times(1/2),
    )

    to_day(Date(2020, 8, 16),
        milk_homo.grams(1350),
        posta105.times(1/2),
        prime_rib.grams(200),
        add(whey.grams(35), peanut_butter.grams(30), eggs.times(2), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 8, 17),
        milk_homo.grams(1350),
        add(whey.grams(35), peanut_butter.grams(100), almond_butter.grams(50), eggs.times(4), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta106 = to_recipe(
        pasta.grams(275),
        mirepoix_sauce9.grams(280),
        butter.grams(37),
        olive_oil.grams(45),
        parmigiano.grams(50),
        name="Pasta w/ sauce"
    )

    to_day(Date(2020, 8, 18),
        milk_homo.grams(1350),
        prime_rib.grams(300).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)),
        add(eggs.times(3), cocoa_powder.grams(4), almond_butter.grams(30), name="Chocolate shake"),
        posta106.times(1/2),
    )

    to_day(Date(2020, 8, 19),
        milk_homo.grams(1350),
        prime_rib.grams(350 - 130).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)),
        posta106.times(1/2),
        add(whey.grams(20), peanut_butter.grams(50), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    homemade_sausage3 = to_recipe(
        pork_shoulder.grams(820),
        pork_tenderloin.grams(300),
        # garlic.grams(35),  # <-- this is excessive tbqh
        # fennel.grams(6),
        # sage.grams(0.8),
        # oregano.grams(10),
        # pepper.grams(5),
        # paprika.grams(9),
        # chili_flakes.grams(8),
        salt.grams(9),
        KCL.grams(10),
        maggi.grams(20),
        name="Homemade sausage"
    )

    to_day(Date(2020, 8, 20),
        milk_homo.grams(1350),
        prime_rib.grams(290).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)).add_no_diff(butter.grams(12)),
        add(
            homemade_sausage3.grams(250),
            fennel.grams(1.5),
            garlic.grams(10),
            butter_chicken_seasoning.grams(7.5),
            name="Butter chicken sausage patties"
        ),
        add(whey.grams(20), peanut_butter.grams(50), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    posta107 = to_recipe(
        pasta.grams(300),
        mirepoix_sauce9.grams(450),
        butter.grams(30),
        olive_oil.grams(40),
        homemade_sausage3.grams(350),
        fennel.grams(3),
        oregano.grams(3),
        garlic.grams(15),
        name="Pasta w/ sausage"
    )

    to_day(Date(2020, 8, 21),
        milk_homo.grams(1350),
        prime_rib.grams(250).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)).add_no_diff(butter.grams(12)),
        posta107.times(3/7),
        add(whey.grams(10), eggs.times(3), cocoa_powder.grams(4), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 8, 22),
        milk_homo.grams(1350),
        posta107.times(2/7),
    )

    to_day(Date(2020, 8, 24),
        milk_homo.grams(1350),
        striploin_steak.grams(428).add_no_diff(salt.grams(3)).add_no_diff(pepper.grams(1.5)).add_no_diff(butter.grams(20)),  # actually blade steak
    )

    to_day(Date(2020, 8, 26),
        milk_homo.grams(1350),
        add(
            homemade_sausage3.grams(420),
            fennel.grams(3),
            garlic.grams(18),
            butter_chicken_seasoning.grams(18),
            name="Butter chicken sausage patties"
        ),
        add(whey.grams(20), almond_butter.grams(50), eggs.times(6), cocoa_powder.grams(5), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 8, 29),
        milk_homo.grams(1000),
        bacon.drain_fat(12).times(10),
        add(whey.grams(35), peanut_butter.grams(50), almond_butter.grams(50), eggs.times(2), cocoa_powder.grams(5), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 8, 30),
        milk_homo.grams(1350),
        add(whey.grams(35), peanut_butter.grams(50), almond_butter.grams(50), eggs.times(4), cocoa_powder.grams(5), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
    )


    neapolitan_sauce2 = to_recipe(
        beef_shanks.grams(418 + 478 + 578),
        canned_tomatoes.grams(796 * 2),
        onion.grams(3644 - 2927),
        garlic.grams(66),
        butter.grams(60),
        oregano.grams(10),
        salt.grams(14),
        chili_flakes.grams(12),
        # red_wine.grams(370),
        # bay_leaves.times(4),
        fennel.grams(3),
        name="Neapolitan ragu"
    )
    neapolitan_sauce2.name="Neapolitan ragu"
    neapolitan_sauce2.mass = 5130 - 2927

    posta108 = add(pasta.grams(211), neapolitan_sauce2.grams(300), butter.grams(15), olive_oil.grams(20), name="Pasta w/ neapolitan sauce")

    to_day(Date(2020, 9, 1),
        milk_homo.grams(1350),
        striploin_steak.grams(311).add_no_diff(butter.grams(15)),
        posta108.times(1/2),
        add(whey.grams(35), peanut_butter.grams(100), almond_butter.grams(50), eggs.times(4), cocoa_powder.grams(5), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 9, 2),
        milk_homo.grams(1350),
        striploin_steak.grams(78).add_no_diff(butter.grams(12)),
        add(pasta.grams(250), neapolitan_sauce2.grams(324), butter.grams(20), olive_oil.grams(30), name="Pasta w/ neapolitan sauce"),
        add(whey.grams(35), peanut_butter.grams(50), almond_butter.grams(50), eggs.times(4), cocoa_powder.grams(5), maple_syrup.grams(10), name="Oat / pb shake w/ 3 eggs"),
    )

    to_day(Date(2020, 9, 9),
        milk_homo.grams(1350),
        striploin_steak.grams(267 - 30).add_no_diff(butter.grams(12)),
        add(peanut_butter.grams(30), almond_butter.grams(90), eggs.times(7), cocoa_powder.grams(7), name="Nut butter egg shake"),
    )

    to_day(Date(2020, 9, 12),
        milk_homo.grams(1350),
        bacon.drain_fat(12).times(7),
        # a lot of neapolitan pasta w/ chicken breast
        add(peanut_butter.grams(50), almond_butter.grams(50), eggs.times(6), cocoa_powder.grams(5), name="Nut butter egg shake"),
    )

    to_day(Date(2020, 9, 13),
        milk_homo.grams(1350),
        striploin_steak.grams(230),
    )

    neapolitan_shakshouka = to_recipe(
        neapolitan_sauce2.grams(550),
        chicken_breast.grams(225),
        eggs.times(4),
        feta.grams(90),
        cumin.grams(1.5),
        name="Shakshouka in neapolitan sauce"
    )

    chili4 = to_recipe(
        ground_beef_medium.grams(612), # Short ribs
        dried_red_beans.grams(200),
        onion.grams(600),
        celery.grams(180),
        garlic.grams(25),
        butter.grams(92),

        chicken_breast.grams(304),

        salt.grams(10),
        KCL.grams(8),

        cream.grams(150),
        # milk.grams(125),
        canned_tomatoes.grams(400),
        worcestershire.grams(25),

        cumin.grams(2.6),
        fennel.grams(2),
        ancho_powder.grams(12),
        habanero_powder.grams(3),
        chipotle_powder.grams(5),
        paprika.grams(15),
        pepper.grams(4),
        oregano.grams(10),
        chili_flakes.grams(6),
        garlic_powder.grams(6),
        onion_powder.grams(6),

        name="Short rib chili"
    )
    chili3.mass = 4100 - 2927 + 350

    to_day(Date(2020, 9, 14),
        milk_homo.grams(1350),
        striploin_steak.grams(267 - 30).add_no_diff(butter.grams(12)),
        add(peanut_butter.grams(100), almond_butter.grams(50), whey.grams(26), eggs.times(6), cocoa_powder.grams(10), name="Nut butter egg shake"),
    )

    # butter_chicken_seasoning = to_recipe(
    #     paprika.grams(12),
    #     oregano.grams(1 + 4),
    #     cumin.grams(4),
    #     garlic_powder.grams(4),
    #     chili_flakes.grams(1),
    #     coriander.grams(1.25),
    #     turmeric.grams(0.25),
    #     salt.grams(2.5),
    #     KCL.grams(2.5)
    # )

    butter_chicken4 = to_recipe(
        buttermilk.grams(200),
        worcestershire.grams(10),
        chicken_thighs.sale().grams(702),

        butter.grams(127),
        garlic.grams(40),
        ginger.grams(35),
        onion.grams(600),
        # red_wine.grams(320),
        canned_tomatoes.grams(300),
        maple_syrup.grams(30),
        KCL.grams(3.5),
        salt.grams(3.5),
        butter_chicken_seasoning,
        cream.grams(100),

        # rice.grams(200).add_no_diff(salt.grams(2)),
        name="Butter chicken"
    )
    butter_chicken3.mass = 4505 - 2927
    butter_chicken3.name = "Butter chicken"
    rice1 = rice.grams(200).add_no_diff(salt.grams(2))

    to_day(Date(2020, 9, 15),
        milk_homo.grams(1350),
        striploin_steak.grams(270).add_no_diff(butter.grams(20)),
        # add(peanut_butter.grams(100), almond_butter.grams(50), whey.grams(26), eggs.times(6), cocoa_powder.grams(10), name="Nut butter egg shake"),
    )

    greek_dressing3 = to_recipe(
        olive_oil.grams(280),
        red_wine_vinegar.grams(100),
        lemon_juice.grams(50),
        oregano.grams(10),
        garlic.grams(40),
        honey.grams(40),
        feta.grams(50),
        pepper.grams(5),
        salt.grams(10),
        KCL.grams(3.3),
        name="Greek dressing"
    )

    to_day(Date(2020, 9, 16),
        milk_homo.grams(1350),
        add(peanut_butter.grams(100), almond_butter.grams(80), whey.grams(26), eggs.times(6), cocoa_powder.grams(10), name="Nut butter egg shake"),
    )

    dumplingz2 = to_recipe(
        flour.grams(350),
        ground_beef_lean.grams(570),
        garlic.grams(30),
        green_onion.times(3),
        ginger.grams(18),
        salt.grams(4),
        light_soy.grams(30),
        sriracha.grams(15),
        carrots.grams(100),
        # celery.grams(75),
        pepper.grams(1.5),
        butter.grams(45),
        name="Dumplings"
    )


shakes = Recipe.where(Recipe.name.like("%shake%"))

for i in shakes[1:]:
    i.base_recipe = shakes[0]

dupes = session.execute("select name from foods where fundamental_level >= 1 and name not like '%shake%' group by name having count(name) > 1 order by count(name) desc").fetchall()
for name, in dupes:  # tuple unpack single item
    recipes = Recipe.where(name=name)
    for i in recipes[1:]:
        i.base_recipe = recipes[0]

session.commit()



all_rq = RecipeQuickadd.all()

filtered_rq = [r for r in all_rq if isinstance(r.primary.base_food, Recipe) and len(Ingredient.where(Ingredient.base_food_id == r.primary.base_food_id)) == 1 and r.primary.quantity == 1]

def push_up(rq):
    primary = rq.primary.base_food
    ingredients = list(primary.ingredients.values())
    for i in ingredients:
        primary.ingredients.pop(i.base_food)
        super(type(rq), rq).add(i)
    rq.fundamental_level = primary.fundamental_level
    rq.ingredients.pop(primary)
    session.delete(primary)
    rq.recalculate_total()

for i in filtered_rq:
    push_up(i)

session.commit()


shakes = Recipe.where(Recipe.name.like("%shake%"))

shakes_reduced = [s for s in shakes if "shake" in s.ingredients.at_position(0).base_food.name and s.ingredients.at_position(0).units == -1]

def push_up_basic(r):
    primary = r.ingredients.at_position(0).base_food
    ingredients = list(primary.ingredients.values())
    for i in ingredients:
        r.add(primary.ingredients.pop(i.base_food))
    r.ingredients.pop(primary)
    session.delete(primary)
    r.recalculate_total()
    session.commit()

for i in shakes_reduced:
    push_up_basic(i)
